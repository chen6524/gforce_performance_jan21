
/******************************************************************************
**
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
*******************************************************************************
*/


#ifndef _KBI_H
#define _KBI_H

#define KBI_INT_EN()  { KBISC_KBIE = 1;}
#define KBI_INT_DIS() { KBISC_KBIE = 0;}

#define KBI_BTN_MSK  0x02
//extern byte btnStat;                     // this variable is useless, commented by Jason Chen, 2013.11.07
extern word btnTimer;
extern word proxTimer;                     // this variable is used in Proximity  by Jason Chen, 2013.11.08
//extern word btnSleepTimer;               // this variable is useless, commented by Jason Chen, 2013.11.07

#define BTN_SLEEP_TIMOUT 2000

extern unsigned char KbiStat;
//extern byte kbiBtnIntEnable ;          // this variable is useless, Jason Chen, 2013.11.07
//#define KBI_BTN_INT_ENABLE ( KBIPE = 0x02;)  
extern volatile byte pwrOffCnt;

extern void KbiInit(void);



#endif
