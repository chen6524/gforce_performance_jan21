/******************************************************************************
**
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
*******************************************************************************
*/

#ifndef _MFLASH_H
#define _MFLASH_H


#include "mImpact.h"
#include "Spi_flash.h"




#define SPIFLASH_RD_NOMORE 0x0
#define SPIFLASH_RD_SUCESS 0x1
#define SPIFLASH_RD_FAILD  0x3

#define SPIFLASH_DATA_START_ADR 0x0 

#define MFLSH_SECTER_USR_PROFILE 0x0


#define SECTION_TAG                     0x00
#define SECTION_PERFORMANCE             0x01
#define SECTION_IMPACTS                 0x02

#define TOTAL_PAGES                     1024l      
#define PAGES_OF_TAGSECTION             0        
#define PAGES_OF_PERFORM_SECTION        512      
#define PAGES_OF_IMPACTS_SECTION        512  
#define FLASH_LAST_WR_ADR_P             0x1FFFFFl //0x3FFFFF
#define FLASH_LAST_WR_ADR_I             0x3FFFFFl //0x1FFFFF

#define FLASH_BASE_ADR_I                 0x200000l   
#define FLASH_BASE_ADR_P                0x0 


#if (PAGES_OF_PERFORM_SECTION  + PAGES_OF_TAGSECTION + PAGES_OF_IMPACTS_SECTION > TOTAL_PAGES)
    #error "Too many pages"
#endif

typedef struct 
{
  unsigned char   Mark_0;
  unsigned char   Mark_1;
  unsigned char   Section;
  unsigned char   ItemSize;
  void *          DataRecord;
  unsigned char   DataSize;
  unsigned char   Flags;          // Bit 0: 0 = Multi Record in one page, 1 = SingleRecord in one page
                                  // Bit 1: 0 = Solely record
  unsigned char   DateOffset;     // Date field (year, month, day, hour, minute, second) offset relative to the data record
} DATARECORD_DEFINE;

typedef struct
{
  unsigned int    FirstPage;
  unsigned int    FirstItem;
  
  unsigned int    LastPage;
  unsigned int    LastItem;
  
  unsigned int    ThisPage;
  unsigned int    ThisItem;

  unsigned char   NeedInit;
  unsigned char   NeedSave;
  unsigned char   NeedClear;
  unsigned char   Erasing;
  unsigned char   Step;
  unsigned int    ErasingPage;
  unsigned int    Page;
  unsigned int    Item;
  unsigned int    RecentErasedPage;
} DATARECORD_CONTROL;


#define FLASH_DATA_ENTRY_FLAG   0xFF

#define FLASH_DATA_ENTRY_I_MAGIC_0  'D'
#define FLASH_DATA_ENTRY_I_MAGIC_1  'A'

typedef struct  
{   
   byte   mag_0;
   byte   mag_1;
   byte   flag;
   word   sn;
   MY_RTC rtc;
   byte  bt;
   byte  gftStatus; //power on/off/sleep etc. 
   dword  pLoad;                                  
   byte  rpe;
   byte  intensity;
   word expCnt;   
   dword expPwr;   
   byte crc;
                
}FLASH_DATA_ENTRY_P;

#define FLASH_DATA_ENTRY_P_MAGIC_0  'P'
#define FLASH_DATA_ENTRY_P_MAGIC_1  'E'


typedef struct  
{   
   byte   mag_0;
   byte   mag_1;
   byte   flag;
   word sn; 
   MY_RTC rtc;
   ACC_DATA  accData;
   GYRO_DATA gyroData; 
   byte crc;
                
}FLASH_DATA_ENTRY_I;

extern FLASH_DATA_ENTRY_P performToSave;
extern FLASH_DATA_ENTRY_I impactToSave;

extern FLASH_DATA_ENTRY_P performToRead;
extern FLASH_DATA_ENTRY_I impactToRead;


//extern SPI_FLASH_CUR_RD_BUFF spiFlashCurRdBuffer;
extern dword spiFlashCurWtAddrI;
extern dword spiFlashCurWtAddrP;

extern dword spiFlashCurRdAddrI;
extern dword spiFlashCurRdAddrP;


byte spiFlashGetCurWtAddrP(void);

byte spiFlashGetCurWtAddrI(void);


byte spiFlashWriteAccBufferP(FLASH_DATA_ENTRY_P* pEntry);
byte spiFlashWriteAccBufferI(FLASH_DATA_ENTRY_I* pEntry);

byte  spiFlashReadAccEntryP(dword addr);
byte  spiFlashReadAccEntryI(dword addr);

byte spiFlashEraseFlash(void);

byte spiFlashRead(byte * dst, dword adr, byte cnt);




void profileSave(void);

uint32_t spiFlashGetID(void);

byte spiFlashFlagEntry(dword addr);

void mFlashInit(void);

byte spiFlashNeedErase(void);


#endif
