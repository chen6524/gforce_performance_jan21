/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/

#include <MC9S08JM60.h>
#include "mcu.h"
#include "kbi.h"
#include "MAX44000.h"
#include "max17047.h"
#include "mImpact.h"
#include "adc.h"


#include "bcd_common.h"               // Jason Chen, Nov.20, 2013

/*Global variable definition*/

extern byte powerCommand;  

//unsigned char Kbi_Stat = 0x00;                     // this variable is useless, commented by Jason Chen, 2013.11.07
//byte btnStat = 0;                                  // this variable is useless, commented by Jason Chen, 2013.11.07
word btnTimer  = BTN_SLEEP_TIMOUT;
word proxTimer = BTN_SLEEP_TIMOUT;                   // this variable is used in proximity process by Jason Chen, 2013.11.08

//word btnSleepTimer;                                // this variable is useless, commented by Jason Chen, 2013.11.07
/*Global function definition*/
void KbiInit(void);

byte kbiBtnIntEnable = 1;

/******************************************************************************
 * Function:        void Kbi_Init(void)
 *
 * OverView:        This routine is the initialization function
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 * Note:            None
 *****************************************************************************/
void KbiInit(void)
{
#ifdef GFT_GEN2
   KBIPE = 0x00;      //kbi enable
   KBISC_KBMOD =0;    //edge or level 1: level
   KBIES = 0x20;      //0:falling edge/low level
#else
   KBIPE       = 0x00;     //kbi KBI7 enable                        , changed from 0x80 to 0x00, 2014.12.08
   KBISC_KBMOD = 0x00;     //edge or level 1: level & edge
   KBIES       = 0x40;     //0:falling edge/low level
#endif
   KBISC_KBACK = 1;   //
   //KBISC_KBIE =0;    //interrupt enable
   return;
}
/******************************************************************************
 * Function:        void Kbi_ISR(void)
 * Overview:        The routine is the KBI interrupt service routine
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 * Note:            None
 *****************************************************************************/
volatile bool rtcRedOn = FALSE;
volatile bool rtcHasInt = FALSE;

#if RTC_TIME_FIFTEEN_MINUTE
#define  REPORT_TIME_PERIOD1     (15*60)
#define  REPORT_TIME_PERIOD2     (5*60)
#else
#define  REPORT_TIME_PERIOD1     60
#define  REPORT_TIME_PERIOD2     60
#endif
volatile byte pwrOffCnt =0;

interrupt void Kbi_ISR(void)
{
	KBISC_KBACK = 1;
#if 0
	volatile byte btnCnt =0;
	if(PTDD_PTDD2 == 0 )
		if(++btnCnt > 5); 
		{
		  LED_GREEN_On();
		  LED_BLUE_On();
		  btnCnt = 0;
       
		}
	    
	#endif	
		
	#ifdef ENABLE_PROXY
	if(PTGD_PTGD1 ==0)
	{
		    ProxIntState = TRUE;
		  //disable proximity interrupt
	    	KBIPE_KBIPE1 =0;		
	}
  #endif
  
  if (KBI_USB_ON) {             // Right now, don't disable USB connect interrupt, it is Rising edge active, 2014.12.08
      
      //KBIPE_KBIPE6 = 0;       // disable USB connect interrupt
  } 
        
  #if 1  //for RTC 
  if (!KBI_RTC_ON)//||(!KBI_RTC_ON & rtcRedOn))
  {
      if(++pwrOffCnt >= 240 )
	        powerCommand = 1; 
  }
  #endif
}



