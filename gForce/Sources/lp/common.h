//
// File:        common.h
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//
#ifndef _COMMON_H_
#define _COMMON_H_

#include <string.h>
//////////////////////////////////////////////////////////////////////////////////
// definitions/macros
#define PCB_NONE        0
#define PCB_DEMOJM      1         // DEMOJM board
#define PCB_gFT         2         // gFT hardware
#define PCB_TYPE        PCB_gFT   //PCB_DEMOJM


//#define fDEBUG          1

// system BUS Clock
#define BUSCLK          24000000L    // 24 MHz with 12 MHz external oscillator in PEE mode
#define BUSCLK_KHZ      (BUSCLK/1000)

// SCI baud rate
#define SCIBAUD         57600

// port in, out
#define INPUT           0
#define OUTPUT          1

// led on, off
#define LED_ON          0
#define LED_OFF         1

// pullup on, off
#define PULLUP_ON       1
#define PULLUP_OFF      0

// general high, low
#define HIGH            1
#define LOW             0

#if (PCB_TYPE==PCB_DEMOJM)
    // radio power
    #define RADIO_PWR_ON()
    #define RADIO_PWR_OFF()

    // sensor power
    #define SENSOR_PWR_ON()
    #define SENSOR_PWR_OFF()
#elif (PCB_TYPE==PCB_gFT)
    // radio power
    #define RADIO_PWR_ON()   (PTFD_PTFD0 = 1)        // PTF0
    #define RADIO_PWR_OFF()  (PTFD_PTFD0 = 0)

    // sensor power
    #define SENSOR_PWR_ON()  (PTED_PTED1 = 1)        // PTE1
    #define SENSOR_PWR_OFF() (PTED_PTED1 = 0)
#endif  // PCB_TYPE

#if (PCB_TYPE==PCB_DEMOJM)

    // led1 (pulled up to Vdd)
    #define Led1_On()       (PTED_PTED2=LED_ON)      // PTE2
    #define Led1_Off()      (PTED_PTED2=LED_OFF)
    #define Led1_Toggle()   (PTED_PTED2^=1)                             // heart beat
    // led2 (pulled up to Vdd)
    #define Led2_On()       (PTED_PTED3=LED_ON)      // PTE3
    #define Led2_Off()      (PTED_PTED3=LED_OFF)
    #define Led2_Toggle()   (PTED_PTED3^=1)                             // when Binding
    // led3 (pulled up to Vdd)
    #define Led3_On()       (PTFD_PTFD0=LED_ON)      // PTF0
    #define Led3_Off()      (PTFD_PTFD0=LED_OFF)
    #define Led3_Toggle()   (PTFD_PTFD0^=1)                             // when a packet is received in usb mode
    // led4 (pulled up to Vdd)
    #define Led4_On()       (PTFD_PTFD1=LED_ON)      // PTF1
    #define Led4_Off()      (PTFD_PTFD1=LED_OFF)
    #define Led4_Toggle()   (PTFD_PTFD1^=1)                             // free
    // sw1 (connects to Gnd)
    #define Sw1_Pushed()    (PTGD_PTGD0==0)     // PTG0
    #define Sw2_Pushed()    (PTGD_PTGD1==0)     // PTG1
    #define Sw3_Pushed()    (PTGD_PTGD2==0)     // PTG2
    #define Sw4_Pushed()    (PTGD_PTGD3==0)     // PTG3

    #define Buzzer_On()     (TPM2SC_CLKSA=1,TPM2C0V=TPM2MOD/2)          // TPM2SC: clock select as BUSCLK
    #define Buzzer_Off()    (TPM2SC_CLKSA=0,TPM2C0V=0)                  // TPM2SC: no clock (CLKSB is already 0)

#elif (PCB_TYPE==PCB_gFT)

    // led1 (pulled up to Vdd)
    #define Led1_On()       (PTFD_PTFD5=LED_ON)      // PTF5            // Jan 30, 13 Schematic change  // (PTDD_PTDD7=LED_ON)      // PTD7 
    #define Led1_Off()      (PTFD_PTFD5=LED_OFF)                                                        // (PTDD_PTDD7=LED_OFF)
    #define Led1_Toggle()   (PTFD_PTFD5^=1)                             // heart beat                   // (PTDD_PTDD7^=1)
    // led2 (pulled up to Vdd)
    #define Led2_On()       (PTFD_PTFD1=LED_ON)      // PTF1            // Jan 30, 13 Schematic change  // (PTAD_PTAD5=LED_ON)      // PTA5
    #define Led2_Off()      (PTFD_PTFD1=LED_OFF)                                                        // (PTAD_PTAD5=LED_OFF)
    #define Led2_Toggle()   (PTFD_PTFD1^=1)                             // when Binding                 // (PTAD_PTAD5^=1)
    // led3 (pulled up to Vdd)
    #define Led3_On()       (PTED_PTED3=LED_ON)      // PTE3            // Jan 30, 13 Schematic change  // (PTCD_PTCD2=LED_ON)      // PTC2
    #define Led3_Off()      (PTED_PTED3=LED_OFF)                                                        // (PTCD_PTCD2=LED_OFF)
    #define Led3_Toggle()   (PTED_PTED3^=1)                 // when a packet is received in usb mode    // (PTCD_PTCD2^=1)
    // led4 (pulled up to Vdd)
    #define Led4_On()
    #define Led4_Off()
    #define Led4_Toggle()
    
    #define USB_VBUS        (PTGD_PTGD2==1)

#endif  // PCB_TYPE

//////////////////////////////////////////////////////////////////////////////////
// prototypes
#ifndef byte
    typedef unsigned char byte;
    typedef unsigned int word;
    typedef unsigned long lword;
#endif
#ifndef bool
    typedef unsigned char bool;
#endif
#ifndef BYTE
    typedef unsigned char BYTE;
    typedef unsigned int WORD;
    typedef unsigned long LWORD;
#endif
#ifndef BOOL
    typedef char BOOL;
#endif

// hw.c
byte SPI_FLASH_Init(bool Enable_SPI);                    // Added by Jason Chen, Jan.22, 2019
void hw_init(void);
void system_init(void);
void clock_init(void);
void board_init(void);
byte myFlashWrite(byte *src, byte len);
//byte flash(byte cmd, byte *adr, byte dat);

// int.c

// main.c

// sci.c
#ifdef fDEBUG
    char kbhit(void);
    byte getch(void);
    void putch(byte data);
    void outstr(byte *msg);
    void outhex(byte data);
    void outdec(byte data);
    void outnibble(byte data);
#else
    #define kbhit()     0
    #define getch()     0
    #define putch(data)
    #define outstr(msg)
    #define outhex(data)
    #define outdec(data)
    #define outnibble(data)
#endif  // fDEBUG

#endif  // #ifndef _COMMON_H_
