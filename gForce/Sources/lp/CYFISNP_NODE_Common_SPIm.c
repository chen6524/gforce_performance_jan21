//
// File:        CYFISNP_NODE_Common_SPIm.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes
#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
#include "CYFISNP_NODE.h"

////////////////////////
// definitions
#if SPICLK==4000000L
    #define WAIT_RESET  6000    // about 50 mSec (6000 *  8uSec)
#elif SPICLK==2000000L
    #define WAIT_RESET  3000    // about 50 mSec (3000 * 16uSec)
#elif SPICLK==1000000L
    #define WAIT_RESET  1500    // about 50 mSec (1500 * 32uSec)
#else
    #error "SPICLK not specified properly..."
#endif

////////////////////////
// internal vars

////////////////////////
// external vars

//////////////////////////////////////////////////////////////////////////////////
//

// write a single byte to a Radio register (high 2 bits must be zero)
//void CYFISNP_NODE_WriteSwapped(byte value, byte regAddr)    // the two parameters are swapped
//{
//    CYFISNP_NODE_Write(regAddr, value);     // swap the parameters and call the normal one
//}

// write a single byte to a Radio register (high 2 bits must be zero)
void CYFISNP_NODE_Write(byte regAddr, byte value)
{
    byte im;

    im = CYFISNP_NODE_SaveAndClearGIE();
    CYFISNP_NODE_WriteRaw(regAddr, value);
    CYFISNP_NODE_RestoreGIE(im);
}

// read a single byte from a Radio register (high 2 bits must be zero)
byte CYFISNP_NODE_Read(byte regAddr)
{
    byte im;
    byte value;

    im = CYFISNP_NODE_SaveAndClearGIE();
    value = CYFISNP_NODE_ReadRaw(regAddr);
    CYFISNP_NODE_RestoreGIE(im);
    return value;
}

// read a single byte from the RX_IRQ_STATUS_ADR register and debounce the update of the
// RXC and RXE bits. If only one of those two bits is set, read the register a second time
// and OR them together. This second read happens in the same SPI transaction as a burst to
// the same address
byte CYFISNP_NODE_ReadStatusDebounced(byte regAddr)
{
    byte im;
    byte value;

    im = CYFISNP_NODE_SaveAndClearGIE();
    value = CYFISNP_NODE_ReadStatusDebouncedRaw(regAddr);
    CYFISNP_NODE_RestoreGIE(im);
    return value;
}

// write a sequence of bytes to a sequence of Radio registers
// use RadioWipPtr instead of RadioPtr as the data pointer
void CYFISNP_NODE_BurstWriteWip(byte regAddr, byte cnt)                     // Burst    Wip
{
    CYFISNP_NODE_FileWriteWip(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// write a sequence of bytes to a sequence of Radio registers
void CYFISNP_NODE_BurstWrite(byte regAddr, byte cnt)                        // Burst    ---
{
    CYFISNP_NODE_FileWrite(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// wite a squence of bytes to a single Radio register
void CYFISNP_NODE_FileWrite(byte regAddr, byte cnt)                         // -----    ---
{
    CYFISNP_NODE_WipPtr = CYFISNP_NODE_Ptr;
    CYFISNP_NODE_FileWriteWip(regAddr, cnt);
}

//
void CYFISNP_NODE_FileWriteWip(byte regAddr, byte cnt)                      // -----    Wip
{
    byte im;

    im = CYFISNP_NODE_SaveAndClearGIE();
    CYFISNP_NODE_FileWriteWipRaw(regAddr, cnt);
    CYFISNP_NODE_RestoreGIE(im);
}

// read a sequence of bytes from a sequence of Radio registers
// using RadioWipPtr as the buffer pointer
void CYFISNP_NODE_BurstReadWip(byte regAddr, byte cnt)                      // Burst    Wip
{
    CYFISNP_NODE_FileReadWip(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// read a sequence of bytes from a sequence of Radio registers
void CYFISNP_NODE_BurstRead(byte regAddr, byte cnt)                         // Burst    ---
{
    CYFISNP_NODE_FileRead(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// read a sequence of bytes from a single Radio register
void CYFISNP_NODE_FileRead(byte regAddr, byte cnt)                          // -----    ---
{
    CYFISNP_NODE_WipPtr = CYFISNP_NODE_Ptr;
    CYFISNP_NODE_WipLen = CYFISNP_NODE_Len;
    CYFISNP_NODE_FileReadWip(regAddr, cnt);
}

//
void CYFISNP_NODE_FileReadWip(byte regAddr, byte cnt)                       // -----    Wip
{
    byte im;

    im = CYFISNP_NODE_SaveAndClearGIE();
    CYFISNP_NODE_FileReadWipRaw(regAddr, cnt);
    CYFISNP_NODE_RestoreGIE(im);
}

//
void CYFISNP_NODE_RestoreGIE(byte ip)
{
    if(ip) {
        /* Disable interrupts */
        DisableInterrupts;
    } else {
        /* Enable interrupts */
        EnableInterrupts;
    }
}

//
byte CYFISNP_NODE_SaveAndClearGIE(void)
{
    byte r;

    asm ("tpa;");     // transfer CCR to A
    asm ("sei;");     // disable interrupts
    asm ("and #8;");  // mask I bit of CCR
    asm ("sta r;");

    return(r);
}

// set the buffer pointer address for RadioBurstRead,  RadioFileRead,
//                                    RadioBurstWrite, RadioFileWrite
void CYFISNP_NODE_SetPtr(byte *ramPtr)
{
    CYFISNP_NODE_Ptr = ramPtr;
}

// set the buffer length for RadioBurstRead & RadioFileRead
void CYFISNP_NODE_SetLength(byte length)
{
    CYFISNP_NODE_Len = length;
}

// This CYFISNP_NODE_Reset only NEEDS to work during soft-reset (NOT POR)
byte CYFISNP_NODE_Reset(void)
{
    int i;

    // deselect Radio
    CYFISNP_NODE_Radio_Deselect();

    // Radio soft reset
    CYFISNP_NODE_Write(CYFISNP_NODE_MODE_OVERRIDE_ADR, CYFISNP_NODE_RST);     // [0x1D]=0x01

    // wait about 50 mSec for the Radio to emerge from POR
    // with 2MHz SPI, each loop takes about 16 uSec
    for(i=0; i<WAIT_RESET; i++) {
        __RESET_WATCHDOG(); // feeds the dog

        // max channel is 0x62; 0x55 has a special pattern 01010101,
        // so that we won't be fooled by SPI bus pullup/pulldn/float
        CYFISNP_NODE_Write(CYFISNP_NODE_CHANNEL_ADR, 0x55);
        if( 0x55 == CYFISNP_NODE_Read(CYFISNP_NODE_CHANNEL_ADR) ) {
            // again, Radio soft reset (CHANNEL_ADR will go back to its default of 0x48)
            CYFISNP_NODE_Write(CYFISNP_NODE_MODE_OVERRIDE_ADR, CYFISNP_NODE_RST);     // [0x1D]=0x01
            return TRUE;    // 1
        }
    }
    return FALSE;   // 0
}
