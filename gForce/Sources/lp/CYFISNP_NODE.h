//
// File:        CYFISNP_NODE.h
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//
#ifndef _CYFISNP_NODE_H_
#define _CYFISNP_NODE_H_

//////////////////////
// includes
#include "CYFISNP_NODE_Regs.h"

////////////////////////
// definitions

// SPI speed
#define SPICLK                  2000000L    //  2 MHz speed (only 1MHz, 2MHz and 4MHz)
#define CYFISNP_NODE_MAX_NODES  63

// spi cs enable, disable
#define SPICS_DESELECT          1
#define SPICS_SELECT            0

#if (PCB_TYPE==PCB_DEMOJM)
    // LpRadio Irq pin
    //#define CYFISNP_NODE_IsRadioIrq()       (PTBD_PTBD4==1) // PTB4, active high
    // spi cs
    #define CYFISNP_NODE_Radio_Select()     (PTED_PTED7=SPICS_SELECT)   // PTE7=0
    #define CYFISNP_NODE_Radio_Deselect()   (PTED_PTED7=SPICS_DESELECT) // PTE7=1
#elif (PCB_TYPE==PCB_gFT)
    // LpRadio Irq pin
   // #define CYFISNP_NODE_IsRadioIrq()       (PTGD_PTGD0==1) // PTG0, active high
    // spi cs
    #define CYFISNP_NODE_Radio_Select()     (PTED_PTED7=1,PTED_PTED0=SPICS_SELECT)   // PTE0=0 (with Ext Flash CS disable first)
    #define CYFISNP_NODE_Radio_Deselect()   (PTED_PTED0=SPICS_DESELECT) // PTE0=1
#endif  // PCB_TYPE
// spi clk
#define SpiCLK_High()                   (PTED_PTED6=HIGH)
#define SpiCLK_Low()                    (PTED_PTED6=LOW)
// spi mosi
#define SpiMOSI_High()                  (PTED_PTED5=HIGH)
#define SpiMOSI_Low()                   (PTED_PTED5=LOW)

#define CYFISNP_NODE_TransmitOn         CYFISNP_NODE_Abort

#define CYFISNP_NODE_IsReceiving()      (CYFISNP_NODE_State & CYFISNP_NODE_RX)
#define CYFISNP_NODE_IsTransmitting()   (CYFISNP_NODE_State & CYFISNP_NODE_TX)

#define CYFISNP_NODE_ReceiveComplete()  (CYFISNP_NODE_State & CYFISNP_NODE_COMPLETE)
#define CYFISNP_NODE_TransmitComplete() (CYFISNP_NODE_State & CYFISNP_NODE_COMPLETE)

#define CYFISNP_NODE_ReceiveError()     (CYFISNP_NODE_State & CYFISNP_NODE_ERROR)
#define CYFISNP_NODE_TransmitError()    (CYFISNP_NODE_State & CYFISNP_NODE_ERROR)

////////////////////////
// LpRadio vars
extern byte *CYFISNP_NODE_WipPtr;       // Working pointer
extern byte  CYFISNP_NODE_WipLen;       // Working buffer length
extern byte *CYFISNP_NODE_Ptr;          // Parameter to SPI access routines - address
extern byte  CYFISNP_NODE_Len;          // Parameter to SPI access routines - length

extern byte CYFISNP_NODE_XactConfig;    // current value of XACT_CFG_ADDR
                                // the value of the Force End State (bit5) in this variable is indeterminate
extern byte CYFISNP_NODE_RestingState;

extern byte CYFISNP_NODE_State;         // the current Rx or Tx state
extern byte CYFISNP_NODE_Temp[4];       // Get/Set CRC Seed & Get/Set Preamble

extern byte CYFISNP_NODE_BytesRead;     //
extern byte CYFISNP_NODE_TxCtrlShadow;  //

////////////////////////
// LpRadio prototypes
byte CYFISNP_NODE_IsRadioIrq(void);

// radio.c
//void MyRadioStartReceive(byte *Data, byte Length);
//void MyRadioTransmitPacket(byte *Data, byte Length);
//byte MyRadioStart(void);
//void MyRadioStop(void);

// CYFISNP_NODE.c
//void CYFISNP_NODE_EnableInt(void);
//void CYFISNP_NODE_DisableInt(void);
byte CYFISNP_NODE_Init(byte xactConfig, byte txConfig);
void CYFISNP_NODE_SetChannel(byte channel);
void CYFISNP_NODE_SetFrequency(byte frequency);
byte CYFISNP_NODE_GetChannel(void);
byte CYFISNP_NODE_GetFrequency(void);
void CYFISNP_NODE_SetTxConfig(byte config);
byte CYFISNP_NODE_GetTxConfig(void);
void CYFISNP_NODE_SetXactConfig(byte xactConfig);
byte CYFISNP_NODE_GetXactConfig(void);
void CYFISNP_NODE_SetFrameConfig(byte config);
byte CYFISNP_NODE_GetFramingConfig(void);
void CYFISNP_NODE_SetThreshold32(byte threshold);
byte CYFISNP_NODE_GetThreshold32(void);
void CYFISNP_NODE_SetThreshold64(byte threshold);
byte CYFISNP_NODE_GetThreshold64(void);
void CYFISNP_NODE_SetPreambleCount(byte count);
byte CYFISNP_NODE_GetPreambleCount(void);
void CYFISNP_NODE_SetPreamblePattern(word pattern);
word CYFISNP_NODE_GetPreamblePattern(void);
void CYFISNP_NODE_SetCrcSeed(word crcSeed);
word CYFISNP_NODE_GetCrcSeed(void);
void CYFISNP_NODE_GetFuses(void);
void CYFISNP_NODE_SetConstSopPnCode(const byte *pPattern);
void CYFISNP_NODE_SetConstDataPnCode(const byte *pPattern);
void CYFISNP_NODE_SetSopPnCode(byte index);

// CYFISNP_NODE_Common_SPIm.c
//void CYFISNP_NODE_WriteSwapped(byte value, byte regAddr);
void CYFISNP_NODE_Write(byte regAddr, byte value);
byte CYFISNP_NODE_Read(byte regAddr);
byte CYFISNP_NODE_ReadStatusDebounced(byte regAddr);
void CYFISNP_NODE_BurstWriteWip(byte regAddr, byte cnt);
void CYFISNP_NODE_BurstWrite(byte regAddr, byte cnt);
void CYFISNP_NODE_FileWrite(byte regAddr, byte cnt);
void CYFISNP_NODE_FileWriteWip(byte regAddr, byte cnt);
void CYFISNP_NODE_BurstReadWip(byte regAddr, byte cnt);
void CYFISNP_NODE_BurstRead(byte regAddr, byte cnt);
void CYFISNP_NODE_FileRead(byte regAddr, byte cnt);
void CYFISNP_NODE_FileReadWip(byte regAddr, byte cnt);
void CYFISNP_NODE_RestoreGIE(byte ip);
byte CYFISNP_NODE_SaveAndClearGIE(void);
void CYFISNP_NODE_SetPtr(byte *ramPtr);
void CYFISNP_NODE_SetLength(byte length);
byte CYFISNP_NODE_Reset(void);

// CYFISNP_NODE_PSoC_SPIm.c
//void CYFISNP_NODE_WriteSwappedRaw(byte value, byte regAddr);
void CYFISNP_NODE_WriteRaw(byte regAddr, byte value);
byte CYFISNP_NODE_ReadRaw(byte regAddr);
byte CYFISNP_NODE_ReadStatusDebouncedRaw(byte regAddr);
void CYFISNP_NODE_BurstWriteWipRaw(byte regAddr, byte cnt);
void CYFISNP_NODE_BurstWriteRaw(byte regAddr, byte cnt);
void CYFISNP_NODE_FileWriteRaw(byte regAddr, byte cnt);
void CYFISNP_NODE_FileWriteWipRaw(byte regAddr, byte cnt);
void CYFISNP_NODE_BurstReadWipRaw(byte regAddr, byte cnt);
void CYFISNP_NODE_BurstReadRaw(byte regAddr, byte cnt);
void CYFISNP_NODE_FileReadRaw(byte regAddr, byte cnt);
void CYFISNP_NODE_FileReadWipRaw(byte regAddr, byte cnt);
byte CYFISNP_NODE_spi_io(byte data);

// CYFISNP_NODE_INT.c
byte CYFISNP_NODE_Radio_Start(void);
void CYFISNP_NODE_Radio_Stop(void);

// CYFISNP_NODE_Core.c
void CYFISNP_NODE_StartTransmit(byte retryCount, byte len);
byte CYFISNP_NODE_RestartTransmit(void);
byte CYFISNP_NODE_GetTransmitState(void);
byte CYFISNP_NODE_GetTransmitStateIsr(void);
void CYFISNP_NODE_EndTransmit(void);
void CYFISNP_NODE_BlockingTransmit(byte retryCount, byte len);
void CYFISNP_NODE_StartReceive(void);
byte CYFISNP_NODE_GetReceiveState(void);
byte CYFISNP_NODE_GetReceiveStateIsr(void);
byte CYFISNP_NODE_EndReceive(void);
void CYFISNP_NODE_GoToEndState(void);
byte CYFISNP_NODE_RxCleanup(void);
byte CYFISNP_NODE_Abort(void);
byte CYFISNP_NODE_GetReceiveStatus(void);
void CYFISNP_NODE_ForceState(byte endState);
void CYFISNP_NODE_WakeupForXact(void);
void CYFISNP_NODE_RestoreXactConfig(void);
void CYFISNP_NODE_Wakeup(void);
void CYFISNP_NODE_ChangeState(byte endState);
void CYFISNP_NODE_WriteForcedState(byte endState);
void CYFISNP_NODE_WriteXactConfigRegUnsafe(byte endState);
byte CYFISNP_NODE_GetRssi(void);

#endif  // _CYFISNP_NODE_H_
