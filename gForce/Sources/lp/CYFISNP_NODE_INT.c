//
// File:        CYFISNP_NODE_INT.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes
#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
#include "CYFISNP_NODE.h"

#include "bcd_common.h"                           // Added by Jason Chen, 2016.01.04

////////////////////////
// definitions

////////////////////////
// internal vars

////////////////////////
// external vars

//////////////////////////////////////////////////////////////////////////////////
//

// start the Radio with default values
byte CYFISNP_NODE_Radio_Start(void)
{
    // make sure SPI1 is enabled
    //SPI1C1_SPE = 1;

    // Radio will work in Polling mode (no Interrupts from LpRadio)

    // Auto Ack, Sleep end-state,
    // 32_8DR (250kbps), PA7
    if( CYFISNP_NODE_Init( (CYFISNP_NODE_DEF_ACK_ENABLE | CYFISNP_NODE_DEF_ACK_TIMEOUT | CYFISNP_NODE_DEF_END_STATE), // para1
#if OUT_OFF_RANGE_TEST    
                           (CYFISNP_NODE_DEF_DATA_RATE | CYFISNP_NODE_PA_N30_DBM  ) ) )                               // para2      // Added by Jason Chen, 2016.01.12  
#else
                           (CYFISNP_NODE_DEF_DATA_RATE | CYFISNP_NODE_DEF_TX_POWER) ) )                               // para2
#endif                           
    { 
        CYFISNP_NODE_SetFrameConfig(CYFISNP_NODE_DEF_SOP_EN | CYFISNP_NODE_DEF_SOP_LEN | CYFISNP_NODE_DEF_LEN_EN | CYFISNP_NODE_DEF_SOP_TSH);    // FRAMING_CFG_ADR
        CYFISNP_NODE_SetThreshold64(CYFISNP_NODE_DEF_64_THOLD);  // DATA64_THOLD_ADR
        CYFISNP_NODE_SetThreshold32(CYFISNP_NODE_DEF_32_THOLD);  // DATA32_THOLD_ADR
        CYFISNP_NODE_SetPreambleCount(CYFISNP_NODE_DEF_PREAMBLE_CNT);
        return 1;
    }
    return 0;
}

// stop the Radio
void CYFISNP_NODE_Radio_Stop(void)
{
    (void)CYFISNP_NODE_Abort();
    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);

    // disable SPI1
    //SPI1C1_SPE = 0;
}
