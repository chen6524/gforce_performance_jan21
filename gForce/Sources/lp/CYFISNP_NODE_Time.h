//
// File:        CYFISNP_NODE_Time.h
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

#ifndef CYFISNP_NODE_TIME_H
#define CYFISNP_NODE_TIME_H

//#include <m8c.h>
#include "common.h"

void    CYFISNP_NODE_TimeSet     (WORD *pTimer, WORD time);
BOOL    CYFISNP_NODE_TimeExpired (WORD *pTimer);

//#pragma fastcall16 CYFISNP_NODE_Delay100uS
void    CYFISNP_NODE_Delay100uS(void);

#define CYFISNP_NODE_TIMER_UNITS     4//16  // 16mS per Sleep Timer
                             // see Radio.h too
#endif  // CYFISNP_NODE_TIME_H
// ###########################################################################
