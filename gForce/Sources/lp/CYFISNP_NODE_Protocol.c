//
// File:        CYFISNP_NODE_Protocol.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

#define CYFISNP_NODE_PROTOCOL_C

#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
#include "CYFISNP_NODE.h"
#include "CYFISNP_NODE_Protocol.h"
#include "CYFISNP_NODE_Time.h"

#include "bcd_common.h"               // Jason Chen, SEP.11, 2013

byte force_bindingMode = 0;                                                          // Added by Jason Chen, 2019.01.25

//================================================================================================= //
// more definitions                                                                                 //
//================================================================================================= //
#ifdef fDEBUG                                                                                       //
    #define CYFISNP_NODE_OutStr     outstr                                                          //
    #define CYFISNP_NODE_OutHex     outhex                                                          //
    #define CYFISNP_NODE_OutChar    putch                                                           //
    #define CYFISNP_NODE_OutNibble  outnibble                                                       //
#else                                                                                               //
    #define CYFISNP_NODE_OutStr(msg)                                                                //
    #define CYFISNP_NODE_OutHex(data)                                                               //
    #define CYFISNP_NODE_OutChar(data)                                                              //
    #define CYFISNP_NODE_OutNibble(data)                                                            //
#endif  // fDEBUG                                                                                   //
  const CYFISNP_NODE_EEP_NET_REC  gsFlash @ CYFISNP_NODE_EEP_PHY_ADR = { 0, 0, 0, 0, 0, 0, 0, 0 };  //
//------------------------------------------------------------------------------------------------- //

#define NO_RX_PACKET    CYFISNP_NODE_ABORT_SUCCESS     // Better name

//extern BYTE Port_1_Data_SHADE;

//#if (CYFISNP_NODE_PSOC_PROJECT == 0) // Workaround. PD50 does not generate Port addr.
//  #define CYFISNP_NODE_nSS_Data_ADDR PRT1DR
//  #define CYFISNP_NODE_IRQ_Data_ADDR PRT1DR
//#endif

// ---------------------------------------------------------------------------
// Calculate the battery powered node's "Response Delay" (dominated by CPU_CLK)
//  This is passed in the BindRequest so the can delay Responses as needed.
//  RULE: HUB IS NEVER SLOWER THAN THE NODE (the node is presumed battery
//        powered and establishes the response time that the hub must meet,
//        it's not negotiated).
// ---------------------------------------------------------------------------
#if BUSCLK==24000000L   //(CYFISNP_NODE_CPU_CLK == 24)
    #define SNP_RSP_DELAY   (160/10)    // 160 uS
#elif BUSCLK==12000000L //(CYFISNP_NODE_CPU_CLK == 12)
    #define SNP_RSP_DELAY   (280/10)    // 280 uS
#elif BUSCLK==6000000L  //(CYFISNP_NODE_CPU_CLK == 6)
    #define SNP_RSP_DELAY   (450/10)    // 450 uS
#elif BUSCLK==3000000L  //(CYFISNP_NODE_CPU_CLK == 3)
    #define SNP_RSP_DELAY   (830/10)    // 830 uS
#else
    #error "ERROR - CPU CLOCK TOO SLOW FOR CYFISNP"
#endif

//#define TEST_DISABLE_DYNAMIC_PA       // (testing only)


// ---------------------------------------------------------------------------
// SNP PACKET HEADERS
// ---------------------------------------------------------------------------
#define PKT_PING_TYPE       0x00
#define PKT_PING_LEN        2       // Protocol + DevID

#define PKT_BINDREQ_TYPE    0x00
#define PKT_BINDREQ_LEN     7

#define PKT_BINDRSP_TYPE    0x10
#define PKT_BINDRSP_LEN     10

#define PKT_CONREQ_TYPE    0x00
#define PKT_CONREQ_LEN     6

#define PKT_CONRSP_TYPE    0x10
#define PKT_CONRSP_LEN     6

#define PKT_UNBIND_TYPE     0x10
#define PKT_UNBIND_LEN      2

#define PKT_SEQBIT_MASK     0x03
#define PKT_SEQSYNC         0x02
#define PKT_DATA_FCD_TYPE   0x20
#define PKT_DATA_BCD_TYPE   0x30
#define PKT_BCDR_MASK       0x04


// ---------------------------------------------------------------------------
//
// PUBLIC VARIABLES
//
// ---------------------------------------------------------------------------
CYFISNP_NODE_PROT_STATE  CYFISNP_NODE_eProtState;

BYTE CYFISNP_NODE_bCurrentChannel=0x00, CYFISNP_NODE_bPreviousChannel=0xFF;                     // -- //


// ---------------------------------------------------------------------------
// CYFISNP_NODE_txBuf - Holds Tx Packets, ONLY for Private Protocol use
// ---------------------------------------------------------------------------
#define SNP_PROT_PKT_TX_MAX 7                   // Largest Tx Protocol Packet
#define SNP_PROT_PKT_RX_MAX 10                  // Largest Rx Protocol Packet
static BYTE SNP_txBuf[SNP_PROT_PKT_TX_MAX];

// ---------------------------------------------------------------------------
// PHYSICAL_BCD_PACKET_MAX = biggest physical BCD buffer required.
// ---------------------------------------------------------------------------
#if CYFISNP_NODE_BCD_PAYLOAD_MAX >= SNP_PROT_PKT_RX_MAX
#define PHYSICAL_BCD_PACKET_MAX     (CYFISNP_NODE_BCD_PAYLOAD_MAX + 1)
#else
#define PHYSICAL_BCD_PACKET_MAX     SNP_PROT_PKT_RX_MAX
#endif

// ---------------------------------------------------------------------------
// RX_PKT - Holds Rx Packets, shared between API and Private Protocol use
// ---------------------------------------------------------------------------
typedef union {
    struct {            // Structure passed to API
        BYTE length;
        BYTE rssi;
        BYTE type;
        BYTE devId;
        BYTE payload[CYFISNP_NODE_BCD_PAYLOAD_MAX]; // Holds Rx API pkt
    } api;
    struct {            // Structure passed from Radio
        BYTE length;
        BYTE rssi;
        BYTE devId;
        BYTE pkt[SNP_PROT_PKT_RX_MAX];      // Holds Rx protocol packet
    } snp;
} SNP_RX_PKT;
static SNP_RX_PKT  SNP_rxBuf;


// ---------------------------------------------------------------------------
//
// Dynamic DataRate and Dynamic PA
//
// ---------------------------------------------------------------------------


BYTE CYFISNP_NODE_radioTxConfig=CYFISNP_NODE_DEF_DATA_RATE;        // [4:3] = GFSK or 8DR       // -- //
// GFSK_INUSE - macro to check whether GFSK is currently specified
//#define GFSK_INUSE ((CYFISNP_NODE_radioTxConfig   \
//                   & CYFISNP_NODE_TX_DATMODE_MSK) \
//                  == CYFISNP_NODE_DATMODE_1MBPS)
// --------------------------
BYTE CYFISNP_NODE_paLevel;              // logical PA level
#if OUT_OFF_RANGE_TEST
  #define PA_LEVEL_MAX 0                // Added by Jason Chen, 2016.01.12
#else
  #define PA_LEVEL_MAX 7
#endif

#define PA_LEVEL_MIN 2


// -----------------------------------------------------------------------
// GFSK (dpwrQualGfsk) and 8DR (dpwrQual8dr) channel quality metrics.
//   these are driven by each transmission AutoAck success or failure.
// -----------------------------------------------------------------------
//#define QUAL_KSHIFT       3
//#define QUAL_KVAL       (1<<QUAL_KSHIFT)
//#define QUAL_ROUNDOFF   (QUAL_KVAL-1)           // round-off so 0 is possible
//#define QUAL_MAX       ((QUAL_KVAL * QUAL_KVAL) - QUAL_ROUNDOFF)
//#define QUAL_AVE        (QUAL_MAX /2)
//#define QUAL_THRESHOLD  (QUAL_MAX * 7/10) //0.70)   // floating?
//static BYTE         dpwrQualGfsk;
//static BYTE         dpwrQual8dr;

// -----------------------------------------------------------------------
// QUAL_CONSEC_MAX - Number of consecutive transmissions at MAX metric before
//                   Tx power is reduced by 1 step.
// -----------------------------------------------------------------------
//#define QUAL_CONSEC_MAX 8
//static BYTE         dpwrMaxConsecGfsk;
//static BYTE         dpwrMaxConsec8dr;
//static char         dpwrStayCt;



// ---------------------------------------------------------------------------
//
// PRIVATE VARIABLES
//
// ---------------------------------------------------------------------------
static BOOL         fTsb;       // Transmit Sequence Bit
//static BOOL         fEsb;       // Expected Sequence Bit

static BOOL         rxBufPend;

static RADIO_STATE  radioStateCopy;
static BYTE         rxLen;


// ---------------------------------------------------------------------------
// CYFISNP_NODE_IdleReceive
//      XACT_STATE_SYNTH - CYFISNP_NODE_XACT_CFG_ADR settings when in SYNTH
//      XACT_STATE_IDLE1 - CYFISNP_NODE_XACT_CFG_ADR settings when in IDLE
//      XACT_STATE_IDLE2 - CYFISNP_NODE_XACT_CFG_ADR settings when in IDLE
//
//
//  wall-powered            battery-powered
//   IDLE = SYNTH           tx->idle1->idle2
// ---------------------------------------------------------------------------

static BOOL idleReceive;            // TRUE = Idle Radio in Rx, not Sleep
#define XACT_COMMON         (CYFISNP_NODE_ACK_EN | CYFISNP_NODE_ACK_TO_8X)
#define XACT_STATE_SYNTH    (XACT_COMMON    | CYFISNP_NODE_END_STATE_RXSYNTH)
#define XACT_STATE_IDLE     (XACT_COMMON    | CYFISNP_NODE_END_STATE_IDLE)
#define XACT_STATE_SLEEP    (XACT_COMMON    | CYFISNP_NODE_END_STATE_SLEEP)
/*static*/ BYTE     CYFISNP_NODE_Pwr_Type=CYFISNP_NODE_PWR_COIN;                                // -- //


// ---------------------------------------------------------------------------
// CYFISNP_NODE_DATA_PACKET_RETRIES - Max Tx data attempts before Timeout
// ---------------------------------------------------------------------------

#define CYFISNP_NODE_DATA_PACKET_RETRIES 10                          // Modified by Jason from 10 to 20, 20140225


static BYTE         bDataRetryCt;
static CYFISNP_NODE_API_PKT *pTxStruct;
static BOOL         txPending;



// ---------------------------------------------------------------------------
// Response Window - How long to wait for a Response to a Request
// ---------------------------------------------------------------------------
//#if CYFISNP_NODE_PWR_TYPE == CYFISNP_NODE_PWR_COIN                                            // -- //
#define RSP_WINDOW_TIMER   (5000/100)               //  5 mS                                    // -- //
//#else                                             //                                          // -- //
//#define RSP_WINDOW_TIMER   (48/CYFISNP_NODE_TIMER_UNITS)     // 50 mS                         // -- //
//#endif                                                                                        // -- //
static WORD     rspWindowTimer;                     // Response Window Timer


// ---------------------------------------------------------------------------
// onChanTxLeft - How many Connect Requests to send before changing channels.
// ---------------------------------------------------------------------------
#define ON_NEW_CHAN_TX_MAX  1       // Retry count if no AutoAck
#define ON_ACK_CHAN_TX_MAX  (10-1)  // Retry count if AutoAck
static BYTE     onChanTxLeft;
static BOOL     fGotChanAck;

#define CON_MODE_COUNT_MAX  (208/4)//200 // Max # Con Req to send before timeout    // 16*13    // -- //
#define PING_MODE_COUNT_MAX (208/8)//200 // Max # PingReq to send before timeout    // 16*13    // -- //
static WORD     wModeCounter;

static WORD wModeDelay;
#define CON_MODE_FAST_TIME  (16/CYFISNP_NODE_TIMER_UNITS)                                       // -- //
#define CON_MODE_SLOW_TIME  (224/CYFISNP_NODE_TIMER_UNITS)                                      // -- //
#define PING_MODE_FAST_TIME (16/CYFISNP_NODE_TIMER_UNITS)                                       // -- //
#define PING_MODE_SLOW_TIME (224/CYFISNP_NODE_TIMER_UNITS)                                      // -- //
#define PING_MODE_TIMEOUT_TIMEOUT  (15000 / CYFISNP_NODE_TIMER_UNITS)                           // -- //
#define CON_MODE_TIMEOUT_TIMEOUT   (15000 / CYFISNP_NODE_TIMER_UNITS)                           // -- //

// ---------------------------------------------------------------------------
//
// Binding variables
//
// ---------------------------------------------------------------------------

static char bindChIdx;

// ---------------------------------------------------------------------------
// RX_WINDOW - Max time to wait for a Hub Bind Respone.
// ---------------------------------------------------------------------------
#define CYFISNP_NODE_RX_WINDOW   RSP_WINDOW_TIMER   // (5000 / 100)    // 5 mS (via 100 uS delay loop)    // -- //

// ---------------------------------------------------------------------------
// BIND_MODE_COUNT_MAX - Number of Bind Requests sent before TIMEOUT
// ---------------------------------------------------------------------------
#define BIND_MODE_COUNT_MAX  600//500        // Give Hub time to write its Flash // 100*6       // -- //
static WORD wBindModeCounter;
    // ------------------------------------------------------------------------
    // bindReqTimer - Throttle BindReq to something reasonable (20 mS)
    //   If send as fast as possible, application may be sending every 2 mS and
    //   we'll burn-through BIND_MODE_COUNT_MAX so fast the person may not
    //   have time to move and push bind button on hub.
    // ------------------------------------------------------------------------
static WORD bindReqTimer;
#define BIND_REQ_TIME   (16/CYFISNP_NODE_TIMER_UNITS)   // (1 + 10/CYFISNP_NODE_TIMER_UNITS) // 10mS or 1 tick  // -- //



// ---------------------------------------------------------------------------
//
// Wall Powered Node: Beacon Timeout Timer
//
// ---------------------------------------------------------------------------
#define BEACON_MAX_TIME     (15000 / CYFISNP_NODE_TIMER_UNITS)                                  // -- //
static WORD beaconMaxTimer;

static BOOL CYFISNP_NODE_inRxGo;


// ---------------------------------------------------------------------------
//
// LOCAL FUNCTIONS
//
// ---------------------------------------------------------------------------
static void runConnectMode      (void);
static void gotoDataMode        (void);
static void runDataMode         (void);
static void runDataModeTx       (void);
static void runDataModeRx       (void);
static void runDataModeRxCrc0   (BYTE mode/*void*/);                                            // -- //
static void gotoPingMode        (void);
static void runPingMode         (void);
static void gotoUnboundMode     (void);
static void gotoDefaultMode     (BOOL autoBind/*void*/);                                        // -- //
// -----------------------------------
static void nextNetCh           (void);
static void putNetParams        (BYTE * pConRspPkt);
static void copyMidToTxPkt      (void);
static BOOL rxMidEquNodeMid     (BYTE *pRxMid);
// -----------------------------------
static void setMaxPaLevel       (void);
static void setPaLevel          (BYTE paLevel);
static void writeHwPa           (void);
//static void decPaLevel          (void);
//static void dynPaApply          (void);
//static void dynPaUpdate         (BOOL ackResult);
//static void toggleDataRate      (void);
static void externalPaDisable   (void);
static void externalPaEnable    (void);
static void issueRxGo           (void);
// -----------------------------------
static void gotoConnectModeBase (void);
//static void gotoConnectMode     (void);
static void setBindPaLevel      (void);
static void bindRun             (void);
/*static*/ BOOL netParamsPresent    (void);                                                     // -- //
static void crc0AutoAck         (BOOL enable);

static void setIdleReceive      (BOOL enable);



// -----------------------------------
static void flashWrite          (BYTE *pSrc);
// ---------------------------------------------------------------------------


static void setNodeSeed(void) {
    CYFISNP_NODE_SetCrcSeed ((CYFISNP_NODE_EEP_NET_REC_ADR->nodeSeedMsb << 8)
                               | (CYFISNP_NODE_EEP_NET_REC_ADR->nodeSeedLsb     ));
}
/*static*/ void setHubSeed(void) {
    CYFISNP_NODE_SetCrcSeed ((CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedMsb << 8)
                               | (CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedLsb     ));
}



// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_Start() - Powerup initialization (Radio powers-up in IDLE)
//
// ---------------------------------------------------------------------------
BYTE CYFISNP_NODE_Start(void)
{
    if( (CYFISNP_NODE_eProtState != 0)
     && (CYFISNP_NODE_eProtState != CYFISNP_NODE_STOP_MODE) ) {
        CYFISNP_NODE_OutStr("\r\n- Radio State Problem!\r\n");                                  // -- //
        return 0;         // START only works at powerup and from STOP
    }

//#if 0//(CYFISNP_NODE_PSOC_EXPRESS_PROJECT == 0)
//    CYFISNP_NODE_SleepTimer_Set64Hz();
//    M8C_EnableIntMask(INT_MSK0, INT_MSK0_SLEEP);
//#endif

    if(!CYFISNP_NODE_Radio_Start())
    {
        CYFISNP_NODE_OutStr("\r\n- Radio Init Failed!\r\n");
        return 0;
    }

//#if 1//!CYFISNP_NODE_DEBUG  // Stop 750KHz default on XOUT (reduce noise)
    CYFISNP_NODE_Write(CYFISNP_NODE_XTAL_CTRL_ADR, CYFISNP_NODE_XOUT_FNC_GPIO);
//#endif

    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);  // Min radio current
    CYFISNP_NODE_SetPtr(&SNP_txBuf[0]);
    CYFISNP_NODE_SetLength(SNP_PROT_PKT_TX_MAX);
    crc0AutoAck(FALSE);

  //CYFISNP_NODE_Pwr_Type = CYFISNP_NODE_PWR_COIN;                                              // -- //
    gotoDefaultMode(TRUE);      // with autoBind                                                // -- //
    CYFISNP_NODE_spiSleep();

    bDataRetryCt = 0;           // No more retries
    txPending = FALSE;          // Empty API's Tx buffer

    return 1;
}


// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_Stop() - Can be used to "abort" a pending transmission.
//
// ---------------------------------------------------------------------------
void CYFISNP_NODE_Stop(void)
{
    CYFISNP_NODE_Radio_Stop();

//#if 0//(CYFISNP_NODE_PSOC_EXPRESS_PROJECT == 0)
//    M8C_DisableIntMask(INT_MSK0, INT_MSK0_SLEEP);
//#endif

    CYFISNP_NODE_eProtState = CYFISNP_NODE_STOP_MODE;

}



// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_Jog() - Wakeup from a Time-Out power-conservation state.
//
// ---------------------------------------------------------------------------
void CYFISNP_NODE_Jog(void)
{
    CYFISNP_NODE_spiWake();
    switch (CYFISNP_NODE_eProtState) {
        case CYFISNP_NODE_CON_MODE_TIMEOUT:
             gotoConnectModeBase();//gotoConnectMode();                                         // -- //
             break;
        case CYFISNP_NODE_PING_MODE_TIMEOUT:
            gotoPingMode();
            break;
    }
}



// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_Run() = Punctuated periodic polling
//
// ---------------------------------------------------------------------------
void CYFISNP_NODE_Run(void)
{
    switch (CYFISNP_NODE_eProtState) {
        // -------------------------------------------------------------------
    case CYFISNP_NODE_BIND_MODE:
            CYFISNP_NODE_spiWake();
            bindRun();
            break;
        // -------------------------------------------------------------------
        case CYFISNP_NODE_CON_MODE:
            CYFISNP_NODE_spiWake();
            runConnectMode();
            break;
        // -------------------------------------------------------------------
        case CYFISNP_NODE_DATA_MODE:
            CYFISNP_NODE_spiWake();
            runDataMode();
            CYFISNP_NODE_spiSleep();
            break;
        // -------------------------------------------------------------------
        case CYFISNP_NODE_PING_MODE:
            CYFISNP_NODE_spiWake();
            runPingMode();
            break;
        // -------------------------------------------------------------------
        //case CYFISNP_NODE_CON_MODE_TIMEOUT:                                                     // -- //
        //    if( CYFISNP_NODE_TimeExpired(&wModeDelay) )                                         // -- //
        //        CYFISNP_NODE_Jog();                                                             // -- //
        //    break;                                                                              // -- //
        // -------------------------------------------------------------------                  // -- //
        case CYFISNP_NODE_CON_MODE_TIMEOUT:                                                     // -- //
        case CYFISNP_NODE_PING_MODE_TIMEOUT:                                                    // -- //
            if( CYFISNP_NODE_TimeExpired(&wModeDelay) ) {                                       // -- //
                if (idleReceive) {                                                              // -- //
                    if (CYFISNP_NODE_inRxGo == TRUE) {       // If RX_GO invoked,               // -- //
                        (void)CYFISNP_NODE_Abort();    //  Then Abort (might get Rx pkt)        // -- //
                        CYFISNP_NODE_inRxGo = FALSE;                                            // -- //
                    }                                                                           // -- //
                }                                                                               // -- //
                CYFISNP_NODE_Jog();     // con/ping again                                       // -- //
            } else {                                                                            // -- //
                // -----------------------------------------------------------------------      // -- //
                // If IDLE in RECEIVE Mode, then                                                // -- //
                // -----------------------------------------------------------------------      // -- //
                if (idleReceive) {                                                              // -- //
                    // -------------------------------------------------------------------      // -- //
                    // Ensure in RX_GO                                                          // -- //
                    // -------------------------------------------------------------------      // -- //
                    if (CYFISNP_NODE_inRxGo == FALSE) {      // If not in Receive               // -- //
                        CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_RXSYNTH);                // -- //
                        issueRxGo();                //  Then get in Receive                     // -- //
                        CYFISNP_NODE_inRxGo = TRUE;                                             // -- //
                    }                                                                           // -- //
                                                                                                // -- //
                    // -------------------------------------------------------------------      // -- //
                    // Monitor and service received packet                                      // -- //
                    // -------------------------------------------------------------------      // -- //
                    radioStateCopy = CYFISNP_NODE_GetReceiveState();                            // -- //
                    if (radioStateCopy & (CYFISNP_NODE_COMPLETE|CYFISNP_NODE_ERROR)) {          // -- //
                        rxLen = CYFISNP_NODE_EndReceive();                                      // -- //
                        if (radioStateCopy & CYFISNP_NODE_ERROR) {                              // -- //
                            // -----------------------------------------------------------      // -- //
                            // Rx CRC0 Packets report as ERROR because DIS_CRC0 is SET          // -- //
                            //  during node Receive to suppress AutoAck, BUT CRC0 packets       // -- //
                            //  have RX_CRC0 status SET indicating they're OK                   // -- //
                            // -----------------------------------------------------------      // -- //
                            char statReg;                                                       // -- //
                            statReg  = CYFISNP_NODE_Read(CYFISNP_NODE_RX_STATUS_ADR);           // -- //
                            statReg &= CYFISNP_NODE_RX_PKTERR|CYFISNP_NODE_RX_EOPERR|CYFISNP_NODE_RX_CRC0;  // -- //
                            if (statReg == CYFISNP_NODE_RX_CRC0) {                              // -- //
                                runDataModeRxCrc0(0);    // Rx packet used CRC0 (no error)      // -- //
                            } else {                                                            // -- //
                                // silently discard Rx packet w/Error                           // -- //
                            }                                                                   // -- //
                        }                                                                       // -- //
                        issueRxGo();                // Rearm Receiver                           // -- //
                        CYFISNP_NODE_inRxGo = TRUE;                                             // -- //
                    }                                                                           // -- //
                }                                                                               // -- //
            }                                                                                   // -- //
            break;                                                                              // -- //
        // -------------------------------------------------------------------
        default:
            break;
    }
}


// ---------------------------------------------------------------------------
//
// gotoDefaultMode() - Goto Connect mode if are bound, otherwise go Unbound
//
// ---------------------------------------------------------------------------
static void gotoDefaultMode(BOOL autoBind/*void*/) {                                            // -- //
    if (netParamsPresent() && (force_bindingMode != 1)) {                                       // Added by Jason Chen, 2019.01.25
        gotoConnectModeBase();
    }
    else {
        if( autoBind )  // coming from ?_Start() function                                       // -- //
    	    CYFISNP_NODE_BindStart(/*0*/); // 0 = on-the-fly ID assignment                      // -- //
    	else            // coming from Bind Timeout                                             // -- //
            gotoUnboundMode();                                                                  // -- //
    }
}


// ---------------------------------------------------------------------------
//
// gotoUnboundMode() - Do nothing until _BindStart() is called
//
// ---------------------------------------------------------------------------
static void gotoUnboundMode(void)
{
    CYFISNP_NODE_eProtState = CYFISNP_NODE_UNBOUND_MODE;
    CYFISNP_NODE_OutStr("- <Unbound: STOP> ");
}
// ---------------------------------------------------------------------------
//
//  CYFISNP_NODE_BindStart() - From Data Mode to Bind Mode
//
// ---------------------------------------------------------------------------
void CYFISNP_NODE_BindStart(void/*BYTE reqDevId*/)                                              // -- //
{
    CYFISNP_NODE_OutStr("\r\n- Start Bind Mode. ");

    CYFISNP_NODE_spiWake();
    bindChIdx = 0;
    CYFISNP_NODE_bCurrentChannel = CYFISNP_NODE_BIND_CH_SEQ[bindChIdx];
    CYFISNP_NODE_SetChannel  (CYFISNP_NODE_bCurrentChannel);
    CYFISNP_NODE_SetSopPnCode(CYFISNP_NODE_BIND_MODE_SOP);

#if ENABLE_BACK_CHANNEL_BIND    
    SNP_txBuf[0] = PKT_BINDREQ_TYPE | CYFISNP_NODE_PWR_WALL;//CYFISNP_NODE_Pwr_Type;          // Jason Chen Added PWR_WALL to Bind Type 2013.0911//        
#else
    SNP_txBuf[0] = PKT_BINDREQ_TYPE;// | CYFISNP_NODE_PWR_TYPE;                               // -- //
#endif    

    SNP_txBuf[1] = 0;//reqDevId;        // only on the fly supported                            // -- //
    copyMidToTxPkt();
    SNP_txBuf[6] = SNP_RSP_DELAY;

    CYFISNP_NODE_eProtState = CYFISNP_NODE_BIND_MODE;
    wBindModeCounter = BIND_MODE_COUNT_MAX;

    CYFISNP_NODE_TimeSet(&bindReqTimer, BIND_REQ_TIME);
}


// ---------------------------------------------------------------------------
//
//  bindRun() -
//
// ---------------------------------------------------------------------------
static void bindRun(void)
{
    RADIO_STATE radioStateCopy;

    if (CYFISNP_NODE_TimeExpired(&bindReqTimer) != TRUE) {
        return;
    }
    CYFISNP_NODE_TimeSet(&bindReqTimer, BIND_REQ_TIME);
    CYFISNP_NODE_OutChar('.'); Led2_Toggle();                                                   // -- //
    // -----------------------------------------------------------------------
    // Send another Bind Request
    // -----------------------------------------------------------------------
    if (++bindChIdx >= sizeof(CYFISNP_NODE_BIND_CH_SEQ)) {
        bindChIdx = 0;
    }
    CYFISNP_NODE_bCurrentChannel = CYFISNP_NODE_BIND_CH_SEQ[bindChIdx];
    CYFISNP_NODE_SetChannel(CYFISNP_NODE_bCurrentChannel);
    setBindPaLevel();
    crc0AutoAck(TRUE);
    CYFISNP_NODE_SetCrcSeed(CYFISNP_NODE_BIND_MODE_CRC_SEED);
    CYFISNP_NODE_SetXactConfig(0
                      | CYFISNP_NODE_ACK_EN          // Expect AutoAck
                      | CYFISNP_NODE_ACK_TO_8X
                      | CYFISNP_NODE_END_STATE_RX);  // Expect Response
    CYFISNP_NODE_SetPtr(&SNP_txBuf[0]);
    CYFISNP_NODE_StartTransmit(0, PKT_BINDREQ_LEN);
    while ((CYFISNP_NODE_State & CYFISNP_NODE_COMPLETE) == 0) 
    {
        (void)CYFISNP_NODE_GetTransmitState();
    }
    radioStateCopy = CYFISNP_NODE_State;
    CYFISNP_NODE_EndTransmit();

    crc0AutoAck(FALSE);
    CYFISNP_NODE_SetCrcSeed( (WORD)(SNP_txBuf[2]<<8) | SNP_txBuf[3]);

    // -----------------------------------------------------------------------
    // If AutoAck failed, power-down radio
    // -----------------------------------------------------------------------
    if ((radioStateCopy & CYFISNP_NODE_ERROR) != 0) {
        CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
    }
    // -----------------------------------------------------------------------
    // Else AutoAck PASSED, open 3 mS BindRsp Rx window
    // -----------------------------------------------------------------------
    else {
        WORD waitDelay;
        waitDelay = CYFISNP_NODE_RX_WINDOW;

        setMaxPaLevel();                    // node sends AutoAck at full power
        CYFISNP_NODE_SetPtr(&SNP_rxBuf.snp.pkt[0]);
        CYFISNP_NODE_SetLength(SNP_PROT_PKT_RX_MAX);
        CYFISNP_NODE_SetXactConfig(0
                                | CYFISNP_NODE_ACK_EN   // Expect AutoAck
                                | CYFISNP_NODE_ACK_TO_8X
                                | CYFISNP_NODE_END_STATE_IDLE);
        CYFISNP_NODE_StartReceive();

        // -------------------------------------------------------------------
        // Maintain 3 mS BindRsp Rx window
        // -------------------------------------------------------------------
        for (;;) {
            BYTE rxLen = 0;
            radioStateCopy = CYFISNP_NODE_GetReceiveState();

            CYFISNP_NODE_Delay100uS();           // Delay 100 uS                        
            if (--waitDelay == 0) {
                rxLen = CYFISNP_NODE_Abort();
                if (rxLen == CYFISNP_NODE_ABORT_SUCCESS) {
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                    break;
                }
            }
            if (radioStateCopy & CYFISNP_NODE_COMPLETE) {
                rxLen = CYFISNP_NODE_EndReceive();
                CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);

                if (radioStateCopy & CYFISNP_NODE_ERROR) {
                    break;
                }
                // -----------------------------------------------------------
                // If BindRsp received
                // -----------------------------------------------------------
                if( (SNP_rxBuf.snp.pkt[0] == PKT_BINDRSP_TYPE)
                 &&                (rxLen == PKT_BINDRSP_LEN) ) {
                    putNetParams(&SNP_rxBuf.snp.pkt[0]);    // Wr to FLASH
                    gotoConnectModeBase();
                    return;         // BIND SUCCESSFUL ----- EXIT HERE
                }
            }   // end Rx Complete
        }  // end for(;;)
    }   // end AutoAck received

    if (--wBindModeCounter == 0) {
        force_bindingMode = 0;                                                                  // Added by Jason Chen, 2019.01.25
        gotoDefaultMode(FALSE);          // Exhausted Bind attempts, without autoBind           // -- //
    }
}


// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_LookupDevId() -
//
// ---------------------------------------------------------------------------
BYTE CYFISNP_NODE_LookupDevId(void)  {
    return(CYFISNP_NODE_EEP_NET_REC_ADR->devId);
}


// ---------------------------------------------------------------------------
//
// gotoConnectMode() - Enter connect from Powerup or Bind Response
//
// ---------------------------------------------------------------------------
static void gotoConnectModeBase(void) {
    CYFISNP_NODE_bCurrentChannel = (CYFISNP_NODE_EEP_NET_REC_ADR->chBase);
    do {    // speed up CON mode by going to the previous known channel                         // -- //
        nextNetCh();                                                                            // -- //
    } while( (CYFISNP_NODE_bCurrentChannel != CYFISNP_NODE_EEP_NET_REC_ADR->chBase) && (CYFISNP_NODE_bCurrentChannel != CYFISNP_NODE_bPreviousChannel) );   // -- //
    setIdleReceive(/*CYFISNP_NODE_PWR_TYPE*/CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL);    // -- //
//    gotoConnectMode();
//}
//
//static void gotoConnectMode(void)
//{
    CYFISNP_NODE_OutStr("\r\n- Start Connect Mode. ");

    setMaxPaLevel();

    CYFISNP_NODE_SetChannel(CYFISNP_NODE_bCurrentChannel);
    CYFISNP_NODE_SetSopPnCode(CYFISNP_NODE_EEP_NET_REC_ADR->sopIdx);

    CYFISNP_NODE_eProtState = CYFISNP_NODE_CON_MODE;
    onChanTxLeft = ON_NEW_CHAN_TX_MAX;
    wModeCounter = CON_MODE_COUNT_MAX;      // 208/4=52 (13 dbl fast, 13 dbl slow)
    CYFISNP_NODE_TimeSet(&wModeDelay, CON_MODE_FAST_TIME);

    // -----------------------------------------------------------------------
    // Since Connect Request packet doesn't change, create it ONCE
    // -----------------------------------------------------------------------
    SNP_txBuf[0] = PKT_CONREQ_TYPE;
    SNP_txBuf[1] = CYFISNP_NODE_EEP_NET_REC_ADR->devId;
    copyMidToTxPkt();
}


// ---------------------------------------------------------------------------
//
//  runConnectMode() -
//
//  The Hub may need to write Node parameters to Flash before returning the
//  ConRsp, so it may a while (4 seconds if using Ice Cube).  If the Node is
//  being a "good neighbor", it shouldn't spew ConReq ASAP if the Hub is
//  busy writing its Flash, thus:
//      1) If Node gets an ACK, it waits a while because it found the HUB
//      2) If Node gets NAK, it retransmits quickly to slew across channels
//
// ---------------------------------------------------------------------------
static void runConnectMode(void)
{
    if (CYFISNP_NODE_TimeExpired(&wModeDelay) == FALSE) {
        return;
    }
    if (wModeCounter > CON_MODE_COUNT_MAX/2 ) {     // 208/4=52 (13 dbl fast, 13 dbl slow)
        CYFISNP_NODE_TimeSet(&wModeDelay, CON_MODE_FAST_TIME);
    } else {
        CYFISNP_NODE_TimeSet(&wModeDelay, CON_MODE_SLOW_TIME);
    }

    if (wModeCounter-- == 0) {
        CYFISNP_NODE_eProtState = CYFISNP_NODE_CON_MODE_TIMEOUT;
        CYFISNP_NODE_TimeSet(&wModeDelay, CON_MODE_TIMEOUT_TIMEOUT);   // 15 Sec                // -- //
        CYFISNP_NODE_OutStr("\r\n- Connect Mode Timeout. ");
        CYFISNP_NODE_spiSleep();
        return;                                                                                 // -- //
    }

    // -------------------------------------------------------------------
    // Send (another) Connect Request
    // -------------------------------------------------------------------
    if (onChanTxLeft-- <= 0) {
        onChanTxLeft = ON_NEW_CHAN_TX_MAX;
        fGotChanAck = 0;
        nextNetCh();
        CYFISNP_NODE_SetChannel(CYFISNP_NODE_bCurrentChannel);
    }

    setHubSeed();
    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_RXSYNTH);  // Expect Rx Response
    CYFISNP_NODE_SetPtr(&SNP_txBuf[0]);
    CYFISNP_NODE_StartTransmit(0, PKT_CONREQ_LEN);
    while ((CYFISNP_NODE_State & CYFISNP_NODE_COMPLETE) == 0) {
        (void)CYFISNP_NODE_GetTransmitState();
    }
    radioStateCopy = CYFISNP_NODE_State;
    CYFISNP_NODE_EndTransmit();


    // -------------------------------------------------------------------
    // If AutoAck failed, power-down radio
    // -------------------------------------------------------------------
    if ((radioStateCopy & CYFISNP_NODE_ERROR) != 0) {
        CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
        //dynPaUpdate(0);               // we are changing channels here, what is this for then?    // -- //
    }
    // -------------------------------------------------------------------
    // Else if AutoAck PASSED, then open ConRsp receive window
    //  and spend a little more time on this channel
    // -------------------------------------------------------------------
    else {
        //dynPaUpdate(1<<QUAL_KSHIFT);  // same as above, leave it to Tx mode to do it.         // -- //

        fTsb = 0;                       // Zero Transmit Seq Bit

        if (fGotChanAck == 0) {
            fGotChanAck = 1;
            onChanTxLeft = ON_ACK_CHAN_TX_MAX;
        }

        //#if CYFISNP_NODE_PWR_TYPE == CYFISNP_NODE_PWR_COIN                                    // -- //
        rspWindowTimer = RSP_WINDOW_TIMER;      // (5000/100)
        //#else                                                                                 // -- //
        //CYFISNP_NODE_TimeSet(&rspWindowTimer, RSP_WINDOW_TIMER);                              // -- //
        //#endif                                                                                // -- //

        CYFISNP_NODE_SetXactConfig(XACT_STATE_IDLE); // Ok because in SYNTH
        issueRxGo();

        for (;;) {
            BYTE rxLen = 0;        
                   
            radioStateCopy = CYFISNP_NODE_GetReceiveState();

            // If Receive Window Expired, then ABORT Receive and EXIT
            //#if CYFISNP_NODE_PWR_TYPE == CYFISNP_NODE_PWR_COIN                                // -- //
            CYFISNP_NODE_Delay100uS();                 // Delay 100 uS
            if (--rspWindowTimer == 0) {
            //#else                                                                             // -- //
            //if (CYFISNP_NODE_TimeExpired(&rspWindowTimer)) {                                  // -- //
            //#endif                                                                            // -- //
                rxLen = CYFISNP_NODE_Abort();
                if (rxLen == NO_RX_PACKET) {
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                    break;
                }   // Else got Rx packet instead, fallthru and process it.
            }
            if (radioStateCopy & CYFISNP_NODE_ERROR) {
                (void)CYFISNP_NODE_EndReceive();
                CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                break;
            }
            if (radioStateCopy & CYFISNP_NODE_COMPLETE) {
                rxLen = CYFISNP_NODE_EndReceive();
                CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                // -----------------------------------------------------------
                // If ConRsp received, then process and goto DATA_MODE
                // -----------------------------------------------------------
                if(                  (SNP_rxBuf.snp.pkt[0] == PKT_CONRSP_TYPE)
                &&                                  (rxLen == PKT_CONRSP_LEN)
                && (rxMidEquNodeMid(&SNP_rxBuf.snp.pkt[2]) == TRUE)
                ) {
                    if (SNP_rxBuf.snp.pkt[1] != 0) { // ConRsp has devId != 0
                        gotoDataMode();
                        return;         // Connect SUCCESSFUL ----- EXIT HERE
                    }
                    else {              // ConRsp has devId == 0
                        CYFISNP_NODE_Unbind();  // UNBIND ################
                        //gotoUnboundMode();
                    }
                }
            } // if (radioStateCopy ....
        }
    }       // Exit "got autoack"
}
// ###########################################################################



// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_TxDataPend()
//
// ---------------------------------------------------------------------------
BOOL CYFISNP_NODE_TxDataPend(void)
{
    if (txPending == 0) {
        return(FALSE);
    }
    return(TRUE);
}

// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_TxDataPut()
//
// ---------------------------------------------------------------------------
BOOL CYFISNP_NODE_TxDataPut(CYFISNP_NODE_API_PKT *pStruct)
{
    BYTE lvType = PKT_DATA_FCD_TYPE;

    switch (pStruct->type) {
        case CYFISNP_NODE_API_TYPE_CONF_BCDR:
            lvType |= PKT_BCDR_MASK;
            // fallthru
        case CYFISNP_NODE_API_TYPE_CONF:
            lvType |= fTsb;               // Add Tx Seq Bit in Hdr
            bDataRetryCt = CYFISNP_NODE_DATA_PACKET_RETRIES;
            break;
        // -------------------------------------------------------------------
        case CYFISNP_NODE_API_TYPE_SYNC_BCDR:
            lvType |= PKT_BCDR_MASK;  // Add BCDR bit
            // fallthru
        case CYFISNP_NODE_API_TYPE_SYNC:
            lvType |= PKT_SEQSYNC;          // Add Sync (no deliv conf needed)
            bDataRetryCt = CYFISNP_NODE_DATA_PACKET_RETRIES;
            break;
        // -------------------------------------------------------------------
        default:        // Ignore everything else
            return FALSE;
    }

    pStruct->type  = lvType;
    pStruct->devId = CYFISNP_NODE_EEP_NET_REC_ADR->devId;
    pTxStruct      = pStruct;           // Point to API structure
    txPending = TRUE;
    return TRUE;
}



// ---------------------------------------------------------------------------
//
//  gotoDataMode() - From Connect or Ping Mode to Data Mode
//
//   runDataMode() - Send a Tx Data packet
//
// ---------------------------------------------------------------------------
static void gotoDataMode(void)
{
    CYFISNP_NODE_OutStr((CYFISNP_NODE_Pwr_Type==CYFISNP_NODE_PWR_WALL)?"\r\n- Start Data WALL Mode. ":"\r\n- Start Data COIN Mode. "); //CYFISNP_NODE_OutHex(CYFISNP_NODE_GetTxConfig());
    CYFISNP_NODE_bPreviousChannel = CYFISNP_NODE_bCurrentChannel;   // remember this channel    // -- //
    setMaxPaLevel();
    CYFISNP_NODE_eProtState = CYFISNP_NODE_DATA_MODE;
    if (CYFISNP_NODE_TxDataPend() == TRUE) {        // If Tx Data pending
        bDataRetryCt = CYFISNP_NODE_DATA_PACKET_RETRIES; // Then retry some more
        CYFISNP_NODE_Jog();     // ???                                                          // -- //
    }
    
#if ENABLE_BACK_CHANNEL_RECV    
    // Jason, SEP. 11, 2013
    CYFISNP_NODE_Pwr_Type = CYFISNP_NODE_PWR_WALL;
    setIdleReceive(/*CYFISNP_NODE_PWR_TYPE*/CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL);    // -- //    
    // End
#endif    

    if (idleReceive) {
        CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_IDLE);     // Manually wake
        issueRxGo();                                              // Enter Receive
        CYFISNP_NODE_inRxGo = TRUE;
        CYFISNP_NODE_TimeSet(&beaconMaxTimer, BEACON_MAX_TIME);
    }
}


// ---------------------------------------------------------------------------
// issueRxGo() -
// ---------------------------------------------------------------------------
static void issueRxGo(void) {
    CYFISNP_NODE_SetPtr      (&SNP_rxBuf.snp.pkt[0]);
    CYFISNP_NODE_SetLength   (PHYSICAL_BCD_PACKET_MAX);
    setNodeSeed();
    CYFISNP_NODE_StartReceive();
}

// ---------------------------------------------------------------------------
// setIdleReceive() - Application may idle in Receive or Sleep
//
// Used in a hybrid node, where SLEEPs most of the time, but infrequently
//  wants to do Application delineated periods of low-latency back channel
//  data (for example Vista SideShow traffic).
// ---------------------------------------------------------------------------
static void setIdleReceive(BOOL enable) {

    if (idleReceive != enable) {
        idleReceive = enable;
        if (enable == FALSE) {
            rxLen = CYFISNP_NODE_Abort();                        // Stop Receive
            CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);    // Low power
            CYFISNP_NODE_inRxGo = FALSE;
        }
        if (enable == TRUE) {
            CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_IDLE);     // Manually wake
            issueRxGo();                                    // Enter Receive
            CYFISNP_NODE_inRxGo = TRUE;
            CYFISNP_NODE_TimeSet(&beaconMaxTimer, BEACON_MAX_TIME);
       }
    }
}



// ---------------------------------------------------------------------------
//
// runDataMode() - Handle Data Mode polled processes.
//
//  If radio idles in Receive, this includes servicing and rearming the
//   radio receiver.  Idling in Receive also means an Rx Abort process before
//   transmitting a packer.
//
//  If radio idles in Sleep, only monitor and service a pending Tx packet.
//
// ---------------------------------------------------------------------------
static void runDataMode(void)
{
    rxLen = NO_RX_PACKET;

    // -----------------------------------------------------------------------
    // If Tx Data is pending, service it
    // -----------------------------------------------------------------------
    if (bDataRetryCt != 0) 
    {
        if (CYFISNP_NODE_inRxGo == TRUE) {       // If RX_GO invoked,
            rxLen = CYFISNP_NODE_Abort();    //  Then Abort (might get Rx pkt)
            CYFISNP_NODE_inRxGo = FALSE;
        }
        if (rxLen == NO_RX_PACKET) {    // Normal case, Rx aborted w/o RxPkt
            runDataModeTx();            // Service pending Tx Data
            return;
        } else {                        // Rx Aborted WITH Rx Packet received
            runDataModeRx();            // Service Rx packet      ???????????????????????????  removed by Jason Chen, 2014.03.26
            return;                     // Exit w/o RX_GO
        }
    }

    // -----------------------------------------------------------------------
    // If IDLE in RECEIVE Mode, then
    // -----------------------------------------------------------------------
    if (idleReceive) {

        // -------------------------------------------------------------------
        // Watch for missing Hub Beacon
        // -------------------------------------------------------------------
        if (CYFISNP_NODE_TimeExpired(&beaconMaxTimer)) {
            rxLen = CYFISNP_NODE_Abort();
            CYFISNP_NODE_inRxGo = FALSE;
            if (rxLen == NO_RX_PACKET) 
            {
                //gotoPingMode();         // Beacon Timeout
            } else {
                //CYFISNP_NODE_TimeSet(&beaconMaxTimer, BEACON_MAX_TIME);   // will be done in the function call     // -- //                
                runDataModeRx();        // Service Rx Packet      
            }
            return;
        }

        // -------------------------------------------------------------------
        // Ensure in RX_GO
        // -------------------------------------------------------------------
        if (CYFISNP_NODE_inRxGo == FALSE) {      // If not in Receive
            CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_RXSYNTH);
            issueRxGo();                //  Then get in Receive
            CYFISNP_NODE_inRxGo = TRUE;
        }

        // -------------------------------------------------------------------
        // Monitor and service received packet
        // -------------------------------------------------------------------
        radioStateCopy = CYFISNP_NODE_GetReceiveState();
        if (radioStateCopy & (CYFISNP_NODE_COMPLETE|CYFISNP_NODE_ERROR)) {                      // -- //
            rxLen = CYFISNP_NODE_EndReceive();
            if (radioStateCopy & CYFISNP_NODE_ERROR) {
                // -----------------------------------------------------------
                // Rx CRC0 Packets report as ERROR because DIS_CRC0 is SET
                //  during node Receive to suppress AutoAck, BUT CRC0 packets
                //  have RX_CRC0 status SET indicating they're OK
                // -----------------------------------------------------------
                char statReg;
                statReg  = CYFISNP_NODE_Read(CYFISNP_NODE_RX_STATUS_ADR);
                statReg &= CYFISNP_NODE_RX_PKTERR|CYFISNP_NODE_RX_EOPERR|CYFISNP_NODE_RX_CRC0;
                if (statReg == CYFISNP_NODE_RX_CRC0) {
                    runDataModeRxCrc0(1);    // Rx packet used CRC0 (no error)                  // -- //
                } else {
                    // silently discard Rx packet w/Error
                }
            }
            else {
                runDataModeRx();        // Service receiver
            }
            issueRxGo();                // Rearm Receiver
            CYFISNP_NODE_inRxGo = TRUE;                                                         // -- //
        }
    }
}


// ---------------------------------------------------------------------------
//
// runDataModeRx() - Received good packet (non-CRC0 Seed), process it
//
//          rxLen = length of received packet
// ---------------------------------------------------------------------------
static void runDataModeRx(void)
{
    BYTE lvRxType;
    lvRxType = SNP_rxBuf.snp.pkt[0];

    if( (rxLen == PKT_UNBIND_LEN) && (lvRxType == PKT_UNBIND_TYPE) ) {
        CYFISNP_NODE_Unbind();
    }
    else if( (rxLen != 0) && ((lvRxType & 0xFC) == PKT_DATA_BCD_TYPE) ) 
	{

        SNP_rxBuf.api.length = rxLen - 1;     // Get Payload Length
        SNP_rxBuf.api.rssi   = CYFISNP_NODE_GetRssi()
                             & CYFISNP_NODE_RSSI_LVL_MSK;

      #if 1
        SNP_rxBuf.api.type = (lvRxType == (PKT_DATA_BCD_TYPE+2)) ?
                                CYFISNP_NODE_API_TYPE_SYNC :
                                CYFISNP_NODE_API_TYPE_CONF;
      #else                                
        SNP_rxBuf.api.type = (lvRxType == PKT_DATA_BCD_TYPE) ?
                                CYFISNP_NODE_API_TYPE_CONF :
                                CYFISNP_NODE_API_TYPE_SYNC;
                                ; 
      #endif                               

        SNP_rxBuf.api.devId  = CYFISNP_NODE_EEP_NET_REC_ADR->devId;
        rxBufPend = TRUE;                   // Pass packet to API

        if( idleReceive )                                                                       // -- //
            CYFISNP_NODE_TimeSet(&beaconMaxTimer, BEACON_MAX_TIME);                             // -- //
    }
}

// ---------------------------------------------------------------------------
//
// runDataModeRxCrc0() - Received good CRC0 packet, process it
//
//          rxLen = length of received packet
// ---------------------------------------------------------------------------
static void runDataModeRxCrc0(BYTE mode/*void*/) {                                              // -- //
                                                                                                // -- //
    if( (rxLen == 4) && (SNP_rxBuf.snp.pkt[0] == PKT_DATA_BCD_TYPE)                             // -- //
                     && (SNP_rxBuf.snp.pkt[1] == CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedMsb)      // -- //
                     && (SNP_rxBuf.snp.pkt[2] == CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedLsb) ) {  // -- //
                                                                                                // -- //
        if( mode == 1 ) {           // from DATA_MODE                                           // -- //
            runDataModeRx();                                                                    // -- //
        } else if( mode == 0 ) {    // from PING_MODE_TIMEOUT                                   // -- //
            runDataModeRx();                                                                    // -- //
            gotoDataMode();                                                                     // -- //
        }                                                                                       // -- //
    }                                                                                           // -- //
}




// ---------------------------------------------------------------------------
//
// runDataModeTx() - Tx 1 Burst in a dedicated function
//
// ---------------------------------------------------------------------------
static void runDataModeTx(void)
{
    if (--bDataRetryCt == 0) {
        gotoPingMode();     // FAILED TO DELIVER PACKET, CHANGE TO PING MODE
        return;                                                                                 // -- //
    }

    // -------------------------------------------------------------------
    // Transmit Data Packet
    // -------------------------------------------------------------------
    setHubSeed();
    //dynPaApply();            // Dynamic Power                                                 // -- //

    if( (pTxStruct->type & PKT_BCDR_MASK) == 0 && (idleReceive == FALSE) ) {
        CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_IDLE); // Set Radio IDLE
    }
    else {
        CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_TXSYNTH); // Set Radio in SYNTH
    }

    CYFISNP_NODE_SetPtr          (&pTxStruct->type);
    CYFISNP_NODE_StartTransmit   (0, pTxStruct->length + 2);
    while ((CYFISNP_NODE_State & CYFISNP_NODE_COMPLETE) == 0) {
        (void)CYFISNP_NODE_GetTransmitState();
    }
    radioStateCopy = CYFISNP_NODE_State;
    CYFISNP_NODE_EndTransmit();

    // -------------------------------------------------------------------
    // If AutoAck failed, Idle radio and EXIT
    // -------------------------------------------------------------------
    if ((radioStateCopy & CYFISNP_NODE_ERROR) != 0) {
        //dynPaUpdate(0);                                                                       // -- //
        if (idleReceive == FALSE) {
            CYFISNP_NODE_ForceState(XACT_STATE_SLEEP);   // Manually put to SLEEP
        }
        return;
    }
    //dynPaUpdate(1<<QUAL_KSHIFT);                                                              // -- //

    // -------------------------------------------------------------------
    // Delivered packet
    // -------------------------------------------------------------------
    bDataRetryCt = 0;           // No more retries
    txPending = FALSE;          // Empty API's Tx buffer

    // Ack is received, so Hub is within reach; reset Beacon timer if in Wall mode to prevent Pinging   // -- //
    if( idleReceive )                                                                           // -- //
        CYFISNP_NODE_TimeSet(&beaconMaxTimer, BEACON_MAX_TIME);                                 // -- //

    // -------------------------------------------------------------------
    // Inc Transmit Seq Bit if Tx Pkt used the Tx Seq Bit
    // -------------------------------------------------------------------
    if ((pTxStruct->type & PKT_SEQBIT_MASK) < PKT_SEQSYNC) {
        fTsb ^= 1;              // Inc mod-2
    }

    // -------------------------------------------------------------------
    // If Back Channel Data Request flag was zero, exit
    // -------------------------------------------------------------------    
    if ((pTxStruct->type & PKT_BCDR_MASK) == 0) {
        if (idleReceive == FALSE)  {
            CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
        }
    }
    // -----------------------------------------------------------------------
    // Else Open Back Channel Receive Window
    // -----------------------------------------------------------------------
    else     
    {
        //#if CYFISNP_NODE_PWR_TYPE == CYFISNP_NODE_PWR_COIN                                    // -- //
        rspWindowTimer = RSP_WINDOW_TIMER;      // (3000/200);     // 3,000 uS / 200 uS         // -- //
        //#else                                                                                 // -- //
        //CYFISNP_NODE_TimeSet(&rspWindowTimer, RSP_WINDOW_TIMER);                              // -- //
        //#endif                                                                                // -- //

        issueRxGo();

        // -------------------------------------------------------------------
        // Wait for Back Channel packet or Window Timeout
        // -------------------------------------------------------------------
        for (;;) {

            radioStateCopy = CYFISNP_NODE_GetReceiveState();

            // ---------------------------------------------------------------
            // Monitor Rx Window Timeout
            // ---------------------------------------------------------------
            //#if CYFISNP_NODE_PWR_TYPE == CYFISNP_NODE_PWR_COIN        // Use short accurate delay // -- //
            CYFISNP_NODE_Delay100uS();                       // Delay 100 uS
            if (--rspWindowTimer == 0)              // to minimize power
            //#else                                   // ELSE Alkaline battery                  // -- //
            //if (CYFISNP_NODE_TimeExpired(&rspWindowTimer))   // use timer                     // -- //
            //#endif                                                                            // -- //
                {
                rxLen = CYFISNP_NODE_Abort();
                if (rxLen == NO_RX_PACKET) {
                    if (idleReceive == FALSE)  {
                        CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                    }
                    break;
                }
            }
            // ---------------------------------------------------------------
            // Monitor for Rx Packet Error
            // ---------------------------------------------------------------
            if (radioStateCopy & CYFISNP_NODE_ERROR) {
                (void)CYFISNP_NODE_EndReceive();
                if (idleReceive == FALSE)  {
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                }
                break;
            }
            // ---------------------------------------------------------------
            // Monitor for Rx Packet Success
            // ---------------------------------------------------------------
            if (radioStateCopy & CYFISNP_NODE_COMPLETE) {
                rxLen = CYFISNP_NODE_EndReceive();
                if (idleReceive == FALSE)  {
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                }
                runDataModeRx();            // Process Rx Packet
                break;
            }
        }
        setHubSeed();
    } // Exit "Else Open Back Channel Receive Window"
}



// ---------------------------------------------------------------------------
// CYFISNP_NODE_RxDataPend()
// ---------------------------------------------------------------------------
BOOL CYFISNP_NODE_RxDataPend(void)
{
    return (rxBufPend);
}

// ---------------------------------------------------------------------------
// CYFISNP_NODE_RxDataGet()
// ---------------------------------------------------------------------------
    CYFISNP_NODE_API_PKT *               // Return pointer to Rx API structure
CYFISNP_NODE_RxDataGet(void)
{
    return((CYFISNP_NODE_API_PKT *) &SNP_rxBuf.api);
}

// ---------------------------------------------------------------------------
// CYFISNP_NODE_RxDataRelease()
// ---------------------------------------------------------------------------
void CYFISNP_NODE_RxDataRelease(void)
{
    rxBufPend = FALSE;
}


// ---------------------------------------------------------------------------
//
// gotoPingMode()
//
// runPingMode() -
//
// ---------------------------------------------------------------------------
static void gotoPingMode(void)
{
    CYFISNP_NODE_OutStr("\r\n- Start Ping Mode. ");
    setMaxPaLevel();
    CYFISNP_NODE_eProtState = CYFISNP_NODE_PING_MODE;
    wModeCounter   = PING_MODE_COUNT_MAX;   // 208/8=26 (13 fast, 13 slow)
    CYFISNP_NODE_TimeSet(&wModeDelay, PING_MODE_FAST_TIME);

    // ------------------
    // Create PING packet
    // ------------------
    SNP_txBuf[0] = PKT_PING_TYPE;
    SNP_txBuf[1] = CYFISNP_NODE_EEP_NET_REC_ADR->devId;
}

static void runPingMode(void)
{

    if (CYFISNP_NODE_TimeExpired(&wModeDelay) == FALSE) {
        return;
    }
    if (wModeCounter > PING_MODE_COUNT_MAX/2 ) {    // 208/8=26 (13 fast, 13 slow)
        CYFISNP_NODE_TimeSet(&wModeDelay, PING_MODE_FAST_TIME);
    } else {
        CYFISNP_NODE_TimeSet(&wModeDelay, PING_MODE_SLOW_TIME);
    }

    if (wModeCounter-- == 0) {
        // -------------------------------------------------------------------
        // Since can't find Hub, it's possible that it's been replaced and the
        //  Node should send a packet with BCDR to revalidate the Node's ID,
        //  so timeout in Connect Mode to revalidate at eventual Jog()
        // -------------------------------------------------------------------
        CYFISNP_NODE_eProtState = CYFISNP_NODE_PING_MODE_TIMEOUT;//CYFISNP_NODE_CON_MODE_TIMEOUT;   // -- //
        CYFISNP_NODE_TimeSet(&wModeDelay, PING_MODE_TIMEOUT_TIMEOUT);   // 15 Sec                   // -- //
        CYFISNP_NODE_OutStr("\r\n- Ping Mode Timeout. ");
        CYFISNP_NODE_spiSleep();
        return;
    }

    // -------------------------------------------------------------------
    // Send another Ping Request on next channel
    // -------------------------------------------------------------------
    nextNetCh();
    CYFISNP_NODE_SetChannel   (CYFISNP_NODE_bCurrentChannel);
    setHubSeed();
    CYFISNP_NODE_SetXactConfig(XACT_STATE_IDLE);     // Expect NO BCD
    CYFISNP_NODE_SetPtr       (&SNP_txBuf[0]);
    CYFISNP_NODE_StartTransmit(0, PKT_PING_LEN);
    while ((CYFISNP_NODE_State & CYFISNP_NODE_COMPLETE) == 0) {
        (void)CYFISNP_NODE_GetTransmitState();
    }
    radioStateCopy = CYFISNP_NODE_State;
    CYFISNP_NODE_EndTransmit();

    // -------------------------------------------------------------------
    // If AutoAck PASSED, then resume Data Mode
    // -------------------------------------------------------------------
    if ((radioStateCopy & CYFISNP_NODE_ERROR) == 0) {
        setHubSeed();
        gotoDataMode();
    }
}


// ---------------------------------------------------------------------------
// nextNetCh() - Advance to next Data Channel (everything but Bind Mode)
// ---------------------------------------------------------------------------
static void nextNetCh(void)
{
    BYTE hop;
    hop = CYFISNP_NODE_EEP_NET_REC_ADR->chHop + 1;
    hop = (hop * 2) + (hop * 4);        // hop * 6 (help "lite" compiler)

    CYFISNP_NODE_bCurrentChannel += hop;

    if (CYFISNP_NODE_bCurrentChannel  > CYFISNP_NODE_CHAN_MAX) {
        CYFISNP_NODE_bCurrentChannel -= CYFISNP_NODE_MAX_CHANNELS;
    }
    while ((signed char)CYFISNP_NODE_bCurrentChannel < CYFISNP_NODE_CHAN_MIN) {
        CYFISNP_NODE_bCurrentChannel += hop;
    }

    if( (CYFISNP_NODE_eProtState==CYFISNP_NODE_CON_MODE) || (CYFISNP_NODE_eProtState==CYFISNP_NODE_PING_MODE) ) {   // -- //
	    CYFISNP_NODE_OutStr("\r\n- curr Channel ");                                             // -- //
    	CYFISNP_NODE_OutHex(CYFISNP_NODE_bCurrentChannel);                                      // -- //
    }                                                                                           // -- //
}


// ---------------------------------------------------------------------------
// rxMidEquNodeMid() - Compare the Rx MID with this Node's MID (from the radio)
// ---------------------------------------------------------------------------
static BOOL rxMidEquNodeMid(BYTE *pRxMid)
{
    BYTE *pNodeMid;
    char ivar;
    BYTE nodeMid[6];        // CYFISNP_NODE_GetFuses() needs 6 Byte MID buffer

    CYFISNP_NODE_SetPtr(&nodeMid[0]);
    CYFISNP_NODE_SetLength(CYFISNP_NODE_SIZEOF_MID);
    CYFISNP_NODE_GetFuses();

    pNodeMid = &nodeMid[0];
    for (ivar = CYFISNP_NODE_SIZEOF_MID; (*pNodeMid == *pRxMid) && (ivar != 0); --ivar) {
        ++pNodeMid;
        ++pRxMid;
    }
    return(ivar == 0);
}

// ---------------------------------------------------------------------------
// copyMidToTxPkt() - Copy Node MID to Bind or Connect Req Tx pkt area
// ---------------------------------------------------------------------------
static void copyMidToTxPkt(void)
{
    CYFISNP_NODE_SetPtr   (&SNP_txBuf[2]);
    CYFISNP_NODE_SetLength(CYFISNP_NODE_SIZEOF_MID);
    CYFISNP_NODE_GetFuses ();
}

// ---------------------------------------------------------------------------
//
// putNetParams() - Extract Network Params from BindRsp and put in FLASH
//
// ---------------------------------------------------------------------------
static void putNetParams(BYTE * pRxPkt)
{
    CYFISNP_NODE_EEP_NET_REC net;

    // -------------------------------------------------------------------
    // Compose RAM image to write to FLASH
    // -------------------------------------------------------------------
    net.devId       = pRxPkt[1];
    net.hubSeedMsb  = pRxPkt[6];
    net.hubSeedLsb  = pRxPkt[7];
    net.sopIdx      = pRxPkt[9] >> 3;
    net.chHop       = pRxPkt[9] & 7;            // Flash is {0-7}
    net.chBase      = pRxPkt[8];
    net.nodeSeedMsb = pRxPkt[6];
    net.nodeSeedLsb = pRxPkt[7] ^ pRxPkt[1];    // LSB Hub Seed ^ devId

    CYFISNP_NODE_OutStr("- Got netParams ");
    flashWrite((char *)&net);
    force_bindingMode = 0;                                                          // Added by Jason Chen, 2019.01.25
}


// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_Unbind()
//
// ---------------------------------------------------------------------------
//extern byte needReboot;
#if NO_REBOOT_UNBIND                                                     //Removed by Jason Chen, 2015.09.03
extern word imSleepTimerTime;                                            //Added by Jason Chen, 2015.09.03
extern word imSleepTimer;                                                //Added by Jason Chen, 2015.09.03
extern void mTimer_TimeSet (unsigned int *pTimer );                      //Added by Jason Chen, 2015.09.03
extern void SetLpStatus(byte status) ;
#endif
void CYFISNP_NODE_Unbind(void)
{
    CYFISNP_NODE_EEP_NET_REC net;

    net.devId       = 0;            // ------------------------------------
    net.hubSeedMsb  = 0;            // Compose RAM image to write to FLASH
    net.hubSeedLsb  = 0;            // ------------------------------------
    net.sopIdx      = 0;
    net.chHop       = 0;
    net.chBase      = 0;
    net.nodeSeedMsb = 0;
    net.nodeSeedLsb = 0;

    CYFISNP_NODE_OutStr("- Unbinding... ");
    flashWrite((char *)&net);
    CYFISNP_NODE_OutStr("Done. ");
    gotoUnboundMode();                      // in main() make it Bind again <-------------------------------------------------------------

#if NO_REBOOT_UNBIND    
    lpState   = 1;        //LP_STATE_INIT;
    lpCmdMail = 0;        //LP_CMD_NONE; 
    imSleepTimerTime = 16;//IM_SLEEP_TIMER;                  // Added by Jason Chen, 2014.12.08
    mTimer_TimeSet( &imSleepTimer );                         // Added by Jason Chen for clearing Sleep timer, 2014.01.15        
#else    
    SetUsrProfileLP_Disable();                               // Added by Jason Chen, 2019.02.07
    //for(;;);
    //needReboot = 1;                                        // Added by Jason Chen for air unbinding, 20140311
#endif

}


// ---------------------------------------------------------------------------
//
// flashWrite() - Write RAM image to FLASH by possibly device-specific method
//
// ---------------------------------------------------------------------------

// Dummy function (for now)
//BYTE CYFISNP_NODE_GetDieTemp(void)
//{
//// @PSoC_UserCode_DieTemp@ (Do not change this line.)
//    return 20;
//// @PSoC_UserCode_END@ (Do not change this line.)
//}


//#if HI_TECH_C
//#pragma warning push
//#pragma warning disable 350
//#endif
static void flashWrite (BYTE *pSrc)
{
    // -----------------------------------------------------------------------
    // If this RAM image already exists in FLASH, then no need to write it
    // -----------------------------------------------------------------------
    {
        char ivar;
        const BYTE *pFlash;
        BYTE       *pRam;
        pRam   = pSrc;
        pFlash = (const BYTE *)CYFISNP_NODE_EEP_PHY_ADR;
        ivar = sizeof(CYFISNP_NODE_EEP_NET_REC);
        for (; (ivar != 0) && (*pRam++ == *pFlash++); --ivar);
        if (ivar == 0) {
            return;
        }
    }
    // -----------------------------------------------------------------------
    // Write the RAM image to FLASH
    // -----------------------------------------------------------------------
    CYFISNP_NODE_OutStr("- Writing to Flash... ");
    (void)myFlashWrite(pSrc, sizeof(CYFISNP_NODE_EEP_NET_REC));
//    {
//      struct {
//          BYTE   bARG_BlockId;           // block ID
//          BYTE  *pARG_FlashBuffer;       // flash buffer pointer - 2 bytes
//          CHAR   cARG_Temperature;       // die Temperature, -40 to 100
//          BYTE   bDATA_PWErase;          // Temporary storage (reserved)
//          BYTE   bDATA_PWProgram;        // Temporary storage (reserved)
//          BYTE   bDATA_PWMultiplier;     // Temporary storage (reserved)
//      } fwStruct;
//        FLASH_WRITE_STRUCT fwStruct;
//
//    #define CYFISNP_NODE_EEP_BLK_NUM (CYFISNP_NODE_EEP_PHY_ADR/0x40)  // 64-Bytes/Flash Page
//
//    #if (CYFISNP_NODE_FLASH_BLOCK_NUMBER_IS_WORD)
//        fwStruct.wARG_BlockId     = CYFISNP_NODE_EEP_BLK_NUM;
//    #else
//        fwStruct.bARG_BlockId     = CYFISNP_NODE_EEP_BLK_NUM;
//    #endif
//
//        fwStruct.pARG_FlashBuffer = pSrc;
//        fwStruct.cARG_Temperature = CYFISNP_NODE_GetDieTemp();
//        if (bFlashWriteBlock((FLASH_WRITE_STRUCT*)&fwStruct) == 0) {
//            while (1);      // Flash write failed, halt here
//        }
//    }
    CYFISNP_NODE_OutStr("Done. ");
}
//#if HI_TECH_C
//#pragma warning pop
//#endif




// ---------------------------------------------------------------------------
//
// dynPaUpdate() - Based on AutoAck result, update GSK or 8DR statistic.
//
// ---------------------------------------------------------------------------
//static void dynPaUpdate(BOOL ackResult)
//{
//    if (GFSK_INUSE) {
//        dpwrQualGfsk = dpwrQualGfsk + ackResult
//                   - ((dpwrQualGfsk + QUAL_ROUNDOFF) >> QUAL_KSHIFT);
//    }
//    else {
//        dpwrQual8dr = dpwrQual8dr  + ackResult
//                  - ((dpwrQual8dr + QUAL_ROUNDOFF) >> QUAL_KSHIFT);
//    }
//}



// ---------------------------------------------------------------------------
//
// dynPaApply() - Select desired power level and modulation for Tx packet
//
// ---------------------------------------------------------------------------
//#define incSaturate(val)   (val = (BYTE)((val<QUAL_CONSEC_MAX) ? val+1 : val))
//static void dynPaApply(void)
//{
//    // -----------------------------------------------------------------------
//    // Track how long each statistic has been at QUAL_MAX
//    // -----------------------------------------------------------------------
//    if (dpwrQualGfsk == QUAL_MAX) {
//        incSaturate(dpwrMaxConsecGfsk);     // ++counter to QUAL_CONSEC_MAX
//    } else {
//        dpwrMaxConsecGfsk = 0;
//    }
//    if (dpwrQual8dr  == QUAL_MAX) {
//        incSaturate(dpwrMaxConsec8dr);      // ++counter to QUAL_CONSEC_MAX
//    } else {
//        dpwrMaxConsec8dr = 0;
//    }
//
//    // -----------------------------------------------------------------------
//    // If both statistics are QUAL_MAX for QUAL_CONSEC_MAX, then reduce PA
//    // -----------------------------------------------------------------------
//    if ((dpwrQualGfsk == QUAL_MAX && dpwrMaxConsecGfsk == QUAL_CONSEC_MAX)
//     && (dpwrQual8dr  == QUAL_MAX && dpwrMaxConsec8dr  == QUAL_CONSEC_MAX)) {
//        decPaLevel();
//        dpwrMaxConsecGfsk = dpwrMaxConsec8dr = 0;
//    }
//
//    // -----------------------------------------------------------------------
//    // Else if both statistics "stink", then PA = MAX
//    // -----------------------------------------------------------------------
//    else if (dpwrQualGfsk < QUAL_THRESHOLD && dpwrQual8dr < QUAL_THRESHOLD) {
//        setMaxPaLevel();
//    }
//
//    // -----------------------------------------------------------------------
//    // Pick the Data Rate with the best statistic, favor GFSK
//    // -----------------------------------------------------------------------
//    if (--dpwrStayCt == 0) {
//        toggleDataRate();                       // CHANGE MODULATION
//        if (dpwrQualGfsk >= QUAL_THRESHOLD) {       // GFSK=HI, 8DR=dont-care
//            dpwrStayCt = GFSK_INUSE ? 4 : 1;        // 4 GFSK : 1 8DR
//        }
//        else if  (dpwrQual8dr >= QUAL_THRESHOLD) {  // GFSK=LO, 8DR=HI
//            dpwrStayCt = GFSK_INUSE ? 1 : 4;        // 1 GFSK : 4 8DR
//        }
//        else {                                      // GFSK=LO, 8DR=LO
//            dpwrStayCt = 1;//GFSK_INUSE ? 1 : 1;    // 1 GFSK : 1 8DR
//        }
//    }
//#if 0
//  // DEBUG stuff only compiles if CYFISNP_NODE_DEBUG is defined
//  CYFISNP_NODE_OutStr("<dynPA:");
//  CYFISNP_NODE_OutHex(CYFISNP_NODE_GetQualGfsk());              // GFSK quality
//  CYFISNP_NODE_OutHex(CYFISNP_NODE_GetQual8dr());               // 8DR quality
//  CYFISNP_NODE_OutChar((CYFISNP_NODE_radioTxConfig & 8) ? '.' : 'G'); // (.)8DR or (G)GFSK
//  CYFISNP_NODE_OutNibble(CYFISNP_NODE_paLevel);                 // PA Level
//  CYFISNP_NODE_OutNibble(CYFISNP_NODE_Read(CYFISNP_NODE_TX_CFG_ADR) & 7);  // Radio PA Level
//  CYFISNP_NODE_OutStr("> ");
//#endif  // 0/1
//}

// ---------------------------------------------------------------------------
//
// toggleDataRate() - Toggle between GFSK and 8DR
//
// ---------------------------------------------------------------------------
//static void toggleDataRate(void)
//{
//    if (GFSK_INUSE) {
//        CYFISNP_NODE_radioTxConfig &= ~CYFISNP_NODE_TX_DATMODE_MSK;
//        CYFISNP_NODE_radioTxConfig |=  CYFISNP_NODE_DATMODE_8DR;
//        CYFISNP_NODE_Write(CYFISNP_NODE_ANALOG_CTRL_ADR, 0);  // Disable all slow
//    }
//    else {  // 8DR Used
//        CYFISNP_NODE_radioTxConfig &= ~CYFISNP_NODE_TX_DATMODE_MSK;
////        CYFISNP_NODE_radioTxConfig |=  CYFISNP_NODE_DATMODE_1MBPS;    // CW warning: OR with 0x00
//        CYFISNP_NODE_Write(CYFISNP_NODE_ANALOG_CTRL_ADR, CYFISNP_NODE_ALLSLOW);
//    }
//    writeHwPa();
//}


// ---------------------------------------------------------------------------
// setMaxPaLevel() - Set Maximum Tx power
// ---------------------------------------------------------------------------
static void setMaxPaLevel(void)
{
    setPaLevel(PA_LEVEL_MAX);                   // idx 7
}

// ---------------------------------------------------------------------------
// setBindPaLevel() - Possibly set lower Tx power for Bind Request
// ---------------------------------------------------------------------------
static void setBindPaLevel(void)
{
    setPaLevel(PA_LEVEL_MIN/*CYFISNP_NODE_PA_LEVEL_BIND*/);     // idx 0                        // -- //
}


// ---------------------------------------------------------------------------
// setPaLevel() - Set Tx power and assume everything's OK
// ---------------------------------------------------------------------------
static void setPaLevel(BYTE paLevel)
{
    CYFISNP_NODE_paLevel = paLevel;
    writeHwPa();

//    dpwrQualGfsk = QUAL_MAX;    // Quality metric at MAX (assume we're OK)
//    dpwrQual8dr  = QUAL_MAX;    // Quality metric at MAX (assume we're OK)

//    dpwrMaxConsecGfsk = 0;      // Zero consecutive counters
//    dpwrMaxConsec8dr  = 0;      // Zero consecutive counters
//    dpwrStayCt = 1;             // Will start toggling
}

// ---------------------------------------------------------------------------
// decPaLevel() - Metrics are good, try decreasing Tx power
// ---------------------------------------------------------------------------
//static void decPaLevel(void)
//{
//    if (CYFISNP_NODE_paLevel == PA_LEVEL_MIN) {
//        return;
//    }
//#ifndef TEST_DISABLE_DYNAMIC_PA
//    --CYFISNP_NODE_paLevel;
//#endif
//    writeHwPa();
//}


// ---------------------------------------------------------------------------
// writeHwPa() - Write PA Level to hardware
// ---------------------------------------------------------------------------
static void writeHwPa(void)
{
    BYTE regVal = CYFISNP_NODE_PA_PHY_TBL[CYFISNP_NODE_paLevel]
                & CYFISNP_NODE_PA_VAL_MSK;

//#if (CYFISNP_NODE_EXTERNAL_PA)
    BOOL paOn = ((CYFISNP_NODE_PA_PHY_TBL[CYFISNP_NODE_paLevel] & 0xF0) != 0);
    if (paOn )
#if OUT_OFF_RANGE_TEST      
      externalPaDisable();                                                   // Added by Jason Chen, 2016.01.12 
#else
      externalPaEnable();
#endif
    else externalPaDisable();
//#endif

#if OUT_OFF_RANGE_TEST      
    CYFISNP_NODE_SetTxConfig(CYFISNP_NODE_radioTxConfig&0xF8);               // Added by Jason Chen, 2016.01.12    
#else
    CYFISNP_NODE_SetTxConfig(CYFISNP_NODE_radioTxConfig + regVal);    
#endif
}


// If external PA hardware is installed (either Disabled or Enabled)
//#if (CYFISNP_NODE_EXTERNAL_PA)                                                                // -- //
// ---------------------------------------------------------------------------
//
// externalPaEnable()  - Enable  external PA
// externalPaDisable() - Disable external PA
//
// ---------------------------------------------------------------------------
static void externalPaEnable(void)
{
    char regVal;
    // -----------------------------------------------------------------------
    // PACTL_pin = PACTL Function
    // -----------------------------------------------------------------------
    regVal  =  CYFISNP_NODE_Read(CYFISNP_NODE_IO_CFG_ADR);
    regVal &= ~CYFISNP_NODE_PACTL_GPIO;                           // Bit2 = 0 --> PACTL as PA Control Function
    CYFISNP_NODE_Write(CYFISNP_NODE_IO_CFG_ADR, regVal);    

    // -----------------------------------------------------------------------
    //  XOUT_pin = ~PACTL Function (inverse of PACTL)
    // -----------------------------------------------------------------------
    regVal  =  CYFISNP_NODE_Read(CYFISNP_NODE_XTAL_CTRL_ADR);
    regVal &= ~CYFISNP_NODE_XOUT_FNC_MSK;
    regVal |=  CYFISNP_NODE_XOUT_FNC_PA_N;                        // B7B6 = 01 --> XOUT as Active Low PA Control
    CYFISNP_NODE_Write(CYFISNP_NODE_XTAL_CTRL_ADR, regVal);
}

static void externalPaDisable(void)
{
    char regVal;

//  // -----------------------------------------------------------------------
//  // Values for PACTL and XOUT when configured for Vendor_A GPIO
//  // PACTL = GPIO = 0 (no power for External PA)
//  //  XOUT = GPIO = 1 (connect Radio directly to antenna)
//  // -----------------------------------------------------------------------
//  regVal  =  CYFISNP_NODE_Read(CYFISNP_NODE_GPIO_CTRL_ADR);
//  regVal |=  CYFISNP_NODE_XOUT_OP;                 //  XOUT = 1
//  regVal &= ~CYFISNP_NODE_PACTL_OP;                // PACTL = 0
//  CYFISNP_NODE_Write(CYFISNP_NODE_GPIO_CTRL_ADR, regVal);

    // -----------------------------------------------------------------------
    //  PACTL_pin = GPIO Function  (= LOW)
    // -----------------------------------------------------------------------
    regVal  = CYFISNP_NODE_Read(CYFISNP_NODE_IO_CFG_ADR);
    regVal |= CYFISNP_NODE_PACTL_GPIO;                      //Bit2 = 1 --> PACTL as GPIO Function
    CYFISNP_NODE_Write(CYFISNP_NODE_IO_CFG_ADR, regVal);

    // -----------------------------------------------------------------------
    //  XOUT_pin = GPIO Function (= HIGH)
    // -----------------------------------------------------------------------
    regVal  =  CYFISNP_NODE_Read(CYFISNP_NODE_XTAL_CTRL_ADR);
    regVal &= ~CYFISNP_NODE_XOUT_FNC_MSK;
    regVal |=  CYFISNP_NODE_XOUT_FNC_GPIO;                       // B7B6 = 11 --> XOUT as GPIO Function
    CYFISNP_NODE_Write(CYFISNP_NODE_XTAL_CTRL_ADR, regVal);
}
//#endif //  #if (CYFISNP_NODE_EXTERNAL_PA)                                                     // -- //


//#if (CYFISNP_NODE_PWR_TYPE != CYFISNP_NODE_PWR_WALL)                                          // -- //
// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_spiSleep() - Minimize LRADIO power
//
//  This disconnects SPI, so you can't communicate with the Radio until
//   subsequently calling CYFISNP_NODE_spiWake().
//
// ---------------------------------------------------------------------------
static BYTE old_XtalCtrlAdr;
static BYTE old_IoCfgAdr;
static BYTE old_GpioCtrlAdr;
static BYTE old_XactCfgAdr;
static BOOL spiSleeping;

void CYFISNP_NODE_spiSleep(void)
{
    if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL )                                        // -- //
        return;                                                                                 // -- //

    if (spiSleeping == TRUE) {
        return;
    }
    spiSleeping = TRUE;

    // -----------------------------------------------------------------------
    // Save CYFISPI settings so they can be restored via CYFISNP_NODE_spiWake()
    // -----------------------------------------------------------------------
    old_XtalCtrlAdr = CYFISNP_NODE_Read(CYFISNP_NODE_XTAL_CTRL_ADR);
    old_IoCfgAdr    = CYFISNP_NODE_Read(CYFISNP_NODE_IO_CFG_ADR);
    old_XactCfgAdr  = CYFISNP_NODE_GetXactConfig();
    old_GpioCtrlAdr = CYFISNP_NODE_Read(CYFISNP_NODE_GPIO_CTRL_ADR);
//#if (CYFISNP_NODE_EXTERNAL_PA)
    old_GpioCtrlAdr |=  CYFISNP_NODE_XOUT_OP;       //  XOUT = 1
    old_GpioCtrlAdr &= ~CYFISNP_NODE_PACTL_OP;      //  PACTL = 0
//#endif

    // -----------------------------------------------------------------------
    // If RADIO is not sleeping, then manually FORCE to sleep
    //  (if it's already asleep, then don't wakeup just to put it to sleep).
    // -----------------------------------------------------------------------
    if ((old_XactCfgAdr&CYFISNP_NODE_END_STATE_MSK) != CYFISNP_NODE_END_STATE_SLEEP) {
        CYFISNP_NODE_ForceState(0);
    }

    // -----------------------------------------------------------------------
    // Since Radio could be operating on lower voltage than CPU, drive
    //  {MOSI,SCK,nSS} CPU outputs LOW to stop leakage from CPU to RADIO power
    //
    // Set XOUT LOW by setting to GPIO to minimize SPI/Radio current.
    // Set MISO LOW by changing to GPIO to keep from floating.
    // Set MOSI LOW by ensuring MISO is LOW during final SPI operation
    // -----------------------------------------------------------------------
    CYFISNP_NODE_Write(CYFISNP_NODE_XTAL_CTRL_ADR,
                  CYFISNP_NODE_XOUT_FNC_GPIO);                // XOUT=GPIO
    CYFISNP_NODE_Write(CYFISNP_NODE_IO_CFG_ADR, 0x42);        // SPI3PIN & MISO GPIO
                                                              // MISO goes LOW because that's MISO_GPIO's default
    CYFISNP_NODE_Write(CYFISNP_NODE_GPIO_CTRL_ADR ,0x00);     // XOUT=0 and leave MISO=0
                                                              // MOSI ends LOW because MISO[7] was 0 during last Write

//    Port_1_Data_SHADE &= ~CYFISNP_NODE_nSS_MASK;
    //CYFISNP_NODE_Radio_Select();//CYFISNP_NODE_nSS_Data_ADDR = Port_1_Data_SHADE;  // Radio_nSS=0

    // SPI NOT FUNCTIONAL UNTIL ?_spiWake() IS CALLED
}

void CYFISNP_NODE_spiSleepforMCUSleep(void)                   // Added by Jason Chen, 2014.04.15
{

    if (spiSleeping == TRUE) {
        return;
    }
    spiSleeping = TRUE;

    // -----------------------------------------------------------------------
    // Save CYFISPI settings so they can be restored via CYFISNP_NODE_spiWake()
    // -----------------------------------------------------------------------
    old_XtalCtrlAdr = CYFISNP_NODE_Read(CYFISNP_NODE_XTAL_CTRL_ADR);
    old_IoCfgAdr    = CYFISNP_NODE_Read(CYFISNP_NODE_IO_CFG_ADR);
    old_XactCfgAdr  = CYFISNP_NODE_GetXactConfig();
    old_GpioCtrlAdr = CYFISNP_NODE_Read(CYFISNP_NODE_GPIO_CTRL_ADR);
//#if (CYFISNP_NODE_EXTERNAL_PA)
    old_GpioCtrlAdr |=  CYFISNP_NODE_XOUT_OP;       //  XOUT = 1
    old_GpioCtrlAdr &= ~CYFISNP_NODE_PACTL_OP;      //  PACTL = 0
//#endif

    // -----------------------------------------------------------------------
    // If RADIO is not sleeping, then manually FORCE to sleep
    //  (if it's already asleep, then don't wakeup just to put it to sleep).
    // -----------------------------------------------------------------------
    if ((old_XactCfgAdr&CYFISNP_NODE_END_STATE_MSK) != CYFISNP_NODE_END_STATE_SLEEP) {
        CYFISNP_NODE_ForceState(0);
    }

    // -----------------------------------------------------------------------
    // Since Radio could be operating on lower voltage than CPU, drive
    //  {MOSI,SCK,nSS} CPU outputs LOW to stop leakage from CPU to RADIO power
    //
    // Set XOUT LOW by setting to GPIO to minimize SPI/Radio current.
    // Set MISO LOW by changing to GPIO to keep from floating.
    // Set MOSI LOW by ensuring MISO is LOW during final SPI operation
    // -----------------------------------------------------------------------
    CYFISNP_NODE_Write(CYFISNP_NODE_XTAL_CTRL_ADR,  CYFISNP_NODE_XOUT_FNC_GPIO);     // XOUT=GPIO                                    
    CYFISNP_NODE_Write(CYFISNP_NODE_IO_CFG_ADR, 0x42);                               // SPI3PIN & MISO GPIO
                                                                                     // MISO goes LOW because that's MISO_GPIO's default
    CYFISNP_NODE_Write(CYFISNP_NODE_GPIO_CTRL_ADR ,0x00);     // XOUT=0 and leave MISO=0
                                                              // MOSI ends LOW because MISO[7] was 0 during last Write 

    //CYFISNP_NODE_Radio_Select()                                                              
                                                              
                                                              
}

// ---------------------------------------------------------------------------
//
// CYFISNP_NODE_spiWake() - Restore normal Radio operation,
//                              Called to exit CYFISNP_NODE_spiSleep().
//
// ---------------------------------------------------------------------------
void CYFISNP_NODE_spiWake(void)
{
    if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL )                                        // -- //
        return;                                                                                 // -- //

    if (spiSleeping == FALSE) {
        return;
    }
    spiSleeping = FALSE;

//    Port_1_Data_SHADE |= CYFISNP_NODE_nSS_MASK;
    CYFISNP_NODE_Radio_Deselect();//CYFISNP_NODE_nSS_Data_ADDR = Port_1_Data_SHADE;  // Radio_nSS=1

    CYFISNP_NODE_Write(CYFISNP_NODE_IO_CFG_ADR,    old_IoCfgAdr);     // Restore SPI3PIN
    CYFISNP_NODE_Write(CYFISNP_NODE_GPIO_CTRL_ADR, old_GpioCtrlAdr);  // Restore GPIO
    CYFISNP_NODE_Write(CYFISNP_NODE_XTAL_CTRL_ADR, old_XtalCtrlAdr);  // Restore XOUT FN

    if ((old_XactCfgAdr&CYFISNP_NODE_END_STATE_MSK) != CYFISNP_NODE_END_STATE_SLEEP) {
        CYFISNP_NODE_ForceState(old_XactCfgAdr);          // Restore radio state
    }
}
//#endif // (CYFISNP_NODE_PWR_TYPE != CYFISNP_NODE_PWR_WALL)                                    // -- //


// ---------------------------------------------------------------------------
//
// netParamsPresent() - Determine currently Bound
//                          A valid hubSeedMsb is never 0x00
//
// ---------------------------------------------------------------------------
/*static*/ BOOL netParamsPresent (void) {
    if (CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedMsb != 0) {              //nodeSeedLsb
        return(TRUE);
    }
    return(FALSE);
}

// ---------------------------------------------------------------------------
// crc0AutoAck() - Disable and enable autoacks for CRC0 packets.
//
//  When transmitting w/CRC0, we expect an AutoAck (e.g. Bind/ParamReq).
//  When receiving w/CRC0, we don't want to return an AutoAck (e.g. Beacon)
// ---------------------------------------------------------------------------
static void crc0AutoAck(BOOL enable) {
    if (enable == TRUE) {
        CYFISNP_NODE_Write(CYFISNP_NODE_RX_OVERRIDE_ADR, 0);
    } else {
        CYFISNP_NODE_Write(CYFISNP_NODE_RX_OVERRIDE_ADR, CYFISNP_NODE_DIS_CRC0);
    }
}


//BYTE CYFISNP_NODE_GetQualGfsk(void) {        // Debug access for API
//    return (dpwrQualGfsk);
//}
//BYTE CYFISNP_NODE_GetQual8dr(void) {         // Debug access for API
//    return(dpwrQual8dr);
//}
// ###########################################################################

void ShelfMode(BOOL state)
{
    if( state ) {
        // go to WALL mode
        if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_COIN ) {
            CYFISNP_NODE_spiWake();     // before changing to WALL mode
            CYFISNP_NODE_Pwr_Type = CYFISNP_NODE_PWR_WALL;
            //gotoConnectModeBase();
            setIdleReceive(/*CYFISNP_NODE_PWR_TYPE*/CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL);    // -- //
        }
    } else {
        // go to COIN mode
        if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL ) {
            CYFISNP_NODE_Pwr_Type = CYFISNP_NODE_PWR_COIN;
            //gotoConnectModeBase();
            setIdleReceive(/*CYFISNP_NODE_PWR_TYPE*/CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL);    // -- //
        }
    }
}

void CYFISNP_NODE_PowerOn_Init(void)
{
    CYFISNP_NODE_XactConfig = 0;
    CYFISNP_NODE_RestingState = 0;
    CYFISNP_NODE_State = 0;
    (void)memset(CYFISNP_NODE_Temp, 0, 4);
    CYFISNP_NODE_WipPtr = (byte*)0;
    CYFISNP_NODE_WipLen = 0;
    CYFISNP_NODE_Ptr = (byte*)0;
    CYFISNP_NODE_Len = 0;
    CYFISNP_NODE_BytesRead = 0;
    CYFISNP_NODE_TxCtrlShadow = 0;

    CYFISNP_NODE_eProtState = 0;
    CYFISNP_NODE_bCurrentChannel = 0x00;//, CYFISNP_NODE_bPreviousChannel = 0xFF;
    (void)memset(SNP_txBuf, 0, SNP_PROT_PKT_TX_MAX);
    (void)memset(&SNP_rxBuf, 0, sizeof(SNP_rxBuf));
    CYFISNP_NODE_radioTxConfig = CYFISNP_NODE_DEF_DATA_RATE;
    CYFISNP_NODE_paLevel = 0;
    fTsb = 0;
    rxBufPend = 0;
    radioStateCopy = 0;
    rxLen = 0;
    idleReceive = 0;
    CYFISNP_NODE_Pwr_Type = CYFISNP_NODE_PWR_COIN;
    bDataRetryCt = 0;
    pTxStruct = (byte*)0;
    txPending = 0;
    rspWindowTimer = 0;
    onChanTxLeft = 0;
    fGotChanAck = 0;
    wModeCounter = 0;
    wModeDelay = 0;
    bindChIdx = 0;
    wBindModeCounter = 0;
    bindReqTimer = 0;
    beaconMaxTimer = 0;
    CYFISNP_NODE_inRxGo = 0;
}
