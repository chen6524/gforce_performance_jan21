//
// File:        CYFISNP_NODE_PSoC_SPIm.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes
#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
#include "CYFISNP_NODE.h"

////////////////////////
// definitions

////////////////////////
// internal vars

////////////////////////
// external vars

// This portion of CYFISNP_NODE_PSoC_SPIm.asm does not manipulate the GIE
// flag in any way.  However, the GIE flag must be cleared when these routines
// are executing.  Therefore, they may only be directly called by interrupt
// service routines (which do not nest interrupts).
//////////////////////////////////////////////////////////////////////////////////
//

// write a single byte to a Radio register (high 2 bits must be zero)
//void CYFISNP_NODE_WriteSwappedRaw(byte value, byte regAddr)    // the two parameters are swapped
//{
//    CYFISNP_NODE_WriteRaw(regAddr, value);     // swap the parameters and call the normal one
//}

// write a single byte to a Radio register (high 2 bits must be zero)
void CYFISNP_NODE_WriteRaw(byte regAddr, byte value)
{
    // enable Radio
    CYFISNP_NODE_Radio_Select();

        // write SPI address
        (void)CYFISNP_NODE_spi_io(regAddr|CYFISNP_NODE_bSPI_WRITE);   // set the write bit

        // write SPI data
        (void)CYFISNP_NODE_spi_io(value);

    // deselect Radio
    CYFISNP_NODE_Radio_Deselect();
}

// read a single byte from a Radio register (high 2 bits must be zero)
byte CYFISNP_NODE_ReadRaw(byte regAddr)
{
    byte spiValue;

    // enable Radio
    CYFISNP_NODE_Radio_Select();

        // write SPI address
        (void)CYFISNP_NODE_spi_io(regAddr);

        // null SPI write
        spiValue = CYFISNP_NODE_spi_io(0xFF);    // get Radio data

    // deselect Radio
    CYFISNP_NODE_Radio_Deselect();

    return spiValue;
}

// read a single byte from the RX_IRQ_STATUS_ADR register and debounce the update of the
// RXC and RXE bits. If only one of those two bits is set, read the register a second time
// and OR them together. This second read happens in the same SPI transaction as a burst to
// the same address
byte CYFISNP_NODE_ReadStatusDebouncedRaw(byte regAddr)
{
    byte spiValue;
    byte spiTemp;

    // enable Radio
    CYFISNP_NODE_Radio_Select();

        // write SPI address
        (void)CYFISNP_NODE_spi_io(regAddr);

        // null SPI write
        spiValue = CYFISNP_NODE_spi_io(0xFF);    // get Radio data
        spiTemp = spiValue;

        // if RXC and RXE (or TXC and TXE) match, then skip the debounce read
        spiValue &= (CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXE_IRQ);
        if( spiValue == 0x00 ) {
            // both bits are ZERO
            ;
        } else if( spiValue == (CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXE_IRQ) ) {
            // both bits are ONE
            ;
        } else {
            // RXC and RXE are different
            // read again to ensure both bits have updated

            // write SPI address   
            (void)CYFISNP_NODE_spi_io(regAddr);                                       // Added By Jason Chen, 2014.04.3

            // null SPI write
            spiValue = CYFISNP_NODE_spi_io(0xFF);    // get Radio data
            spiTemp |= spiValue;
        }
        //
        spiValue = spiTemp;

    // deselect Radio
    CYFISNP_NODE_Radio_Deselect();

    return spiValue;
}

// write a sequence of bytes to a sequence of Radio registers
// use RadioWipPtr instead of RadioPtr as the data pointer
void CYFISNP_NODE_BurstWriteWipRaw(byte regAddr, byte cnt)                     // Burst    Wip
{
    CYFISNP_NODE_FileWriteWipRaw(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// write a sequence of bytes to a sequence of Radio registers
void CYFISNP_NODE_BurstWriteRaw(byte regAddr, byte cnt)                        // Burst    ---
{
    CYFISNP_NODE_FileWriteRaw(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// wite a squence of bytes to a single Radio register
void CYFISNP_NODE_FileWriteRaw(byte regAddr, byte cnt)                         // -----    ---
{
    CYFISNP_NODE_WipPtr = CYFISNP_NODE_Ptr;
    CYFISNP_NODE_FileWriteWipRaw(regAddr, cnt);
}

//
void CYFISNP_NODE_FileWriteWipRaw(byte regAddr, byte cnt)                      // -----    Wip
{
    // do if cnt is not zero
    if( cnt ) {
        // enable Radio
        CYFISNP_NODE_Radio_Select();

            // write SPI address
            (void)CYFISNP_NODE_spi_io(regAddr|CYFISNP_NODE_bSPI_WRITE);   // set the write bit

            // write SPI data
            while(cnt--)
                (void)CYFISNP_NODE_spi_io(*CYFISNP_NODE_WipPtr++);

        // deselect Radio
         CYFISNP_NODE_Radio_Deselect();
    }
}

// read a sequence of bytes from a sequence of Radio registers
// using RadioWipPtr as the buffer pointer
void CYFISNP_NODE_BurstReadWipRaw(byte regAddr, byte cnt)                      // Burst    Wip
{
    CYFISNP_NODE_FileReadWipRaw(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// read a sequence of bytes from a sequence of Radio registers
void CYFISNP_NODE_BurstReadRaw(byte regAddr, byte cnt)                         // Burst    ---
{
    CYFISNP_NODE_FileReadRaw(regAddr|CYFISNP_NODE_bSPI_AUTO_INC, cnt);
}

// read a sequence of bytes from a single Radio register
void CYFISNP_NODE_FileReadRaw(byte regAddr, byte cnt)                          // -----    ---
{
    CYFISNP_NODE_WipPtr = CYFISNP_NODE_Ptr;
    CYFISNP_NODE_WipLen = CYFISNP_NODE_Len;
    CYFISNP_NODE_FileReadWipRaw(regAddr, cnt);
}

//
void CYFISNP_NODE_FileReadWipRaw(byte regAddr, byte cnt)                       // -----    Wip
{
    byte spiValue;
    byte BytesToBuffer;
    byte BytesToDiscard;

    // do if cnt is not zero
    if( cnt ) {

        // to keep Radio reads more smooth, we prepare these two variables in
        // advance and then start Radio access, rather than accessing like this:
        //
        //  (void)CYFISNP_NODE_spi_io(regAddr);
        //  while(cnt--) {
        //      spiValue = CYFISNP_NODE_spi_io(0xFF);    // get Radio data
        //      if( CYFISNP_NODE_WipLen ) {
        //          *CYFISNP_NODE_WipPtr++ = spiValue;
        //          CYFISNP_NODE_WipLen--;
        //      }
        //  }
        //
        if( cnt <= CYFISNP_NODE_WipLen ) {
            BytesToBuffer = cnt;
            BytesToDiscard = 0;
            CYFISNP_NODE_WipLen -= cnt;
        } else {
            BytesToBuffer = CYFISNP_NODE_WipLen;
            BytesToDiscard = cnt-CYFISNP_NODE_WipLen;
            CYFISNP_NODE_WipLen = 0;
        }

        // enable Radio
        CYFISNP_NODE_Radio_Select();

            // write SPI address
            (void)CYFISNP_NODE_spi_io(regAddr);

            // null SPI write
            while(BytesToBuffer--)
                *CYFISNP_NODE_WipPtr++ = CYFISNP_NODE_spi_io(0xFF);    // get Radio data

            // null SPI write
            while(BytesToDiscard--)
                spiValue = CYFISNP_NODE_spi_io(0xFF);    // get Radio data

        // deselect Radio
        CYFISNP_NODE_Radio_Deselect();
    }
}

// send 1 byte, receive 1 byte
// During the transfer, the master shifts data out (on the MOSI pin) to the slave
// while simultaneously shifting data in (on the MISO pin) from the slave. The transfer
// effectively exchanges the data that was in the SPI shift registers of the two SPI systems.
// The SPSCK signal is a clock output from the master and an input to the slave. The slave
// device must have been selected by a low level on the slave select output (SS pin).
byte CYFISNP_NODE_spi_io(byte data)
{
    // wait while SPI transmit buffer is not empty
    while( !SPI1S_SPTEF )   // SPI Transmit Buffer Empty Flag
        ;

    // send the data
    SPI1DL = data;

    // wait while no data available in the receive data buffer
    while( !SPI1S_SPRF )    // SPI Read Buffer Full Flag
        ;

    // return the received data
    return (byte)SPI1DL;
}
