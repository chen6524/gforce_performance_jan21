//
// File:        CYFISNP_NODE_Time.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
//#include <m8c.h>
#include "CYFISNP_NODE_Time.h"
#include "CYFISNP_NODE_Config.h"

//#if (CYFISNP_NODE_PSOC_EXPRESS_PROJECT)
//    #include "SystemTimer.h"
//    extern WORD SystemTimer_TickCount;
//#else
    //#include "SleepTimer.h"
    extern volatile WORD SleepTimer_TickCount;
//#endif


// ---------------------------------------------------------------------------
// SleepTimer_TickCount = free-running 16-bit incrementing Tick counter from
//                         SleepTimer User Module.
// ---------------------------------------------------------------------------


// ---------------------------------------------------------------------------
//
// ?__TimeSet()     -  Set caller's timer for polling
// ?__TimeExpired() - Poll caller's timer
//
// ---------------------------------------------------------------------------


// ---------------------------------------------------------------------------
// This specifies a decrementing Tick Counter instead of incrementing
// ---------------------------------------------------------------------------
//#define SYSTIMER_DECREMENTS


    void
CYFISNP_NODE_TimeSet(
    WORD *pTimer,           // Ptr to store expiration time
    WORD time               // 32767 counts maximum
    ) {                     // -----------------------------------------------
    WORD tmpTime;
//    M8C_DisableIntMask(INT_MSK0, INT_MSK0_SLEEP);
//#if (CYFISNP_NODE_PSOC_EXPRESS_PROJECT)
//    tmpTime = SystemTimer_TickCount;
//#else
    tmpTime = SleepTimer_TickCount;
//#endif
//    M8C_EnableIntMask(INT_MSK0, INT_MSK0_SLEEP);
//#ifdef SYSTIMER_DECREMENTS      // free-running Timer decrements
//    *pTimer = tmpTime - time;
//#else                           // ?_SysTimer16 increments
    *pTimer = (WORD)(tmpTime + time);
//#endif
    if( *pTimer == 0 )
        *pTimer = 1;
}
            

BOOL                    // Ret TRUE if expired
CYFISNP_NODE_TimeExpired(
    WORD *pTimer            // ptr to expiration time
    ) {                     // -----------------------------------------------
    WORD tmpTime;
//    M8C_DisableIntMask(INT_MSK0, INT_MSK0_SLEEP);
//#if (CYFISNP_NODE_PSOC_EXPRESS_PROJECT)
//    tmpTime = SystemTimer_TickCount;
//#else
    if( *pTimer == 0 )
        return TRUE;

    tmpTime = SleepTimer_TickCount;
//#endif
//    M8C_EnableIntMask(INT_MSK0, INT_MSK0_SLEEP);
//#ifdef SYSTIMER_DECREMENTS
//    tmpTime = tmpTime - *pTimer;
//#else
    tmpTime = (WORD)(*pTimer - tmpTime);
//#endif
    if (((BYTE)(tmpTime>>8)&0x80) == 0)
        return FALSE;   // not expired

    *pTimer = 0;
    return TRUE;    // expired
}


void CYFISNP_NODE_Delay100uS(void)
{
    byte i;

    for(i=0; i<(100*BUSCLK_KHZ)/1000/13; i++) 
        asm(nop;);
//
// [1]             NOP
// [4]             INC   ,X
// [3]             LDA   ,X
// [2]             CMP   #184
// [3]             BCS   for
//
// 13 cycles * 184 / 24 MHz = 99.7 uSec (~100uSec)
}

// ###########################################################################
