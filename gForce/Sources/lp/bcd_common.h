//
// File:        bcd_common.h
// Date:        Sep 2013
// Author:      JASON Chen
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,11 2013 First Release
//  10/13/2013   -- Added two-way wireless functionality by JASON CHEN
//  Nov.12, 2013    Added proximity application  by JASON CHEN


#ifndef _BCD_COMMON_H_
#define _BCD_COMMON_H_

//////////////////////////////////////////////////////////////////////////////////
// definitions/macros
//Jason Define Back Channel, SEP 11, 2013
#define ENABLE_BACK_CHANNEL_BIND        1
#if ENABLE_BACK_CHANNEL_BIND
  #define ENABLE_BACK_CHANNEL_RECV      1
#else
  #define ENABLE_BACK_CHANNEL_RECV      0
#endif
#define ENABLE_SUMMARY_ACK              0


//#define ENABLE_BACK_CHANNEL_RECV_PWR  1

#define SLEEP_MODE_RADIO_OFF            1

// Added by Jason Chen, 20140106
#define POWER_OFF_MODE_RADIO_OFF        1


//#if SLEEP_MODE_RADIO_OFF | POWER_OFF_MODE_RADIO_OFF
 // #define RADIO_PWR_ON()   (PTFD_PTFD0 = 1)        // PTF0
 // #define RADIO_PWR_OFF()  (PTFD_PTFD0 = 0)
//extern void CYFISNP_NODE_PowerOn_Init(void);
 // extern unsigned char CYFISNP_NODE_Start(void);  
 // extern void CYFISNP_NODE_Stop(void);
  //extern void CYFISNP_NODE_spiSleepforMCUSleep(void);                   // Added by Jason Chen, 2014.04.15
  //extern void Radio_Stack_Save_and_Prog_Spi(void);
 // extern void Radio_Stack_Restore_Spi(void);
 // extern void USB_MODE_to_IDLE(void);
  
  extern volatile byte lpCmdMail;
  extern byte lpState;
//#endif


// Added by Jason Chen, 2013.11.14
#define POWER_ON_BAT_REPORT             0
//#define LG_POWER_ON_ENABLE            0

// Added by Jason Chen, 2013.11.12
//#define JASON_PROXIMITY_ENABLE        1
//#define PROXIMITY_ON_ENABLE           0


// Added by Jason Chen, 2013.11.15
#define LOW_G_WAKEUP_ENABLE             1
#define ENABLE_SLEEP_MODE               1//??????????????????????????????????????????????

// Added by Jason Chen, 2014.01.15
#define ENABLE_SLEEP_PACKET_SEND        0

// Added by Jason Chen, 2013.11.25
#define POWEROFF_LED_ON                 1

// Added by Jason Chen, 2013.11.26/20
//#define KASHIF_POWERON_LED_PROCESS    1
//#define ADC_POWERON_SENDING           1

//#define KASHIF_POWEROFF_LED_PROCESS   1      //Enable it because of new Power off process, 2014.12.19// Disabled by Jason Chen according to requirement of Paul and Gerry, 2014.06.02
//#define KASHIF_SLEEP_LED_PROCESS      1
//#define ENABLE_EXTERNAL_RTC_WAKE_UP   1

//Added by Jason Chen, 2013.12.11
#define BACK_CHANNEL_UPLOAD             1
//#define DISABLE_UPLOAD_D1_D0_MODE     1

//Added by Jason Chen, 2013.12.19
//#define IMPACT_TIME_STAMP_FIX         1

// Added by Jason Chen, 2014.01.13
#define POWER_OFF_UNPLUG                1//??????????????????????

// Added by Jason Chen, 2014.02.12
//#define ENABLE_REAL_TIME_REPORT       1


// Added by Jason Chen, 2014.04.15
#define ENABLE_RECV_WINDOW_TIMER        1

// Added by Jason Chen, 2014.04.17
#define ENABLE_SEND_LOW_BATTERY         1

// Added by Jason Chen, 2014.04.23
#define VERSION_13                      1

// Added by Jason Chen, 2014.04.25
#define RTC_TIME_FIFTEEN_MINUTE         0     // Default Value Slected as 1 minute, 2014.08.05
#define ENABLE_LOW_BATT_OFF             1

//Added by Jason Chen, 2014.05.07
#define ENABLE_FAKE_IMPACT_FILTER       1
#define ENABLE_ACC_DATA_OUTPUT          0
#define USE_INTERRUPT_FILTER            0

#define IIC_ENABLE_IN_STOP              1    // Added by Jason Chen, 2014.12.08

#define DISABLE_RECORDING_DURING_UNPLUG 1    // Added by Jason Chen, 2015.09.01
#define ENABLE_RECORDING_UNBUND         0    // Added by Jason Chen, 2015.09.01
#define DISABLE_STACK_POWERON_INIT      1    // Added by Jason Chen, 2015.09.01
#define ENABLE_BUZZER_BEEP_PROX         0    // Added by Jason Chen, 2015.09.01
#define UPLOAD_BY_WIRE_IN_BINDING_MODE  1    // Added by Jason Chen, 2015.09.03
#define NO_REBOOT_UNBIND                0    // Modified by Jason Chen, 2019.02.07,   // Added by Jason Chen, 2015.09.03
#define FIX_FLASHWRITE_BUG              1    // Added by Jason Chen, 2015.09.08
#define OFF_UNPLUG_IN_BINDINGM_MODE     1    // Added by Jason Chen, 2015.09.08
#define WAKUP_15_SECs_IN_BINDINGM_MODE  1    // Added by Jason Chen, 2015.09.08

#define ENABLE_TRANSMISSION_FILTER      0    // Added by Jason Chen, 2015.09.10
#define CREATE_END_SESSION_UPLOAD       1    // Added by Jason Chen, 2015.09.10

#define ENABLE_SESSION_START_END_RETRY  1    // Added by Jason Chen, 2015.09.24

#define DISABLE_RECORDING_DURING_DOWNLOADING   1   // Added by Jason Chen, 2016.01.11

// Out of range test, 2016.01.05
#define OUT_OFF_RANGE_TEST              0
extern void SetUsrProfileLP_Disable(void);   // Added by Jason Chen, 2019.02.07     
#endif  // #ifndef _BCD_COMMON_H_
