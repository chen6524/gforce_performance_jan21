//
// File:        CYFISNP_NODE_Core.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes
#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
#include "CYFISNP_NODE.h"

////////////////////////
// definitions
#define CYFISNP_NODE_LP_FIFO_SIZE        16  // Radio Rx/Tx FIFO size
#define CYFISNP_NODE_LP_FIFO_HALF        8   // Radio Rx/Tx FIFO half-way mark (B8 flag)
#define CYFISNP_NODE_IsRadioSleep()      ((CYFISNP_NODE_RestingState & CYFISNP_NODE_END_STATE_MSK)==CYFISNP_NODE_END_STATE_SLEEP)  // 0x1C
#define CYFISNP_NODE_IsSleepDesired()    ((CYFISNP_NODE_XactConfig   & CYFISNP_NODE_END_STATE_MSK)==CYFISNP_NODE_END_STATE_SLEEP)  // 0x1C
                            // xxx000xx Sleep
                            // xxx001xx Idle
                            // xxx010xx Synth Tx
                            // xxx011xx Synth Rx
                            // xxx100xx Rx

#define CYFISNP_NODE_PacketLength   CYFISNP_NODE_Temp[0]    // to CYFISNP_NODE_TX_LENGTH_ADR @ [0x01]
#define CYFISNP_NODE_TxControl      CYFISNP_NODE_Temp[1]    // to CYFISNP_NODE_TX_CTRL_ADR   @ [0x02]
#define CYFISNP_NODE_RetryCount     CYFISNP_NODE_Temp[2]    // in Tx mode
#define CYFISNP_NODE_RxCount        CYFISNP_NODE_Temp[2]    // in Rx mode
#define CYFISNP_NODE_TxCount        CYFISNP_NODE_Temp[3]    //

#if SPICLK==4000000L
    #define WAIT_FORCE  500     // about 2 mSec (500 *  4uSec)
#elif SPICLK==2000000L
    #define WAIT_FORCE  250     // about 2 mSec (250 *  8uSec)
#elif SPICLK==1000000L
    #define WAIT_FORCE  125     // about 2 mSec (125 * 16uSec)
#else
    #error "SPICLK not specified properly..."
#endif

////////////////////////
// internal vars

////////////////////////
// external vars

//////////////////////////////////////////////////////////////////////////////////
//

// start the transmission of a packet
// the location of the packet buffer to transmit must have
// previously been set by calling RadioSetPtr()
// after starting the transmission of a packet with this call,
// the transmit operation should be monitored via RadioGetTransmitState()
// when RadioGetTransmitState() indicates that the transmission has completed,
// then RadioEndTransmit() should be called
// after calling RadioStartTransmit() NO CALLS can be made to configuration access routines
// until transmit operation is terminated by calling RadioEndTransmit() or RadioAbort()
// until a call is made to end the transmit operation,
// the only other call supported is RadioGetTransmitState()
void CYFISNP_NODE_StartTransmit(byte retryCount, byte len)
{
    byte im;

    CYFISNP_NODE_RetryCount = retryCount;  // CYFISNP_NODE_Temp[2]
    CYFISNP_NODE_PacketLength = len;       // CYFISNP_NODE_Temp[0], to CYFISNP_NODE_TX_LENGTH_ADR @ [0x01]

    im = CYFISNP_NODE_SaveAndClearGIE();
    (void)CYFISNP_NODE_RestartTransmit();
    CYFISNP_NODE_RestoreGIE(im);
}

//
byte CYFISNP_NODE_RestartTransmit(void)
{
    byte txLen;

    // if Radio is in Sleep, make it Idle
    CYFISNP_NODE_WakeupForXact();

    CYFISNP_NODE_State = CYFISNP_NODE_TX;              // 0x20
    CYFISNP_NODE_TxControl = (CYFISNP_NODE_TX_GO | CYFISNP_NODE_TX_CLR | CYFISNP_NODE_TXC_IRQ | CYFISNP_NODE_TXE_IRQ); // CYFISNP_NODE_Temp[1], to CYFISNP_NODE_TX_CTRL_ADR @ [0x02]
    // enable TXE, enable TXC, clear Tx buffer and start Tx

    // check if packet is longer than TxFIFO
    if( CYFISNP_NODE_PacketLength <= CYFISNP_NODE_LP_FIFO_SIZE ) {
        txLen = CYFISNP_NODE_PacketLength;
        CYFISNP_NODE_TxCount = 0;                                   // no extra bytes to send
    } else {
        txLen = CYFISNP_NODE_LP_FIFO_SIZE;
        CYFISNP_NODE_TxCount = CYFISNP_NODE_PacketLength - CYFISNP_NODE_LP_FIFO_SIZE;    // save extra bytes to send
        CYFISNP_NODE_TxControl |= CYFISNP_NODE_TXB8_IRQ;     // need to enable TXB8 too
    }
    CYFISNP_NODE_TxCtrlShadow = CYFISNP_NODE_TxControl;

    // TX_GO is issued (starting the radio transmitting)
    // IMMEDIATELY after this (during radio "start-up"), the data
    // is copied to the radio.  Global Interrupts are disabled, because
    // the data must be in the radio before the "startup" is finished
    CYFISNP_NODE_WipPtr = &CYFISNP_NODE_Temp[0];    // buffer start address (2 bytes)
    CYFISNP_NODE_WipLen = 2;                // 2 bytes
    CYFISNP_NODE_BurstWriteWipRaw(CYFISNP_NODE_TX_LENGTH_ADR, 2);               // [0x01] & [0x02]

    // Load the data AFTER setting the TX_GO bit
    // If data is loaded after setting this bit, the length of time available to load the buffer
    // depends on the starting state (sleep, idle or synth), the length of the SOP code,
    // the length of preamble, and the packet data rate
    CYFISNP_NODE_FileWriteRaw(CYFISNP_NODE_TX_BUFFER_ADR, txLen);               // [0x20], file register

    return CYFISNP_NODE_State;  // 0x20
}

// returns the state of the current transmit operation
// this call should be made after starting a transmit
// operation with the RadioStartTransmit() function
byte CYFISNP_NODE_GetTransmitState(void)
{
    byte im;
    byte status;

    im = CYFISNP_NODE_SaveAndClearGIE();

    if( CYFISNP_NODE_IsRadioIrq() ) {    // if LpIrq pin is high
        // IRQ indicates TX_GO is finished,
        // so we have (TXB8, TXBERR,) TXC_IRQ or TXE_IRQ or both
        status = CYFISNP_NODE_GetTransmitStateIsr();
    } else
        status = CYFISNP_NODE_State;

    CYFISNP_NODE_RestoreGIE(im);
    return status;
}

// RadioIrq pin is high, either TX_GO is finished or we have TXB8_IRQ
byte CYFISNP_NODE_GetTransmitStateIsr(void)
{
    byte status;
    byte txLen;
    byte isMore;

    // IRQ indicates TX_GO is finished,
    // so we have (TXB8, TXBERR,) TXC_IRQ or TXE_IRQ or both
    status = CYFISNP_NODE_ReadStatusDebouncedRaw(CYFISNP_NODE_TX_IRQ_STATUS_ADR);   // [0x04]
    status &= (CYFISNP_NODE_TXB8_IRQ | CYFISNP_NODE_TXE_IRQ | CYFISNP_NODE_TXC_IRQ);
    status &= CYFISNP_NODE_TxCtrlShadow;
    CYFISNP_NODE_State |= status;

    // check if we are streaming (RadioPacketLength was more than 16)
    if( status & CYFISNP_NODE_TXB8_IRQ ) {
        // the first 16 bytes have already been moved, so move more in burst of 8 bytes,
        // moving data, set TX IRQ mask to only TXB8 to simplify checking
        // when subsequent data can be moved after the first burst
        CYFISNP_NODE_WriteRaw(CYFISNP_NODE_TX_CTRL_ADR, CYFISNP_NODE_TXB8_IRQ);

        // move data loop
        do {
            // is the remaining data longer than the 8 free bytes in TxFIFO
            if( CYFISNP_NODE_TxCount <= CYFISNP_NODE_LP_FIFO_HALF ) {
                txLen = CYFISNP_NODE_TxCount;
                CYFISNP_NODE_TxCount = 0;                               // no extra bytes to move
            } else {
                txLen = CYFISNP_NODE_LP_FIFO_HALF;
                CYFISNP_NODE_TxCount = CYFISNP_NODE_TxCount - CYFISNP_NODE_LP_FIFO_HALF;     // save extra bytes to move
            }
            CYFISNP_NODE_FileWriteWipRaw(CYFISNP_NODE_TX_BUFFER_ADR, txLen);

            // check for more
            if( CYFISNP_NODE_TxCount==0 ) {
                // no more data, stop
                isMore = 0;
                // set the required TX INT bits
                CYFISNP_NODE_TxCtrlShadow = (CYFISNP_NODE_TXC_IRQ | CYFISNP_NODE_TXE_IRQ);
            } else if( CYFISNP_NODE_IsRadioIrq() ) {
                // RadioTxCount is not zero and
                // RadioInt is asserted, meaning there are 8 or more bytes free in Tx buffer
                isMore = 1;
            } else {
                // RadioTxCount is not zero and
                // RadioInt is not asserted, meaning there are less than 8 bytes free in Tx buffer
                // we will stop for now, and continue at the next TXB8 int
                isMore = 0;
                // set the required TX INT bits
                CYFISNP_NODE_TxCtrlShadow = (CYFISNP_NODE_TXB8_IRQ | CYFISNP_NODE_TXC_IRQ | CYFISNP_NODE_TXE_IRQ);
            }
        } while(isMore);

        // restore TX_CTRL_ADR
        CYFISNP_NODE_WriteRaw(CYFISNP_NODE_TX_CTRL_ADR, CYFISNP_NODE_TxCtrlShadow);

        // check for Tx errors
        if( CYFISNP_NODE_State & (CYFISNP_NODE_TXE_IRQ | CYFISNP_NODE_TXBERR_IRQ) )
            CYFISNP_NODE_State |= CYFISNP_NODE_TXC_IRQ;

    } else {
        // We're here, but NOT to move data. 'status' contains the last TX status read with
        // TXC masked out. RadioState has had the last TX status read OR'd in
        // including TXC bit. If TXC, TXE, and TXBERR are all clear, that is an error.
        if( CYFISNP_NODE_State & CYFISNP_NODE_TXC_IRQ ) {
            // Tx completed, check for Tx errors
            if( CYFISNP_NODE_State & (CYFISNP_NODE_TXE_IRQ | CYFISNP_NODE_TXBERR_IRQ) )
                CYFISNP_NODE_State |= CYFISNP_NODE_TXC_IRQ;

        } else if( status & (CYFISNP_NODE_TXE_IRQ | CYFISNP_NODE_TXBERR_IRQ) ) {
            // there was errors in Tx
            CYFISNP_NODE_State |= CYFISNP_NODE_TXC_IRQ;

        } else {
            // Tx complete with error
            // during Transaction Ack Rx window, if SOP is received
            // then get RXC/RXE (not TXC/TXE); RadioStartReceive() ties RXC/RXE to IRQ,
            // reading RX_IRQ_STATUS_ADR here ensures IRQ deasserts
            (void)CYFISNP_NODE_ReadRaw(CYFISNP_NODE_RX_IRQ_STATUS_ADR);
            CYFISNP_NODE_State |= CYFISNP_NODE_TXC_IRQ;
        }
    }

    // in case of error, retry
    if( CYFISNP_NODE_State & CYFISNP_NODE_TXE_IRQ ) {
        // maybe retry the failed Tx
        if( CYFISNP_NODE_RetryCount ) {     // RadioTemp[2]
            CYFISNP_NODE_RetryCount--;
            return CYFISNP_NODE_RestartTransmit();  // returns with RadioState=0x20
        }
    }
    return CYFISNP_NODE_State;
}

//
void CYFISNP_NODE_EndTransmit(void)
{
    CYFISNP_NODE_State = CYFISNP_NODE_IDLE;        // 0x00
}

//
void CYFISNP_NODE_BlockingTransmit(byte retryCount, byte len)
{
    byte status;

    CYFISNP_NODE_StartTransmit(retryCount, len);

    do {
        status = CYFISNP_NODE_GetTransmitState();
    } while( (status & (CYFISNP_NODE_TXC_IRQ | CYFISNP_NODE_TXE_IRQ))==0x00 );    // until one or both of TXC/TXE is set

    CYFISNP_NODE_EndTransmit();
}

// start the reception of a packet
// the location and length of the packet buffer to receive the data into must have
// previously been set with a call to RadioSetPtr() and  RadioSetLength()
// after starting the reception of a packet with this call, the state of the receive operation
// should be checked by calling RadioGetReceiveState()
// when RadioGetReceiveState() indicates that the transmission has completed a call
// should be made to RadioEndReceive()
// receive is started by setting "RX_GO" bit; All interested interrupt enables are set and
// RadioGetReceiveState() can be called in a polling loop in systems that do not use interrupts,
// or can be called directly in an interrupt handler
// after calling RadioStartReceive(), NO CALLS can be made to the configuration access routines
// until receive operation is terminated with a call to RadioEndReceive() or RadioAbort()
// until a call is made to end the receive operation, the only other calls supported are
// RadioGetReceiveState() and RadioGetRssi()
void CYFISNP_NODE_StartReceive(void)
{
    byte im;
    byte endState;

    CYFISNP_NODE_WipPtr = CYFISNP_NODE_Ptr;         // Working pointer = SPI access routines - address
    CYFISNP_NODE_WipLen = CYFISNP_NODE_Len;         // Working buffer length = SPI access routines - length

    CYFISNP_NODE_State = CYFISNP_NODE_RX;          // 0x80
    CYFISNP_NODE_BytesRead = 0;                    //

    // if radio's end-state is Sleep, need:
    //  1) workaround to startup oscilator
    //  2) workaround to keep awake briefly at the end of receive

    im = CYFISNP_NODE_SaveAndClearGIE();
    CYFISNP_NODE_Wakeup();
    CYFISNP_NODE_RestoreGIE(im);

    // if Radio is in Sleep, make it Idle
    if( CYFISNP_NODE_IsSleepDesired() ) {
        // Radio End state is IDLE so it has time to clean up RXE/RXC
        // however, don't change the value of CYFISNP_NODE_XactConfig
        endState = CYFISNP_NODE_XactConfig;             // don't forcs end, just let go IDLE
        CYFISNP_NODE_RestingState = endState;

        endState &= (byte)~CYFISNP_NODE_END_STATE_MSK;                          // 0x1C
        endState |= CYFISNP_NODE_END_STATE_IDLE;
        CYFISNP_NODE_Write(CYFISNP_NODE_XACT_CFG_ADR, endState);
    }

    // keep Rx clock running at RXE/RXC
    CYFISNP_NODE_Write(CYFISNP_NODE_CLK_OVERRIDE_ADR, CYFISNP_NODE_RXF);

    CYFISNP_NODE_Write(CYFISNP_NODE_RX_CTRL_ADR, (CYFISNP_NODE_RX_GO | CYFISNP_NODE_RXB8_IRQ | CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXE_IRQ));   // [0x05]=0x83B
    // enable RXE, enable RXC, RXB8 and start Rx

    // reading RX_IRQ_STATUS_ADR here ensures RX IRQ deasserts
    (void)CYFISNP_NODE_Read(CYFISNP_NODE_RX_IRQ_STATUS_ADR);                     // [0x07]
}

// returns the state of the current receive operation
// this call should be made after RadioStartReceive()
// although  bits in the state register in the hardware clear
// automatically, we make them sticky until RadioEndReceive()
byte CYFISNP_NODE_GetReceiveState(void)
{
    byte im;
    byte status;

    im = CYFISNP_NODE_SaveAndClearGIE();

    if( CYFISNP_NODE_IsRadioIrq() ) {    // if LpIrq pin is high
        // IRQ indicates RX_GO is finished,
        // so we have RXC_IRQ or RXE_IRQ or both
        status = CYFISNP_NODE_GetReceiveStateIsr();
    } else
        status = CYFISNP_NODE_State;

    CYFISNP_NODE_RestoreGIE(im);
    return status;
}



//
#if 0
byte CYFISNP_NODE_GetReceiveStateIsr(void)
{
    byte status;

    while( CYFISNP_NODE_IsRadioIrq() ) {    // if LpIrq pin is still high
        // IRQ indicates RX_GO is finished,
        // so we have RXC_IRQ or RXE_IRQ or both
        status = CYFISNP_NODE_ReadStatusDebouncedRaw(CYFISNP_NODE_RX_IRQ_STATUS_ADR);   // [0x07]
        CYFISNP_NODE_State |= status;

        // RXBERR and RXE imply RXC, so any of these 3 flags imply there's
        // no rush to unload RxFIFO
        status &= (CYFISNP_NODE_RXBERR_IRQ | CYFISNP_NODE_RXE_IRQ | CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXB8_IRQ);

        if( status & CYFISNP_NODE_RXB8_IRQ ) {
            // 8 or more bytes are in the RxFIFO
            // read 8 bytes on the fly
            CYFISNP_NODE_FileReadWipRaw(CYFISNP_NODE_RX_BUFFER_ADR, CYFISNP_NODE_LP_FIFO_HALF);
            CYFISNP_NODE_BytesRead += CYFISNP_NODE_LP_FIFO_HALF;

            // set RX IRQ mask to only RXB8 to simplify checking
            // when subsequent data can be moved after the first burst
            CYFISNP_NODE_WriteRaw(CYFISNP_NODE_RX_CTRL_ADR, CYFISNP_NODE_RXB8_IRQ);

            while( CYFISNP_NODE_IsRadioIrq() ) {
                CYFISNP_NODE_FileReadWipRaw(CYFISNP_NODE_RX_BUFFER_ADR, CYFISNP_NODE_LP_FIFO_HALF);
                CYFISNP_NODE_BytesRead += CYFISNP_NODE_LP_FIFO_HALF;
            }
            CYFISNP_NODE_WriteRaw(CYFISNP_NODE_RX_CTRL_ADR, (CYFISNP_NODE_RXB8_IRQ | CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXE_IRQ));

            continue;
        } else {
            // any errors?
            if( CYFISNP_NODE_State & (CYFISNP_NODE_RXBERR_IRQ | CYFISNP_NODE_RXE_IRQ) ) {
                // yes, then set RXC/RXE
                CYFISNP_NODE_State |= (CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXE_IRQ);  // because Radio is done
            }
            break;
        }
    }

    return CYFISNP_NODE_State;
}
#else
byte CYFISNP_NODE_GetReceiveStateIsr(void)
{
    byte status;

    while( CYFISNP_NODE_IsRadioIrq() ) {    // if LpIrq pin is still high

        // IRQ indicates RX_GO is finished,
        // so we have RXC_IRQ or RXE_IRQ or both
        status = CYFISNP_NODE_ReadStatusDebouncedRaw(CYFISNP_NODE_RX_IRQ_STATUS_ADR);   // [0x07]
        CYFISNP_NODE_State |= status;

        // RXBERR and RXE imply RXC, so any of these 3 flags imply there's
        // no rush to unload RxFIFO
        status &= (CYFISNP_NODE_RXBERR_IRQ | CYFISNP_NODE_RXE_IRQ | CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXB8_IRQ);
        if(!( status ^ CYFISNP_NODE_RXB8_IRQ )) {
            // 8 or more bytes are in the RxFIFO
            // read 8 bytes on the fly
            CYFISNP_NODE_FileReadWipRaw(CYFISNP_NODE_RX_BUFFER_ADR, CYFISNP_NODE_LP_FIFO_HALF);
            CYFISNP_NODE_BytesRead += CYFISNP_NODE_LP_FIFO_HALF;

            // set RX IRQ mask to only RXB8 to simplify checking
            // when subsequent data can be moved after the first burst
            CYFISNP_NODE_WriteRaw(CYFISNP_NODE_RX_CTRL_ADR, CYFISNP_NODE_RXB8_IRQ);

            while( CYFISNP_NODE_IsRadioIrq() ) {
                CYFISNP_NODE_FileReadWipRaw(CYFISNP_NODE_RX_BUFFER_ADR, CYFISNP_NODE_LP_FIFO_HALF);
                CYFISNP_NODE_BytesRead += CYFISNP_NODE_LP_FIFO_HALF;
            }
            CYFISNP_NODE_WriteRaw(CYFISNP_NODE_RX_CTRL_ADR, (CYFISNP_NODE_RXB8_IRQ | CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXE_IRQ));

            continue;
        } else 
        {
            // any errors?
            if( CYFISNP_NODE_State & (CYFISNP_NODE_RXBERR_IRQ | CYFISNP_NODE_RXE_IRQ) ) {
                // yes, then set RXC/RXE
                CYFISNP_NODE_State |= (CYFISNP_NODE_RXC_IRQ | CYFISNP_NODE_RXE_IRQ);  // because Radio is done
            } 
            break;
        }
    }

    return CYFISNP_NODE_State;
}
#endif


// Complete a receive operation
byte CYFISNP_NODE_EndReceive(void)
{
    byte rxCfg;
    byte BytesToRead;

    // read current VLD_EN setting
    rxCfg = CYFISNP_NODE_Read(CYFISNP_NODE_RX_CFG_ADR);                          // [0x06]

    // total number of bytes in Rx FIFO
    CYFISNP_NODE_RxCount = CYFISNP_NODE_Read(CYFISNP_NODE_RX_COUNT_ADR);         // [0x09]
    if( rxCfg & CYFISNP_NODE_VLD_EN )
        CYFISNP_NODE_RxCount *= 2;      // 2x, every data byte has a valid byte

    BytesToRead = CYFISNP_NODE_RxCount - CYFISNP_NODE_BytesRead;   // subtract # bytes we've extracted
    if( CYFISNP_NODE_State & CYFISNP_NODE_RXE_IRQ )  // gets updated with RadioGetReceiveState()
        BytesToRead = CYFISNP_NODE_LP_FIFO_SIZE;   // if error, read all of Rx Buffer, regardless of RX_COUNT_ADR

    CYFISNP_NODE_FileReadWip(CYFISNP_NODE_RX_BUFFER_ADR, BytesToRead);           // [0x21], file register

    // if user wanted END_STATE_SLEEP, then undo
    // RadioStartReceive()'s intermediate END_STATE_IDLE override
    if( CYFISNP_NODE_IsSleepDesired() ) 
    {
        CYFISNP_NODE_GoToEndState();       // workaround if user's end state is Sleep
    }

    // is awake
    return CYFISNP_NODE_RxCleanup();
}

//
void CYFISNP_NODE_GoToEndState(void)
{
    byte xact;

    CYFISNP_NODE_Write(CYFISNP_NODE_XACT_CFG_ADR, CYFISNP_NODE_XactConfig | CYFISNP_NODE_FRC_END_STATE);  // [0x0F] = RadioXactConfig|0x20
    do {
        xact = CYFISNP_NODE_Read(CYFISNP_NODE_XACT_CFG_ADR);                     // [0x0F]
    } while( (xact & CYFISNP_NODE_FRC_END_STATE)!=0x00 );    // 0x20
    // loop until END_STATE is set
}

//
byte CYFISNP_NODE_RxCleanup(void)
{
    CYFISNP_NODE_Write(CYFISNP_NODE_CLK_OVERRIDE_ADR, 0);    // stop forcing the RXF clock
    CYFISNP_NODE_State = CYFISNP_NODE_IDLE;    // 0x00
    return CYFISNP_NODE_RxCount;
}

//
byte CYFISNP_NODE_Abort(void)
{
    byte i;
    byte status;
    byte retValue;

    if( CYFISNP_NODE_State & CYFISNP_NODE_RX ) {
        // don't issue Force End if receive has started
        CYFISNP_NODE_Write(CYFISNP_NODE_RX_ABORT_ADR, CYFISNP_NODE_ABORT_EN);             // [0x29]=0x20
        // delay to see if SOP arrives
        for(i=0; i<(98*BUSCLK_KHZ)/1000/13; i++)
            asm("nop;");
        // [1]             NOP
        // [4]             INC   ,X
        // [3]             LDA   ,X
        // [2]             CMP   #92
        // [3]             BCS   for
        // 13 cycles * 92 / 24 MHz = 49.8 uSec (~50uSec)

        status = CYFISNP_NODE_ReadStatusDebounced(CYFISNP_NODE_RX_IRQ_STATUS_ADR);   // [0x07]
        CYFISNP_NODE_State |= status;

        // check rx started?
        if( (CYFISNP_NODE_State & CYFISNP_NODE_SOPDET_IRQ)==0x00 ) {
            // no
            CYFISNP_NODE_GoToEndState();
            (void)CYFISNP_NODE_RxCleanup();
            retValue = CYFISNP_NODE_ABORT_SUCCESS;   // 0xFF
        } else {
            // yes, we are receiving a packet, allow to finish naturally
            // probably has errors (RXE) because we activated Digital Loopback,
            // but if Loopback activated during AutoAck, we'll have a valid packet
            // (which was AutoAcked, so we'd better not discard it)
            do{
                status = CYFISNP_NODE_GetReceiveState();
            } while( (status & (CYFISNP_NODE_RXE_IRQ | CYFISNP_NODE_RXC_IRQ))==0x00 );
                        
          //CYFISNP_NODE_State |= status;                                  
            retValue = CYFISNP_NODE_EndReceive();                         // This call clears all flags in "CYFISNP_NODE_State", 2014.04.04, so can't still use CYFISNP_NODE_State
          //if( (CYFISNP_NODE_State & CYFISNP_NODE_RXE_IRQ)==0x00 ) 
            if( (status & CYFISNP_NODE_RXE_IRQ)==0x00 )                   // Added Jason Chen, 2014.04.04
            {
                ;
                // retValue has the number of bytes received
            } else {
                // rx errors happened
                retValue = CYFISNP_NODE_ABORT_SUCCESS;   // 0xFF
            }
        }
    } else
        retValue = CYFISNP_NODE_ABORT_SUCCESS;   // 0xFF

    CYFISNP_NODE_Write(CYFISNP_NODE_RX_ABORT_ADR, 0);                    // [0x29]=0x00
    return retValue;
}

//
byte CYFISNP_NODE_GetReceiveStatus(void)
{
    return CYFISNP_NODE_Read(CYFISNP_NODE_RX_STATUS_ADR);                        // [0x08]
}

//
void CYFISNP_NODE_ForceState(byte endState)
{
    byte im;

    im = CYFISNP_NODE_SaveAndClearGIE();

    CYFISNP_NODE_Wakeup();
    CYFISNP_NODE_ChangeState(endState);

    endState = CYFISNP_NODE_RestingState;
    CYFISNP_NODE_XactConfig &= (byte)~CYFISNP_NODE_END_STATE_MSK;               // 0x1C
    CYFISNP_NODE_XactConfig |= endState;

    CYFISNP_NODE_RestoreGIE(im);
}

//
void CYFISNP_NODE_WakeupForXact(void)
{
    CYFISNP_NODE_Wakeup();
    CYFISNP_NODE_RestoreXactConfig();
}

//
void CYFISNP_NODE_RestoreXactConfig(void)
{
    byte endState;

    endState = CYFISNP_NODE_XactConfig & (byte)~CYFISNP_NODE_FRC_END_STATE;     // 0x20
    CYFISNP_NODE_RestingState = endState;

    CYFISNP_NODE_WriteXactConfigRegUnsafe(endState);
}

//
void CYFISNP_NODE_Wakeup(void)
{
    // if radio is not sleep, then just return (no wakeup needed)
    if( !CYFISNP_NODE_IsRadioSleep() )
        return;             // Radio already awake

    // radio is sleep, wake it up
    CYFISNP_NODE_ChangeState(CYFISNP_NODE_END_STATE_IDLE);
}

//
void CYFISNP_NODE_ChangeState(byte endState)
{
    int j;
    byte state;

    CYFISNP_NODE_WriteForcedState(endState);
    // with 2MHz SPI bus, this loop takes about 2 mSec (250 * 8 uSec)
    for(j=0; j<WAIT_FORCE; j++) {
        __RESET_WATCHDOG(); // feeds the dog

        state = CYFISNP_NODE_ReadRaw(CYFISNP_NODE_XACT_CFG_ADR);                // [0x0F]
        state &= CYFISNP_NODE_FRC_END_STATE;         // 0x20
        if( state == 0x00 ) // end state has reached?
            return;     // yes
    }
    CYFISNP_NODE_WriteForcedState(CYFISNP_NODE_END_STATE_SLEEP);
    CYFISNP_NODE_WriteForcedState(CYFISNP_NODE_END_STATE_IDLE);
}

//
void CYFISNP_NODE_WriteForcedState(byte endState)
{
    endState &= CYFISNP_NODE_END_STATE_MSK;                                     // 0x1C
    CYFISNP_NODE_RestingState = endState;

    endState = CYFISNP_NODE_XactConfig & (byte)~CYFISNP_NODE_END_STATE_MSK;     // 0x1C
    endState |= CYFISNP_NODE_RestingState;
    endState |= CYFISNP_NODE_FRC_END_STATE;                                     // 0x20
    CYFISNP_NODE_WriteXactConfigRegUnsafe(endState);
}

//
void CYFISNP_NODE_WriteXactConfigRegUnsafe(byte endState)
{
    CYFISNP_NODE_WriteRaw(CYFISNP_NODE_XACT_CFG_ADR, endState);
}

//
byte CYFISNP_NODE_GetRssi(void)
{
    return CYFISNP_NODE_Read(CYFISNP_NODE_RSSI_ADR);                             // [0x13]
}
