//
// File:        CYFISNP_NODE_Protocol.h
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

#ifndef CYFISNP_NODE_PROTOCOL_H
#define CYFISNP_NODE_PROTOCOL_H

//#include <m8c.h>
#include "CYFISNP_NODE_Config.h"

//#if (CYFISNP_NODE_PSOC_EXPRESS_PROJECT == 0)
//  #pragma fastcall16 CYFISNP_NODE_SleepTimer_Set64Hz
//  void CYFISNP_NODE_SleepTimer_Set64Hz(void);
//#endif

// ---------------------------------------------------------------------------
//
// ?_eProtState - Overall Protocol State
//
// ---------------------------------------------------------------------------
typedef enum {
    CYFISNP_NODE_BIND_MODE           = 0x10,  // Binding (not Data Mode)
    CYFISNP_NODE_UNBOUND_MODE        = 0x11,  // (Node only)
    CYFISNP_NODE_CON_MODE            = 0x20,  // (Node only)
    CYFISNP_NODE_CON_MODE_TIMEOUT    = 0x21,  // (Node only)
    CYFISNP_NODE_PING_MODE           = 0x30,  // Lost contact with Hub
    CYFISNP_NODE_PING_MODE_TIMEOUT   = 0x31,  // (Node only)
    CYFISNP_NODE_DATA_MODE           = 0x40,  // Last packet was AutoACKed
    CYFISNP_NODE_STOP_MODE           = 0x50,    
} CYFISNP_NODE_PROT_STATE;
extern CYFISNP_NODE_PROT_STATE CYFISNP_NODE_eProtState;



// ---------------------------------------------------------------------------
//
// ?_API_PKT - Packet structure between Protocol and API
//
// ---------------------------------------------------------------------------
#define   CYFISNP_NODE_API_TYPE_CONF         0x10  // Deliv confirm, no BCDR
#define   CYFISNP_NODE_API_TYPE_CONF_BCDR    0x20  // Deliv confirm, BCDR
#define   CYFISNP_NODE_API_TYPE_SYNC         0x30  // Best effort, no BCDR
#define   CYFISNP_NODE_API_TYPE_SYNC_BCDR    0x40  // Best effort, BCDR

typedef struct {                        //
    BYTE length;                        // Payload length
    BYTE rssi;                          // RSSI of packet
    BYTE type;                          // Packet type
    BYTE devId;                         // [7:0] = Device ID
    BYTE payload[CYFISNP_NODE_FCD_PAYLOAD_MAX/*14*/];
} CYFISNP_NODE_API_PKT;



// ---------------------------------------------------------------------------
//
// ?_CurrentChannel - API may view current channel for informative purposes only
//
// ---------------------------------------------------------------------------
extern BYTE CYFISNP_NODE_bCurrentChannel;


// ---------------------------------------------------------------------------
//
// Public Functions
//
// ---------------------------------------------------------------------------
BYTE            CYFISNP_NODE_Start           (void);
void            CYFISNP_NODE_Run             (void);
//#pragma fastcall16 CYFISNP_NODE_Radio_Start
//#pragma fastcall16 CYFISNP_NODE_Radio_Start
BYTE            CYFISNP_NODE_Radio_Start     (void);
void            CYFISNP_NODE_Radio_Stop      (void);
void            CYFISNP_NODE_Stop            (void);
void            CYFISNP_NODE_Jog             (void);
void            CYFISNP_NODE_BindStart       (void/*BYTE reqDevId*/);

BOOL            CYFISNP_NODE_TxDataPend      (void);
BOOL            CYFISNP_NODE_TxDataPut       (CYFISNP_NODE_API_PKT *ps);

BOOL            CYFISNP_NODE_RxDataPend      (void);
CYFISNP_NODE_API_PKT *   CYFISNP_NODE_RxDataGet       (void);
void            CYFISNP_NODE_RxDataRelease   (void);

void            CYFISNP_NODE_Unbind          (void);
BYTE            CYFISNP_NODE_LookupDevId     (void);
//BYTE            CYFISNP_NODE_GetDieTemp      (void);

//BYTE            CYFISNP_NODE_GetQualGfsk     (void);     // Debug access for API
//BYTE            CYFISNP_NODE_GetQual8dr      (void);     // Debug access for API


// ---------------------------------------------------------------------------
//
// SPI Power Management (most applications can ignore these API functions)
//
//  The underlying radio CYRF6936 uses a 4-wire SPI interface and (for battery
//   powered applications), SNP tri-states the SPI lines to minimize leakage
//   currents in some cases.  Some of these CYRF3936 MAY be useful to a battery
//   powered application (some GPIOs, a low-voltage detector, etc).
//   The app should call _spiWake() before trying to read from the radio.
// ---------------------------------------------------------------------------
//#if (CYFISNP_NODE_PWR_TYPE == CYFISNP_NODE_PWR_WALL) // SPI always ON
//    #define CYFISNP_NODE_spiSleep()
//    #define CYFISNP_NODE_spiWake()
//#else                                                 // Sleep SPI when Sleep Radio
    void    CYFISNP_NODE_spiSleep    (void);            // Set SPI to minimize leakage current
    void    CYFISNP_NODE_spiSleepforMCUSleep(void);     // Set SPI to minimize leakage current,    Added by Jason Chen, 2014.04.14
    void    CYFISNP_NODE_spiWake     (void);            // Set SPI to communicate w/Radio
//#endif


// ---------------------------------------------------------------------------
// EEP_NET_REC - Hub or Node EEPROM Network Parameter Record
// ---------------------------------------------------------------------------
typedef struct {                // -------------------------------------------
    byte sopIdx;                //
    byte chBase;                //
    byte chHop;                 //
    byte hubSeedMsb;            // Hub CRC Seed[15:8]
    byte hubSeedLsb;            // Hub CRC Seed[ 7:0]
                                // -------------------------------------------
    byte devId;                 // only used by Nodes
    byte nodeSeedMsb;           // used only by Nodes
    byte nodeSeedLsb;           // used only by Nodes
} CYFISNP_NODE_EEP_NET_REC;              // -------------------------------------------
#define CYFISNP_NODE_EEP_NET_REC_ADR ((CYFISNP_NODE_EEP_NET_REC const *)(CYFISNP_NODE_EEP_PHY_ADR))

#endif  // CYFISNP_NODE_PROTOCOL_H
// ###########################################################################
