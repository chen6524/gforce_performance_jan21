//
// File:        CYFISNP_NODE.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes
#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
#include "CYFISNP_NODE.h"

#include "bcd_common.h"                           // Added by Jason Chen, 2016.01.04
////////////////////////
// definitions

////////////////////////
// internal vars

////////////////////////
// external vars

//////////////////////////////////////////////////////////////////////////////////
//
byte getHardIDMajor(void);
byte getHardIDMinor(void);
byte CYFISNP_NODE_IsRadioIrq(void)
{
    #if (PCB_TYPE==PCB_DEMOJM)
        return  (PTBD_PTBD4==1); // PTB4, active high
    #elif (PCB_TYPE==PCB_gFT)
       if(getHardIDMajor() == 0x1)
         return (PTGD_PTGD0==0x1); // PTG0, active high)   // PTE0=0 (with Ext Flash CS disable first)
       else if (getHardIDMajor() == 0x4)
	   	   return (PTBD_PTBD5 == 0x1);
       else if (getHardIDMajor() > 0x1)
         return (PTCD_PTCD3 == 0x1);
       else return 0;
    #endif  // PCB_TYPE
    

}

//
//void CYFISNP_NODE_EnableInt(void)
//{
//}

//
//void CYFISNP_NODE_DisableInt(void)
//{
//}

// initialize the Radio
byte CYFISNP_NODE_Init(byte xactConfig, byte txConfig)
{
    byte spiValue;

    CYFISNP_NODE_XactConfig = CYFISNP_NODE_XACT_CFG_RST;        // 0x80
    CYFISNP_NODE_RestingState = CYFISNP_NODE_XACT_CFG_RST;      // 0x80
    if( CYFISNP_NODE_Reset() == FALSE )                 // [0x1D]=0x01
        return FALSE;

    // we're going to want to force the RXF clock on in the streaming driver
    // not necessary for nonstreaming driver but harmless
    CYFISNP_NODE_Write(CYFISNP_NODE_CLK_EN_ADR, CYFISNP_NODE_RXF);  // [0x28]=0x02

    // set up to use auto-cal for VCO. Register 0x34 is left at its default value
    // to allow auto-cal to be used
    CYFISNP_NODE_Write(CYFISNP_NODE_AUTO_CAL_TIME_ADR,   CYFISNP_NODE_AUTO_CAL_TIME_MAX);         // [0x32]=0x3C
    CYFISNP_NODE_Write(CYFISNP_NODE_AUTO_CAL_OFFSET_ADR, CYFISNP_NODE_AUTO_CAL_OFFSET_MINUS_4);   // [0x35]=0x14, AutoCal offset top -4

    spiValue = CYFISNP_NODE_Read(CYFISNP_NODE_IO_CFG_ADR);  // [0x0D], read the default value
    spiValue |= CYFISNP_NODE_IRQ_POL;           // 0x40, set the IRQ polarity to positive (active high)
    CYFISNP_NODE_Write(CYFISNP_NODE_IO_CFG_ADR, spiValue);  // [0x0D]=0x40
    // (some have asked that this be left at the default negative, active low)

    // set the XTAL Startup delay to 150uSec to handle warm restarts of the XTAL
    CYFISNP_NODE_Write(CYFISNP_NODE_XTAL_CFG_ADR, 0x08);    // [0x26]=0x08

    // enable HiLo for quick-turn-around. Use low side injection for receive - this
    // should result in non-inverted data, so no need to hit the invert bit
    // turn off AGC and force the LNA on        // [0x06]=0x48

#if OUT_OFF_RANGE_TEST    
    // for test of out of range, added by Jason Chen, 2016.01.04
    CYFISNP_NODE_Write(CYFISNP_NODE_RX_CFG_ADR, ((CYFISNP_NODE_RX_CFG_RST | CYFISNP_NODE_FASTTURN_EN|CYFISNP_NODE_ATT_EN) & (byte)~(CYFISNP_NODE_HI | CYFISNP_NODE_RXOW_EN | CYFISNP_NODE_AUTO_AGC_EN | CYFISNP_NODE_LNA_EN)));
#else
    CYFISNP_NODE_Write(CYFISNP_NODE_RX_CFG_ADR, ((CYFISNP_NODE_RX_CFG_RST | CYFISNP_NODE_FASTTURN_EN|CYFISNP_NODE_LNA_EN) & (byte)~(CYFISNP_NODE_HI | CYFISNP_NODE_RXOW_EN | CYFISNP_NODE_AUTO_AGC_EN)));
#endif

    // set the TX Offset to +1MHz
    // THIS MEANS THE ACTUAL TRANSMIT CARRIER WILL BE 1MHz ABOVE THE PLL
    // FREQUENCY PROGRAMMED IN THE CHANNEL OR A & N REGISTERS
    // RadioSetFrequency COMPENSATES FOR THIS OFFSET
    CYFISNP_NODE_Write(CYFISNP_NODE_TX_OFFSET_LSB_ADR, 0x55);        // [0x1B]=0x55
    CYFISNP_NODE_Write(CYFISNP_NODE_TX_OFFSET_MSB_ADR, 0x05);        // [0x1C]=0x05

    // set ALL_SLOW bit when operation in GFSK mode
#if CYFISNP_NODE_DEF_DATA_RATE  // 0x08:32_8DR
#else
    // if FGSK, 1Mbps
    CYFISNP_NODE_Write(CYFISNP_NODE_ANALOG_CTRL_ADR, CYFISNP_NODE_ALLSLOW);
#endif

    // set the radio transaction and TX configuration
    CYFISNP_NODE_SetXactConfig(xactConfig);             // [0x0F]=xactConfig&0xDF (bit5 is FRC_END bit)
    CYFISNP_NODE_SetTxConfig(txConfig);                 // [0x03]=txConfig

    // set to idle
    CYFISNP_NODE_State = CYFISNP_NODE_IDLE;             // 0x00

    return TRUE;    // 1
}

// set the Channel at (frequenct+1MHz)
void CYFISNP_NODE_SetChannel(byte channel)
{
    CYFISNP_NODE_SetFrequency(channel+2);               // [0x00]=channel+1
}

// set the Frequency on which the communication will occur
// THIS DRIVER SETS TX OFFESET AT 1 WHICH MEANS THAT THE TX
// CARRIER WILL BE 1MHz HIGHER THAN CHANNEL NUMBER WE PROGRAM
// THIS FUNCTION COMPENSATES FOR THAT OFFSET BY DECREMENTING
// THE CHANNEL NUMBER
void CYFISNP_NODE_SetFrequency(byte frequency)
{
    if( frequency )
        frequency -= 1;

    CYFISNP_NODE_Write(CYFISNP_NODE_CHANNEL_ADR, frequency);         // [0x00]=frequency-1
}

// get the Channel
byte CYFISNP_NODE_GetChannel(void)
{
    byte channel;

    channel = CYFISNP_NODE_GetFrequency();              // [0x00]-2
    return (byte)(channel-2);
}

// return the Frequency in MHz above 2.4GHz
byte CYFISNP_NODE_GetFrequency(void)
{
    byte frequency;

    frequency = CYFISNP_NODE_Read(CYFISNP_NODE_CHANNEL_ADR);         // [0x00]-1
    return (byte)(frequency+1);
}

// set the Transmitter configuration
void CYFISNP_NODE_SetTxConfig(byte config)
{
    CYFISNP_NODE_Write(CYFISNP_NODE_TX_CFG_ADR, config);             // [0x03]=config
}

// return the Transmitter configuration
byte CYFISNP_NODE_GetTxConfig(void)
{
    return CYFISNP_NODE_Read(CYFISNP_NODE_TX_CFG_ADR);               // [0x03]
}

// set the Transaction configuration
// ACK_EN   ---   FRC_END_STATE   END_STATE2   END_STATE1   END_STATE0   ACK_TO1   ACK_TO0
void CYFISNP_NODE_SetXactConfig(byte config)
{
    config &= (byte)~CYFISNP_NODE_FRC_END_STATE;    // ~0x20=0xDF, user shouldn't set FRC_END bit
    CYFISNP_NODE_XactConfig = config;
    CYFISNP_NODE_Write(CYFISNP_NODE_XACT_CFG_ADR, config);           // [0x0F]=config&0xDF
}

// return the Transaction configuration
byte CYFISNP_NODE_GetXactConfig(void)
{
    return CYFISNP_NODE_Read(CYFISNP_NODE_XACT_CFG_ADR);             // [0x0F]
}

// set the Framing configuration
void CYFISNP_NODE_SetFrameConfig(byte config)
{
    CYFISNP_NODE_Write(CYFISNP_NODE_FRAMING_CFG_ADR, config);        // [0x10]=config
}

// get the Framing configuration
byte CYFISNP_NODE_GetFramingConfig(void)
{
    return CYFISNP_NODE_Read(CYFISNP_NODE_FRAMING_CFG_ADR);          // [0x10]
}

// set the Threshold for the 32 chip data modes
void CYFISNP_NODE_SetThreshold32(byte threshold)
{
    CYFISNP_NODE_Write(CYFISNP_NODE_DATA32_THOLD_ADR, threshold);    // [0x11]=threshold
}

// get the Threshold for the 32 chip data modes
byte CYFISNP_NODE_GetThreshold32(void)
{
    return CYFISNP_NODE_Read(CYFISNP_NODE_DATA32_THOLD_ADR);         // [0x11]
}

// set the Threshold for the 64 chip data modes
void CYFISNP_NODE_SetThreshold64(byte threshold)
{
    CYFISNP_NODE_Write(CYFISNP_NODE_DATA64_THOLD_ADR, threshold);    // [0x12]=threshold
}

// get the Threshold for the 64 chip data modes
byte CYFISNP_NODE_GetThreshold64(void)
{
    return CYFISNP_NODE_Read(CYFISNP_NODE_DATA64_THOLD_ADR);         // [0x12]
}

// set the Preamble repition count
void CYFISNP_NODE_SetPreambleCount(byte count)
{
    CYFISNP_NODE_Write(CYFISNP_NODE_PREAMBLE_ADR, count);            // [0x24]=count, file register

    // 2 more dummy reads
    CYFISNP_NODE_WipPtr = &CYFISNP_NODE_Temp[0];        // LSB,MSB
    CYFISNP_NODE_WipLen = 2;                            // not needed
    CYFISNP_NODE_FileReadWip(CYFISNP_NODE_PREAMBLE_ADR, 2);          // [0x24], file register
}

// get the Preamble repition count
byte CYFISNP_NODE_GetPreambleCount(void)
{
    CYFISNP_NODE_WipPtr = &CYFISNP_NODE_Temp[0];        // count,LSB,MSB
    CYFISNP_NODE_WipLen = 3;                            //
    CYFISNP_NODE_FileReadWip(CYFISNP_NODE_PREAMBLE_ADR, 3);          // [0x24], file register
    return CYFISNP_NODE_Temp[0];                        // the count is in offset 0
}

// set the Preamble pattern
void CYFISNP_NODE_SetPreamblePattern(word pattern)
{
    CYFISNP_NODE_Temp[0] = (pattern>>0)&0xFF;   // LSB
    CYFISNP_NODE_Temp[1] = (pattern>>8)&0xFF;   // MSB
    // one dummy read
    (void)CYFISNP_NODE_Read(CYFISNP_NODE_PREAMBLE_ADR);              // [0x24], file register

    CYFISNP_NODE_WipPtr = &CYFISNP_NODE_Temp[0];        // LSB,MSB
  //CYFISNP_NODE_WipLen = 2;                            // not needed
    CYFISNP_NODE_FileWriteWip(CYFISNP_NODE_PREAMBLE_ADR, 2);         // [0x24], file register
}

// get the Preamble pattern
word CYFISNP_NODE_GetPreamblePattern(void)
{
    CYFISNP_NODE_WipPtr = &CYFISNP_NODE_Temp[0];        // count,LSB,MSB
    CYFISNP_NODE_WipLen = 3;                            //
    CYFISNP_NODE_FileReadWip(CYFISNP_NODE_PREAMBLE_ADR, 3);          // [0x24], file register
    return (word)((CYFISNP_NODE_Temp[1]<<0)|(CYFISNP_NODE_Temp[2]<<8)); // the pattern is in offset 1 and 2
}

// set CRC Seed for both Tx and Rx
void CYFISNP_NODE_SetCrcSeed(word crcSeed)
{
    CYFISNP_NODE_Temp[0] = (crcSeed>>0)&0xFF;   // LSB
    CYFISNP_NODE_Temp[1] = (crcSeed>>8)&0xFF;   // MSB

    CYFISNP_NODE_WipPtr = &CYFISNP_NODE_Temp[0];        // LSB,MSB
  //CYFISNP_NODE_WipLen = 2;                            // not needed
    CYFISNP_NODE_BurstWriteWip(CYFISNP_NODE_CRC_SEED_LSB_ADR, 2);    // [0x15] & [0x16]
}

// get CRC Seed for both Tx and Rx
word CYFISNP_NODE_GetCrcSeed(void)
{
    CYFISNP_NODE_WipPtr = &CYFISNP_NODE_Temp[0];        // LSB,MSB
    CYFISNP_NODE_WipLen = 2;                            //
    CYFISNP_NODE_BurstReadWip(CYFISNP_NODE_CRC_SEED_LSB_ADR, 2);     // [0x15] & [0x16]
    return (word)((CYFISNP_NODE_Temp[0]<<0)|(CYFISNP_NODE_Temp[1]<<8)); // the pattern is on offset 0 and 1
}

// get 6 bytes of Fuse data specified by RadioSetPtr() function
void CYFISNP_NODE_GetFuses(void)
{
    byte i;

    // turn on Fuse read bias
    CYFISNP_NODE_Write(CYFISNP_NODE_MFG_ID_ADR, 0xFF);               // [0x25], file register

    CYFISNP_NODE_FileRead(CYFISNP_NODE_MFG_ID_ADR, CYFISNP_NODE_Len); // LSB to MSB (len must be 6)
    for(i=0; i<CYFISNP_NODE_Len; i++)   // len must be 6
        CYFISNP_NODE_Ptr[i] ^= 0xFF;

    // turn off Fuse read bias
    CYFISNP_NODE_Write(CYFISNP_NODE_MFG_ID_ADR, 0x00);               // [0x25], file register
}

// set SOP PN code
void CYFISNP_NODE_SetConstSopPnCode(const byte *pPattern)
{
    byte i;

    for(i=0; i<8; i++)
        CYFISNP_NODE_Write(CYFISNP_NODE_SOP_CODE_ADR, *pPattern++);  // [0x22], file register
}

// set DATA PN code
void CYFISNP_NODE_SetConstDataPnCode(const byte *pPattern)
{
    byte i;

    for(i=0; i<16; i++)
        CYFISNP_NODE_Write(CYFISNP_NODE_DATA_CODE_ADR, *pPattern++); // [0x23], file register
}

// set the SOP PN code from internal data base
void CYFISNP_NODE_SetSopPnCode(byte index)
{
    //These are the 20 Multiplicative codes documented in RWW-87. The first 10 are
    // the set recommended as the best at the conclusion of that memo.
    // Bit ordering for these codes:
    //  The LSB of the first byte is the first bit transceived
    //  The MSB of the first byte is the eighth bit transceived
    //  The MSB of the eighth byte is the last bit transceived
    static byte const CYFISNP_NODE_PnCodeTable[][8] = {
        0x3C, 0x37, 0xCC, 0x91, 0xE2, 0xF8, 0xCC, 0x91, // PN Code 0
        0x9B, 0xC5, 0xA1, 0x0F, 0xAD, 0x39, 0xA2, 0x0F, // PN Code 1
        0xEF, 0x64, 0xB0, 0x2A, 0xD2, 0x8F, 0xB1, 0x2A, // PN Code 2
        0x66, 0xCD, 0x7C, 0x50, 0xDD, 0x26, 0x7C, 0x50, // PN Code 3
        0x5C, 0xE1, 0xF6, 0x44, 0xAD, 0x16, 0xF6, 0x44, // PN Code 4
        0x5A, 0xCC, 0xAE, 0x46, 0xB6, 0x31, 0xAE, 0x46, // PN Code 5
        0xA1, 0x78, 0xDC, 0x3C, 0x9E, 0x82, 0xDC, 0x3C, // PN Code 6
        0xB9, 0x8E, 0x19, 0x74, 0x6F, 0x65, 0x18, 0x74, // PN Code 7
        0xDF, 0xB1, 0xC0, 0x49, 0x62, 0xDF, 0xC1, 0x49, // PN Code 8
        0x97, 0xE5, 0x14, 0x72, 0x7F, 0x1A, 0x14, 0x72, // PN Code 9
    //Most systems do not need 20 codes and they take up much space, so we've
    // commented out most of the codes. Uncomment them if you need more.
    //  0x82, 0xC7, 0x90, 0x36, 0x21, 0x03, 0xFF, 0x17, // PN Code 10
    //  0xE2, 0xF8, 0xCC, 0x91, 0x3C, 0x37, 0xCC, 0x91, // PN Code 11
    //  0xAD, 0x39, 0xA2, 0x0F, 0x9B, 0xC5, 0xA1, 0x0F, // PN Code 12
    //  0xD2, 0x8F, 0xB1, 0x2A, 0xEF, 0x64, 0xB0, 0x2A, // PN Code 13
    //  0xDD, 0x26, 0x7C, 0x50, 0x66, 0xCD, 0x7C, 0x50, // PN Code 14
    //  0xAD, 0x16, 0xF6, 0x44, 0x5C, 0xE1, 0xF6, 0x44, // PN Code 15
    //  0xB6, 0x31, 0xAE, 0x46, 0x5A, 0xCC, 0xAE, 0x46, // PN Code 16
    //  0x9E, 0x82, 0xDC, 0x3C, 0xA1, 0x78, 0xDC, 0x3C, // PN Code 17
    //  0x6F, 0x65, 0x18, 0x74, 0xB9, 0x8E, 0x19, 0x74, // PN Code 18
    //  0x62, 0xDF, 0xC1, 0x49, 0xDF, 0xB1, 0xC0, 0x49  // PN Code 19
    };
    byte i;
    const byte *p;

    if( index < (sizeof(CYFISNP_NODE_PnCodeTable)/8) ) {
        p = &CYFISNP_NODE_PnCodeTable[index][0];
        for(i=0; i<8; i++)
            CYFISNP_NODE_Write(CYFISNP_NODE_SOP_CODE_ADR, *p++);  // [0x22], file register
    }
}
