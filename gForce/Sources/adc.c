
#include <MC9S08JM60.h>
#include "mImpact.h"
#include "adc.h"
#include "mcu.h"
#include "gft.h"
#include <stdlib.h>
#include "gLP.h"
#include "max17047.h"
#include "max14676.h"
#include "rtc.h"

/* global variable definition*/
unsigned char HiAdcResult;
unsigned char CovEndFlag;
unsigned char Channel = 0;


#include "bcd_common.h"
#include "LSM330DLC.h"



volatile  byte batData  = 0x7D;
volatile  word batData1 = 0x7D * 10;

/*  GFTW version
*   ADP4   HG_X
*   ADP8   HG_Y
*   ADP9   HG_Z                            // ACC Z for GFT3S
*
*   ADP9   Battery  
*   ADP27  Band Gap 
*/
/**********************************************************************************************
 * ADC_Init: This function initilizes the ADC module
 *
 * Parameters:      none
 *
 * Subfunctions:    none.
 *
 * Return:          void
 *********************************************************************************************/ 
void ADC_Init(void)
{
   byte countADC;//,temp;
   ADCSC1 = 0x0
     //| ADCSC1_COCO_MASK      // conversion complete flag
     //| ADCSC1_AIEN_MASK      // interrupt enable AIEN enables conversion complete interrupts
     //| ADCSC1_ADCO_MASK      // Continuous conversion enable
       | ADCSC1_ADCH4_MASK     // input channel select  11111- module disable 
       | ADCSC1_ADCH3_MASK
       | ADCSC1_ADCH2_MASK
       | ADCSC1_ADCH1_MASK
       | ADCSC1_ADCH0_MASK;
	 
   ADCSC2 = 0x0;  
     //| ADCSC2_ADACT_MASK  // conversion active
     //| ADCSC2_ADTRG_MASK  // conversion trigger select 0- software trigger
     //| ADCSC2_ACFE_MASK   // compare function enable 0 - disabled
     //| ADCSC2_ACFGT_MASK  // compare function greater than enable 0 - triggers < compare value
			 ;
                         
     //ADCCV = 0x80;
     //the sample clock should <4M for low power
   ADCCFG = 0x0
     //| ADCCFG_ADLPC_MASK     // low-power conifguration 1- low power config
     //| ADCCFG_ADIV1_MASK     // input clock divider 1/2/4/8
       | ADCCFG_ADIV0_MASK
     //| ADCCFG_ADLSMP_MASK    // long sample time config 1 long time
     //| ADCCFG_MODE1_MASK     // 00 - 8 bit 10 10 bit
     //| ADCCFG_MODE0_MASK
     //| ADCCFG_ADICLK1_MASK   // input clock select  00- bus 01 - bus /2
     //| ADCCFG_ADICLK0_MASK 
   			;
   	 /*Change the channel, to check the releation between pins and channels */
   APCTL1 = 0x10; //0x30
   APCTL2 = 0x03; 
   	 
   	 
   ADCSC1 = 0x1F;                           // Added by Jason Chen, 2013.11.22
   Cpu_Delay100US(50);   	      
   	 
   ADCSC1 = adcChanGame[0];
   while(!ADCSC1_COCO);// __RESET_WATCHDOG();       // Commented by Jason Chen, 2014.08.05                      
   Cpu_Delay100US(3);   	 
   batData = ADCRL;
 //ADCSC1 = 0x1F;//stop

   ADCSC1 = adcChanGame[1];
   while(!ADCSC1_COCO);// __RESET_WATCHDOG();        // Commented by Jason Chen, 2014.08.05                     
   Cpu_Delay100US(3);   	 
   batData = ADCRL;
 //ADCSC1 = 0x1F;//stop

   ADCSC1 = adcChanGame[2];                          // start batt adc in usb mode                
   while(!ADCSC1_COCO);// __RESET_WATCHDOG();        // Commented by Jason Chen, 2014.08.05                    
   Cpu_Delay100US(3);   	 
   batData = ADCRL;
  
   countADC = 0;
   batData1 = 0;
	 #if 0//ENABLE_BATTERY_NET
     for(;;)//countADC--) 
     {
       __RESET_WATCHDOG();
       ADCSC1 = adcChanGame[3];                // start batt adc in usb mode                
       while(!ADCSC1_COCO) 
       {        
        //__RESET_WATCHDOG();                  // Commented by Jason Chen, 2014.08.05                       
       }
       batData = ADCRL;
       batData1 += batData; 
       Cpu_Delay100US(3);
       ADCSC1 = adcChanGame[2];//stop
       temp = ADCRL;
       Cpu_Delay100US(3);
       //__RESET_WATCHDOG();
       countADC++;
       if(countADC == 10)
         break;              
     }     
   #endif
   ADCSC1 = 0x0
	 	 | ADCSC1_AIEN_MASK  //interrupt enable AIEN enables conversion complete interrupts
	 	 | ADCSC1_ADCH4_MASK //input channel select  11111- module disable 
	 	 | ADCSC1_ADCH3_MASK
	 	 | ADCSC1_ADCH2_MASK
	 	 | ADCSC1_ADCH1_MASK
	 	 | ADCSC1_ADCH0_MASK
	 		 ;       
   lowGEnable = 0;//usrProfile.tR < LOWG_HIGHG_MERGE_THRESHOLD ? 1:0;
}



/****************************************************************************************
 * ADC_Cvt :  start to conversion of one channel
 * input:     channel is the channel number which will do conversion
 *
 * Return : None
 ***************************************************************************************/ 
enum 
{
    ADC_STATE_INIT,
	  ADC_STATE_START,
	  ADC_STATE_POLL,
	  ADC_STATE_DATA,
	  ADC_STATE_LOW,
	  ADC_STATE_HIGH
};

#define ADC_LOW_BAT 3400   //3.3V on battery 0x60
static byte adcState = ADC_STATE_INIT;
volatile byte adcChanIndex =0;
//byte adcChanUsb[2]  = {9,27};
byte adcChanGame[5] = {4,8,9,9,27}; // x, y, z, batt; adp5- z adp8 - Y adp4 x

WORD BatteryVoltage;
WORD BatteryVoltageRaw;

//volatile word batVoltSum = 0; 
//volatile word batVoltSum_count = 0; 

//volatile byte tempByte = 0;
void updateBatStatus(void)
{

	 if(max17047Exist) 
   {	  
       max17047_process(); 
       if(BAT_CHARGED)
     	    gftInfo.batState = BAT_STAT_CHARGED ;
       else if((max17047Voltage >= F_BATT_LEVEL_50PECENT)) 
  	      gftInfo.batState = BAT_STAT_GREEN;
       else if((max17047Voltage >= F_BATT_LEVEL_30PECENT) && (max17047Voltage < F_BATT_LEVEL_50PECENT)) 
  	      gftInfo.batState = BAT_STAT_YELLOW;
       else if((max17047Voltage >= F_BATT_LEVEL_10PECENT)&&(max17047Voltage < F_BATT_LEVEL_30PECENT)) 
  	      gftInfo.batState = BAT_STAT_RED;
       else if(max17047Voltage	<  F_BATT_LEVEL_10PECENT) 
  	      gftInfo.batState = BAT_STAT_LOW;
   } 
   else if (max14676Exist)
   {
       max14676_process();
       max17047Voltage =  max14676Voltage; // for quick fix the wireless
       //force the GFT turn off power due to battery protection could kick in early
	     if(max14676Voltage  <	F_BATT_LEVEL_20PECENT) 
		   	   max14676Charge = 14;
       if(max14676ChargeStat)
     	    gftInfo.batState = BAT_STAT_CHARGED;
#ifdef USE_MAX14676PT		   
		   else if((max14676Charge >= 30)) 
		   {
			  gftInfo.batState = BAT_STAT_GREEN;
			 
		   }
		   else if(max14676Charge >= 20)  
		   {
			  gftInfo.batState = BAT_STAT_YELLOW;
			  
		   }
		   else if(max14676Charge >= 15) 
			  gftInfo.batState = BAT_STAT_RED;
		   else 
		   {
			  gftInfo.batState = BAT_STAT_LOW;
			 
		   }
		   
#else
       else if((max14676Voltage >= F_BATT_LEVEL_50PECENT)) 
  	      gftInfo.batState = BAT_STAT_GREEN;
       else if((max14676Voltage >= F_BATT_LEVEL_30PECENT) && (max14676Voltage < F_BATT_LEVEL_50PECENT)) 
  	      gftInfo.batState = BAT_STAT_YELLOW;
       else if((max14676Voltage >= F_BATT_LEVEL_10PECENT)&&(max14676Voltage < F_BATT_LEVEL_30PECENT)) 
  	      gftInfo.batState = BAT_STAT_RED;
       else if(max14676Voltage	<  F_BATT_LEVEL_10PECENT) 
  	      gftInfo.batState = BAT_STAT_LOW;	          
#endif
	
	   }
	   else 
	   {		
	     if(BAT_CHARGED)
		   	    gftInfo.batState = BAT_STAT_CHARGED ;
		   else if((batData1 >= BATT_LEVEL_50PECENT)) 
			  gftInfo.batState = BAT_STAT_GREEN;
		   else if((batData1 >= BATT_LEVEL_30PECENT) && (batData1 < BATT_LEVEL_50PECENT)) 
			  gftInfo.batState = BAT_STAT_YELLOW;
		   else if((batData1 >= BATT_LEVEL_10PECENT)&&(batData1 <BATT_LEVEL_30PECENT)) 
			  gftInfo.batState = BAT_STAT_RED;
		   else if(batData1  <	BATT_LEVEL_10PECENT) 
			  gftInfo.batState = BAT_STAT_LOW;
	   }
	   //gftInfo.batState =  BAT_STAT_YELLOW; // for testing

}

void adcProcess(void) 
{   
   static byte batStat = BAT_STAT_NORM; 	 
    
   adcState = ADC_STATE_START;
   BatteryVoltageRaw._word = (word)((unsigned long)batData*3541/100-110);//   1200/ LowAdcResult[1] *118 / 43; take bandgap 93
    
   updateBatStatus();
   #if 0
	 if(!gftInfo.lpBusy)  
	 {					 
		 
		 Power_Off_Message.gid[0] = 0x80;			 // New Battery level report					  
		 lpState=LP_STATE_IDLE; 			
		 lpCmdMail = LP_CMD_OFF;   
	 }
   #endif
	
}

/****************************************************************************************
 * ADCISR: The ADC interrupt service routine which reads the ADC data register and places
 *         the value into global variables resulth and resultl
 *
 * Parameters: None
 *
 * Return : None
 ***************************************************************************************/ 
 
enum{
	 
	 HG_ISR_STATE_X,
	 HG_ISR_STATE_Y,
	 HG_ISR_STATE_Z,
	 
};
 
static byte hgIsrState = HG_ISR_STATE_X;
 
volatile GYRO_DATA gyroData; 
volatile GYRO_DATA gyroDataM; 

//volatile GYRO_DATA lpGyroDataM; 
volatile ACC_DATA accData; 
volatile ACC_DATA accDataHM;
volatile ACC_DATA accDataLM;
volatile ACC_DATA accDataM;
//volatile ACC_DATA lpAccDataM;
//volatile byte lpAccDataMValid;
//volatile byte battSampEnable = 0;
 
volatile byte hgAlarm = 0;
volatile byte stopHg = 0; 
volatile byte lowGEnable =0;
volatile byte motionDetected =0;
volatile int32_t  highGMax =0;
volatile int32_t highGMaxTemp =0;
volatile int32_t lowGMax =0;
volatile int32_t lowGMaxTemp =0;
volatile int32_t gyroMax =0;
volatile int32_t gyroMaxTemp =0;
volatile int32_t  hlGMax =0;

volatile byte useHighG =0;
volatile byte LOWG_CV_HG(byte y)
{
  byte lg;
  if( y > 128 )
	  lg = 128 - ((255-y+1) >> 3); 
  else 
    lg = 128 + (y >> 3);
	return lg;
}
 
#define ZERO_G_IN_12BIT   2048
volatile byte sampleCnt = 0;

interrupt  void ADCISR (void)
{	
  volatile word  tmpInt =0;
	volatile static word mTick =0;
  if (!KBI_USB_ON )//&& !stopHg)
  {
      if(IRQSC_IRQF) 
	    {
	  	 motionDetected = 2;
		   IRQSC_IRQACK = 1;
      }
        //LED_EN_On();
      if(ADCSC1_COCO)
    	  adcChanIndex++;
        
      if(adcChanIndex ==1)
      {
      #if 1
        accData.x =   (ADCRL); 
	      accData.x -=127;
	      //accData.x *=200;
	       
		  
	    #else
    	  tmpInt = ADCRH;
		    tmpInt << 8;
    	  accData.x = (int16_t)( tmpInt| ADCRL ) ;  
    	  
		  //accData.x -=ZERO_G_IN_12BIT;
	    #endif
    	  ADCSC1 = adcChanGame[adcChanIndex] | 0x40;
      }
      else if(adcChanIndex ==2)
      {
      #if 1
	      HiAdcResult = ADCRH;
        accData.y =  (ADCRL  ) ; 
		    accData.y -=127;
	      //accData.y *=200;
	    #else
    	  tmpInt = ADCRH;
		    tmpInt<<8;
    	  accData.y = (int16_t)(tmpInt| ADCRL ) ;  
		  //accData.y -=ZERO_G_IN_12BIT;
	    #endif
    	  ADCSC1 = adcChanGame[adcChanIndex] | 0x40;
      }
      else if(adcChanIndex ==3)
      {
      #if 1
	       accData.z = (ADCRL ); 
	       accData.z -=127;
	     //accData.z *=200;
	    #else
	       LED_YELLOW_On();
          
         tmpInt = ADCRH;
		     tmpInt<<8;
    	   accData.z = (int16_t)(tmpInt| ADCRL )  ;  
		   //accData.z -=ZERO_G_IN_12BIT;
	    #endif  
	  
         highGMaxTemp = accGetSum16(accData.x, accData.y, accData.z);
	     //highGMaxTemp = (long)accGetResult(accData.x, accData.y,accData.z);
	     //highGMaxTemp = (int32_t)accData.x *accData.x + (int32_t)accData.y *accData.y+(int32_t)accData.z *accData.z ;
	     //highGMaxTemp = accData.x  +  accData.y  +accData.z   ;
	    if(highGMaxTemp > highGMax) 
		  {
		      highGMax = highGMaxTemp;
		      accDataHM = accData;
 
		  }
		 
	 	  if (mTick != g_Tick1mS)
    	{
			  mTick = g_Tick1mS;
			  (void)lgGyroGet();
			  gyroMaxTemp = accGetResult((long)lgGyroVT.x,(long)lgGyroVT.y,(long)lgGyroVT.z);	
			//gyroMaxTemp = ((long)lgGyroVT.x+(long)lgGyroVT.y+(long)lgGyroVT.z);	
			//gyroMaxTemp = accGetSum16((int32_t)lgGyroVT.x, (int32_t)lgGyroVT.y, (int32_t)lgGyroVT.z);
	      if(gyroMaxTemp > gyroMax)
	      {
		      gyroMax =  gyroMaxTemp;
		      gyroDataM = lgGyroVT;
		    }
	 	  }
         
		  if (g_SampleFlag)
		  {		  
		  //if((abs(accDataHM.x)> (int16_t)LOWG_HIGHG_MERGE_THRESHOLD * 84)||(abs(accDataHM.y)> (int16_t)LOWG_HIGHG_MERGE_THRESHOLD * 84)||(abs(accDataHM.z)> (int16_t)LOWG_HIGHG_MERGE_THRESHOLD * 84))
			//if(highGMax > (int32_t) LOWG_HIGHG_MERGE_THRESHOLD * LOWG_HIGHG_MERGE_THRESHOLD*16393  )
			  if(highGMax > (int32_t) LOWG_HIGHG_MERGE_THRESHOLD )              
			  {
		       		//LED_YELLOW_On();
		       if(motionDetected || gftInfo.pwrMode)		       			
		       {
					    imUpdateBucket(lgAccVT,1);
						  sampleCnt++;
		       }
					//if(highGMax >(int32_t) usrProfile.tR*usrProfile.tR * 16393 
					 useHighG =1;					
		    }
        else  
			  {			       				  
				 //(void)lgAccGet();
				   lgAccVT.x = lgAccVT.x >>4;
				   lgAccVT.y = lgAccVT.y >>4;
				   lgAccVT.z = lgAccVT.z >>4;
				   
			     lowGMaxTemp = accGetSum16(lgAccVT.x,lgAccVT.y,lgAccVT.z);	
				 //lowGMaxTemp = accGetResult((long)lgAccVT.x,(long)lgAccVT.y,(long)lgAccVT.z);
				   if(lowGMaxTemp >lowGMax)
				   {
				        
				      lowGMax =  lowGMaxTemp;				  
					    accDataLM = lgAccVT;
						//if(usrProfile.tR < LOWG_HIGHG_MERGE_THRESHOLD)
						//   if(lowGMax >(int32_t) usrProfile.tAlarm*usrProfile.tAlarm * 16393 
					  //	  useHighG =1;
						
				   }	
				   if(motionDetected || gftInfo.pwrMode)
				   {
				   	  imUpdateBucket(lgAccVT,0);
						  sampleCnt++;
				   }
				   
        }   
			 	 
			  //(void)lgGyroGet();	
			  #if 0
			  gyroMaxTemp = accGetResult((long)lgGyroVT.x,(long)lgGyroVT.y,(long)lgGyroVT.z);	
			  if(gyroMaxTemp >gyroMax)
			  {
			   	  gyroMax =  gyroMaxTemp;
				    gyroDataM = lgGyroVT;
			  }
			  #endif
			  g_SampleFlag = 0;

		  }  
      if( (sampleCnt ==50)&& ( motionDetected || gftInfo.pwrMode))
    //if( g_RecordFlag &&  ( motionDetected || gftInfo.pwrMode))
		//if( g_RecordFlag )//&&  (  gftInfo.pwrMode))	
      {
         
			  //if(ccGetResult((int32_t)accDataHM.x,(int32_t)accDataHM.y,(int32_t)accDataHM.z) >  LOWG_HIGHG_MERGE_THRESHOLD * 84 * LOWG_HIGHG_MERGE_THRESHOLD * 84  )
			  //if((abs(accDataHM.x)> (int16_t)LOWG_HIGHG_MERGE_THRESHOLD * 84)||(abs(accDataHM.y)> (int16_t)LOWG_HIGHG_MERGE_THRESHOLD * 84)||(abs(accDataHM.z)> (int16_t)LOWG_HIGHG_MERGE_THRESHOLD * 84))
         if(useHighG)
			   {
		       		
					 accDataM.x = accDataHM.x *200;
					 accDataM.y = accDataHM.y *200;
					 accDataM.z = accDataHM.z *200;
					 hlGMax = highGMax*200;
					
		     }
			   else 
			   {
			     accDataM = accDataLM;
					 hlGMax = lowGMax;
			   }
			  
			  //if(hlGMax >(int32_t) usrProfile.tR*usrProfile.tR * 16393 )
			  if(hlGMax >(int32_t) usrProfile.tR *128)
			     imSaveDataItemToEntry();
			  sampleCnt = 0;
		    useHighG =0;
        highGMax = 0; 
			  lowGMax =0;
			  gyroMax = 0;
        g_RecordFlag =0;
			  hlGMax =0;
           
      }
#if 0//ENABLE_BATTERY_NET
    	if(battSampEnable) {    	    
    	      //battSampEnable = 0;
    	      ADCSC1 = adcChanGame[adcChanIndex] | 0x40;
    	}
    	else
#endif
    	{
    	  ADCSC1_AIEN = 0;
    	  adcChanIndex =0;
    	}		  
    }      
  }
}

byte adcTest(ACC_DATA* dp)
{
  static byte fin =0;
	if(adcChanIndex ==0)
	{	  	   
    ADCSC1 = adcChanGame[0];//start adc
	  //ADCSC1_AIEN = 1;
	  fin =0;			
	}
	if(ADCSC1_COCO)
    adcChanIndex++;
        
  if(adcChanIndex ==1)
  {
    accData.x =   (ADCRL  ); 
	  //accData.x -=127;
	  //accData.x *=200;
	  fin =0;
    ADCSC1 = adcChanGame[adcChanIndex] ;
  }
  else if(adcChanIndex ==2)
  {    
	  HiAdcResult = ADCRH;
    accData.y =  (ADCRL  ) ; 
		//accData.y -=127;
	  //accData.y *=200;
	  
    ADCSC1 = adcChanGame[adcChanIndex] ;
	  fin =0;
  }
  else if(adcChanIndex ==3)
  {
	  accData.z = (ADCRL ); 
	  //accData.z -=127;

		//ADCSC1_AIEN = 0;
    adcChanIndex =0;
      	
		dp->x = accData.x;
		dp->y = accData.y;
		dp->z = accData.z;
		fin =1;
  }
	else
	  fin =0;
	  
	return fin;
}
