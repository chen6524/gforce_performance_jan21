/********************************************************************************
*  
*
* History:
*
* 04/01/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#include "mGyro.h"
#include "LSM6DS3.h"
#include <string.h>
#include "mImpact.h"
#include "adc.h"
#include "mHid.h"
#include "rtc.h"
#include <stdlib.h>
#include "kbi.h"

#include "bcd_common.h"                        // Added by Jason Chen for time stamp fix, 2015.05.07

//GYRO_INFO gyroInfo[ACC_MAX_BD_NUM];

GYRO_STATE gyroState=GYRO_STATE_SOFT_RESET;

volatile byte lgIsrState =0;
//volatile static ACC_DATA accData; 
byte Statup_flag = 3;                          // Added by Jason Chen for first time startup, 2017.03.30

void lgISR (void)
{
   DISABLE_LOWG_INT;
   pwrOffCnt =0;
   if(Statup_flag) 
   {
      Statup_flag--;
      imSleepTimerTime = 75;                   // Added by Jason Chen, 2017.03.30
   } 
   else
   		imSleepTimerTime = IM_SLEEP_TIMER;       // Added by Jason Chen, 2014.11.26
   motionDetected =2;
   return;
   #if 0
   lgIsrState++;
   //adc sample now
    if (!KBI_USB_ON)
    {
  	  //gftInfo.gHGCnt++;
      accHitStat = ACC_HIT_STAT_INIT;
      //if(imState == IM_STATE_SLEEP_WAIT)
	  //  imState = IM_STATE_IDLE;
	  //ADCSC1 = adcChanGame[0] | 0x40;//start adc
	  //ADCSC1_AIEN = 1;
      //testMsS = g_Tick;
      
    }	
   imSleepTimerTime = IM_SLEEP_TIMER;
   DISABLE_LOWG_INT;
   return;
   #endif
}

void gyroInit(void)
{
	  
    SPI_MEMS_Acc_Config( 0);
    SPI_MEMS_Gyro_Config();
  //set stream mode
    //gyro_FifoStream();
}

/* 
	the sample rate for the gyro is 760Hz
*/
#define GYRO_SAMPLE_INTERVAL (1/760)
byte lgGyroGet(void)
{

   int32_t gyroMax =0;
   int32_t gyroMaxTemp =0;
   
   
   
    
     
   //for(i = 0; i<1; i++)
   {   		  		
   		 gyro_FIFO_SPIGetRawData((uint8_t*)(&lgGyroVT));
		 //gyroMaxTemp = accGetResult((long)lgGyroVT.x,(long)lgGyroVT.y,(long)lgGyroVT.z);	
	    // if(gyroMaxTemp >gyroMax)
	     // {
		//	  gyroMax =  gyroMaxTemp;
		 //     gyroData = lgGyroVT;
		//  }
   		  
		
   }
   //lgGyroVT = gyroData;
   return 1;
}



#define     gyroGetSum(x,y,z) (x*x+y*y+z*z)



//static LOWG_DATA lgAccData[32];
#define BUFFER_SIZE  30
//static LOWG_DATA lgAccVData[BUFFER_SIZE];                                 // Added by Jason Chen for preventing wrong recording , 2014.05.06 
ACC_DATA lgAccV;
ACC_DATA lgAccVT;

GYRO_DATA lgGyroV;
GYRO_DATA lgGyroVT;

#define NEGATIVE_SIGN =0x8000
#if  0
static int16_t convertLg(int16_t a)
{
	 int16_t d = a;
	 if(d > 0) 
		 d = (d  >>  4);
	 else
		 d = (int16_t)((((unsigned int)d & (unsigned int)0x7fff)	>>	4) | (unsigned int)0x8000);
	
	 return d;
}

#endif
byte lgAccGet(void) {

   
   acc_FIFO_SPIGetRawData((byte *)(&lgAccV));
   
   return 1;
}


void lgAccGetData(void)                                  // Added by Jason Chen for preventing wrong recording , 2014.05.06 
{
   byte i;
   ACC_DATA mData[32];
   int32_t sumX = 0,sumY = 0,sumZ = 0;
   //there are 32  sets of data 
   
   for(i = 0; i<32; i++)
   {
   		
   		gyro_FIFO_SPIGetRawData((byte *)(&mData[i]));  
		if(i < 16)
		{
		 sumX += mData[i].x;
		 sumY += mData[i].y;
		 sumZ += mData[i].z;
		}
   }
   lgAccV.x = (int16_t)sumX/16;
   lgAccV.y = (int16_t)sumY/16;
   lgAccV.z = (int16_t)sumZ/16;
}
#if 0
static byte lgAccCalc(void)
{
     lgAccV.x = lgAccV.x>>4;
     lgAccV.y = lgAccV.y>>4;
     lgAccV.z = lgAccV.z>>4;
    return 1;
}
#endif
static byte lgState = LG_STATE_INIT;

void lowGTask(byte usbUnpluged)
{
    static byte cnt = 0;
	if(startCal)
	{
       switch(lgState)
       {
          case LG_STATE_INIT:     
             
              //SPI_MEMS_Acc_Config(1);
              lgAccV.x = 0;
              lgAccV.y = 0;
              lgAccV.z = 0;
              
              lgState = LG_STATE_CAL_START;
              break;
          case LG_STATE_CAL_START:
              
              lgAccGet();
              if(cnt++ == 1)
              {
                lgState = LG_STATE_CAL_CAC;
                cnt = 0;
              }
            break;
          case LG_STATE_CAL_CAC:
              //lgAccCalc();
              lgState = LG_STATE_CAL_END;
            break;
          case LG_STATE_CAL_END:
              lgState = LG_STATE_INIT;
              startCal =0;
          break;
        }
    }
	else if (g_SampleFlagL & usbUnpluged)
	{
     (void)lgAccGet();
	 //(void)lgGyroGet();	
	 DisableInterrupts;
	  lgAccVT = lgAccV;
	  //lgGyroVT = lgGyroV;
	 EnableInterrupts; 
     g_SampleFlagL = 0;
	}

}

