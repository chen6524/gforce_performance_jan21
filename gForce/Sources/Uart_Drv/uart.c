/********************************************************************************
*  
*
* History:
*
* 06/01/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/
#ifdef ENABLE_LP

// includes
#include "gft.h"
#include "mImpact.h"
#include "typedef.h"
#include "mcu.h"
#include <string.h>
#include "hid.h"
#include "mhid.h"
#include "mFlash.h"
#include "uart.h"
#include "rtc.h"

////////////////////////
// definitions


UartParams uartComm;

void uartInit(void)
{
    byte data; 
    // 9.6K baud rate 156 19200 - 78   38400 47.47
    // 115200 use 13
    //38400 39    24M = 25165824 
    //
    SCI1BD = 13 ;        //16 bits register LBKDIE & RXEDGIE set to 0 
    SCI1C1 = 0x00   
	         // |  SCI1C1_LOOPS_MASK 
	         // |  SCI1C1_SCISWAI_MASK 
	         // |  SCI1C1_RSRC_MASK  
	         // |  SCI1C1_M_MASK        //0 normal 
	         // |  SCI1C1_WAKE_MASK   
	         // |  SCI1C1_ILT_MASK      
	         // |  SCI1C1_PE_MASK       //Parity Enable  0 No parity 1: Parity enabled
	         // |  SCI1C1_PT_MASK       //Parity Type 0 : even  1: odd
	         ;
	
    SCI1C2 = 0
		     //| SCI1C2_TIE_MASK          //Transmit interrupt enable
             //| SCI1C2_TCIE_MASK        // tx complete interrupt enable
               | SCI1C2_RIE_MASK            // Rx interrupt enable
             //| SCI1C2_ILIE_MASK         //  
             | SCI1C2_TE_MASK             // Tx enable 1
             | SCI1C2_RE_MASK             // Rx enable 1
             //| SCI1C2_RWU_MASK          // Rx wake up control
             //| SCI1C2_SBK_MASK           // Send break
             ;
             
    SCI1C3 = 0     // 
            //| SCI1C3_R8_MASK   
            //| SCI1C3_T8_MASK    
            //| SCI1C3_TXDIR_MASK  
            //| SCI1C3_TXINV_MASK   
            //| SCI1C3_ORIE_MASK
            //| SCI1C3_NEIE_MASK  
            //| SCI1C3_FEIE_MASK  
            //| SCI1C3_PEIE_MASK   
            ;
    //sci STATUS REGISTER 1 
    // tdre
    // tc 
    //rdrf
    //IDLE
    //OR
    //Nf
    //fe
    //pf
    
    // clear status and data registers
    data = SCI1S1;
    data = SCI1D;
    uartComm.RXg = 0;
	uartComm.RXp = 0;
	
	uartComm.TXg = 0;
	uartComm.TXp = 0;
	
	uartComm.RXl = UART_COMM_BUFF_SIZE;
	uartComm.TXl = UART_COMM_BUFF_SIZE;

	uartComm.TXf =0;
	uartComm.RXf =0;
}

interrupt void uartRxISR(void)
{
   byte c;
   byte index;
   byte txrxStatus = SCI1S1;
   c = (byte)SCI1D;				    // get char
   index = uartComm.RXp;			// adjust put pointer
   if(++index == uartComm.RXl)
		index = 0;
   if(index != uartComm.RXg)
   {
	  uartComm.RX[uartComm.RXp]= c;
      uartComm.RXp = index;
      uartComm.RXf = 1;
   }
}
interrupt void uartTxISR(void)
{
	byte txrxStatus = SCI1S1;
	if( uartComm.TXg ==  uartComm.TXp)
	{
		 uartComm.TXf = 0;				// not TXing anymore
		 SCI1C2_TIE = 0;
		 return;
	}
	SCI1D = uartComm.TX[uartComm.TXg];
	
	if(++uartComm.TXg == uartComm.TXl)
		uartComm.TXg = 0;
}

int uartCommGetc(void)
{
	volatile byte index;
	int c;
	if(uartComm.RXg == uartComm.RXp)
    {
		uartComm.RXf = 0;								// rx flag is only an indication
        return(-1);
    }
	
	c = uartComm.RX[uartComm.RXg] & 0xff;
	index = uartComm.RXg;
	if(++index == uartComm.RXl)
		uartComm.RXg = 0;
	else
		uartComm.RXg = index;
	return(c);
	
}

int uartCommGetb(byte *s, int len)
{
	volatile byte index;
	int count;

	for(count = 0; count < len; count++)
	{
		if(uartComm.RXg ==uartComm.RXp)
		{
			uartComm.RXf = 0;								// rx flag is only an indication
			return(count);								    // disable()/enable() for exact
		}
		
		*s++ = uartComm.RX[uartComm.RXg] & 0xff;
		index = uartComm.RXg;
		if(++index == uartComm.RXl)
			uartComm.RXg = 0;
		else
			uartComm.RXg = index;
	}
	return(count);
}

byte uartCommPutc( byte c)
{
   volatile byte  index;
   index  = uartComm.TXp;
   if(++index == uartComm.TXl)
   	 index = 0;
   while( index == uartComm.TXg)
   	 Cpu_Delay100US(1);
   uartComm.TX[uartComm.TXp] = c;
   uartComm.TXp = index;

   if(uartComm.TXf  == 0)
   {
		DisableInterrupts;
		uartComm.TXf =1;
        SCI1C2_TIE = 0;
		SCI1C2_TIE = 1;
		EnableInterrupts; 
   }
   return 0;
  
}

int uartCommPutb( byte *s, int len)
{
	int count;
	
	for(count = 0; count < len; count++)
	{
		if(uartCommPutc( *s++))
			break;
	}
	return(count);
}
int uartCommPuts(byte *s)
{
	int count;
	byte c;
	
	for(count = 0; *s != 0; count++)
	{
		c = *s++;
		if(uartCommPutc(c))
		  break;
	}
	return(count);
}


#define STUPID_PKT_LEN  31
byte stupidPkt[STUPID_PKT_LEN];
byte stupidRx[STUPID_PKT_LEN];

int uartConstructPkt( byte *s, int len)
{
	int count;
	static const byte hex[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	for(count = 0; count < len*2; )
	{
		stupidPkt[count+1] =  hex[*s >> 4];
		count++;
		stupidPkt[count+1] =   hex[*s++ & 0xf];	
		count++;
	}
	
	return(count);
}
int uartCommSendPkt(void)
{
	int count;
	for(count = 0; count < STUPID_PKT_LEN; count++)
	{
		if(uartCommPutc( stupidPkt[count]))
			break;
	}
	return(count);
}

volatile byte uartCmdMail;

volatile byte uartCmdMailF;

UART_PROTO_PKT uartCommPkt;
byte *ptr;
#define UART_RT_TIME 150
void uartTask(void)
{
	static word mTick=0;
	static byte inAlarm =0;
	static word whichImpact =0;
	static word uartTimer =0;
	static word accImpactCnt =0;
	byte res;
    static word i=0;
	static word txLen =0;
    volatile byte len;
	static byte rtyTime =150;
	static byte rtyFlag =0;
	static UART_STATE uartState=UART_STATE_INIT;
	static ACC_ENTRY * pEntry = &spiFlashCurRdBuffer.flashEntry;
    switch(uartState)
    {
		case UART_STATE_INIT:
			uartCommPkt.magic[0] = 0x48;
			uartCommPkt.magic[1] = 0x4A;
			stupidPkt[0]  = 0x3A;
			stupidPkt[29] = 0x0D;
			stupidPkt[30] = 0x0A;
			uartState=UART_STATE_IDLE;
			ptr = (byte *)&uartCommPkt;
			uartTimer = 0;
			uartCmdMail = UART_CMD_ON;
			break;
		case UART_STATE_IDLE:
			if(uartCmdMail == UART_CMD_ON)
			{
				uartState=UART_STATE_ON;
				uartCmdMail = UART_CMD_NONE;
			}
			else if(uartCmdMail == UART_CMD_OFF)
			{
				uartState=UART_STATE_OFF;
                uartCmdMail = UART_CMD_NONE;
			}
			else if(uartCmdMail == UART_CMD_RPT)
			{
				uartState=UART_STATE_RPT;
                uartCmdMail = UART_CMD_NONE;
			}
			else if (uartCmdMail == UART_CMD_ALARM)
			{
				uartState=UART_STATE_ALARM;
				
                //spiFlashGetCurWtAddr();
				
				accImpactCnt =  spiFlashCurRdBuffer.accEntryCnt;
				
				uartCommPkt.cmd = UART_CMD_ALARM;
			    uartCommPkt.len = sizeof(ACC_ENTRY);
				i =whichImpact;
				uartTimer =0;
				uartCmdMail = UART_CMD_NONE;
			}
			#if 0
			else
			{   
				if (mTick != g_Tick1mS)
	            {
		           mTick = g_Tick1mS;
				   if(uartTimer++ > 10000)
				   {
				      uartCmdMail =UART_CMD_ON;
					  uartTimer =0;
				   }
				   else
				   	  uartCmdMail =UART_CMD_NONE;
				}
			}
			#endif
			else
				uartCmdMail =UART_CMD_NONE;
			break;
		case UART_STATE_ALARM:
			if( i< accImpactCnt )
			{
			   res = spiFlashReadAccEntry(i);
			   if (res == SPIFLASH_RD_SUCESS)
			   {
				  (void)memcpy((void *)uartCommPkt.data, (void *)pEntry, sizeof(ACC_ENTRY));	
			   }
			   uartState=UART_STATE_TX;
			   txLen =0;
			   ptr = (byte *)&uartCommPkt;
			   inAlarm = 1;
			}
			else
			{
				whichImpact = accImpactCnt;
				inAlarm =0;
				uartState = UART_STATE_IDLE;
			}
			i++;
			break;
		case UART_STATE_ON:
			uartCommPkt.cmd = UART_CMD_ON;
			uartCommPkt.len = 16+1+1;
			uartState=UART_STATE_TX;
			txLen =0;
			uartCommPkt.data[0] = uartCommPkt.data[0] = usrProfile.AlarmMode & PROFILE_ALARM_ON_MSK;
			uartCommPkt.data[1] = 10;
			memcpy(&(uartCommPkt.data[2]), usrProfile.name, 10);
			ptr = (byte *)(&(uartCommPkt.magic));
			break;

		case UART_STATE_RPT:
			uartCommPkt.cmd = UART_CMD_RPT;
			uartCommPkt.len = 3;
			uartCommPkt.data[0] = accMaxValue._byte.byte0;
			uartCommPkt.data[1] = accMaxValue._byte.byte1;
			uartCommPkt.data[2] = usrProfile.tAlarm;
			uartState=UART_STATE_TX;
			txLen =0;
			ptr = (byte *)(&(uartCommPkt.magic));
			break;
		case UART_STATE_OFF:
			uartCommPkt.cmd = UART_CMD_OFF;
			uartCommPkt.len = 0;
			uartState=UART_STATE_TX;
			txLen =0;
			ptr = (byte *)&uartCommPkt;
			break;
			
		case UART_STATE_RX:
			if (mTick != g_Tick1mS)
	        {
				
		        mTick = g_Tick1mS;
                if(uartTimer++ > rtyTime)
                {
					#define ENABLE_UART_RETRY
					#ifdef ENABLE_UART_RETRY
					if(rtyFlag)
					{
						uartState = UART_STATE_RE_TX;
						rtyFlag =0;
					}
					else
					{
					    len = (byte)uartCommGetb(stupidRx, STUPID_PKT_LEN+5);
					    if(len >= STUPID_PKT_LEN-1)
					    {
							if(txLen < uartCommPkt.len + UART_PROTO_PKT_HEAD_LEN)
								uartState = UART_STATE_TX;
							else
							{
								if (uartCommPkt.cmd == UART_CMD_RPT)
								{
									uartState = UART_STATE_IDLE;
									uartCmdMail = UART_CMD_ALARM;
									//uartCmdMail = UART_CMD_NONE;
								}
								else if(inAlarm)
									uartState = UART_STATE_ALARM;
								else 
							        uartState = UART_STATE_IDLE;
							}
							rtyTime = UART_RT_TIME;
					    }
						else
						{
							rtyTime = UART_RT_TIME;
							rtyFlag =1;
						}
						
					}
					uartTimer = 0;
					#endif
                }
                
			}	
			break;
		case UART_STATE_RE_TX:
            (void)uartCommSendPkt();
			uartState = UART_STATE_RX;
			break;
		case UART_STATE_TX:
			
			if(txLen + UART_MAX_PKT_LEN < uartCommPkt.len + UART_PROTO_PKT_HEAD_LEN)
			{                   
				 (void)uartConstructPkt( ptr, UART_MAX_PKT_LEN);
				 (void)uartCommSendPkt();
				 ptr += UART_MAX_PKT_LEN;
				 txLen += UART_MAX_PKT_LEN;
				 uartState = UART_STATE_RX;
					 
			}
			else
			{  
                 //uartCommSendPkt( ptr, uartCommPkt.len + 4 - txLen);
				(void)uartConstructPkt( ptr, UART_MAX_PKT_LEN);
				 (void)uartCommSendPkt();
				 txLen = uartCommPkt.len + UART_PROTO_PKT_HEAD_LEN;
				 uartState = UART_STATE_RX; 
			}
            break;
    }

}
#endif
