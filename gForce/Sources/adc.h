/*************************************************************************
 * DISCLAIMER *
 * Services performed by FREESCALE in this matter are performed          *
 * AS IS and without any warranty. CUSTOMER retains the final decision   *
 * relative to the total design and functionality of the end product.    *
 * FREESCALE neither guarantees nor will be held liable by CUSTOMER      *
 * for the success of this project. FREESCALE disclaims all warranties,  *
 * express, implied or statutory including, but not limited to,          *
 * implied warranty of merchantability or fitness for a particular       *
 * purpose on any hardware, software ore advise supplied to the project  *
 * by FREESCALE, and or any product resulting from FREESCALE services.   *
 * In no event shall FREESCALE be liable for incidental or consequential *
 * damages arising out of this agreement. CUSTOMER agrees to hold        *
 * FREESCALE harmless against any and all claims demands or actions      *
 * by anyone on account of any damage, or injury, whether commercial,    *
 * contractual, or tortuous, rising directly or indirectly as a result   *
 * of the advise or assistance supplied CUSTOMER in connection with      *
 * product, services or goods supplied under this Agreement.             *
 *************************************************************************/
/*************************************************************************************************
 *
 * File name   : 	Adc.h
 *
 * Description : 	the header file for ADC module
 *
 * History     :
 * 04/01/2007  : Initial Development
 * 
 *************************************************************************************************/

#ifndef _ADC_H
#define _ADC_H

#include "typedef.h"
#include "mImpact.h"


/*#define BANDGAP_EN*/

enum
{
    BATT_LEVEL_10PECENT  = 0x67 * 10,   //103,//0x69 = 105, 0x71 = 113, 0x48 = 72,  102 = 0x66          GFT3 tested
    BATT_LEVEL_20PECENT  = 0x6A * 10,   //106,//0x72 = 114,      50,   //80
    BATT_LEVEL_30PECENT  = 0x6F * 10,   //111,//0x74 = 116
    BATT_LEVEL_40PECENT  = 0x75 * 10,   //117
    BATT_LEVEL_50PECENT  = 0x76 * 10,   //118
    BATT_LEVEL_60PECENT  = 0x77 * 10,   //119
    BATT_LEVEL_70PECENT  = 0x78 * 10,   //120
    BATT_LEVEL_80PECENT  = 0x79 * 10,   //121
    BATT_LEVEL_90PECENT  = 0x7A * 10,   //122
    BATT_LEVEL_100PECENT = 0x7B * 10,   //123
};

enum
{
#if 0
    F_BATT_LEVEL_10PECENT  = 5600,      //103,  3.500 Voltage
    F_BATT_LEVEL_20PECENT  = 5829,      //106,  3.643 Voltage
    F_BATT_LEVEL_30PECENT  = 6113,      //111,  3.821 Voltage
    F_BATT_LEVEL_40PECENT  = 6453,      //117,  4.033 Voltage
    F_BATT_LEVEL_50PECENT  = 6509,      //118,  4.068 Voltage
    F_BATT_LEVEL_60PECENT  = 6566,      //119,  4.104 Voltage
    F_BATT_LEVEL_70PECENT  = 6623,      //120,  4.139 Voltage
    F_BATT_LEVEL_80PECENT  = 6679,      //121,  4.174 Voltage
    F_BATT_LEVEL_90PECENT  = 6736,      //122,  4.216 Voltage
    F_BATT_LEVEL_100PECENT = 6720,      //123,  4.200 Voltage
#else
    F_BATT_LEVEL_10PECENT  = 5440,      //103,  3.400 Voltage
    F_BATT_LEVEL_20PECENT  = 5584,      //106,  3.490 Voltage
    F_BATT_LEVEL_30PECENT  = 5712,      //111,  3.570 Voltage    
    F_BATT_LEVEL_40PECENT  = 5856,      //117,  3.660 Voltage    
    F_BATT_LEVEL_50PECENT  = 6000,      //118,  3.750 Voltage    
    F_BATT_LEVEL_60PECENT  = 6144,      //119,  3.840 Voltage    
    F_BATT_LEVEL_70PECENT  = 6288,      //120,  3.930 Voltage    
    F_BATT_LEVEL_80PECENT  = 6432,      //121,  4.020 Voltage
    F_BATT_LEVEL_90PECENT  = 6576,      //122,  4.110 Voltage
    F_BATT_LEVEL_100PECENT = 6720,      //123,  4.200 Voltage
    F_BATT_LEVEL_15PECENT  = 5512,      //103,  3.440 Voltage
#endif    
};


extern unsigned char HiAdcResult;
extern WORD BatteryVoltage;
extern volatile byte batData;
extern volatile word batData1;

extern volatile  byte lowGEnable;
//extern volatile byte tempByte;
//high g accelerometer
#define HG_FULL_RANGE  200
#define HG_0G_V 128
#define HG_DIG_PER_G   ((HG_0G_V*10)/HG_FULL_RANGE)
//X means g number
#define HG_HIGH_THR(X)  ((HG_DIG_PER_G *X)/10 + HG_0G_V )   
#define HG_LOW_THR(X)   (HG_0G_V - (HG_DIG_PER_G *X)/10)
extern unsigned char CovEndFlag;
extern byte adcChanGame[];
//extern byte adcChanUsb[];
//extern volatile byte battSampEnable;

extern volatile GYRO_DATA gyroData; 
extern volatile GYRO_DATA gyroDataM;
//extern volatile GYRO_DATA lpGyroDataM; 
extern volatile ACC_DATA accData; 
extern volatile ACC_DATA accDataM;

//extern volatile ACC_DATA lpAccDataM;
//extern volatile byte lpAccDataMValid;
extern volatile byte stopHg;

extern volatile byte motionDetected;
//extern volatile word batVoltSum;
//extern volatile word batVoltSum_count;
#define LOWG_HIGHG_MERGE_THRESHOLD 8
extern volatile byte LOWG_CV_HG(byte y);
extern void ADC_Init(void);
extern void adcProcess(void);
void updateBatStatus(void);

byte adcTest(ACC_DATA* dp);
#endif
