/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/

#include "gft.h"
#include "rtc.h"
#include "mcu.h"
#include <hidef.h>
#include "kbi.h"
#include "mHid.h"
#include "adc.h"
#include "rtc_drv.h"
#include "utils.h"

#include "bcd_common.h"               // Jason Chen, Apr.22, 2014 for harry lattest version

/*Global variable definition*/
volatile word g_Tick1mS;            // 1 mSec timer increment
volatile word g_Tick =0;            
volatile word g_TickSecond;

volatile byte g_StartHg =0;
volatile byte g_SampleFlag = 0;
volatile byte g_SampleFlagL = 0;
volatile byte g_RecordFlag  = 0;

volatile MY_RTC myRTC;
MY_RTC lostRTC; //rtc time before sync
MY_RTC newRTC; //rtc time before sync
MY_RTC startRTC;
word sessionStartcnt=0;

//Faraz
volatile byte radioClock =0;
volatile word SleepTimer_TickCount=0;

#if RTC_TIME_FIFTEEN_MINUTE
  #define  REPORT_TIME_PERIOD1     (15*60)
  #define  REPORT_TIME_PERIOD2     (5*60)
#else
  #define  REPORT_TIME_PERIOD1     60
  #define  REPORT_TIME_PERIOD2     60
#endif

byte g_SampleFlagCount  = 0;
//byte g_SampleFlagLCount = 0;
word g_RecordFlagCount  = 0;
interrupt void  RTCInterrupt(void)
{
  RTCSC_RTIF = 1;            /* Reset real-time counter request flag */     
  //if(++g_Tick%3 == 0)
  if(++g_Tick >= 3)
  {
    g_Tick = 0;
    if(++g_Tick1mS > 999)
    {            
        g_Tick1mS = 0;
        //myRTC.Second ++;
			
    	  //g_Tick1mS =0;
    	  g_TickSecond++;
		
        if( ++myRTC.Second >59)
		    {
			   //myRTC.tag.Minute ++;
			     myRTC.Second =0;
			   //g_TickSecond =0;
			     if( ++myRTC.Minute>59)
			     {
				    //myRTC.tag.Hour++;
				      myRTC.Minute=0;
				      if( ++myRTC.Hour>23)
				      {					
					      (void)getRtcStamp();			   		
				      }
			     }
			   //if( (myRTC.tag.Minute & 0xf) == 0)
			   //{
			   //	  myRTC.action = MY_RTC_ACTION_READ;				
			   //}
		    }
		    
        if (KBI_USB_ON)
        {
#if 0//ENABLE_BATTERY_NET

           ADCSC1 = adcChanGame[3] | 0x40 ;//start batt adc in usb mode
           ADCSC1_AIEN = 1;
#endif
        }
		}    
		
	  // This is for recording in low freqency
	  // if( g_Tick1mS % 10  == 5) g_SampleFlag  = 1;
	  // if( g_Tick1mS % 10  == 1) g_SampleFlagL = 1;
	  // if (g_Tick1mS % 500 == 0) g_RecordFlag  = 1;	  
	  g_SampleFlagCount++;
	  if(g_SampleFlagCount == 2)       g_SampleFlagL     = 1;
    else if(g_SampleFlagCount == 6)  g_SampleFlag      = 1;
    else if(g_SampleFlagCount >= 10) g_SampleFlagCount = 0;
	  	  	  	  
	  g_RecordFlagCount++;
	  if(g_RecordFlagCount >= 500) 
	  {
	    g_RecordFlagCount = 0;
	    g_RecordFlag = 1;
	  }	  

  }
  
  //Faraz
  // This is 4 millisecond timer for radio functions
  if( ++radioClock >= 12 ) {
    radioClock = 0;
    SleepTimer_TickCount++;
  }
  
  //read high G data read 40 millisecond data
  if (!KBI_USB_ON && g_StartHg)
  {
  	//gftInfo.gHGCnt++;
	  ADCSC1 = adcChanGame[0] | 0x40;//start adc
	  ADCSC1_AIEN = 1;
  }  
}

// use internal  clock 
//353uSeconds per sample
void RtcInit(void)
{
  /* RTCSC: RTIF=0,RTCLKS=0,RTIE=0,RTCPS=0 */
  RTCSC = 0x00
  		//|RTCSC_RTIF_MASK	 
		    |RTCSC_RTCLKS1_MASK
		  //|RTCSC_RTCLKS0_MASK  //   00 - LPO  01 - external  10 - internal
		  //|RTCSC_RTIE_MASK     //   enable RTC interrupt
		    |RTCSC_RTCPS3_MASK   // 
		  //|RTCSC_RTCPS2_MASK
	 	  //|RTCSC_RTCPS1_MASK
		  //|RTCSC_RTCPS0_MASK
        ;                
  
  RTCMOD = 9;          /* A value of 0 sets the RTIF bit on each rising edge 
                        of the prescaler output. This will reset RTCCNT and
                        prescaler to 0*/

  myRTC.Year =0;
  myRTC.Month = 1;
  myRTC.Day = 1;
  myRTC.Hour = 0;
  myRTC.Minute = 0;
  myRTC.Second = 0;
  myRTC.StartMS = 0;

}

bool getRtcStamp(void)
{
    
    byte RtcTime[10];
    MY_RTC tempRtc;                                          // Added by Jason Chen according to harry latest version, 2014.04.22
    volatile byte valid = 0;
    (void)Get_YearMonthDay((byte *)&RtcTime);
    tempRtc.Year   = BCD2BIN(RtcTime[0]);                    // Added by Jason Chen according to harry latest version, 2014.04.22
	  tempRtc.Month  = BCD2BIN(RtcTime[1]);                    // Added by Jason Chen according to harry latest version, 2014.04.22	  
	  tempRtc.Day    = BCD2BIN(RtcTime[2]);                    // Added by Jason Chen according to harry latest version, 2014.04.22
	  tempRtc.Hour   = BCD2BIN(RtcTime[4]);                    // Added by Jason Chen according to harry latest version, 2014.04.22
	  tempRtc.Minute = BCD2BIN(RtcTime[5]);                    // Added by Jason Chen according to harry latest version, 2014.04.22
	  tempRtc.Second = BCD2BIN(RtcTime[6]);                    // Added by Jason Chen according to harry latest version, 2014.04.22   
   //////////////////////////////////////////////////        // Added by Jason Chen according to harry latest version, 2014.04.22  
#if 0
	  if(IsValidDateTime(tempRtc.Year, tempRtc.Month, tempRtc.Day, 
#else
	  if( (tempRtc.Year <100) &&   (tempRtc.Month <13) &&   (tempRtc.Hour<25)
        && ( (tempRtc.Minute <60)   && (tempRtc.Second <60))) 
#endif 
    {      
          valid = TRUE;
		  myRTC.Year   = tempRtc.Year;
		  myRTC.Month  = tempRtc.Month;
		  myRTC.Day    = tempRtc.Day;
		  myRTC.Hour   = tempRtc.Hour;
		  myRTC.Minute = tempRtc.Minute;
		  myRTC.Second = tempRtc.Second;
    }
    /////////////////////////////////////////////////        // Added by Jason Chen according to harry latest version, 2014.04.22          
	  //myRTC.Year   = BCD2BIN(RtcTime[0]);                    // Removed by Jason Chen according to harry latest version, 2014.04.22
	  //myRTC.Month  = BCD2BIN(RtcTime[1]);                    // Removed by Jason Chen according to harry latest version, 2014.04.22
	  //myRTC.Day    = BCD2BIN(RtcTime[2]);                    // Removed by Jason Chen according to harry latest version, 2014.04.22
	  //myRTC.Hour   = BCD2BIN(RtcTime[4]);                    // Removed by Jason Chen according to harry latest version, 2014.04.22
	  //myRTC.Minute = BCD2BIN(RtcTime[5]);                    // Removed by Jason Chen according to harry latest version, 2014.04.22
	  //myRTC.Second = BCD2BIN(RtcTime[6]); 	                 // Removed by Jason Chen according to harry latest version, 2014.04.22
	  //if( (myRTC.Year <100) &&   (myRTC.Month <13) &&   (myRTC.Hour<25)  // Removed by Jason Chen according to harry latest version, 2014.04.22
    //    && ( (myRTC.Minute <60)   && (myRTC.Second <60))) 
    //    valid = TRUE;   
    
    return valid;
}

