/********************************************************************************
*  
*
* History:
*
* 11/04/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/
// use this define for generic use
#ifndef _MAIN_H_
#define _MAIN_H_

#include <MC9S08JM60.h>
#include "typedef.h"

//define hardware version
//---------------------------

//#define GFT_GEN2  //last none wireless version

//#define GFT_GENW1 //first wireless version failed on porting 

#define GFT_GENW2   //second run 
#define GFT3S
#define ENABLE_6DS3
#define ENABLE_6DS3_G
//Modules/functions
//---------------------------

#define ENABLE_PWR_SAVING

#define _DEBUG_


#define ENABLE_RADIO    

#define ENABLE_BUZZ   

#define USE_MAX14676PT

//#define DISABLE_BTN_WAKEUP

//---------------------------

 #define BAT_STAT_NORM    0x0
 #define BAT_STAT_CHARGED 0x1
 #define BAT_STAT_LOW     0x2


 #define BAT_STAT_GREEN   BAT_STAT_NORM                 // Added by Jason Chen, 2013.11.14
 #define BAT_STAT_YELLOW  0x4
 #define BAT_STAT_RED     0x5
 

 #define GMODE_LP_DISABLE 0x0
 #define GMODE_LP_ENABLE  0x1
 #define GMODE_LP_BIND    0x2 
 #define GMODE_LP_GAME    0x4
 #define GMODE_LP_USB     0x8
 #define GMODE_LP_IDLE    0x10
 #define GMODE_LP_TEST    0x20

typedef struct 
{
	
	byte batState;     // gft mode 
	byte lpMode;
    byte lpBusy;
	byte pwrMode;
	byte uploadflag;   // Added by Jason Chen, 2014.04.09    
	byte proxStat;
	byte rpe;
    long load;
	long rpeLoad;
	long expCnt;  //explosive count
	long expPwr;  //explosive power
	byte explosiveFlag;
	long bucketCnt;
	long psn; //performance number
	long isn;
} GFT_INFO;

extern GFT_INFO gftInfo;
//sleep check time 
#define G_SLEEP_CNT 10  //unit in second must <60


#define GFT_HARD_ID_MAGIC 0x88

extern bool Message_Registered;

extern byte radioTxMsg(byte type, byte *msg, word size);
extern byte Immediate_Data_Message[];
//Hardware connection 
//------------------------------
/* port pins assignment:

GFT_GEN1
+---+-----------+-----+-------+-------+------+-----------+-------+-------+ 
|   |  7        |  6  |  5    | 4     |  3   |  2        |  1    |  0    |
|---+-----------+-----+-------+-------+------+-----------+-------+-------+
| A | ---       | --- |EN_CHRG| ---   | ---  |---        |---    | .     |
| B | ---       | --- |USB_INT|GREEN  |SS_ACC|SCK2       |MOSI2  |MISO2  |
| C | ---       | --- |TORCH  |ACC_SEL|LED_EN|RED        |IIC_SDA|IIC_SCL|
| D |POWER_MODE | --- | ---   | ---   | ---  |CHARGE_DONE|BAT_V  |  .    |
| E |SS_FLASH   |SCK1 |MOSI1  |MISO1  |ARM Y1|ARMX1      |RxD1   |TxD1   |
| F | ---       |  .  |YELLOW |BUZZ   |  --- | ---       |ARM Y2 |ARM X2 |
| G | ---       | --- |Extal  |Xtal   |RTCINT|F32KH      |BTN    |.      |
+---+-----------+-----+-------+-------+------+-----------+-------+-------+

GFT GEN2
+---+-----------+------+-------+-------+------+-----------+-------+-------+ 
|   |  7        |  6   |  5    | 4     |  3   |  2        |  1    |  0    |
|---+-----------+------+-------+-------+------+-----------+-------+-------+
| A | ---       | ---  |CHA_EN2| ---   | ---  |---        |---    |POK    |
| B | ---       | ---  |USB_INT|INT_A  |SS_ACC|SCK2       |MOSI2  |MISO2  |
| C | ---       | ---  |TORCH  |ACC_SEL|LED_EN|CY_CS      |IIC_SDA|IIC_SCL|
| D |CHA_EN1    | ---  | ---   | ---   | ---  |GY_READY   |BAT_V  |LOWG_ON|
| E |SS_FLASH   |SCK1  |MOSI1  |MISO1  |RED   |SENS_ON    |RxD1   |TxD1   |
| F | ---       |3STATE|YELLOW |BUZZ   | ---  | ---       |CHARGE |GREEN  |
| G | ---       | ---  |Extal  |Xtal   |RTCINT|F32KH      |GY_INT |ARM    |
+---+-----------+------+-------+-------+------+-----------+-------+-------+
changes:
    LED: Green B4->F0
    	 Red   C2->E3
    BTN: move from PTG1 -> IRQ
    ARM: high g Acc move grom IRQ -> PTG0
Newly added:
	Low G Acc:
	----------
	LOWG_ON:   power on 
	3STATE:    low for power down High for normal operation 
	INT_A:     interrupt
	
	Gyroscope:
	----------
	GY_INT: G1
	GY_READY:
	GY_CS: C2 

	Charger:
	--------
	EN1:
	EN2:
	POK:
	CHG: CHARGE

	High G acc:
	SENS_ON: power control  E2
 
interrupt:
KBIP0: ARM
KBIP1: GY_INT
KBIP2: GY_READY
KBIP3: xxxx
KBIP4: LOW G INT_A
KBIP5: USB_Int 

GFT_GENW1

+---+-----------+------+-------+-------+------+-----------+-------+-------+ 
|   |  7        |  6   |  5    | 4     |  3   |  2        |  1    |  0    |
|---+-----------+------+-------+-------+------+-----------+-------+-------+
| A | ---       | ---  |BLUE   | ---   | ---  |---        |---    |POK    |
| B | ---       | ---  |HG_Z   |HG_X   |SS_ACC|SCK2       |MOSI2  |MISO2  |
| C | ---       | ---  |LED_EN |HG_ST  |TORCH |RED        |IIC_SDA|IIC_SCL|
| D |GREEN      | ---  | ---   | ---   | ---  |BTN        |BAT_V  |HG_Y   |
| E |SS_FLASH   |SCK1  |MOSI1  |MISO1  |CS_G  |PWR_ON     |SENS_ON|SS_RF  |
| F | ---       |CS_A  |DEN_G  |BUZZ   | ---  | ---       |CHARGE |RF_ON  |
| G | ---       | ---  |Extal  |Xtal   |RTCINT|USBINT     |PROXINT|IRQ_RF |    
+---+-----------+------+-------+-------+------+-----------+-------+-------+

INT_AG: connect to IRQ
the yellow led changed to blue
--------------------
High G accelerometer
--------------------
  ADP5 Z
  ADP8 Y
  ADP4 X
--------------------
Charger
--------------------
POK: open drain low for charge has valid source
CHARGE:open drain low for charged or otherwise charging
PWR_ON:Need be high

-------------------
interrupt
-------------------
KBIP0:  IRQ_RF radio    active high? or open drain   G0
KBIP1:  Proxy interrupt  active low  G1
KBIP2:  BTN active low   D2
KBIP3:  
KBIP4:
KBIP5:  
KBIP6:  USB active high    G2
KBIP7:  RTC      
------------------------
LED
---------------
Green:D7
Blue/yellow:A5
Red: C2


GFT_GENW2
GFT_GENW2 vs GFT_GENW1
green red cs_g Blue,Den_g chg changed
+---+-----------+------+-------+-------+------+-----------+-------+-------+ 
|      |  7        |  6   |  5    | 4     |  3   |  2        |  1    |  0    |
|---+-----------+------+-------+-------+------+-----------+-------+-------+
| A | ---       | ---  |CHARGE | ---   | ---  |---        |---    |POK    |
| B | ---       | ---  |HG_Z     |HG_X  |DRDY_G|SCK2       |MOSI2  |MISO2  |
| C | ---       | ---  |LED_EN |HG_ST  |TORCH |CS_G       |IIC_SDA|IIC_SCL|
| D |DEN_G      | ---  | ---   | ---   | ---  |BTN        |BAT_V  |HG_Y   |
| E |SS_FLASH   |SCK1  |MOSI1  |MISO1  |RED   |PWR_ON     |SENS_ON|SS_RF  |
| F | ---       |CS_A  |GREEN  |BUZZ   | ---  | ---       |BLUE   |RF_ON  |
| G | ---       | ---  |Extal  |Xtal   |RTCINT|USBINT     |PROXINT|IRQ_RF |    
+---+-----------+------+-------+-------+------+-----------+-------+-------+

INT_AG: connect to IRQ
the yellow led changed to blue
--------------------
High G accelerometer
--------------------
  ADP5 Z
  ADP8 Y
  ADP4 X
--------------------
Charger
--------------------
POK: open drain low for charge has valid source
CHARGE:open drain low for charged or otherwise charging
PWR_ON:Need be high

-------------------
interrupt
-------------------
KBIP0:  IRQ_RF radio    active high? or open drain   G0
KBIP1:  Proxy interrupt  active low                  G1
KBIP2:  BTN active low                               D2
KBIP3:  
KBIP4:
KBIP5:  
KBIP6:  USB active high    G2
KBIP7:  RTC      
------------------------
LED
---------------
Green:F5/TPM2CH1
Blue/yellow:F1/TPM1CH3
Red: E3 //TPM1CH1


GFTW2(V2)

IRQ_RF use PTC3


GFTW3

*/
	

#endif
