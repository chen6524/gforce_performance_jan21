
/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#include "mFlash.h"
#include "mImpact.h"
#include "spi_flash.h"
#include <string.h>
#include "mHid.h"
#include "mcu.h"
#include "rtc.h"

//static ACC_DATA flashData;
//static ACC_ENTRY flashEntry;
//static USR_PROFILE flashUsrProfile;

//unsigned long accEntryAddr = 0;
//unsigned long accDataAddr =0;

dword spiFlashCurWtAddrI =0;
dword spiFlashCurWtAddrP =0;

dword spiFlashCurRdAddrI =0;
dword spiFlashCurRdAddrP =0;

word spiFlashEntryCuntP =0;
word spiFlashEntryCuntI =0;




byte sessionID = 0;

FLASH_DATA_ENTRY_P performToSave;
FLASH_DATA_ENTRY_I impactToSave;


//SPI_FLASH_CUR_RD_BUFF spiFlashCurRdBuffer;
//SPI_FLASH_CUR_RD_BUFF spiFlashRdBuffer;


FLASH_DATA_ENTRY_P performToRead;
FLASH_DATA_ENTRY_I impactToRead;


static struct {
    byte Flag;
    byte Spi1C1;
    byte Spi1C2;
    byte Spi1Br;
} Stack_Spi = {0,0,0,0};

static void Radio_Stack_Save_and_Prog_Spi_F(void)
{
    if( Stack_Spi.Flag == 0x55 )
        return;

    // deselect Radio Chip Select & Ext Flash Chip Select
    PTED_PTED0 = 1;     PTEDD_PTEDD0 = OUTPUT;      // PTE0 output, SPI1 SS1 high
    PTED_PTED7 = 1;     PTEDD_PTEDD7 = OUTPUT;      // PTE7 output, Ext Flash Chip Select

    // save SPI regs here - set a flag that you did it
    Stack_Spi.Spi1C1 = SPI1C1;
    Stack_Spi.Spi1C2 = SPI1C2;
    Stack_Spi.Spi1Br = SPI1BR;
    Stack_Spi.Flag   = 0x55;

    // program your own
    SPI1C1 = //SPI1C1_SPIE_MASK     |
             SPI1C1_SPE_MASK        |
             //SPI1C1_SPTIE_MASK    |
             SPI1C1_MSTR_MASK       |
             //SPI1C1_CPOL_MASK     |
             //SPI1C1_CPHA_MASK     |
             //SPI1C1_SSOE_MASK     |
             //SPI1C1_LSBFE_MASK    |
             0;
    SPI1C2 = //SPI1C2_SPMIE_MASK    |
             //SPI1C2_SPIMODE_MASK  |
             //SPI1C2_MODFEN_MASK   |
             //SPI1C2_BIDIROE_MASK  |
             //SPI1C2_SPISWAI_MASK  |
             //SPI1C2_SPC0_MASK     |
             0;

  
    SPI1BR = //SPI1BR_SPPR2_MASK    |
             //SPI1BR_SPPR1_MASK      |
             //SPI1BR_SPPR0_MASK    | // BUSCLK divide by 3
             //SPI1BR_SPR2_MASK     |
             //SPI1BR_SPR1_MASK     |
             //SPI1BR_SPR0_MASK     | // divide by 2
             0;

}

//
static void Radio_Stack_Restore_Spi_F(void)
{
    if( Stack_Spi.Flag == 0x55 ) {
        //putch('=');
        SPI1C1 = Stack_Spi.Spi1C1;
        SPI1C2 = Stack_Spi.Spi1C2;
        SPI1BR = Stack_Spi.Spi1Br;
        Stack_Spi.Flag = 0;

        // deselect Radio Chip Select
        PTED_PTED7 = 1;     PTEDD_PTEDD7 = OUTPUT;      // PTE0 output, SPI1 SS1 high
      //PTED_PTED0 = 0;                                 // Added by Jason Chen according to harry latest version, 2014.04.24     Radio CS
    }
}




#define  FLASH_ENTRY_MAGIC_NUM_CNT 4
byte spiFlashGetCurWtAddrP(void)                     
{
	
    byte isTagValid = 0;
	spiFlashEntryCuntP = 0;
	spiFlashCurWtAddrP = (dword)FLASH_BASE_ADR_P;
	spiFlashCurRdAddrP = (dword)FLASH_BASE_ADR_P;
	spiFlashEntryCuntP =0;
	Radio_Stack_Save_and_Prog_Spi_F();
	do
	{
		SPI_FLASH_BufferRead((byte *)&performToRead, (dword)spiFlashCurWtAddrP,FLASH_ENTRY_MAGIC_NUM_CNT);
		isTagValid = (performToRead.mag_0 == FLASH_DATA_ENTRY_P_MAGIC_0)&& (performToRead.mag_1 == FLASH_DATA_ENTRY_P_MAGIC_1);
		if (isTagValid)	 
		{	
			spiFlashEntryCuntP++;	
			spiFlashCurWtAddrP += sizeof(FLASH_DATA_ENTRY_P);    
			if(!performToRead.flag)
	   	       spiFlashCurRdAddrP = spiFlashCurWtAddrP;
		}
		__RESET_WATCHDOG(); 	 
	}while (isTagValid);
	 
  Radio_Stack_Restore_Spi_F();
  
  return SPIFLASH_RD_SUCESS;
}



byte spiFlashGetCurWtAddrI(void)
{
	
    byte isTagValid = 0;
	spiFlashCurWtAddrI = (dword)FLASH_BASE_ADR_I;
	spiFlashCurRdAddrI = (dword)FLASH_BASE_ADR_I;
	spiFlashEntryCuntI = 0;
	Radio_Stack_Save_and_Prog_Spi_F();
	do
	{
		SPI_FLASH_BufferRead((byte *)&impactToRead, (dword)spiFlashCurWtAddrI,FLASH_ENTRY_MAGIC_NUM_CNT);
		isTagValid = (impactToRead.mag_0 == FLASH_DATA_ENTRY_I_MAGIC_0)&& (impactToRead.mag_1 == FLASH_DATA_ENTRY_I_MAGIC_1);
		if (isTagValid)	 
		{	
			spiFlashEntryCuntI++;	
			spiFlashCurWtAddrI += sizeof(FLASH_DATA_ENTRY_I);     
			if(!impactToRead.flag)
	   	       spiFlashCurRdAddrI = spiFlashCurWtAddrI;
			
		}
		__RESET_WATCHDOG(); 
		 
	}while (isTagValid);
	 
    Radio_Stack_Restore_Spi_F();
	
	return SPIFLASH_RD_SUCESS;

}




byte spiFlashWriteAccBufferP(FLASH_DATA_ENTRY_P* pEntry)
{
	word size;
	dword tmp = spiFlashCurWtAddrI;
	int i =0;
	size  = sizeof(FLASH_DATA_ENTRY_P);
	if(spiFlashCurWtAddrP + size >  FLASH_LAST_WR_ADR_P) 
		return 1; //not enough space 	
	pEntry->sn = spiFlashEntryCuntP;
	Radio_Stack_Save_and_Prog_Spi_F();
  SPI_FLASH_BufferWrite( (uint8_t *)pEntry, (uint32_t)spiFlashCurWtAddrP, size);
    
	SPI_FLASH_BufferRead((uint8_t*)&performToRead, (uint32_t)spiFlashCurWtAddrP, size);
  Radio_Stack_Restore_Spi_F();
  #if 1
	  if(memcmp((void*)&performToRead,(void*)pEntry, size)) 			   
	  {
			
			//we screw up
			//ACC_PWR_Off();    //poweroff acc 		
			needEraseFlash = 1; //clear flash
			needReboot = 1;
			return 1;		 
	  }
  #endif
	spiFlashCurWtAddrP += size;
	spiFlashEntryCuntP++;
	return 0;
}

byte spiFlashWriteAccBufferI(FLASH_DATA_ENTRY_I*          pEntry)
{
  dword tmp = spiFlashCurWtAddrI;
  word size;
	int i =0;
	size  = sizeof(FLASH_DATA_ENTRY_I);
	if(spiFlashCurWtAddrI + size >  FLASH_LAST_WR_ADR_I) 
		return 1; //not enough space 	
  pEntry->sn = spiFlashEntryCuntI;
	Radio_Stack_Save_and_Prog_Spi_F();
  SPI_FLASH_BufferWrite( (uint8_t *)pEntry, (uint32_t)spiFlashCurWtAddrI, size);
	SPI_FLASH_BufferRead((uint8_t*)&impactToRead, (uint32_t)spiFlashCurWtAddrI, size);
  Radio_Stack_Restore_Spi_F();
  #if 1   
	if(memcmp((void*)&impactToRead,(void*)pEntry, size)) 			   
	{
			
			//we screw up
			//ACC_PWR_Off(); //poweroff acc 		
			needEraseFlash = 1; //clear flash
			needReboot = 1;
			return 1;		 
	}
	#endif 
	spiFlashCurWtAddrI += size;
	spiFlashEntryCuntI++;
	return 0;
			
	
}


//zero based indexing
byte  spiFlashReadAccEntryI(dword addr)
{
	//dword tmpAddr;
    //RADIO_CS_HI();
	//tmpAddr = sizeof(FLASH_DATA_ENTRY_I)*(dword)index + FLASH_BASE_ADR_I;	
    Radio_Stack_Save_and_Prog_Spi_F();  
    SPI_FLASH_BufferRead((byte *)&impactToRead, addr,sizeof(FLASH_DATA_ENTRY_I));
    Radio_Stack_Restore_Spi_F();
  
	if ( (impactToRead.mag_0 == FLASH_DATA_ENTRY_I_MAGIC_0)
		&& (impactToRead.mag_1 == FLASH_DATA_ENTRY_I_MAGIC_1) )
	{
        
	   //spiFlashCurRdAddrI = tmpAddr;
	   
	   return SPIFLASH_RD_SUCESS;
	}
    return  SPIFLASH_RD_NOMORE;
}

byte  spiFlashReadAccEntryP(dword addr)
{
	//dword tmpAddr;
    //RADIO_CS_HI();
	//tmpAddr = sizeof(FLASH_DATA_ENTRY_P)*(dword)index + FLASH_BASE_ADR_P;	
    Radio_Stack_Save_and_Prog_Spi_F();  
    SPI_FLASH_BufferRead((byte *)&performToRead, addr,sizeof(FLASH_DATA_ENTRY_P));
    Radio_Stack_Restore_Spi_F();
  
	if ( (performToRead.mag_0 == FLASH_DATA_ENTRY_P_MAGIC_0)
		&& (performToRead.mag_1 == FLASH_DATA_ENTRY_P_MAGIC_1) )
	{
        
	    //spiFlashCurRdAddrP = tmpAddr;
		return SPIFLASH_RD_SUCESS;
	}
    return  SPIFLASH_RD_NOMORE;
}


byte spiFlashEraseFlash(void)
{
	//uint32_t i;
    //RADIO_CS_HI();
	#if 0
	  for(i =0; i < spiFlashCurWtAddr; i += SPI_FLASH_SectorSize)
	  {
	 
	   SPI_FLASH_SectorErase(i);
	 
  	}
	#endif
    Radio_Stack_Save_and_Prog_Spi_F();
  	SPI_FLASH_BulkErase();
    Radio_Stack_Restore_Spi_F();
    return 0;
}



uint32_t spiFlashGetID(void)
{
      uint32_t tmp;
	  Radio_Stack_Save_and_Prog_Spi_F();
      tmp =SPI_FLASH_ReadID();
	  Radio_Stack_Restore_Spi_F();
	  return tmp;
	  

}

byte spiFlashFlagEntry(dword addr)
{
    
    uint8_t flag = 0;
	
    Radio_Stack_Save_and_Prog_Spi_F();
    SPI_FLASH_BufferWrite( (uint8_t *)&flag, addr, 1);
	
	Radio_Stack_Save_and_Prog_Spi_F();
	return 0;
}

byte spiFlashNeedErase(void)
{
	if((spiFlashCurWtAddrI  == spiFlashCurRdAddrI) && (spiFlashCurWtAddrP  == spiFlashCurRdAddrP ))
		return 1;
	else
		return 0;

}
