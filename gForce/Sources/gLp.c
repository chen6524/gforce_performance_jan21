#include "gft.h"
#include "mcu.h"
#include "LSM330DLC.h"
#include <string.h>
#include "mImpact.h"
#include "adc.h"
#include "mFlash.h"
#include "mHid.h"
#include "rtc.h"
#include "utils.h"
#include "gLp.h"
#include "radio.h"
#include "utils.h"
#include "MAX14676.h"
#include "bcd_common.h"               // Jason Chen, SEP.11, 2013
#include "max17047.h"                 // Jason Chen, NOV.18, 2013
#include "rtc_drv.h"                  // Jason Chen, Apr.28, 2014

volatile byte lpCmdMail;
extern byte mMail;


#ifdef ENABLE_RADIO

#if 0                                 // Removed by Jason, 2014.12.05
//CYFISNP_NODE_Run() 35-50mS

//#define RADIO_PERIODIC_TIME  (50/RADIO_STACK_TIMER_UNITS)    
//#define RADIO_PERIODIC_TIME  (80/RADIO_STACK_TIMER_UNITS)    // Modified by Jason Chen, 20140212
//#define RADIO_PERIODIC_TIME  (160/RADIO_STACK_TIMER_UNITS)   // Modified by Jason Chen, 20140221, for 6 GFTs, about 4 minutes
//#define RADIO_PERIODIC_TIME  (100/RADIO_STACK_TIMER_UNITS)   // Modified by Jason Chen, 20140212, for 4 GFTs, about 2 minutes and 40 Seconds
//#define RADIO_PERIODIC_TIME  (92/RADIO_STACK_TIMER_UNITS)    // Modified by Jason Chen, 20140212, for 4 GFTs, about 2 minutes and 20 seconds
#define RADIO_PERIODIC_TIME    (84/RADIO_STACK_TIMER_UNITS)    // Modified by Jason Chen, 20140212, for 3 GFTs, about 2 minutes and 10 seconds
//#define RADIO_PERIODIC_TIME  (88/RADIO_STACK_TIMER_UNITS)    // Modified by Jason Chen, 20140212, for 3 GFTs, about 2 minutes and 10 seconds
#endif 

#define BIND_TIME_1             (30000/RADIO_STACK_TIMER_UNITS)
#define BIND_TIME_2             (5000/RADIO_STACK_TIMER_UNITS)
#define POWER_DOWN_TIME         (5000/ RADIO_STACK_TIMER_UNITS) //  5000*1.024=5120                // -- //
//#define POWER_OFF             0
//#define POWER_ON              1
//#define USB_OFF               0
//#define USB_ON                1
#define CYFISNP_NODE_FCD_PAYLOAD_MAX   14

LP_PWR_ON_PKT  Power_On_Message;
LP_PWR_OFF_PKT Power_Off_Message;
LP_RPT_PKT     lpRptPkt;
LP_RPT_PKT     lpImpactPkt;

LP_BUCKET_PKT lpBucketMessage;

//byte Immediate_Data_Message[10];
//LP_ALARM_PKT lpAlarmPkt;


byte lpState      = LP_STATE_INIT;
byte retValue     = 0;
byte myId         = 0;
//bool Power_Mode   = POWER_OFF;
//bool old_Usb_Mode =  USB_OFF;
//byte Count_Upload =0;

//static word RadioPeriodicTimer = 0;
/*static*/ 
word RadioPeriodicTimer = 0;                               // Changed by Jason, 2014.12.05

bool Message_Registered        = FALSE;
bool Bind_Registered           = FALSE;
                                 
static word RebindTimer        =0; 

#if ENABLE_RECV_WINDOW_TIMER
word RecvWindowTimer    = 0;
#endif

                                       
byte hidCmdLpUnbind =0;
bool Power_Down = FALSE;

#define LP_SLEEP_TIMER   2  //in seconds
word lpSleepTimer        = 0;
word lpSleepTimerTime    = LP_SLEEP_TIMER;

#if BACK_CHANNEL_UPLOAD                         // Added by Jason Chen, 2013.12.11
byte  upload_mode = 0;
#endif

//void radioBuiltImPkt(ACC_ENTRY * pEntry, byte type, byte index);
//byte lpGetNextIndex(ACC_ENTRY* pEntry);





byte radioUnBind(void)
{
    buzzerBeep(100);
    (void)Radio_Stack_UnBind();
    return 0;                
}
#if 0
byte CalcCRC8(byte* data_p, byte length)                       // Added by Jason Chen, 2016.03.29
{

  byte i,j;
  byte data = 0;
  
  for(j = 0; j< length; j++) 
  {
    data = data ^ *data_p++;
    for ( i = 0; i < 8; i++ )
    {
        if (( data & 0x80 ) != 0 )
        {
            data <<= 1;
            data ^= 0x07;        // Poly = 0x07;
        }
        else
        {
            data <<= 1;
        }
    }   
  } 
  
  return data; 
}
#endif
#define PKT_TYPE_SUMMARY 0x1
#define PKT_TYPE_SUMMARY_SIN 0x2

#if 0
void radioBuiltImPkt(ACC_ENTRY * pEntry, byte type, byte index)
{
        uint32_t tmp;
		uint8_t tmp2;
		static byte preIndex;
		MY_RTC tRtc;
        byte  *lpRptPkt_ptr  = (byte*)&lpImpactPkt; 
	
		if(type == PKT_TYPE_SUMMARY)
		{
		   tRtc = pEntry->t;
		   tmp = pEntry->t.StartMS + (uint16_t)500*index;
		   tRtc.StartMS = tmp%1000;
		   tmp2 = (uint8_t)(tmp/1000);		   
           rtcAdjust(&tRtc, tmp2);
		   lpImpactPkt.magic    =IMMEDIATE_SUB_TYPE_SUMMARY;
		   lpImpactPkt.Year	   = tRtc.Year;
		   lpImpactPkt.Month	   = tRtc.Month;
		   lpImpactPkt.Day 	   = tRtc.Day;
		   lpImpactPkt.Hour	   = tRtc.Hour;		 
		   lpImpactPkt.Minute	   = tRtc.Minute;
		   lpImpactPkt.Second	   = tRtc.Second;
		   lpImpactPkt.StartMS    = tRtc.StartMS; 																						   
           
		   lpImpactPkt.mag         = index; 
		   lpImpactPkt.x		   = pEntry->rtcAcc.data[index].accData.x;
		   lpImpactPkt.y		   = pEntry->rtcAcc.data[index].accData.y;
		   lpImpactPkt.z		   = pEntry->rtcAcc.data[index].accData.z;
		   
		   lpImpactPkt.alarmThres = usrProfile.tAlarm;
		   lpImpactPkt.gyro_x	   = pEntry->rtcAcc.data[index].gyroData.x;
		   lpImpactPkt.gyro_y	   = pEntry->rtcAcc.data[index].gyroData.y;
		   lpImpactPkt.gyro_z	   = pEntry->rtcAcc.data[index].gyroData.z;
		   

           lpImpactPkt.impactNum  += 1;//spiFlashCurRdBuffer.accEntryMagicCnt;      
           #if ENABLE_SUMMARY_ACK
               lpImpactPkt.crcSum = CalcCRC8(lpRptPkt_ptr,(byte)sizeof(LP_RPT_PKT)-1); 
		   #endif
		   

		}
		else if(type == PKT_TYPE_SUMMARY_SIN)
			{
           //index = lpImData.index;
           tRtc = lpImData.rtc;
		  // tmp = tRtc.StartMS + (uint32_t)500*index;
		   //tRtc.StartMS = tmp%1000;
		   //tmp2 = (uint8_t)(tmp/1000);		   
           //rtcAdjust(&tRtc, tmp2);
           
		   lpImpactPkt.magic    =IMMEDIATE_SUB_TYPE_SUMMARY;
		   lpImpactPkt.Year	   = tRtc.Year;
		   lpImpactPkt.Month	   = tRtc.Month;
		   lpImpactPkt.Day 	   = tRtc.Day;
		   lpImpactPkt.Hour	   = tRtc.Hour;		 
		   lpImpactPkt.Minute	   = tRtc.Minute;
		   lpImpactPkt.Second	   = tRtc.Second;
		   lpImpactPkt.StartMS     = tRtc.StartMS; 																						   
           
		   lpImpactPkt.mag         = index;
		   lpImpactPkt.x		   = lpImData.lpAccDataM.x;
		   lpImpactPkt.y		   = lpImData.lpAccDataM.y;
		   lpImpactPkt.z		   = lpImData.lpAccDataM.z;
		   
		   lpImpactPkt.alarmThres = usrProfile.tAlarm;
		   lpImpactPkt.gyro_x	   = lpImData.lpGyroDataM.x;
		   lpImpactPkt.gyro_y	   = lpImData.lpGyroDataM.y;
		   lpImpactPkt.gyro_z	   = lpImData.lpGyroDataM.z;



			}

		
}
#endif

byte lpGetNextIndex(uint32_t mFlag )
{
	
	    byte i;
		uint32_t bit;
		byte rI;
		//DisableInterrupts;
		
		if (mFlag == 0 )
			rI = 20;
		//else if(pEntry->iFlag & 0x1)
		else 
		{
		    
			for(i = 0;i <20 ; i++)
			{		
				bit  = (uint32_t)1 << i;
				if(mFlag & bit)
				{
					mFlag &= ~bit;
					rI	= i;
					break;
	
				}
				//if(imSummaryData.flag & bit)
				
				
			}
			if(i>=20) rI = 20;
		}
		
		
		//EnableInterrupts; 
		return rI;
		
	
	


}


byte radioTxMsg(byte type, byte *msg, word size)  
{
    byte returnValue =0;
	
    if( Radio_Stack_isBound() && !Message_Registered   ) 
    {           
      returnValue = Radio_Stack_Register_Message(msg,  size, type);            
      
      if( returnValue == RADIO_STACK_REGISTER_MESSAGE_SUCCESS ) 
      {
         Message_Registered = TRUE;  // <-------------------- Registered
                           
     #if ENABLE_RECV_WINDOW_TIMER
         Radio_Stack_TimeSet(&RecvWindowTimer, 400);          // Added by Jason Chen, 2014.04.15, leave the 250ms window for receiving  
     #endif
         
         returnValue = TRUE;
      }
    }
    return returnValue;
}

//////////////////////////////////////////////////////////
// Jason Chen
#define   RADIO_PERIODIC_FAST     8
word RadioPeriodicTime = RADIO_PERIODIC_TIME;
word UploadCount = 0;
//extern word acc2;                 // Added by Jason Chen for the control of field xmit, 2014.03.19
extern byte field_xmit_enable;      // Added by Jason Chen for the control of field xmit, 2014.03.19

byte BiByteCMDFlag = FALSE;         // Added by Jason Chen, 2014.04.09
static word RadioBiByteTimer = 0;   
static byte BiByteCMDCnt = 0;       // Added by Jason Chen, 2014.04.28
static byte rtcTime[7];             // Added by Jason Chen, 2014.04.28
static byte name_len = 0;           // Added by Jason Chen, 2014.05.01
static byte sync_req = 0;           // Added by Jason Chen, 2014.05.02
extern byte powerCommand;           // Added by Jason Chen, 2014.05.22
///////////////////////////////////////////////////////////
extern word impact_count;                                //09.11
extern byte wakeup_poweroff;        // Added by Jason Chen, 2014.12.08 



void poweroff_reset(void)                  // Added by Jason Chen, 2017.03.24 
{                             
     lpState   = LP_STATE_IDLE;
     lpCmdMail = LP_CMD_NONE;
                          
     BiByteCMDFlag = FALSE;                
     BiByteCMDCnt  = 0; 
     
     //gftInfo.lpBusy = FALSE;               
     sync_req = 0;                         
     
     Statup_flag = 0;                      
}    

void processLp(void)
{
  static byte slvTimeRequest =0;
  static word accImpactCnt =0;

  static word i=1,j = 0;

  static word txLen =0;
  byte result = 0;  
  //static ACC_ENTRY * pEntry;
  //static ACC_ENTRY * pEntryT;
  static byte test =0 ;
  byte retRadioValue = 0;                                // Added by Jason Chen for back channel, 20131201
  byte recvBuffer[CYFISNP_NODE_FCD_PAYLOAD_MAX];                                       // Jason_Chen, 20130911

  //lpInitBDs();
  if( gftInfo.lpMode & GMODE_LP_TEST) 
  {
    
	lpState = LP_STATE_TEST_WRITE;
	//Radio_Stack_PowerOn_Init();
    //gftInfo.lpBusy = TRUE;
	//gftInfo.lpStat = 0; 
	//LED_RED_On();
  }
  else if(gftInfo.lpMode == GMODE_LP_DISABLE) 
  {
     
    lpState = LP_STATE_INIT;
    return;
  }
  
  if(Radio_Stack_TimeExpired(&RadioPeriodicTimer)) 
  {
     
      (void)Radio_Stack_Usb_Ignore();
      
      //pEntry = &spiFlashCurRdBuffer.flashEntry;
      switch(lpState)
      {
          case LP_STATE_INIT:    
              Radio_Stack_PowerOn_Init();        
              //gftInfo.lpBusy = TRUE;
              lpState = LP_STATE_BIND_START;
              break;
          case LP_STATE_BIND_START:
              //Radio Init
              Radio_Stack_TimeSet(&RadioPeriodicTimer, RADIO_PERIODIC_TIME);
            //Radio_Stack_TimeSet(&RadioPeriodicTimer, RadioPeriodicTime);
            //are we bound to any hub?
              if(!Radio_Stack_isBound())
              {
                gftInfo.lpMode = GMODE_LP_BIND;
                retValue = Radio_Stack_Bind();
                if(retValue == RADIO_STACK_REGISTER_MESSAGE_SUCCESS)
                {
                   Radio_Stack_TimeSet(&RebindTimer, BIND_TIME_1);  // 30  Sec
                   Bind_Registered = TRUE;
                   
                }
                lpState = LP_STATE_BIND;
              } 
              else
              {
                myId = Radio_Stack_Node_Number();
                //send Power On 
                lpCmdMail = LP_CMD_PWR_ON;
                lpState   = LP_STATE_IDLE; 
              }  
              break;
              
          case LP_STATE_BIND:
             #if 0
               if(!KBI_USB_ON)
               {
                 gftInfo.lpMode = GMODE_LP_DISABLE;
                 usrProfile.lpEnable = GMODE_LP_DISABLE;
                 needSaveProfile = 1;
               //needReboot =1;
                 break;
               }
             #endif
               if( !Radio_Stack_isBound() ) 
               {
                
                    // not bound to a Hub
                    if( Radio_Stack_TimeExpired(&RebindTimer) ) 
                    {                        
                        lpState = LP_STATE_INIT;                        
                    }
                    
                    // no valid node number
                    myId = 0;
               } 
               else 
               {
                  // bound to a Hub
                  if( Bind_Registered ) 
                  {
                      needReboot  =1;//for(;;);   // instead of sleep we reset with WDT
                      lpCmdMail = LP_CMD_PWR_ON;
                      lpState = LP_STATE_IDLE;   
                      
                  }
                  // valid node number
                  myId = Radio_Stack_Node_Number();
               }
               
               break;
          
          case LP_STATE_IDLE:
              if(gftInfo.lpMode == GMODE_LP_BIND)
              {
                 lpState = LP_STATE_INIT;
                 lpCmdMail = LP_CMD_NONE; 
                 //gftInfo.lpBusy = TRUE;
                 
              }   
              else if(lpCmdMail == LP_CMD_PWR_ON)
              {
                  lpState=LP_STATE_ON;
                  //lpCmdMail = LP_CMD_NONE;
                  //gftInfo.lpBusy = TRUE;
              }
              else if(lpCmdMail == LP_CMD_OFF)
              {
                  lpState=LP_STATE_OFF;
                  //lpCmdMail = LP_CMD_NONE;
                  //gftInfo.lpBusy = TRUE;                         // Added by Jason Chen, 2014.05.01
              }
                           
             
              else if (lpCmdMail == LP_CMD_UNBIND)
              {
                  buzzerBeep(100);
                  (void)Radio_Stack_UnBind();
                  lpState = LP_STATE_INIT;
                  lpCmdMail = LP_CMD_NONE; 
                  //gftInfo.lpBusy = TRUE;
                  
              }
			        else if (lpCmdMail == LP_CMD_HUB_ACK)
              {
                  buzzerBeep(100);
                  lpState=LP_STATE_HUB_ACK;
                  
              }
			 
              else if(((myRTC.Year < 14)||(myRTC.Year > 50))&&(!sync_req))
              {  
                 sync_req = 1;                
                 Power_Off_Message.gid[0] = 0xFD;            //252                // Added by Jason Chen for time synchronization, 2014.05.02
                 Power_Off_Message.gid[1] = myRTC.Year;
                 Power_Off_Message.gid[2] = myRTC.Month;
                 Power_Off_Message.gid[3] = myRTC.Day;
                 Power_Off_Message.gid[4] = myRTC.Hour;
                 Power_Off_Message.gid[5] = myRTC.Minute;
                 Power_Off_Message.gid[6] = myRTC.Second;
                 Power_Off_Message.gid[7] = 0;
                 lpState=LP_STATE_IDLE;             
                 lpCmdMail = LP_CMD_OFF;
                 Radio_Stack_TimeSet(&RadioBiByteTimer, 250);         // Wait 250ms for second byte of bi-byte cmd, 2014,05.02
              }
              else if(slvTimeRequest)
              {
				         Power_Off_Message.gid[0] = 0xFC;            
                 Power_Off_Message.gid[1] = myRTC.Year;
                 Power_Off_Message.gid[2] = myRTC.Month;
                 Power_Off_Message.gid[3] = myRTC.Day;
                 Power_Off_Message.gid[4] = myRTC.Hour;
                 Power_Off_Message.gid[5] = myRTC.Minute;
                 Power_Off_Message.gid[6] = myRTC.Second;
                 Power_Off_Message.gid[7] = 0;
                 lpState=LP_STATE_IDLE;             
                 lpCmdMail = LP_CMD_OFF;

              }
			  #if 1
			        else if (spiFlashCurRdAddrI != spiFlashCurWtAddrI )
			        {
			  	     //sent impact packets
                 lpState=LP_STATE_RPT;
				 
			        }
			  #endif
			  #if 1
			        else if (spiFlashCurRdAddrP != spiFlashCurWtAddrP )
			        {
				      //sent performance packets
				        lpState=LP_STATE_BUCKET;     		
			        }
			  #endif
              else 
              {                                                        
                lpCmdMail =LP_CMD_NONE;
                //gftInfo.lpBusy = FALSE;                  
              }                  
              
              break;
         
                      
          case LP_STATE_ON:                      //  Send Power ON Packet       
              //if(Message_Registered) break;
              //spiMyFlashGetCurWtAddr();                                                      // Added by Jason Chen for reporting the number of impacts, 2014.02.10          
              //lpRptPkt.impactNum                      = spiFlashCurRdBuffer.accEntryMagicCnt;// Added by Jason Chen, 2015.01.09              
              (void)memcpy(Power_On_Message.gid, gHardInfoFlash,6);              
              (void)memcpy(Power_On_Message.name, usrProfile.name, 20);                      // 20 --> 16 changed by Jason Chen, 2013.11.18
                            
              Power_On_Message.pn = usrProfile.playerNo;              
              Power_On_Message.batRawAverage._word  = batData1;           // added battery level value into Power on Packet              
              
              if(max17047Exist)
                Power_On_Message.batFuelAverage._word = max17047Voltage;                    // added by Jason Chen, battery level value into Power on Packet using Fuel gauge, 2013.11.18
              else if(max14676Exist)
              	{
                Power_On_Message.batFuelAverage._word = max14676Voltage;                    // added by Jason Chen, New Fuel gauge, 2016.01.16
                Power_On_Message.batPT = max14676Charge;
              	}
              else
                Power_On_Message.batFuelAverage._word = 0;                
              
              //Power_On_Message.impactCnt._word      = spiFlashCurRdBuffer.accEntryMagicCnt; // added by Jason Chen, Impact Number into Power on Packet,   2013.12.16              
              Power_On_Message.gftLpMode            = gftInfo.lpMode & 0x7F;
			        (void)memcpy(Power_On_Message.loc, usrProfile.loc, 9); 
              if(KBI_USB_ON)
                Power_On_Message.gftLpMode         = gftInfo.lpMode | 0x80;               
              
              Power_On_Message.alarmThres             = usrProfile.tAlarm;
              Power_On_Message.m17047Exist            = max17047Exist|max14676Exist;
               
              Power_On_Message.RecThres               = usrProfile.tR;                         // added by Jason Chen, 2013.04.29
                                          
              if(!usrProfile.pwrMode)
                Power_On_Message.AlarmMode             = usrProfile.AlarmMode & 0xF7;// added by Jason Chen, 2013.04.30
              else
                Power_On_Message.AlarmMode             = usrProfile.AlarmMode | 0x08;// added by Jason Chen, 2013.04.30
                
              if(!field_xmit_enable)
                Power_On_Message.AlarmMode            &= 0xFB;// added by Jason Chen, 2013.05.21
              else
                Power_On_Message.AlarmMode            |= 0x04;// added by Jason Chen, 2013.05.21
                
              if(!usrProfile.proxEnable)
                Power_On_Message.AlarmMode            &= 0xFD;// added by Jason Chen, 2013.05.21
              else
                Power_On_Message.AlarmMode            |= 0x02;// added by Jason Chen, 2013.05.21                
                
                
              Power_On_Message.mntType= usrProfile.mntLoc;
                  
                                                          
              retValue = radioTxMsg((byte)MESSAGE_TYPE_POWER_ON,(byte *)(&Power_On_Message),sizeof(LP_PWR_ON_PKT));
              
              if(retValue)
              {
                lpState   = LP_STATE_IDLE;
                lpCmdMail = LP_CMD_NONE;                              
                RadioPeriodicTime = RADIO_PERIODIC_FAST;                                    // Speed up "Run" for receiving BCD CMD
              }
              else 
              {                
                lpState=LP_STATE_TRY_AGAIN_DLY_SET;
              }
              break;
  
          case LP_STATE_RPT:               // Immediate data
               
               if(Message_Registered) break;
                                             
               result = spiFlashReadAccEntryI(spiFlashCurRdAddrI);
                              
               if (result == SPIFLASH_RD_SUCESS)
               {
                       
                  
                   LED_RED_Off();
                   retValue = radioTxMsg((byte)MESSAGE_TYPE_HIT_DATA, (byte *)&impactToRead, sizeof(FLASH_DATA_ENTRY_I));                               
                   imSleepTimerTime = IM_SLEEP_TIMER;      
				         //for clearing Sleep timer
                   mTimer_TimeSet( &imSleepTimer );          
                   
                   if(retValue)
                   {
                      lpState=LP_STATE_IDLE;
                      (void)spiFlashFlagEntry(spiFlashCurRdAddrI+2);
                      spiFlashCurRdAddrI += sizeof(FLASH_DATA_ENTRY_I);
					  
                     
                   }
                   else
                   {                   
                    
                    lpState=LP_STATE_TRY_AGAIN_DLY_SET;
                   
                     
                   }
               }                                      
               else
               {                                
                    lpState = LP_STATE_IDLE;
                              
                    // leave the 250ms window for receiving  
                    Radio_Stack_TimeSet(&RecvWindowTimer, 600);          
               }
               break;
		      case LP_STATE_BUCKET:
		   	       if(Message_Registered) break;
                                             
              
               result = spiFlashReadAccEntryP(spiFlashCurRdAddrP);
               
                              
               if (result == SPIFLASH_RD_SUCESS)
               {
                       
                  
                   retValue = radioTxMsg((byte)MESSAGE_TYPE_IMMEDIATE,(byte *)(&performToRead),sizeof(FLASH_DATA_ENTRY_P));              
                   //retValue = radioTxMsg((byte)MESSAGE_TYPE_HIT_DATA,(byte *)(void *)&performToRead,sizeof(FLASH_DATA_ENTRY_P));                               
                   imSleepTimerTime = IM_SLEEP_TIMER;      
				  //  for clearing Sleep timer
                   mTimer_TimeSet( &imSleepTimer );          
                   
                   if(retValue)
                   {
                     lpState=LP_STATE_IDLE;
                     (void)spiFlashFlagEntry(spiFlashCurRdAddrP+2);
                     spiFlashCurRdAddrP += sizeof(FLASH_DATA_ENTRY_P);
                     
                   }
                   else
                   {                   
                    
                    lpState=LP_STATE_TRY_AGAIN_DLY_SET;
                   
                     
                   }
               }                                      
               else
               {                                
                                      
                    lpState = LP_STATE_IDLE;
                    // leave the 250ms window for receiving  
                    Radio_Stack_TimeSet(&RecvWindowTimer, 600);          
               }
               break;
		  
         
          case LP_STATE_OFF:
             // if(Message_Registered) break;
#if BACK_CHANNEL_UPLOAD          
              if(Power_Off_Message.gid[0] == 0x00) 
                   Radio_Stack_Abort();
              else if(Power_Off_Message.gid[0] == 0x80) //
              {        
              
                Power_Off_Message.gid[0]   = Power_Off_Message.gid[0]&0xFE;                 
                if(KBI_USB_ON)             
                  Power_Off_Message.gid[0] = Power_Off_Message.gid[0]|0x01;
              
                //spiMyFlashGetCurWtAddr();                                                               // Added by Jason Chen for reporting the number of impacts, 2014.03.14          
                Power_Off_Message.gid[1] = max17047Exist|max14676Exist;;//(byte)(spiFlashCurRdBuffer.accEntryCnt>>8);               
                Power_Off_Message.gid[2] = (byte)(batData1>>8);                                         // added battery level value into the special Packet, 20140314
                Power_Off_Message.gid[3] = (byte)batData1;
                //Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
                // Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt; 
                                                            
                //Power_Off_Message.accEntryCnt = spiFlashCurRdBuffer.accEntryCnt;           // Added by Jason Chen, 2016.01.11
                Power_Off_Message.activeTime = gActiveTime;                                // Added by Jason Chen, 2015.02.06
                 if(max17047Exist) 
                 {                      
                    Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8); 
                    Power_Off_Message.gid[7] = (byte)max17047Voltage;                                                 
                 } 
                 else if(max14676Exist)                                                     // Added by Jason Chen, 2016.01.11
                 {                                                                          // Added by Jason Chen, 2016.01.11
                    Power_Off_Message.gid[6] = (byte)(max14676Voltage>>8);                  // Added by Jason Chen, 2016.01.11
                    Power_Off_Message.gid[7] = (byte)max14676Voltage;                       // Added by Jason Chen, 2016.01.11         
                    Power_Off_Message.batPT =  (byte)max14676Charge;
                 }  
				       //update rpe
				       //gftInfo.rpe =gftInfo.load / (totalSecond/10);
				         Power_Off_Message.load =gftInfo.load;
				         Power_Off_Message.rpe = gftInfo.rpe;
              }

              retValue = radioTxMsg((byte)MESSAGE_TYPE_POWER_OFF,(byte *)(&Power_Off_Message),sizeof(LP_PWR_OFF_PKT));            
              
              if(retValue)
              {                                                           
                lpState=LP_STATE_IDLE;
                lpCmdMail = LP_CMD_NONE;
                
                if(KBI_USB_ON) 
                {                 
                  if(Power_Off_Message.gid[0] != 0x00) 
                  {                                  
                     if(Power_Off_Message.gid[0] == 0x55) 
                     {
                        upload_mode = 1;
                        lpCmdMail = LP_CMD_UPLOAD;                       
                        UploadCount = 0;
                        
                    		#if DISABLE_RECORDING_DURING_DOWNLOADING	
            		    	  field_xmit_enable = 0;                     // Disable recording during Downlodinh, 2015.11.03
                    	  #endif                                                
                     }
                     else if(Power_Off_Message.gid[0] == 0x57)     // Added by Jason Chen for GUI side force  GFT to reboot after unbinding
                     {
    		                
                        lpState=LP_STATE_IDLE;    
                        lpCmdMail = LP_CMD_NONE; 
                        //gftInfo.lpBusy = TRUE;
                     }                     
                  }               
                } 
                else                                              // Added by Jason Chen for GUI side force  GFT to upload data 
                {
                  if(Power_Off_Message.gid[0] == 0x56)      
                  {
                     upload_mode = 1;
                     lpCmdMail = LP_CMD_UPLOAD;
                     
                     UploadCount = 0;
                     
                     #if DISABLE_RECORDING_DURING_DOWNLOADING	
            		     field_xmit_enable = 0;                       // Disable recording during Downlodinh, 2015.11.03
                     #endif                                             
                  } 
                  else if(Power_Off_Message.gid[0] == 0x57)       // Added by Jason Chen for GUI side force  GFT to reboot after unbinding
                  {
  		                
                      lpState=LP_STATE_IDLE;
                      lpCmdMail = LP_CMD_NONE; 
                      //gftInfo.lpBusy = TRUE;
                  }                  
                }                                                        
              } 
#else              
              Radio_Stack_Abort();
              //Power_On_Message.len = 0;             
              retValue = radioTxMsg((byte)MESSAGE_TYPE_POWER_OFF,(byte *)(&Power_Off_Message),sizeof(LP_PWR_OFF_PKT));
              if(retValue)
              {
                lpState=LP_STATE_IDLE;
                lpCmdMail = LP_CMD_NONE;                                
              } 
#endif              
              
              else
                lpState=LP_STATE_TRY_AGAIN_DLY_SET;
              break;    
		  case LP_STATE_HUB_ACK:

		      break;
          case LP_STATE_TRY_AGAIN_DLY_SET:
              //mTimer_TimeSet( &imSleepTimer );                                // removed by Jason Chen for clearing IM Sleep timer, 2014.05.26
              mTimer_TimeSet(&lpSleepTimer );
              lpState=LP_STATE_TRY_AGAIN_DLY;
              break;             
          case LP_STATE_TRY_AGAIN_DLY:
              //mTimer_TimeSet( &imSleepTimer );                                // removed by Jason Chen for clearing IM Sleep timer, 2014.05.26
              if(mTimer_TimeExpired( &lpSleepTimer, lpSleepTimerTime ) ) {                
                lpState=LP_STATE_IDLE;
                
              }
              break;
           case LP_STATE_TEST_WRITE:
		  	 // if(Message_Registered) break;
		  	  test  = Radio_Stack_Loop_Test();
		  	  if(test)
		  	  {
		  	    
			  	lpState=LP_STATE_IDLE;
				gftInfo.lpMode &= ~GMODE_LP_TEST;

		  	  }
			  
			  break;    
          default:
              lpState=LP_STATE_IDLE;
              break;
      } 
            
    //radio stack heatbeat        		
    //every 35-50mSecond
    //Radio_Stack_TimeSet(&RadioPeriodicTimer, RADIO_PERIODIC_TIME);
      Radio_Stack_TimeSet(&RadioPeriodicTimer, RadioPeriodicTime);        
               
           
      retRadioValue = Radio_Stack_Periodic(recvBuffer); // Jason_Chen, 20130911
                
      if( retRadioValue && (gftInfo.lpMode != GMODE_LP_BIND)) 
      {
          if(Statup_flag)                                          // Added by Jason Chen, 2017.03.31
            imSleepTimerTime = 75;                                 // Added by Jason Chen, 2017.03.31
          else                                                     // Added by Jason Chen, 2017.03.31
            imSleepTimerTime = IM_SLEEP_TIMER;                    // Added by Jason Chen, 2014.12.08
          mTimer_TimeSet( &imSleepTimer );                         // Added by Jason Chen for clearing Sleep timer, 2014.01.15          
          //if(BiByteCMDFlag == FALSE)
          if(!wakeup_poweroff)                                     // Game mode // Added by Jason Chen, 2014.12.08
          {
            
            if(!BiByteCMDCnt)
            {                                                                   
                
			 
              if((recvBuffer[0]&0xFE) == 0x12 )   // 18,19,Added by Jason Chen for GUI side force  GFT to prepare receiving UPLOAD CMD, 20140408
              {             
                 //mTimer_TimeSet( &imSleepTimer );                             
                   gftInfo.uploadflag = recvBuffer[0]&0x01;
                    
                   lpState=LP_STATE_IDLE;             
                   lpCmdMail = LP_CMD_NONE;//LP_CMD_OFF
              }  
             
              else if(recvBuffer[0] == 0x16 )          //22       // Added by Jason Chen for powering off gft, 2014.05.22
              {     
                  ResetADCTimer();                                               // Added by Jason Chen, 2016.01.15                
                  powerCommand = 1;                                      
              }  
                    
              else if(recvBuffer[0] == 0xBF )           //191        Erase Flash
              {
               //if(KBI_USB_ON)    ??
                 {                
                     usrProfile.AlarmMode &= ~PROFILE_ALARM_ON_MSK;
                   //pRpt->h = MHID_CMD_ERASE_ACC_DATA;	
    	             //pRpt->cnt = 1;
     		           //pRpt->data[0] = 'S';
      		           //spiFlashCurWtAddr =0;
      		           needEraseFlash = 1;
      		           needSaveProfile =1; 
      		               		    
      		           ResetADCTimer();                                     // Added by Jason Chen, 2016.01.15        
      		         //needReboot = 1;    		           
      		         //powerCommand = 1;                                    // Replaced " needReboot = 1 ", by Jason Chen, 2014.08.05
                     powerCommand = 3;                                    // Replaced " powerCommand = 1 ", indicate the power off after erased, 2016.01.11     
                     if(spiFlashNeedErase())
	 	                 spiFlashEraseFlash();
                 }           
              } 
              else if(recvBuffer[0] == 0xD0)                                //208, request tx impact  
              { 
                 //buzzerBeep(100);
                
				 if(recvBuffer[1] ==0x22) 
				 	buzzerBeep(100);
                 BiByteCMDCnt  = 1;
                 BiByteCMDFlag = recvBuffer[0];

                 Power_Off_Message.gid[0] = 0xD0;              // 0x208
                 Power_Off_Message.gid[1] = field_xmit_enable;              
                 
                 if(!usrProfile.pwrMode)
                   Power_Off_Message.gid[2]             = usrProfile.AlarmMode & 0xF7;// added by Jason Chen, 2013.04.30
                 else
                   Power_Off_Message.gid[2]             = usrProfile.AlarmMode | 0x08;// added by Jason Chen, 2013.04.30
                   
                 Power_Off_Message.gid[3] = gftInfo.uploadflag;//usrProfile.pwrMode;             
                
                 
                 if(KBI_USB_ON)
                   Power_Off_Message.gid[3]             |= 0x80;            
                 
                 lpState=LP_STATE_IDLE;             
                 lpCmdMail = LP_CMD_OFF;                             
                 Radio_Stack_TimeSet(&RadioBiByteTimer, 250);         // Wait 250ms for second byte of bi-byte cmd, 2014,04.09
                 
              }            
              else if(recvBuffer[0] == 0xD1)                    //209    Setup Threshold , 2014.04.15
              {
                 BiByteCMDCnt  = 1;
                 BiByteCMDFlag = recvBuffer[0];

                 Power_Off_Message.gid[0] = 0xD1;            // 209
                 Power_Off_Message.gid[1] = 1;//usrProfile.realXmitEnable;              
                 
                 if(!usrProfile.pwrMode)
                   Power_Off_Message.gid[2]             = usrProfile.AlarmMode & 0xF7;// added by Jason Chen, 2013.04.30
                 else
                   Power_Off_Message.gid[2]             = usrProfile.AlarmMode | 0x08;// added by Jason Chen, 2013.04.30
                 
                 Power_Off_Message.gid[3] = gftInfo.uploadflag;//usrProfile.pwrMode;                                            
                
                 if(KBI_USB_ON)
                   Power_Off_Message.gid[3]             |= 0x80;            
                 
                 lpState=LP_STATE_IDLE;             
                 lpCmdMail = LP_CMD_OFF;                             
                 Radio_Stack_TimeSet(&RadioBiByteTimer, 250);         // Wait 250ms for second byte of bi-byte cmd, 2014,04.09
                 
              }
              else if(recvBuffer[0] == 0xD2)                    //210    Sychronize GFT Time with PC , 2014.04.28
              {
                 BiByteCMDFlag  = recvBuffer[0];
                 BiByteCMDCnt   = 1;

                 Power_Off_Message.gid[0] = 0xD2;            //210
                 Power_Off_Message.gid[1] = 1;Power_Off_Message.gid[2] = 0;Power_Off_Message.gid[3] = 0;
                 Power_Off_Message.gid[4] = 0;Power_Off_Message.gid[5] = 0;Power_Off_Message.gid[6] = 0;
                 Power_Off_Message.gid[7] = 0;               
                 
                 lpState=LP_STATE_IDLE;             
                 lpCmdMail = LP_CMD_OFF;                             
                 Radio_Stack_TimeSet(&RadioBiByteTimer, 300);         // Wait 250ms for second byte of bi-byte cmd, 2014,04.09               
                 //gftInfo.lpBusy = TRUE;                               // Added by Jason Chen, 2014.05.01
              }            
                                          
             
            } 


            else if(BiByteCMDFlag == 0xD0)                     // Setup Player number  , 2014.04.09
            {          
               BiByteCMDCnt = 0;
               BiByteCMDFlag = FALSE;
               
               slvTimeRequest =1;
                            
  	           
            }           
            else if(BiByteCMDFlag == 0xD1)                     // Setup Threshold , 2014.04.15
            {          
               BiByteCMDCnt = 0;
               BiByteCMDFlag = FALSE;
               //rtcAdjust(recvBuffer);
                                                                                             
            }
			
            else if(BiByteCMDFlag == 0xD2)                     // Sychronize GFT Time with PC , 2014.04.28
            {          
               //BiByteCMDFlag = 0xD2;                          //?????????????????????????????????????????

               if(BiByteCMDCnt == 1) {
           		   rtcTime[0] = recvBuffer[0];
                 Power_Off_Message.gid[2] = rtcTime[0];//myRTC.Year;
               }
               else if(BiByteCMDCnt == 2) {              
                 rtcTime[1] = recvBuffer[0];
                 Power_Off_Message.gid[3] = rtcTime[1];//myRTC.Year;
               }
               else if(BiByteCMDCnt == 3) {              
                 rtcTime[2]   = recvBuffer[0];
                 Power_Off_Message.gid[4] = rtcTime[2];//myRTC.Year;
               }
               else if(BiByteCMDCnt == 4) {              
                 rtcTime[4] = recvBuffer[0]; 
                 Power_Off_Message.gid[5] = rtcTime[4];//myRTC.Year;             
               }
               else if(BiByteCMDCnt == 5) {              
                 rtcTime[5] = recvBuffer[0];
                 Power_Off_Message.gid[6] = rtcTime[5];//myRTC.Year;             
               }
               else if(BiByteCMDCnt == 6) {              
                 rtcTime[6] = recvBuffer[0];
                 Power_Off_Message.gid[7] = rtcTime[6];//myRTC.Year;                            
               }
                                        
               BiByteCMDCnt++;
               if(BiByteCMDCnt > 6) {
                 BiByteCMDCnt = 0;
                 BiByteCMDFlag = FALSE;
                 
                //before update let save the time for time correction on pc side
			    (void)memcpy((byte *)&lostRTC, (void *)&myRTC, sizeof(MY_RTC));                // Added by Jason Chen, 2015.02.06
                 
               
                 myRTC.Year  = rtcTime[0];
              	 myRTC.Month = rtcTime[1];
              	 myRTC.Day   = rtcTime[2];
              	 rtcTime[3]  = 0;
              	 myRTC.Hour  = rtcTime[4];
              	 myRTC.Minute= rtcTime[5];
              	 myRTC.Second= rtcTime[6];
                                                                        
              	 Set_YearMonthDay((byte *)&rtcTime);    
                 (void)memcpy((byte *)&newRTC, (void *)&myRTC, sizeof(MY_RTC));              	 // Added by Jason Chen, 2015.02.06
                 g_Tick1mS =0;
                                                                               
    	           //spiFlashCurWtAddr =0;
       		       //needSaveProfile =1;  
       		       //needReboot = 1;     		            		            		            		            		            		      
               } 
               {
                 Power_Off_Message.gid[0] = 0xD2;            //210              
                 Power_Off_Message.gid[1] = BiByteCMDCnt;  
                 Power_Off_Message.gid[2] = myRTC.Year;
                 Power_Off_Message.gid[3] = myRTC.Month;
                 Power_Off_Message.gid[4] = myRTC.Day;
                 Power_Off_Message.gid[5] = myRTC.Hour;
                 Power_Off_Message.gid[6] = myRTC.Minute;
                 Power_Off_Message.gid[7] = myRTC.Second;                                                                        
                 
                 lpState=LP_STATE_IDLE;             
                 lpCmdMail = LP_CMD_OFF;
                 Radio_Stack_TimeSet(&RadioBiByteTimer, 2500);         // Wait 500ms for second byte of bi-byte cmd, 2014,04.28
                 //gftInfo.lpBusy = TRUE;                               // Added by Jason Chen, 2014.05.01
               }
            }
         
            else 
            {                                  
                 BiByteCMDFlag = FALSE;                // Clear bi-byte cmd flag                       
                 BiByteCMDCnt  = 0; 
            }          
            RadioPeriodicTime = RADIO_PERIODIC_TIME;                // Added by Jason Chen for Run Speed Changed when need to get CMD, 20140212
          } 
          else  // Power off mode                                    
          {
             
              if((recvBuffer[0] == 0x17)&&(!BiByteCMDCnt))             //23 just receive Power on Command in power off mode  
              {   
                  //mMail ='P';
                  __asm("BGND");                                      
              }                                      

          }
      } 
      else 
      {      
          if(BiByteCMDFlag)                      
          {            
            if(Radio_Stack_TimeExpired(&RadioBiByteTimer)) 
            {  
               Power_Off_Message.gid[0] = 0xFE;                                              
               Power_Off_Message.gid[1] = BiByteCMDCnt;              
               Power_Off_Message.gid[2] = BiByteCMDFlag;
               Power_Off_Message.gid[3] = gftInfo.uploadflag;
               if(KBI_USB_ON)
                 Power_Off_Message.gid[3]             |= 0x80;                            
               //Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
               //Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;;  
               //Power_Off_Message.accEntryCnt = spiFlashCurRdBuffer.accEntryCnt;                        // Added by Jason Chen, 2015.09.30                             
               
               
               lpState=LP_STATE_IDLE;             
               lpCmdMail = LP_CMD_OFF;               // 0x56 for testing, 20140107
               
               BiByteCMDFlag = FALSE;                // Clear bi-byte cmd flag                       
               BiByteCMDCnt  = 0; 
               
               //gftInfo.lpBusy = FALSE;               // Added by Jason Chen, 2014.05.01
               sync_req = 0;                         // Added by Jason Chen, 2014.05.02
            }             
          }
      }
                   
      if( Radio_Stack_isBound() && Message_Registered && (Radio_Stack_State() == RADIO_STACK_IDLE))
      {        
          Message_Registered = FALSE;
      }
        
  }
   
}

#endif
