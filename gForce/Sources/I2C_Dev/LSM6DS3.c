/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West
 * Markham, ON L3R 3J9
 * Canada
 *
 * Tel:   (905) 470-0109
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/


#include "I2C_driver.h"
#include "LSM6DS3.h"

#ifdef GYROS_SPI_CS
  #include "GYRO_SPI_CS.h" 
#endif
#include "mcu.h"
#include "typedef.h"

#include "bcd_common.h"                          // Added by Jason Chen, 2014.05.07

#define CONFIG_GYRO_USE_FIFO

static char FIFO_Src       = 0;
static char FIFO_Ovr       = 0;

/*******************************************************************************
* Function Name  : SPI_BYTE_IO
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI2 bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
static byte SPI2_BYTE_IO(byte dataByte)
{
#if 1
  while( !SPI2S_SPTEF );   // SPI Transmit Buffer Empty Flag
  SPI2DL = dataByte;      // send the data

  // wait while no data available in the receive data buffer
  while( !SPI2S_SPRF );    // SPI Read Buffer Full Flag
  return (byte)SPI2DL;     // return the received data
#else
  byte Chr;
  while(SPIM_Acc_SendChar(data_byte)!=ERR_OK);
  while(SPIM_Acc_GetCharsInRxBuf() == 0);
  while(SPIM_Acc_RecvChar(&Chr)!= ERR_OK);
  return Chr;
#endif
  
}


/*******************************************************************************
* Function Name  : Gyroscope SPI_BYTE/Buffer Read
* Description    : read a numByteToRead bytes from SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/

#if 0
/*******************************************************************************
* Function Name  : FifoBypassToStream
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void gyro_BypassToStream(void) 
{
  byte data;
  
  // Clear Interrupt if LIR enabled
  //do {
    data = gyro_SPIReadReg(GYRO_INT1_SRC);    
  //} while( (data & INT_SRC_IA) );
  
  // Interrupt spend 1/2 samples to go low!!!
			
  data = gyro_SPIReadReg(GYRO_CTRL_REG5);
  data = data | CTRL_REG5_FIFO_EN;
  gyro_SPIWriteReg(GYRO_CTRL_REG5,data);
  
  data = gyro_SPIReadReg(GYRO_FIFO_CTRL_REG);
  data = (data & FIFO_CONTROL_WTMSAMP) | DEF_FIFO_CTRL_BYPASS_TO_STREAM;
  gyro_SPIWriteReg(GYRO_FIFO_CTRL_REG,data);  
}

#endif

 /*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Read
* Description    : read a numByteToRead bytes from SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/

void acc_SPIBufferRead(byte* pBuffer, byte readAddr, byte numByteToRead)
{ 
  byte i;
  
  readAddr |= 0x80;
  //if(numByteToRead > 1) 
  //    readAddr |= 0x40;           // Auto Incremental register Address
  
  ACC_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(readAddr);
  for(i = 0;i<numByteToRead;i++) pBuffer[i] = SPI2_BYTE_IO(Dummy_Byte);  
  ACC_SPI_CS_HIGH();
}

void acc_FIFO_SPIGetRawData(byte *pBuffer) 
{
  byte data;
  
  acc_SPIBufferRead(pBuffer, LSM6DS3_ACCEL_OUT_X_L_ADDR, 6); 
  
  data       = pBuffer[1];
  pBuffer[1] = pBuffer[0];
  pBuffer[0] = data;
  
  data       = pBuffer[3];
  pBuffer[3] = pBuffer[2];
  pBuffer[2] = data;  
  
  data       = pBuffer[5];
  pBuffer[5] = pBuffer[4];
  pBuffer[4] = data; 
  
   
  
  
   
}


void gyro_FIFO_SPIGetRawData(uint8_t *pBuffer)
{
	 byte data;
	  
	  acc_SPIBufferRead(pBuffer, LSM6DS3_GYRO_OUT_X_L_ADDR, 6); 
	  
	  data		 = pBuffer[1];
	  pBuffer[1] = pBuffer[0];
	  pBuffer[0] = data;
	  
	  data		 = pBuffer[3];
	  pBuffer[3] = pBuffer[2];
	  pBuffer[2] = data;  
	  
	  data		 = pBuffer[5];
	  pBuffer[5] = pBuffer[4];
	  pBuffer[4] = data; 
	  



}

byte acc_SPIReadReg(byte regAddr) 
{
  byte readValue;
  
  acc_SPIBufferRead(&readValue, regAddr, 1);
  
  return readValue; 
    
}

/*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Write
* Description    : write a Byte to SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  WriteAddr is the register address you want to write to
*                  pBuffer contains bytes to write
* Output         : None
* Return         : None
*******************************************************************************/
void acc_SPIWriteReg(byte regAddr, byte regValue)
{    
  ACC_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(regAddr);
  (void)SPI2_BYTE_IO(regValue);  
  ACC_SPI_CS_HIGH();
 }


void acc_SPIBufferWrite(byte regAddr,byte *writeBuf, byte numByteToWrite) 
{
  byte i;
  
  if(numByteToWrite > 1)
    regAddr |= 0x40;
  
  ACC_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(regAddr);
  for(i = 0;i<numByteToWrite;i++) (void)SPI2_BYTE_IO(writeBuf[i]);  
  ACC_SPI_CS_HIGH();
}



/*******************************************************************************
* Function Name  : SPI2_MEMS_Init
* Description    : I2C Initialization
* Input          : None 
* Output         : None
* Return         : None
*******************************************************************************/
#define INT1_THRESHOLD_VALUE   12                                   //  125 * 12/1000 = 1.5g
void SPI_MEMS_Acc_Config(byte mode)
{
  volatile byte abc=0;  

//if(mode == 0) 
  {
  #if 0
   //acc_SPIWriteReg(ACC_CTRL_REG5,FIFO_STREAM_A);            // Reboot this acc each time restart, 2014.05.07
    Cpu_Delay100US(1000);                                      // wait a while make it working.
   
    abc =  acc_SPIReadReg(0x0F);
   // CTRL_REG1 
    acc_SPIWriteReg(LSM6DS3_ACCEL_AXIS_EN_ADDR,  0x38);        //enable acc x,y,z  
    
  //acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x08);        // 416hz,4g range
  //acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x0C);        // 416hz,16g range
    acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x04);        // 416hz,16g range
   
    //  interrupt pin assignment
    acc_SPIWriteReg(LSM6DS3_MD1_ADDR,        0x04);    
    acc_SPIWriteReg(LSM6DS3_WAKE_UP_THS_ADDR,0x84|0x06);
    if(mode)
  	    acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x08);    // 416hz,4g range   
  #else
    // Added by Jason Chen
                                                                // double-tap event, 6D even and tilt event 
     acc_SPIWriteReg(LSM6DS3_CTRL3_C_ADDR,    0x81);            // Reboot this device and reset memmery each time restart, Recommended by Jason Chen, 2016.01.13,LSM6DS3_BDU_ADDR
     Cpu_Delay100US(1000);                                      // wait a while make it working.  
     abc =  acc_SPIReadReg(0x0F);
  // CTRL_REG1 
     acc_SPIWriteReg(LSM6DS3_CTRL9_XL_ADDR,   0x38);            // enable acc x,y,z  
          
     // Wake-up and 6D Interrupt configr=uration                                                           
     acc_SPIWriteReg(LSM6DS3_CTRL1_XL_ADDR,   0x64);            // 416hz,16g range                                    
     acc_SPIWriteReg(LSM6DS3_CTRL8_XL_ADDR,   0x01);            // Low-pass filter on 6D function selection for 6D event recognition                                    
     acc_SPIWriteReg(LSM6DS3_TAP_CFG_ADDR,    0x10);            // Enable accelerometer HP and LPF2 filters for wakeup event, no interrupt latched    
     acc_SPIWriteReg(LSM6DS3_WAKE_UP_DUR_ADDR,0x00);            // No duration
     acc_SPIWriteReg(LSM6DS3_WAKE_UP_THS_ADDR,0x02);            // Disable Single-tap and Double-tap, and Threshold for wake up --> 2 = 2*ODR_XL time                                                              
     acc_SPIWriteReg(LSM6DS3_TAP_THS_6D,      0x61);            // 6D Threshold Value = 50 degree, Tap threshed 1 ---> ? value
     acc_SPIWriteReg(LSM6DS3_MD1_CFG_ADDR,    0x24);            // Rounting on INT1 of wakeup event, 6D event  
    if(mode)
  	    acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x08);    // 416hz,4g range        
  #endif
  }   
}

void SPI_MEMS_Gyro_Config(void)
{        
#ifdef CONFIG_GYRO_USE_FIFO
       
   //volatile uint8_t mBuffer[6];
   //volatile uint8_t a;
  // acc_SPIWriteReg(LSM6DS3_GYRO_ODR_ADDR,  0x6C);    // 416hz 2000dps
   acc_SPIWriteReg(LSM6DS3_GYRO_ODR_ADDR,  0x7C);    // 833hz 2000dps
   //fifo setup
   acc_SPIWriteReg(LSM6DS3_FIFO_THR_L_ADDR,1);
   //acc_SPIWriteReg(LSM6DS3_FIFO_THR_H_ADDR,0x0);
   acc_SPIWriteReg(LSM6DS3_INT2_ON_INT1_ADDR,0x5); //Stop_on_FTH , and I2C disabled
   acc_SPIWriteReg(LSM6DS3_FIFO_CTRL3_ADDR,0x0); //decimation: 0 , and Accelerometer sensor not in FIFO
   acc_SPIWriteReg(LSM6DS3_FIFO_MODE_ADDR,0x3e); //continue ;833hz
   
   acc_SPIWriteReg(LSM6DS3_FUNC_EN_ADDR,0x38);  // Gyro x,y,z output enabled
#else
   acc_SPIWriteReg(LSM6DS3_GYRO_ODR_ADDR,  0x7C); // 833hz 2000dps
#endif
}



