/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
  * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#ifndef _MAX4400000_H_
#define _MAX4400000_H_

#include "typedef.h"
// FIFO CONFIGURATIONS //

#define BIT(x) ( 1<<(x) )

#define PROXIMITY_DEV_ADDR 0x4a    // MAX44000 slave address 0x94

#define PROXIMITY_RSTx

#ifdef PROXIMITY_RST
  #define CAP_RST_HIGH()  CAP_RST_SetVal()
  #define CAP_RST_LOW()   CAP_RST_ClrVal()
#else
  #define CAP_RST_HIGH()
  #define CAP_RST_LOW()
#endif  

// I2C Register Address
enum
{
    PROX_INT_STATUS   = 0x00,                  // Status register
    PROX_MAIN_CFG     = 0x01,                  // Main Configuration
    PROX_RECV_CFG     = 0x02,                  // Receive Configuration
    PROX_XMIT_CFG     = 0x03,                  // Transmit Configuration
    PROX_ADCALS_HIGH  = 0x04,                  // ADC High Byte
    PROX_ADCALS_LOW   = 0x05,                  // ADC Low Byte
    PROX_ADCPROX_BYTE = 0x16,                  // ADC Byte (PROX)
    PROX_ALS_UPTHR_H  = 0x06,                  // ALS Upeer Threshold High Byte
	  PROX_ALS_UPTHR_L  = 0x07,                  // ALS Upper Threshold Low Byte
	  PROX_ALS_LOTHR_H  = 0x08,                  // ALS Lower Threshold High Byte
	  PROX_ALS_LOTHR_L  = 0x09,                  // ALS Lower Threshold Low Byte
	
	  PROX_THR_PERTIMER = 0x0A,                  // THreshold Persist Timer
	  PROX_THR_INDICATOR= 0x0B,                  // PROX Threshold Indicator
	  PROX_THRESHOLD    = 0x0C,                  // PROX Threshold
	
	  PROX_GRN_TRIM     = 0x0F,                  // Digital Gain Trim of Green Channel
    PROX_INFRARED_TRIM= 0x10,                  // Digital Gain Trim of Infrared Channel
};

enum
{
    PROX_DRIVER_DISABLE = 0x00,                  
    PROX_DRIVER_10mA    = 0x01,
    PROX_DRIVER_20mA    = 0x02,
    PROX_DRIVER_30mA    = 0x03,
    PROX_DRIVER_40mA    = 0x04,
    PROX_DRIVER_50mA    = 0x05,
    PROX_DRIVER_60mA    = 0x06,
    PROX_DRIVER_70mA    = 0x07,
    PROX_DRIVER_80mA    = 0x0C,
    PROX_DRIVER_90mA    = 0x0D,
    PROX_DRIVER_100mA   = 0x0E,
    PROX_DRIVER_110mA   = 0x0F,
};

enum
{
    PROX_TRIGER_01CONSECUTIVE  = 0x00,
    PROX_TRIGER_04CONSECUTIVE  = 0x04,
    PROX_TRIGER_08CONSECUTIVE  = 0x08,
    PROX_TRIGER_16CONSECUTIVE  = 0x0C,
};

// Interrupt Status Register
#define IRQ_STATUS_PWRON              BIT(3)
#define IRQ_STATUS_PRXINTS            BIT(1)
#define IRQ_STATUS_ALSINTS            BIT(0)

// Main Configuration Register
#define TRIM_FACTORY_GAIN             BIT(5)
#define MODE_SHUTDOWN                 (0x00 << 2)
#define MODE_ALS_GIR                  (0x01 << 2)
#define MODE_ALS_G                    (0x02 << 2)
#define MODE_ALS_IR                   (0x03 << 2)
#define MODE_ALS_PROX                 (0x04 << 2)
#define MODE_PROX_ONLY                (0x05 << 2)
#define INTERUPT_PRXINTE              BIT(1)
#define INTERUPT_ALSINTE              BIT(0)


// Receive Configuration
#define ADC_ALS_14BITS                ( 0x00 << 2 )
#define ADC_ALS_12BITS                ( 0x01 << 2 )
#define ADC_ALS_10BITS                ( 0x02 << 2 )
#define ADC_ALS_8BITS                 ( 0x03 << 2 )
#define ADC_ALS_MEAS_GAIN(x)          (x)  //    0< x < 4


// Transmit Configuration
#define PROX_LED_CUR(x)               (x)  // x .=0, x< 16

#define ALS_ADC_OFL                   BIT(6)
#define PROX_THR_ABOVE                BIT(6)

extern uint16_t ALS_ADC_Data;
extern uint8_t  PROX_ADC_Data;
extern uint8_t  ALS_newData_Flag;
extern uint8_t  PROX_newData_Flag;
extern volatile bool ProxIntState;
extern void prox_WriteReg(uint8_t regValue, uint8_t regAddr);
extern uint8_t prox_ReadReg(uint8_t regAddr);
extern uint16_t prox_ReadWord(uint8_t regAddr);
extern uint8_t ClearInterruptFlag(void);
extern void prox_init(byte cur);
//void prox_interruptProcess(void);

extern void prox_enableInterupt(void);
extern void prox_disableInterupt(void);
extern void prox_shutdown(void);
extern uint8_t prox_getpinstatus(void);
//#define     prox_start()          prox_init()

void processProx(void);

void poxReadADC(void);

#endif

