/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
  * http:  www.artaflex.com

 *
 ***************************************************************************/
#ifndef _MAX17047_H_
#define _MAX17047_H_

#include "typedef.h"


#define MAX17047_DEV_ADDR 0x36      // 0x6C

#define ALS_UPPER_THRESHOLD        5000u
#define ALS_LOWER_THRESHOLD        1000u
#define PROX_THRESHOLD_VALUE       32


#define MAX17047_STATUS 				        0x00
#define MAX17047_V_ALRT_THRESHOLD 	    0x01
#define MAX17047_T_ALRT_THRESHOLD 	    0x02
#define MAX17047_SOC_ALRT_THRESHOLD	    0x03
#define MAX17047_AT_RATE			        	0x04
#define MAX17047_REM_CAP_REP		      	0x05
#define MAX17047_SOC_REP			        	0x06
#define MAX17047_AGE				          	0x07
#define MAX17047_TEMPERATURE		      	0x08
#define MAX17047_V_CELL				        	0x09
#define MAX17047_CURRENT			        	0x0A
#define MAX17047_AVERAGE_CURRENT	    	0x0B

#define MAX17047_SOC_MIX			        	0x0D
#define MAX17047_SOC_AV				        	0x0E
#define MAX17047_REM_CAP_MIX		      	0x0F
#define MAX17047_FULL_CAP			        	0x10
#define MAX17047_TTE				          	0x11
#define MAX17047_Q_RESIDUAL_00		    	0x12
#define MAX17047_FULL_SOC_THR		      	0x13

#define MAX17047_AVERAGE_TEMP		        0x16
#define MAX17047_CYCLES					        0x17
#define MAX17047_DESIGN_CAP			      	0x18
#define MAX17047_AVERAGE_V_CELL		      0x19
#define MAX17047_MAX_MIN_TEMP		      	0x1A
#define MAX17047_MAX_MIN_VOLTAGE	    	0x1B
#define MAX17047_MAX_MIN_CURRENT    		0x1C
#define MAX17047_CONFIG			        		0x1D
#define MAX17047_I_CHG_TERM		      		0x1E
#define MAX17047_REM_CAP_AV		      		0x1F
#define MAX17047_VERSION			        	0x21
#define MAX17047_Q_RESIDUAL_10	    		0x22
#define MAX17047_FULL_CAP_NOM	      		0x23
#define MAX17047_TEMP_NOM			        	0x24
#define MAX17047_TEMP_LIM			        	0x25
#define MAX17047_AIN				          	0x27
#define MAX17047_LEARN_CFG			      	0x28
#define MAX17047_FILTER_CFG			      	0x29
#define MAX17047_RELAX_CFG		          0x2A
#define MAX17047_MISC_CFG			          0x2B
#define MAX17047_T_GAIN				          0x2C
#define MAX17047_T_OFF					        0x2D
#define MAX17047_C_GAIN					        0x2E
#define MAX17047_C_OFF					        0x2F
#define MAX17047_Q_RESIDUAL_20			    0x32
#define MAX17047_I_AVG_EMPTY			      0x36
#define MAX17047_F_CTC					        0x37
#define MAX17047_RCOMP_0				        0x38
#define MAX17047_TEMP_CO				        0x39
#define MAX17047_V_EMPTY				        0x3A
#define MAX17047_F_STAT					        0x3D
#define MAX17047_TIMER					        0x3E
#define MAX17047_SHDN_TIMER				      0x3F
#define MAX17047_Q_RESIDUAL_30			    0x42
#define MAX17047_D_QACC					        0x45
#define MAX17047_D_PACC					        0x46
#define MAX17047_QH						          0x4D
#define MAX17047_V_FOCV				          0xFB
#define MAX17047_SOC_VF				          0xFF


extern volatile bool Max17047IntState ;  
extern word max17047_result;
extern uint16_t max17047Version;
extern uint16_t max17047Voltage;
//extern int16_t  max17047Current;
extern uint8_t  max17047Exist;
//extern volatile byte battSampEnableFuel;
//extern volatile byte battMsgXmit;
//extern volatile word g_TickSecondBat;

extern void max17047_init(void);
extern void max17047_GetVer(void);

extern uint16_t max17047_ReadReg(uint8_t regAddr);
extern void max17047_process(void);

#endif

