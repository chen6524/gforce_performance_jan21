/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West
 * Markham, ON L3R 3J9
 * Canada
 *
 * Tel:   (905) 470-0109
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/

#ifndef _LSM6DS3_H_
#define _LSM6DS3_H_





#include "Typedef.h"
#include "mcu.h"
////

/* COMMON VALUES FOR ACCEL-GYRO SENSORS */
#define LSM6DS3_WHO_AM_I			0x0f
#define LSM6DS3_WHO_AM_I_DEF			0x69
#define LSM6DS3_AXIS_EN_MASK			0x38
#define LSM6DS3_INT1_CTRL_ADDR			0x0d
#define LSM6DS3_INT2_CTRL_ADDR			0x0e
#define LSM6DS3_INT1_FULL			0x20
#define LSM6DS3_INT1_FTH			0x08
#define LSM6DS3_MD1_ADDR			0x5e
#define LSM6DS3_ODR_LIST_NUM			5
#define LSM6DS3_ODR_POWER_OFF_VAL		0x00
#define LSM6DS3_ODR_26HZ_VAL			0x02
#define LSM6DS3_ODR_52HZ_VAL			0x03
#define LSM6DS3_ODR_104HZ_VAL			0x04
#define LSM6DS3_ODR_208HZ_VAL			0x05
#define LSM6DS3_ODR_416HZ_VAL			0x06
#define LSM6DS3_FS_LIST_NUM			4
#define LSM6DS3_BDU_ADDR			0x12
#define LSM6DS3_BDU_MASK			0x40
#define LSM6DS3_EN_BIT				0x01
#define LSM6DS3_DIS_BIT				0x00
#define LSM6DS3_FUNC_EN_ADDR			0x19
#define LSM6DS3_FUNC_EN_MASK			0x04
#define LSM6DS3_FUNC_CFG_ACCESS_ADDR		0x01
#define LSM6DS3_FUNC_CFG_ACCESS_MASK		0x01
#define LSM6DS3_FUNC_CFG_ACCESS_MASK2		0x04
#define LSM6DS3_FUNC_CFG_REG2_MASK		0x80
#define LSM6DS3_FUNC_CFG_START1_ADDR		0x62
#define LSM6DS3_FUNC_CFG_START2_ADDR		0x63
#define LSM6DS3_SELFTEST_ADDR			0x14
#define LSM6DS3_SELFTEST_ACCEL_MASK		0x03
#define LSM6DS3_SELFTEST_GYRO_MASK		0x0c
#define LSM6DS3_SELF_TEST_DISABLED_VAL		0x00
#define LSM6DS3_SELF_TEST_POS_SIGN_VAL		0x01
#define LSM6DS3_SELF_TEST_NEG_ACCEL_SIGN_VAL	0x02
#define LSM6DS3_SELF_TEST_NEG_GYRO_SIGN_VAL	0x03
#define LSM6DS3_LIR_ADDR			0x58
#define LSM6DS3_LIR_MASK			0x01
#define LSM6DS3_TIMER_EN_ADDR			0x58
#define LSM6DS3_TIMER_EN_MASK			0x80
#define LSM6DS3_PEDOMETER_EN_ADDR		0x58
#define LSM6DS3_PEDOMETER_EN_MASK		0x40
#define LSM6DS3_INT2_ON_INT1_ADDR		0x13
#define LSM6DS3_INT2_ON_INT1_MASK		0x20
#define LSM6DS3_MIN_DURATION_MS			1638
#define LSM6DS3_ROUNDING_ADDR			0x16
#define LSM6DS3_ROUNDING_MASK			0x04
#define LSM6DS3_FIFO_MODE_ADDR			0x0a
#define LSM6DS3_FIFO_MODE_MASK			0x07
#define LSM6DS3_FIFO_MODE_BYPASS		0x00
#define LSM6DS3_FIFO_MODE_CONTINUOS		0x06
#define LSM6DS3_FIFO_THRESHOLD_IRQ_MASK		0x08
#define LSM6DS3_FIFO_ODR_ADDR			0x0a
#define LSM6DS3_FIFO_ODR_MASK			0x78
#define LSM6DS3_FIFO_ODR_MAX			0x07
#define LSM6DS3_FIFO_ODR_MAX_HZ			800
#define LSM6DS3_FIFO_ODR_OFF			0x00
#define LSM6DS3_FIFO_CTRL3_ADDR			0x08
#define LSM6DS3_FIFO_ACCEL_DECIMATOR_MASK	0x07
#define LSM6DS3_FIFO_GYRO_DECIMATOR_MASK	0x38
#define LSM6DS3_FIFO_CTRL4_ADDR			0x09
#define LSM6DS3_FIFO_STEP_C_DECIMATOR_MASK	0x38
#define LSM6DS3_FIFO_THR_L_ADDR			0x06
#define LSM6DS3_FIFO_THR_H_ADDR			0x07
#define LSM6DS3_FIFO_THR_H_MASK			0x0f
#define LSM6DS3_FIFO_THR_IRQ_MASK		0x08
#define LSM6DS3_FIFO_PEDO_E_ADDR		0x07
#define LSM6DS3_FIFO_PEDO_E_MASK		0x80
#define LSM6DS3_FIFO_STEP_C_FREQ		25

/* CUSTOM VALUES FOR ACCEL SENSOR */
#define LSM6DS3_ACCEL_ODR_ADDR			0x10
#define LSM6DS3_ACCEL_ODR_MASK			0xf0
#define LSM6DS3_ACCEL_FS_ADDR			0x10
#define LSM6DS3_ACCEL_FS_MASK			0x0c
#define LSM6DS3_ACCEL_FS_2G_VAL			0x00
#define LSM6DS3_ACCEL_FS_4G_VAL			0x02
#define LSM6DS3_ACCEL_FS_8G_VAL			0x03
#define LSM6DS3_ACCEL_FS_16G_VAL		0x01
#define LSM6DS3_ACCEL_FS_2G_GAIN		61
#define LSM6DS3_ACCEL_FS_4G_GAIN		122
#define LSM6DS3_ACCEL_FS_8G_GAIN		244
#define LSM6DS3_ACCEL_FS_16G_GAIN		488
#define LSM6DS3_ACCEL_OUT_X_L_ADDR		0x28
#define LSM6DS3_ACCEL_OUT_Y_L_ADDR		0x2a
#define LSM6DS3_ACCEL_OUT_Z_L_ADDR		0x2c
#define LSM6DS3_ACCEL_AXIS_EN_ADDR		0x18
#define LSM6DS3_ACCEL_DRDY_IRQ_MASK		0x01
#define LSM6DS3_ACCEL_STD			1
#define LSM6DS3_ACCEL_STD_FROM_PD		2

/* CUSTOM VALUES FOR GYRO SENSOR */
#define LSM6DS3_GYRO_ODR_ADDR			0x11
#define LSM6DS3_GYRO_ODR_MASK			0xf0
#define LSM6DS3_GYRO_FS_ADDR			0x11
#define LSM6DS3_GYRO_FS_MASK			0x0c
#define LSM6DS3_GYRO_FS_245_VAL			0x00
#define LSM6DS3_GYRO_FS_500_VAL			0x01
#define LSM6DS3_GYRO_FS_1000_VAL		0x02
#define LSM6DS3_GYRO_FS_2000_VAL		0x03
#define LSM6DS3_GYRO_FS_245_GAIN		8750
#define LSM6DS3_GYRO_FS_500_GAIN		17500
#define LSM6DS3_GYRO_FS_1000_GAIN		35000
#define LSM6DS3_GYRO_FS_2000_GAIN		70000
#define LSM6DS3_GYRO_OUT_X_L_ADDR		0x22
#define LSM6DS3_GYRO_OUT_Y_L_ADDR		0x24
#define LSM6DS3_GYRO_OUT_Z_L_ADDR		0x26
#define LSM6DS3_GYRO_AXIS_EN_ADDR		0x19
#define LSM6DS3_GYRO_DRDY_IRQ_MASK		0x02
#define LSM6DS3_GYRO_STD			6
#define LSM6DS3_GYRO_STD_FROM_PD		2

#define LSM6DS3_OUT_XYZ_SIZE			8

/* CUSTOM VALUES FOR SIGNIFICANT MOTION SENSOR */
#define LSM6DS3_SIGN_MOTION_EN_ADDR		0x19
#define LSM6DS3_SIGN_MOTION_EN_MASK		0x01
#define LSM6DS3_SIGN_MOTION_DRDY_IRQ_MASK	0x40

/* CUSTOM VALUES FOR STEP DETECTOR SENSOR */
#define LSM6DS3_STEP_DETECTOR_DRDY_IRQ_MASK	0x80

/* CUSTOM VALUES FOR STEP COUNTER SENSOR */
#define LSM6DS3_STEP_COUNTER_DRDY_IRQ_MASK	0x80
#define LSM6DS3_STEP_COUNTER_OUT_L_ADDR		0x4b
#define LSM6DS3_STEP_COUNTER_OUT_SIZE		2
#define LSM6DS3_STEP_COUNTER_RES_ADDR		0x19
#define LSM6DS3_STEP_COUNTER_RES_MASK		0x06
#define LSM6DS3_STEP_COUNTER_RES_ALL_EN		0x03
#define LSM6DS3_STEP_COUNTER_RES_FUNC_EN	0x02
#define LSM6DS3_STEP_COUNTER_DURATION_ADDR	0x15

/* CUSTOM VALUES FOR TILT SENSOR */
#define LSM6DS3_TILT_EN_ADDR			0x58
#define LSM6DS3_TILT_EN_MASK			0x20
#define LSM6DS3_TILT_DRDY_IRQ_MASK		0x02

#define LSM6DS3_ENABLE_AXIS			0x07
#define LSM6DS3_FIFO_DIFF_L			0x3a
#define LSM6DS3_FIFO_DIFF_MASK			0x0fff
#define LSM6DS3_FIFO_DATA_OUT_L			0x3e
#define LSM6DS3_FIFO_ELEMENT_LEN_BYTE		6
#define LSM6DS3_FIFO_BYTE_FOR_CHANNEL		2
#define LSM6DS3_FIFO_DATA_OVR_2REGS		0x4000
#define LSM6DS3_FIFO_DATA_OVR			0x40

#define LSM6DS3_SRC_FUNC_ADDR			0x53
#define LSM6DS3_FIFO_DATA_AVL_ADDR		0x3b

#define LSM6DS3_SRC_SIGN_MOTION_DATA_AVL	0x40
#define LSM6DS3_SRC_STEP_DETECTOR_DATA_AVL	0x10
#define LSM6DS3_SRC_TILT_DATA_AVL		0x20
#define LSM6DS3_SRC_STEP_COUNTER_DATA_AVL	0x80
#define LSM6DS3_FIFO_DATA_AVL			0x80
#define LSM6DS3_RESET_ADDR			0x12
#define LSM6DS3_RESET_MASK			0x01

#define LSM6DS3_TAP_THS_6D                         0x59                       // Added by Jason Chen, 2016.01.13
#define LSM6DS3_INT_DUR2          			           0x5a                       // Added by Jason Chen, 2016.01.13
#define LSM6DS3_WAKE_UP_THS_ADDR                   0x5b
#define LSM6DS3_WAKE_UP_DUR_ADDR                   0x5C                       // Added by Jason Chen, 2016.01.13
#define LSM6DS3_FREE_FALL_ADDR                     0x5D                       // Added by Jason Chen, 2016.01.13
#define LSM6DS3_CTRL1_XL_ADDR                      LSM6DS3_ACCEL_ODR_ADDR		  // Added by Jason Chen, 2016.01.13
#define LSM6DS3_CTRL3_C_ADDR                       LSM6DS3_BDU_ADDR           // Added by Jason Chen, 2016.01.15
#define LSM6DS3_CTRL8_XL_ADDR                      0x17                       // Added by Jason Chen, 2016.01.13
#define LSM6DS3_CTRL9_XL_ADDR                      LSM6DS3_ACCEL_AXIS_EN_ADDR // Added by Jason Chen, 2016.01.13
#define LSM6DS3_TAP_CFG_ADDR                       LSM6DS3_LIR_ADDR           // Added by Jason Chen, 2016.01.13
#define LSM6DS3_MD1_CFG_ADDR                       LSM6DS3_MD1_ADDR           // Added by Jason Chen, 2016.01.13


/////



#define BIT(x) ( 1<<(x) )
#define FIFO_DEPTH           32


#define Dummy_Byte  0xA5

typedef struct _sample_struct 
{
  int16_t Y;
  int16_t X;
  int16_t Z;
  
} sample_t;

typedef union _whole_FIFO 
{
  uint8_t     rawByte[FIFO_DEPTH*6];
  sample_t    samplet[FIFO_DEPTH];  
  uint8_t     rawData[FIFO_DEPTH][6];
}whole_FIFO;



//  FIFO CONFIGURATIONS
#define FIFO_CONTROL_WTMSAMP            (BIT(4) | BIT(3) | BIT(2) | BIT(1) | BIT(0))

#define DEF_FIFO_CTRL_BYPASS		        0x00
#define DEF_FIFO_CTRL_FIFO_MODE		      BIT(5)
#define DEF_FIFO_CTRL_STREAM		        BIT(6)
#define DEF_FIFO_CTRL_STREAM_TO_FIFO	  (BIT(6) | BIT(5))
#define DEF_FIFO_CTRL_BYPASS_TO_STREAM	BIT(7)

#define FIFO_SOURCE_SAMPLES             (BIT(4) | BIT(3) | BIT(2) | BIT(1) | BIT(0))
#define FIFO_SOURCE_EMPTY               BIT(5)
#define FIFO_SOURCE_OVRN                BIT(6)
#define FIFO_SOURCE_WTM                 BIT(7)


//    Control Register
//		Control Register 1
#define CTRL_REG1_ODR_760HZ             (BIT(7)|BIT(6))
#define CTRL_REG1_BW11                  (BIT(5)|BIT(4))
#define CTRL_REG1_NORMALMODE            BIT(3)
#define CTRL_REG1_DEFAULT               (BIT(2)|BIT(1)|BIT(0))

//		Control Register 2
#define CTRL_REG2_DEFAULT               0
#define EXTREN                          0x80
#define LVLEN                           0x40

//		Control Register 3
#define CTRL_REG3_DEFAULT               0   // Disable all interrupt

//		Control Register 4
#define CTRL_REG4_SCALE2000             (BIT(5)|BIT(4))        // FULL Scale --2000dps
#define CTRL_REG4_DEFAULT               0


//		Control Register 5
#define CTRL_REG5_DEFAULT               0
#define CTRL_REG5_FIFO_EN		            BIT(6)
#define CTRL_REG5_STOP_ON_WTM		        BIT(5)

//		INT_SRC
#define INT_SRC_IA		                  BIT(6)

//gyro define
#define GYRO_DATAREADY_BIT              3


// ACC_CTRL_REG1
#define ACC_ODR_POWERDOWN       (0x00 << 4)
#define ACC_ODR_50HZ            (BIT(6))
#define ACC_ODR_200HZ           (BIT(6)|BIT(5))
#define ACC_ODR_400HZ           (BIT(6)|BIT(5)|BIT(4))
#define ACC_ODR_LOWPOWER_1620HZ (0x08 << 4)
#define ACC_ODR_NORMAL_1344HZ   (0x09 << 4)
#define ACC_LOW_POWER           BIT(3)
#define ACC_XYZAXIS_EN          BIT(2)|BIT(1)|BIT(0)

// ACC_CTRL_REG3
#define INT1_AOI1               BIT(6)
#define INT1_DRDY1              BIT(4)
#define INT1_DRDY2              BIT(3)
#define INT1_FIFO_WATERMARK     BIT(2)
#define INT1_FIFO_OVERRUN       BIT(1)

// ACC_CTRL_REG4
#define ACC_FULLSCALE_02G       (0x00 << 4)
#define ACC_FULLSCALE_04G       (0x01 << 4)
#define ACC_FULLSCALE_08G       (0x02 << 4)
#define ACC_FULLSCALE_16G       (0x03 << 4)
#define ACC_HIGH_RESOLUTION     BIT(3)       // High-->12bits, Low--->8bits

// ACC_CTRL_REG5
#define ACC_FIFO_ENABLE         BIT(6)
#define ACC_LIR_INT1_ENABLE     BIT(3)

// ACC_CTRL_REG6
#define ACC_INT_ACTIVE_LOW      BIT(1)


// ACC_FIFO_CTRL_REG
#define FIFO_BYPASS_A           (0x00 << 6)
#define FIFO_MODE_A             (0x01 << 6)
#define FIFO_STREAM_A           (0x02 << 6)
#define FIFO_TRIGGER_A          (0x03 << 6)   // Stream to FIFO
#define FIFO_TRIGGER_INT2       BIT(6)        // Trigger event linked to trigger signal on INT2_A
#define FIFO_THR(x)             (x)

// INT1_CFG
#define ACC_INT1_CFG_6DIR_MOVE  BIT(6)
#define ACC_INT1_CFG_6DIR_POS   BIT(7)|BIT(6)

#define XYZ_UP_INT_ENABLE       BIT(5)|BIT(3)|BIT(1)   // 0x2A
#define XYZ_DOWN_INT_ENABLE     BIT(4)|BIT(2)|BIT(0)   // 0x15

// INT1_SRC
#define ACC_X_LOW_INT           BIT(0)
#define ACC_X_HIGH_INT          BIT(1)
#define ACC_Y_LOW_INT           BIT(2)
#define ACC_Y_HIGH_INT          BIT(3)
#define ACC_Z_LOW_INT           BIT(4)
#define ACC_Z_HIGH_INT          BIT(5)
#define ACC_INT_EVENT           BIT(6)


// ACC_ACT_THS, ACC_ACT_DUR
#define SLEEP_TO_WAKE_THR(x)  (x)           // x<127, 1LSB = 16mg
#define ACT_DURATION_TIME     (x)           // x<255, Real Duration time = (x+1)*8/ODR



extern void gyro_FIFO_SPIGetRawData(uint8_t *pBuffer); 


extern void acc_SPIBufferRead(uint8_t* pBuffer, uint8_t readAddr, uint8_t numByteToRead);
extern void acc_FIFO_SPIGetRawData(byte *pBuffer);
extern byte acc_SPIReadReg(uint8_t regAddr); 
extern void acc_SPIWriteReg(uint8_t regAddr, uint8_t regValue);
extern void acc_SPIBufferWrite(uint8_t regAddr,uint8_t *writeBuf, uint8_t numByteToWrite); 

extern void SPI_MEMS_Acc_Config(byte mode);
extern void SPI_MEMS_Gyro_Config(void);

#endif
