/*************************************************************************
 * DISCLAIMER *
 * Services performed by FREESCALE in this matter are performed          *
 * AS IS and without any warranty. CUSTOMER retains the final decision   *
 * relative to the total design and functionality of the end product.    *
 * FREESCALE neither guarantees nor will be held liable by CUSTOMER      *
 * for the success of this project. FREESCALE disclaims all warranties,  *
 * express, implied or statutory including, but not limited to,          *
 * implied warranty of merchantability or fitness for a particular       *
 * purpose on any hardware, software ore advise supplied to the project  *
 * by FREESCALE, and or any product resulting from FREESCALE services.   *
 * In no event shall FREESCALE be liable for incidental or consequential *
 * damages arising out of this agreement. CUSTOMER agrees to hold        *
 * FREESCALE harmless against any and all claims demands or actions      *
 * by anyone on account of any damage, or injury, whether commercial,    *
 * contractual, or tortuous, rising directly or indirectly as a result   *
 * of the advise or assistance supplied CUSTOMER in connection with      *
 * product, services or goods supplied under this Agreement.             *
 *************************************************************************/
/*************************************************************************************************
 * File name   : Usb_Description.c
 * Description : This file defines the USB descriptor and intizlize them 
 * History     :
 * 04/01/2007  : Initial Development
 * 
 *************************************************************************************************/

#include "typedef.h"
#include "Usb_Descriptor.h"
#include "Usb_Config.h"
#include "Hid.h"


const byte Hid_Rpt[HID_RPT_SIZE]  = 
{
	   0x06, 0x00, 0xFF,						// Usage Page
	   0x09, 0x01,								// Usage
	   0xA1, 0x01,								// Collection
	   0x09, 0x01,								// Usage
	   0x15, 0x00,								// Logical Minimum
	   0x26, 0xFF, 0x00, 						// Logical Maximum
	   0x75, 0x08,								// Report Size
	   0x95, 64,								// Report Count
	   0x81, 0x02,								// Input
	   0x09, 0x02,								// Usage
	   0x91, 0x02,								// Output
	   0xC0									    // End Collection

 };
  
  
const USB_DEV_DSC Device_Dsc =
{    
    sizeof(USB_DEV_DSC),    /* Size of this descriptor in bytes     */
    DSC_DEV,                /* DEVICE descriptor type               */
    0x1001,                 /* USB Spec Release Number in BCD format*/
    0x00,                   /* Class Code                           */
    0x00,                   /* Subclass code                        */
    0x00,                   /* Protocol code                        */
    EP0_BUFF_SIZE,          /* Max packet size for EP0,             */
#if 1
    0xD11C,                 /* Vendor ID		//little-endian for USB */
    0x0204,                 /* Product ID:	//little-endian for USB */
#else
    0xB404,
    0x0712,
#endif
    0x0001,                 /* Device release number in BCD format  */
    0x01,                   /* Manufacturer string index            */
    0x0,                   /* Product string index                 */
    0x0,                   /* Device serial number string index    */
    0x01                    /* Number of possible configurations    */
};

 const USB_CFG  Cfg_01={
    {
    sizeof(USB_CFG_DSC),    /* Size of this descriptor in bytes   */
    DSC_CFG,                /* CONFIGURATION descriptor type      */
    (word) ((sizeof(USB_CFG)<<8) | (sizeof(USB_CFG)>>8)),          /* Total length of data for this cfg*/
    1,                      /* Number of interfaces in this cfg   */
    1,                      /* Index value of this configuration  */
    0,                      /* Configuration string index         */
    _DEFAULT, //|_REMOTEWAKEUP, /* Attributes, see usbdefs_std_dsc.h  */
    50                      /* Max power consumption (2X mA)      */
    },
    /* Interface Descriptor */
    {
    sizeof(USB_INTF_DSC),   /* Size of this descriptor in bytes   */
    DSC_INTF,               /* INTERFACE descriptor type          */
    0,                      /* Interface Number                   */
    0,                      /* Alternate Setting Number           */
    2,                      /* Number of endpoints in this intf   */
    HID_INTF,               /* Class code                         */
    0,//BOOT_INTF_SUBCLASS,     /* Subclass code                  */
    HID_PROTOCOL_NONE,      /* Protocol code                      */
    0                       /* Interface string index             */
    },
    /* HID Class-Specific Descriptor */
    {
      
    sizeof(USB_HID_DSC),    /* Size of this descriptor in bytes   */
    DSC_HID,                /* HID descriptor type                */
    0x1101,                 /* HID Spec Release Number in BCD format; ***Little-endian***/
    0x00,                   /* Country Code (0x00 for Not supported)*/
    HID_NUM_OF_DSC,         /* Number of class descriptors, see usbcfg.h */
    {
    DSC_RPT,                /* Report descriptor type  */
    (word) ((sizeof(Hid_Rpt)<<8) | (sizeof(Hid_Rpt)>>8))   /* Size of the report descripton*/
    }
    },
    /* Endpoint Descriptor*/
    {sizeof(USB_EP_DSC),DSC_EP,_EP01_IN,_INT,(word)(HID_INT_IN_EP_SIZE << 8 | HID_INT_IN_EP_SIZE>>8) , 1} ,
    {sizeof(USB_EP_DSC),DSC_EP,_EP02_OUT,_INT,(word)(HID_INT_OUT_EP_SIZE << 8 |HID_INT_OUT_EP_SIZE >> 8),1}
};
//language code string descriptor
struct
{
  byte bLength;
  byte bDscType;
  word string[1];
} const sd000 
= {sizeof(sd000),DSC_STR,0x0409};


//Manufacturer string descriptor
struct
{
  byte bLength;
  byte bDscType;
  byte string[sizeof("Artaflex Inc.") * 2];
} const sd001
  ={
  
    sizeof(sd001),DSC_STR,
    'A',0,'r',0,'t',0,'a',0,'f',0,'l',0,'e',0,'x',0,' ',0,'I',0,
    'n',0,'c',0,'.',0
   }; 
//product string descriptor       
struct
{
  byte bLength;
  byte bDscType;
  byte string[sizeof("gForce") * 2];
} const sd002
={
    sizeof(sd002),DSC_STR,
    'g',0,'F',0,'o',0,'r',0,'c',0,'e',0
   }; 
   
struct
{
  byte bLength;
  byte bDscType;
  byte string[sizeof("USB") * 2];
} const sd003
={
   sizeof(sd003),DSC_STR,
   'U',0,'S',0,'B',0
  }; 

struct
{
  byte bLength;
  byte bDscType;
  byte string[sizeof("Config") * 2];
} const sd004
={
   sizeof(sd004),DSC_STR,
   'C',0,'o',0,'n',0,'f',0,'i',0,'g',0
  }; 

struct
{
  byte bLength;
  byte bDscType;
  byte string[sizeof("Interface") * 2];
} const sd005
={
   sizeof(sd005),DSC_STR,
   'I',0,'n',0,'t',0,'e',0,'r',0,'f',0,'a',0,'c',0,'e',0
  }; 
   struct
   {
	 byte bLength;
	 byte bDscType;
   } const sd006
   ={
	  0,0
	 }; 

const unsigned char* const Str_Des[] = 
    { (unsigned char*)&sd000,(unsigned char*)&sd001,(unsigned char*)&sd002, 
      (unsigned char*)&sd003,(unsigned char*)&sd004,(unsigned char*)&sd005, (unsigned char*)&sd006};   

const unsigned char* const Cfg_Des[] =
    {(unsigned char*)&Cfg_01,(unsigned char*)&Cfg_01 };
    
pFunc Class_Req_Handler[1] = { &HIDClass_Request_Handler };


