/******************** (C) COPYRIGHT 2011 ARTAFLEX INC **************************
* File Name          : spi_flash.h
* Author             : Jason Chen
* Version            : V2.0.0
* Date               : 20/27/2009
* Description        : Header for spi_flash.c file.
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H

/* Includes ------------------------------------------------------------------*/

#include "gft.h"
#include "typedef.h"
#include "mcu.h"
#define USE_SPI1
#define M25P128x
#define M25P64x
#define M25P32x
#define MX25L3206E
#define GD25Q128x //giga devices

#define SPI_WATCHDOG_ENABLE

/* Private typedef -----------------------------------------------------------*/
#define SPI_FLASH_PageSize       256

#ifdef M25P128  
  #define SPI_FLASH_SectorNum   0x40
  #define SPI_FLASH_SectorSize  0x40000      // (0x400 * SPI_FLASH_PageSize)
  #define SPI_FLASH_SIZE        0x1000000    // (0x40  * SPI_FLASH_SectorSize)
#endif

#ifdef M25P64
  #define SPI_FLASH_SectorNum   0x80
  #define SPI_FLASH_SectorSize  0x10000      // (0x100 * SPI_FLASH_PageSize)
  #define SPI_FLASH_SIZE        0x800000     // (0x80  * SPI_FLASH_SectorSize)
#endif

#ifdef M25P32
  #define MFT_FLASH_ID  0x202016
  #define SPI_FLASH_SectorNum   0x40
  #define SPI_FLASH_SectorSize  0x10000      // (SPI_FLASH_PageSize*0x100)
  #define SPI_FLASH_SIZE        0x400000     // (0x40*SPI_FLASH_SectorSize)
  #define ST_DP                 0xB9         // Enter Deep Power Down
  #define ST_RES                0xAB         // Resume from Deep Power Down
#endif
#ifdef MX25L3206E
  #define MFT_FLASH_ID  0xC22016
  #define SPI_FLASH_SectorNum   0x400
  #define SPI_FLASH_SectorSize  0x1000      // (SPI_FLASH_PageSize*0x100)
  #define SPI_FLASH_SIZE        0x400000     // (0x40*SPI_FLASH_SectorSize)
  #define ST_DP                 0xB9         // Enter Deep Power Down
  #define ST_RES                0xAB         // Resume from Deep Power Down
#endif
#ifdef GD25Q128
  #define MFT_FLASH_ID 0xC82016   // size ????
  #define SPI_FLASH_SectorNum   0x400
  #define SPI_FLASH_SectorSize  0x1000      // (SPI_FLASH_PageSize*0x100)
  #define SPI_FLASH_SIZE        0x1000000     //  
  #define ST_DP                 0xB9         // Enter Deep Power Down
  #define ST_RES                0xAB         // Resume from Deep Power Down

#endif


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Uncomment the line corresponding to the STMicroelectronics evaluation board
   used to run the example */

typedef enum {ST_RESET = 0, ST_SET = !ST_RESET} FlagStatus, ITStatus;

/* Exported functions ------------------------------------------------------- */
/*----- High layer function -----*/
byte SPI_FLASH_Init(bool Enable_SPI);
void SPI_FLASH_SectorErase(uint32_t SectorAddr);
void SPI_FLASH_BulkErase(void);
void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void SPI_FLASH_RingWrite(uint8_t* pBuffer, uint16_t NumByteToWrite);
void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);
uint32_t SPI_FLASH_ReadID(void);
void SPI_FLASH_StartReadSequence(uint32_t ReadAddr);

#ifdef M25P32 
#define ENABLE_DEEP_PWR
#endif
#ifdef MX25L3206E
#define ENABLE_DEEP_PWR
#endif
#ifdef ENABLE_DEEP_PWR
void SPI_FLASH_DeepPowerDown(void);
byte SPI_FLASH_ResumeFromDeepPowerDown(void);
#endif

/*----- Low layer function -----*/
uint8_t SPI_FLASH_ReadByte(void);
uint8_t SPI_FLASH_SendByte(uint8_t data_byte);

void SPI_FLASH_WriteEnable(void);
void SPI_FLASH_WaitForWriteEnd(void);
uint8_t SPI_FLASH_ReadStatusReg(void);
void SPI_FLASH_WriteStatusReg(uint8_t statusValue);

#endif /* __SPI_FLASH_H */

/******************* (C) COPYRIGHT 2011 ARTAFLEX INC *****END OF FILE****/
