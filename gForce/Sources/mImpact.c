/********************************************************************************
*  
* High G accelerometer 
* History:
*
* 11/01/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/

#include "gft.h"
#include "mcu.h"
#include "LSM330DLC.h"
#include <string.h>
#include "mImpact.h"
#include "adc.h"
#include "mFlash.h"
#include "mHid.h"
#include "rtc.h"
#include "utils.h"
#include "gLp.h"
//Faraz
#include "radio.h"
#include "MAX44000.h"
#include "max14676.h"
#include <stdlib.h>
#include "bcd_common.h"                        // Added by Jason Chen for time stamp fix, 2013.12.19
#include "kbi.h"




//static byte accRamState =0;
//test
//word testMsS = 0;
//word testMsE = 0;

RAM_QUE_I ramQueI[RAM_QUE_MAX_NUM_I];
RAM_QUE_P ramQueP[RAM_QUE_MAX_NUM_P]; 


RAM_QUE_I *pRamQueRdI;
RAM_QUE_I *pRamQueWtI;

RAM_QUE_P *pRamQueRdP;
RAM_QUE_P *pRamQueWtP;

byte accHitStat = ACC_HIT_STAT_INIT;
word bucketCnt =0;
byte accHitPreCur =0;
byte accHitPreTotal = 0;

//WORD accMaxValue;
volatile byte accHitOn =0;

GFT_PROFILE usrProfile;

extern byte field_xmit_enable;                         // Added by Jason Chen for Recording Control, 2014.08.05

IM_MOTION_DATA imSummaryData;

//long AccCalc(ACC_ENTRY *entry);

void SetUsrProfileLP_Disable(void)                     // Added by Jason Chen, 2019.02.07
{
   LED_BLUE_On();
   Cpu_Delay100US(5000);                               
   usrProfile.lpEnable = GMODE_LP_DISABLE;    
   needSaveProfile     = 1;					                      
   needReboot          = 1;                                
}


void imRstBucket(RAM_QUE_P *pEntry)
{
  int i;
  #define BUCKET_TOTAL_NUMBER 12
  for (i =0;i<BUCKET_TOTAL_NUMBER;i++)
  	 pEntry->bucket.bucket[i] = 0; 
  
  gftInfo.explosiveFlag = 0;

}
int initQueI(void)
{
	byte i;
	for ( i = 0; i< RAM_QUE_MAX_NUM_I; i++)
	{
	    ramQueI[i].cstatus =0;
	    ramQueI[i].next =   (void *)&ramQueI[i+1];
		
	}
	ramQueI[RAM_QUE_MAX_NUM_I-1].next = (void *)&ramQueI[0];
	pRamQueRdI = pRamQueWtI = &ramQueI[0];
	return(E_OK);
} 

int initQueP(void)
{
	byte i;
	for ( i = 0; i< RAM_QUE_MAX_NUM_P; i++)
	{
	    ramQueP[i].cstatus =0;
	    ramQueP[i].next =   (void *)&ramQueP[i+1];
		
	}
	ramQueP[RAM_QUE_MAX_NUM_P-1].next = (void *)&ramQueP[0];
	pRamQueRdP = pRamQueWtP = &ramQueP[0];
	
	imRstBucket(pRamQueWtP);
	return(E_OK);
} 

uint32_t gActiveTime;

// Changed by Jason Chen,   2015.02.06		   
#define RECORD_ACTIVE_TIME \
		   if(motionDetected)gActiveTime++; 
		   
// Added by Jason Chen,   2015.02.06		   
#define RECORD_ACTIVE_TIME2 \
		   gActiveTime++; 	

#define   RPE_COE_0    1
#define   RPE_COE_1    2
#define   RPE_COE_2    3
#define   RPE_COE_3    5

#define   RPE_COE_4    7
#define   RPE_COE_5    7
#define   RPE_COE_6    9
#define   RPE_COE_7    9
#define   RPE_COE_8    9
#define   RPE_COE_9    10
#define   RPE_COE_10   10
#define   RPE_COE_11   10


const byte rpeCoe[10] = {RPE_COE_1,RPE_COE_2,RPE_COE_3,RPE_COE_4,RPE_COE_5,RPE_COE_6,RPE_COE_7,RPE_COE_8,RPE_COE_9,RPE_COE_10};
long totalSecond =0;

void imUpdateRpe(void)
{
       int i;
	   long j =0;
	   long rpe = 0;

		for(i =0;i<10;i++)
		{
			 
			rpe += pRamQueRdP->bucket.bucket[i] * rpeCoe[i];
			
		}
			
		//j= 1000 - pAccNowR->pEntry->below_1_5G;
		//rpe =10* rpe/j ;
		gftInfo.rpeLoad += rpe;  
		totalSecond += 6*1000; // j 10ms
		gftInfo.rpe = (byte)(10*gftInfo.rpeLoad/(totalSecond));
		gftInfo.load += gftInfo.rpe;

	
}
void imUpdateBucket(ACC_DATA data, byte over10)
{
    byte i =0;
	
	int32_t r;
	long rpe =0;
	 if(pRamQueWtP->cstatus & ACC_BUFF_CSTAT_FULL_MSK) 
	   	  return;  //full nothing to do
	if(over10)
	{
		
		r  = B_10G; // make one value

	}
	else
		r =(int32_t)accGetResult((int32_t) data.x>>2,(int32_t)data.y>>2,(int32_t)data.z>>2);
    if((r > (int32_t)B_2G))
	{
	  gftInfo.explosiveFlag ++; 
	}
	else
	{
	  if(gftInfo.explosiveFlag  >=50 )
	  {
	    gftInfo.expCnt++;
		gftInfo.expPwr += gftInfo.explosiveFlag;
		
	  	(pRamQueWtP->bucket.bucket[10])++;
		(pRamQueWtP->bucket.bucket[11]) += gftInfo.explosiveFlag;

	  }
	  
	  gftInfo.explosiveFlag  = 0;  
	}
    
	
	if(over10)
		(pRamQueWtP->bucket.bucket[9])++; 
	//else if(r < (int32_t)B_1G)
	//{
	//  i =10;
	//  (pAccNowW->pEntry->bucket.bucket[i])++; 
	//}
	//else if(r < (int32_t)B_1G+ B_1G_OFFSET && r > (int32_t)B_1G -B_1G_OFFSET)
	//{
	//  i =11;
	//  (pAccNowW->pEntry->bucket.bucket[i])++; 
	//}	
	else if(r < (int32_t)B_2G)
	{
	  i =0;
	  rpe += pRamQueWtP->bucket.bucket[i] * rpeCoe[i];
	  (pRamQueWtP->bucket.bucket[i])++; 
	  
	}
	else if(r < (int32_t)B_3G)
	{
	  i = 1;
	  (pRamQueWtP->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_4G)
	{
	  i = 2;
	  (pRamQueWtP->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_5G)
	{
	  i = 3;
	 (pRamQueWtP->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_6G)
	{
	  i = 4;
	 (pRamQueWtP->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_7G)
	{
	  i = 5;
	  (pRamQueWtP->bucket.bucket[i])++; 
	}
	else if(r <(int32_t)B_8G)
	{
	  i = 6;
	  (pRamQueWtP->bucket.bucket[i])++; 
	}
	else if(r < (int32_t)B_9G)
	{
	//LED_YELLOW_On();
	  i = 7;
	  (pRamQueWtP->bucket.bucket[i])++; 
	}
	else if(r < B_10G)
	{
	 //LED_YELLOW_On();
	  i = 8;
	  (pRamQueWtP->bucket.bucket[i])++; 
	}
	else  
	{
	  i = 9;
	 (pRamQueWtP->bucket.bucket[i])++; 
	}
    bucketCnt++;
    
	if(bucketCnt >= 1000 )
	{
		   		
			motionDetected = 0;
    		pRamQueWtP->cstatus = ACC_BUFF_CSTAT_FULL_MSK;
    		(void)memcpy((void *)&(pRamQueWtP->rtc), (void *)&myRTC, sizeof(MY_RTC));
    		pRamQueWtP = pRamQueWtP->next;

			bucketCnt = 0;
			if(!pRamQueWtP->cstatus & ACC_BUFF_CSTAT_FULL_MSK) 
				imRstBucket(pRamQueWtP);
	}
    
}



void imSaveDataItemToEntry(void)
{
	volatile byte rtcV =0;   
	static volatile uint32_t mFlag =0;
	
	if(pRamQueWtI->cstatus & ACC_BUFF_CSTAT_FULL_MSK)
	{        
		LED_RED_On();
		return;  // out of ram  and get out of here  	 
	}
  	                            
    if (usrProfile.proxEnable) 
    {  
    
       if((!ProxIntState)||(!field_xmit_enable))                
       {
        accHitStat = ACC_HIT_STAT_INIT;
        return;
       }
    } 
    else                                                   
    {                                                     
        if(!field_xmit_enable)                            // 
        {                                                 // 
          accHitStat = ACC_HIT_STAT_INIT;                 // 
          return;                                         // 
        }                                                 // 
    }                                                    

     
   
	pRamQueWtI->cstatus = ACC_BUFF_CSTAT_FULL_MSK;
	 
	 
	pRamQueWtI->lpAccDataM= accDataM;
	pRamQueWtI->lpGyroDataM = gyroDataM;
	(void)memcpy((void *)&(pRamQueWtI->rtc), (void *)&myRTC, sizeof(MY_RTC));
	pRamQueWtI->rtc.StartMS = g_Tick1mS;
	pRamQueWtI = pRamQueWtI->next;

	

}






IMPACT_STATE imState=IM_STATE_SOFT_RESET;
volatile byte myTest;
volatile byte accLive =0;

long acc2;
byte field_xmit_enable = 1;           // Added by Jason Chen for the control of field xmit, 2014.03.19
extern uint8_t  max17047Exist;        // Added by Jason Chen for, 2014.04.22
extern uint16_t max17047Voltage;      // Added by Jason Chen for, 2014.04.22

word gSleepCnt =0; 
//Faraz
#define USB_OFF       0
#define USB_ON        1
//extern bool Usb_Mode;                    // Commented by Jason Chen because don't need it in this module, 2013.12.13
extern byte Message_Registered;
                                            

word imSleepTimer = 0;
//word imSleepSendTimer = 0;

word imSleepTimerTime = IM_SLEEP_TIMER;
byte lpTxEnable = 0;
byte timeA; 
byte timeB;



void imTask(void)
{
  //Faraz 
  byte Return_Value = 0;
  dword wTemp;
  static byte waitLoop = 0;
	//static ACC_STATE lowPriState = ACC_STATE_NONE;
	long accT = 0;
	byte alarmCoefficent =100;  
	byte static tenSecondCnt = 0;
	//byte i;
  //
  switch(imState)
  {
	  case IM_STATE_SOFT_RESET:
		  imState=IM_STATE_INIT;
			
		  break;
	  case IM_STATE_INIT:
	  	 
      if(usrProfile.mntLoc <2 || usrProfile.mntLoc >=100)
				  alarmCoefficent =100;
			else 
				  alarmCoefficent = usrProfile.mntLoc;
			
		  wTemp = usrProfile.tAlarm*100;
			wTemp = wTemp/alarmCoefficent;
		//acc2 = (word)(wTemp * wTemp * ACC_RESOLUTION_SQR);
		//acc2 = (long)(wTemp * wTemp * 6944);
		//acc2 = (long)(wTemp * wTemp * ACC_RESOLUTION_SQR);//((MMA68_G_LSB*4)*(MMA68_G_LSB*4));
			acc2 = (long)(wTemp * wTemp * 16393);//((MMA68_G_LSB*4)*(MMA68_G_LSB*4));
		//acc2 = 70000000;
		   
			(void)initQueI();
			(void)initQueP();
			(void)spiFlashGetCurWtAddrI();
			(void)spiFlashGetCurWtAddrP();
		  imState = IM_STATE_WAIT;
			tenSecondCnt =0;
		  break;
	  case IM_STATE_WAIT:
		  if( waitLoop++ > WAIT_AFTER_RESET)
		  {			
			   imState=IM_STATE_IDLE;
		  }
		  break;		
	  case IM_STATE_IDLE:	
      if(!gftInfo.pwrMode) 
      { 
         mTimer_TimeSet(&imSleepTimer );
            
      }
      imState=IM_STATE_SLEEP_WAIT;  
    case IM_STATE_SLEEP_WAIT:
		
      if(pRamQueRdP->cstatus & ACC_BUFF_CSTAT_FULL_MSK)
		  {	 
		
			  imState=IM_STATE_CALC_P;				 				  
		  } 
		  else if((pRamQueRdI->cstatus & ACC_BUFF_CSTAT_FULL_MSK))
		  {	 
		
			  imState=IM_STATE_CALC_I;
				 
				  
		  } 
      else if(!gftInfo.pwrMode) 
      { 
                     
        if(mTimer_TimeExpired( &imSleepTimer, imSleepTimerTime ) )
        {                                                    
#if ENABLE_SLEEP_PACKET_SEND                
               mTimer_TimeSet( &imSleepTimer );                                           // Added by Jason Chen, 2014.01.15 for Sending enter Sleep mode MSG
                                   
               Power_Off_Message.gid[0] = 0x01;                                           // 0x01 means Sleep Packet, added by Jason Chen, 2014.01.15                                                     
               Power_Off_Message.gid[1] = max17047Exist|max14676Exist;                                  // added battery level value into the special Packet, 20140314   
               Power_Off_Message.gid[2] = (byte)(batData1>>8);                            
               Power_Off_Message.gid[3] = (byte)batData1;
            // Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
            // Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;                                                
               Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8); 
               Power_Off_Message.gid[7] = (byte)max17047Voltage; 
               
             //Power_Off_Message.accEntryCnt = spiFlashCurRdBuffer.accEntryCnt;           // Added by Jason Chen, 2016.01.27                                                               
               Power_Off_Message.batPT =  (byte)max14676Charge;                                                                                                                         
               lpCmdMail = LP_CMD_OFF;                // 0x57 for testing, 20140115             
               lpState   = LP_STATE_IDLE;                                        
               imState   =IM_STATE_SENDING_SLEEP_MSG;                      
#else
               imState = IM_STATE_SLEEP;
#endif                   
                                            
            }
            else
            {
                if(IRQSC_IRQF)
                {
                    mTimer_TimeSet( &imSleepTimer );                    
                    IRQSC_IRQACK = 1;
					motionDetected =2;
					pwrOffCnt =0;
                }
            }
			
        }
       
	    break;
	    
#if ENABLE_SLEEP_PACKET_SEND	      
    case IM_STATE_SENDING_SLEEP_MSG:
        
        if(mTimer_TimeExpired( &imSleepTimer, 3 ))     // Added by Jason Chen, 2014.01.15 for Sending "enter Sleep mode" Packet 
        {
           //if(!gftInfo.lpBusy ) 
           {            
    		     imState = IM_STATE_SLEEP;                          
    		     buzzerOff();                              // Added by Jason Chen, 2014.04.14
           }
        } 
        else 
        {
            if(IRQSC_IRQF)                             // Added by Jason Chen, 2014.03.20, not enter Sleep mode MSG
            {
                mTimer_TimeSet( &imSleepTimer );
                IRQSC_IRQACK = 1;
                
                imState=IM_STATE_SLEEP_WAIT;  
                
                Power_Off_Message.gid[0] = 0x80;       // New Battery level report, 2014.03.20
                lpCmdMail = LP_CMD_OFF;
                lpState=LP_STATE_IDLE;   
				motionDetected =2;
            }            
        }
                             
        break;
#endif                        
    case IM_STATE_SLEEP:				
#if POWER_ON_BAT_REPORT                           // Added by Jason Chen for sending Power-On Packet periodically, 2013.11.26
        lpCmdMail = LP_CMD_PWR_ON;                     
        lpState   = LP_STATE_IDLE;          
          
        imState=IM_STATE_IDLE;
#else
		  //DisableInterrupts;
		  //pRamQueRdP->cstatus = 0;   //?????
			//pRamQueRdI->cstatus = 0;
		  //EnableInterrupts; 
			  timeA = myRTC.Second;
        ResetADCTimer(); 		          
		    gotoStop3();  
			
			  (void)getRtcStamp();	
			//if(myRTC.Second > timeA)
			//totalSecond += myRTC.Second - timeA;
			//motionDetected = 2;
      //reset previous recording 
        imState=IM_STATE_IDLE;  
	      accHitStat =  ACC_HIT_STAT_INIT;   
#endif		             
	  break;
	  case IM_STATE_CALC_I:   
		    __RESET_WATCHDOG();
		    if(pRamQueRdI->cstatus  & ACC_BUFF_CSTAT_FULL_MSK)  
		       imState=IM_STATE_SAVE_I;   
			break;

	  case IM_STATE_CALC_P:	
		    __RESET_WATCHDOG();
           
		     
			imUpdateRpe();
	  
			#define SECTION_IN_ONE_MIN 6
			if (++tenSecondCnt >= SECTION_IN_ONE_MIN)
			{
				imState=IM_STATE_SAVE_P;	
				tenSecondCnt =0;
				(void)memcpy((void *)&(performToSave.rtc), (void *)(&(pRamQueRdP->rtc)), sizeof(MY_RTC));
			}
			else 
				imState = IM_STATE_IDLE;
      DisableInterrupts;
		  pRamQueRdP->cstatus =0;    
		  EnableInterrupts; 
		  pRamQueRdP = pRamQueRdP->next;
				
	       
			
		  break;
    case IM_STATE_SAVE_I: //take 0.6ms/256 bytes
        // save data to flash
        // 
        
       
     __RESET_WATCHDOG();  
     LED_YELLOW_On()
		 impactToSave.mag_0 = FLASH_DATA_ENTRY_I_MAGIC_0;
		 impactToSave.mag_1 = FLASH_DATA_ENTRY_I_MAGIC_1;
		 impactToSave.accData = pRamQueRdI->lpAccDataM;
		 impactToSave.gyroData = pRamQueRdI->lpGyroDataM;
		 impactToSave.flag = FLASH_DATA_ENTRY_FLAG;
		 (void)memcpy((void *)&(impactToSave.rtc), (void *)&(pRamQueRdI->rtc), sizeof(MY_RTC));
			
     if(spiFlashWriteAccBufferI(&impactToSave))
     {
        LED_RED_On();
     }
		 else 
		 {
	      LED_YELLOW_Off();
	      DisableInterrupts;
	      pRamQueRdI->cstatus =0;    
	      EnableInterrupts; 
	      pRamQueRdI = pRamQueRdI->next;

		 }
		 imState=IM_STATE_IDLE;
     break;
	case IM_STATE_SAVE_P: 
	   __RESET_WATCHDOG();  
     LED_YELLOW_On()
     performToSave.mag_0 = FLASH_DATA_ENTRY_P_MAGIC_0;
	   performToSave.mag_1 = FLASH_DATA_ENTRY_P_MAGIC_1;
	   performToSave.expPwr = gftInfo.expPwr;
     performToSave.expCnt =  gftInfo.expCnt;
     performToSave.rpe =    gftInfo.rpe;
		 performToSave.intensity =  gftInfo.rpe;
     performToSave.pLoad = gftInfo.rpeLoad;
		 performToSave.flag = FLASH_DATA_ENTRY_FLAG;
		 performToSave.bt = (byte)max14676Charge;
		 performToSave.gftStatus =0;
		 if(spiFlashWriteAccBufferP(&performToSave))
		 {
			
			LED_RED_On();
		 }
		 else
		 {
			LED_YELLOW_Off();
		   
		 }
	   imState=IM_STATE_IDLE;
		  
     break;
	
	  default:
		 imState=IM_STATE_IDLE;
     break;
  }	
}
