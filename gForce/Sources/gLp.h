#ifndef __G_LP_H
#define __G_LP_H



// Moved to here by Jason from glp.c, 2014.12.05
//CYFISNP_NODE_Run() 35-50mS

//#define RADIO_PERIODIC_TIME  (50/RADIO_STACK_TIMER_UNITS)    
//#define RADIO_PERIODIC_TIME  (80/RADIO_STACK_TIMER_UNITS)    // Modified by Jason Chen, 20140212
//#define RADIO_PERIODIC_TIME  (160/RADIO_STACK_TIMER_UNITS)   // Modified by Jason Chen, 20140221, for 6 GFTs, about 4 minutes
//#define RADIO_PERIODIC_TIME    (100/RADIO_STACK_TIMER_UNITS)   // Modified by Jason Chen, 20140212, for 4 GFTs, about 2 minutes and 40 Seconds
//#define RADIO_PERIODIC_TIME  (92/RADIO_STACK_TIMER_UNITS)    // Modified by Jason Chen, 20140212, for 4 GFTs, about 2 minutes and 20 seconds
#define RADIO_PERIODIC_TIME  (84/RADIO_STACK_TIMER_UNITS)  // Modified by Jason Chen, 20140212, for 3 GFTs, about 2 minutes and 10 seconds
//#define RADIO_PERIODIC_TIME  (88/RADIO_STACK_TIMER_UNITS)    // Modified by Jason Chen, 20140212, for 3 GFTs, about 2 minutes and 10 seconds
/////////////////////////////////


typedef enum 
{
	
    LP_STATE_INIT = 1,
    LP_STATE_ALARM,  
    LP_STATE_TX,
    LP_STATE_IDLE,
    LP_STATE_RPT,
    LP_STATE_HIT_DATA,
    LP_STATE_ON,
    LP_STATE_OFF,
    LP_STATE_BIND_START,
    LP_STATE_BIND,
    LP_STATE_TRY_AGAIN_DLY_SET,
    LP_STATE_TRY_AGAIN_DLY,
    LP_STATE_TEST_WRITE,
    LP_STATE_TEST_READ,   
    LP_STATE_SUMMARY,      
    LP_STATE_SUMMARY_LOOP,    
    LP_STATE_BUCKET,
    LP_STATE_SUMMARY_IM,
    LP_STATE_HUB_ACK
              
    
}LP_STATE;

typedef enum 
{
	LP_CMD_NONE,
	LP_CMD_PWR_ON,
    LP_CMD_OFF,
    //LP_CMD_ALARM, 
    LP_CMD_RPT_RE,
    LP_CMD_RPT, //impacts
    LP_CMD_UNBIND,
    LP_CMD_UPLOAD,                    
    LP_CMD_BUCKET, //performance
    LP_CMD_BUCKET_RE, 
    LP_CMD_SUMMARY_RAM,
    LP_CMD_HUB_ACK
     
}LP_CMD;

typedef struct
{
#if 0
  byte alarm;
  word gforce;
  word cunt;
  byte alarmThres;
  
#elif 1
    byte magic0;
    byte magic1;
 	byte Year;          // 0                // Added the new construct for Summary Package transmission, 2015.01.08
	byte Month;         // 1
	byte Day;           // 2
	byte Hour;          // 3
	byte Minute;        // 4
	byte Second;        // 5
	word StartMS;       // 6  7
	
	word mag;  	        // 8  9
	word impactNum;     // 10 11
 
    signed int x;
    signed int y;
    signed int z; 
    byte alarmThres;    
    signed int gyro_x;
    signed int gyro_y;
    signed int gyro_z;	
#if ENABLE_SUMMARY_ACK
	byte crcSum;
#endif
  
#endif  
} LP_RPT_PKT;

typedef struct 
{   
	//byte magic[2];  
	//word len;    
	byte cmd;   
	byte data[400];    
} LP_ALARM_PKT; 

typedef struct
{
  //byte len;
  byte gid[6];
  byte name[20];        
  byte pn;
  
  WORD batRawAverage;     // added by Jason Chen, 2013.11.14 for Baterry report rawData
  WORD batFuelAverage;    // added by Jason Chen, 2013.11.18 for Baterry report rawData from fuel gauge
  WORD impactCnt;         // added by Jason Chen, 2013.12.16 for uploading data
  byte gftLpMode;         // added by Jason Chen, 2013.12.09 for Backchannel
  byte alarmThres;        // added by Jason Chen, 2014.04.15 for Threshold Xmit
  byte m17047Exist;       // added by Jason Chen, 2014.04
  byte RecThres;          // added by Jason Chen, 2014.04.29 for Record Threshold
  byte AlarmMode;         // added by Jason Chen, 2014.04.30 for Alarm Mode 
  byte loc[9];
  byte batPT;
  byte mntType; //49
  byte crcSum;
} LP_PWR_ON_PKT;

typedef struct
{ 
  byte gid[6];
  byte Rssi;
} LP_TEST_PKT;

typedef struct
{
  //byte len;
  byte gid[8];
  unsigned long activeTime;                                 // Added by Jason Chen, 2015.02.06
  word accEntryCnt;                                         // Added by Jason Chen, 2016.01.27
  byte batPT; //battery charged percentage 
  byte rpe;
  long load;
} LP_PWR_OFF_PKT;
#define IMMEDIATE_SUB_TYPE_BUCKET   0x55
#define IMMEDIATE_SUB_TYPE_SUMMARY   0xAA
typedef struct
{ 
  
  byte magic; //always be  
  unsigned long pLoad;                                  
  byte  rpe;
  unsigned long expCnt;  //explosive count
  unsigned long expPwr;  //explosive power
  
} LP_BUCKET_PKT;



typedef struct
{
	byte cmd;
    union 
  	{
  		
  		word perfIndex;
		word dataIndex;
		
  	}load;

}LP_CMD_STRUCT;







extern byte myId;                                           // Added by Jason Chen, 2017.03.24 

extern byte Statup_flag;                                    // Added by Jason Chen for first time startup, 2017.03.30

extern LP_PWR_ON_PKT Power_On_Message;
extern LP_PWR_OFF_PKT Power_Off_Message;
extern LP_RPT_PKT lpRptPkt;

//byte const Upload_Data_Message[416]
extern volatile byte lpCmdMail;
extern void processLp(void);
extern byte radioUnBind(void);

	extern void poweroff_reset(void);                           // Added by Jason Chen, 2017.03.24 
#endif
