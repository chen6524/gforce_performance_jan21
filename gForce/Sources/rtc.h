/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/

#ifndef _RTC_H
#define _RTC_H
#include "typedef.h"

#define RTC_TICK_PER_MSECOND  3  
#define RTC_ONE_SEC_CNT      1000*RTC_TICK_PER_MSECOND




#define RTC_ENABLE()     {RTCSC_RTIE=1; RTCSC_RTIF =1;}
#define RTC_DISABALE()   {RTCSC_RTIE=0; RTCSC_RTIF =1;}




typedef struct
{
	//unsigned char mutex;
	//unsigned char action;
	//MY_RTC_TAG tag;
	byte Year;
	byte Month;
	byte Day;
	byte Hour;
	byte Minute;
	byte Second;
	word StartMS;
} MY_RTC;


extern volatile word g_Tick;
extern volatile word g_Tick1mS;            // 1 mS timer increment
extern volatile word g_TickSecond;         // 1 // 1 second
   
extern volatile MY_RTC myRTC;
extern MY_RTC lostRTC; //rtc time before sync
extern MY_RTC newRTC;
extern MY_RTC startRTC;

extern word sessionStartcnt;
extern volatile byte g_StartHg;
extern volatile byte g_RecordFlag;
extern volatile byte g_SampleFlag;
extern volatile byte g_SampleFlagL;


void RtcInit(void);

bool getRtcStamp(void);

extern  word buzzTimer;
#endif






