
/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#include "gft.h"
#include <hidef.h>

#include "typedef.h"
#include "rtc.h"
#include "mcu.h"
#include "gLowG.h"
#include "LSM330DLC.h"
#include "adc.h"
#include "spi_flash.h"
#include "mHid.h"

#include "I2C_driver.h"
#include "bcd_common.h"
#include "radio.h"

#include "MAX44000.h"
#include "max17047.h"
#include "max14676.h"
#include "RTC_Drv.h"

#include "gLp.h"                                // Added by Jason chen, 2013.11.26
#include "Utils.h"                              // Added by Jason chen, 2013.11.26  

#include "mFlash.h"                             // Added by Jason Chen, 2014.12.08
#include "LSM6DS3.h"                            // Added by Jason Chen, 2016.01.15

byte profile_alarm_au_backup;                   // Added by Jason chen, 2014.11.26

/*****************************************************************************
 * Function     McuInit
 * Overview:    Initialization of the MCU.
 *              It will configure the JM60 to disable STOP and COP Modules.
 *              It also set the MCG configuration and bus clock frequency.
 *
 * Input  : None
 *
 * Return : None
 ****************************************************************************/
void McuInit()
{
    
   // STOP S3.6 page 36 for setting stop mode
   // watchdog will stop when in stop mode
   // Disable the COP,  and enable the MCU stop mode  (C5.7.4 p74)
    SOPT1 =   0 //0xF3; 
            | 0x1                              // bit 0 N/A
            | 0x2                              // bit 1 N/A
            | 0x0                              // bit 2 N/A
            | 0x0                              // bit 3 N/A
            | 0x10                             // bit 4  N/A
            | SOPT1_STOPE_MASK   // bit 5   1 -- STOP Mode Enable  
            | SOPT1_COPT0_MASK   // bit 6 COP Watchdog Timeout, bit 0    
            | SOPT1_COPT1_MASK   // bit 7 COP Watchdog Timeout, bit 1    
            ;                                  //     bit 7|6     10 -- long timeout 256 mS if COPCKS in SOPT2 is 0)
                                               //                 11 -- POR  1.024 S 
                                               //                      
    //C5.7.5 p76        
    SOPT2 =   0 //00 
           // | SOPT2_ACIC             //  bit 0  Analog Comparator to Input Capture Enable 
           // | SOPT2_SPI2FE            // bit 1 SPI2 Ports Input Filter Enable
           // | SOPT2_SPI1FE            // bit 2 SPI1 Ports Input Filter Enable
            | 0                              // bit 3 N/A
            | 0                              // bit 4  N/A
            | 0                              // bit 5   N/A
            // | SOPT2_COPW_MASK    // bit 6 COP Window    when use LPO it is not available
            // | SOPT2_COPCLKS_MASK// bit 7 COP Watchdog Clock Select     0 internal 1K LPO 1 bus clock 
              ;
            
         

    SPMSC1 =  0 //0x40;
              | SPMSC1_BGBE_MASK       //  bit 0  Bandgap Buffer Enable 
              | 0                                 // bit 1  N/A
             // | SPMSC1_LVDE_MASK       // bit 2 Voltage Detect Enable
             //| SPMSC1_LVDSE_MASK      // bit 3 Low-Voltage Detect Stop Enable
             //| SPMSC1_LVDRE_MASK      // bit 4  Low-Voltage Detect Reset Enable
             //| SPMSC1_LVWIE_MASK      // bit 5   Low-Voltage Warning Interrupt Enable 
             //| SPMSC1_LVWACK_MASK   // bit 6 Low-Voltage Warning Acknowledge        
             //| SPMSC1_LVWF_MASK      // bit 7 Low-Voltage Warning status     
              ;

            
    SPMSC2 =  0 //0x00 
             //| SPMSC2_PPDC_MASK  //bit0:   PPDC       Partial Power Down Control
              |0  //bit1:   0           
             // | SPMSC2_PPDACK_MASK  // bit2:   PPDACK Partlal Power Down Acknowledge
             // | SPMSC2_PPDF_MASK   // bit3:   PPDF        partlal Power Down Flag
             // | SPMSC2_LVWV_MASK  //bit4:   LVWV      Low-Voltage Warning Voltage Select
             //  | SPMSC2_LVDV_MASK  // bit5:   LVDV     Low_Voltage Detect Voltage select
              | 0                             // bit6:   N/A
              | 0                              //bit7:   N/A
              ;

    //C1.3 p21  C12.1 181
    //in order to use USB need config MCG to PEE mode 
    // MCG is default set to FEI mode
    //see C12.5.2.1 p200 example 
    //MCGOUT 48M BUSCLK 24M
        	
	/*	System clock initialization */
	//if (*(unsigned char*)0xFFAF != 0xFF) { /* Test if the device trim value is stored on the specified address */
	  MCGTRM = 179;//*(unsigned char*)0xFFAF;  /* Initialize MCGTRM register from a non volatile memory */
	  MCGSC = *(unsigned char*)0xFFAE;	 /* Initialize MCGSC register from a non volatile memory */
	//}
	
#if 0
    /* change to FBE mode*/
    MCGC2 = 0 //0x36 
                   | MCGC2_EREFSTEN_MASK    //bit0:   EREFSTEN   Exernal reference stop enable
                   | MCGC2_ERCLKEN_MASK     //bit1:   ERCLKEN    External reference enable
                   | MCGC2_EREFS_MASK        //bit2:   EREFS      External reference select
                 //  | MCGC2_LP_MASK            //bit3:   LP          Low power select
                   | MCGC2_HGO_MASK         //bit4:   HGO     High gain oscillator select for >=1MHz
                   | MCGC2_RANGE_MASK// bit5:   Range     Frequency range select
                   //| MCGC2_BDIV_MASK//bit6:   BDIV      Bus frequency divider   
                  // | MCGC2_BDIV_MASK//bit7:   BDIV  
                   ;
                         
    while(!(MCGSC & MCGSC_OSCINIT_MASK)){
        __RESET_WATCHDOG();                         /*wait for the OSC stable*/
                                                     
    }
                            /*  
                         *   MCGSC 
                         *    bit0:   FTRIM     MCG fine trim
                         *    bit1:   OSCINIT   OSC initialization
                         *    bit2:   CLKST0        Clock Mode status
                         *    bit3:   CLKST1
                         *    bit4:   IREFST        Internal reference status
                         *    bit5:   PLLST     PLL select status
                         *    bit6:   LOCK      Lock Status
                         *    bit7:   LOLS      Loss of lock status
                         */
                         
    MCGC1 =  0 //0x9B  
                     | MCGC1_IREFSTEN_MASK  //bit0:   IREFSTEN   Internal reference stop enable
                     | MCGC1_IRCLKEN_MASK   //bit1:   IRCLKEN    Internal reference clock enable     
                   //| MCGC1_IREFS_MASK     //bit2:   IREFS      Internal reference select
                     | MCGC1_RDIV0_MASK     //bit3:   RDIV   0   Reference divider
                     | MCGC1_RDIV1_MASK     //bit4:   RDIV1
                   //| MCGC1_RDIV2_MASK     //bit5:   RDIV 2
                  // | MCGC1_CLKS0_MASK     //bit6:   CLKS0       Clock source select
                     | MCGC1_CLKS1_MASK     //bit7:   CLKS1
                     ;
                            
    while(( MCGSC & (MCGSC_IREFST_MASK| MCGSC_CLKST_MASK)) !=  MCGSC_CLKST1_MASK) // check the external reference clock is selected 
    {
        __RESET_WATCHDOG();
    }
    
    /* Switch to PBE mode from FBE*/
    MCGC3 = 0 //0x48 
            //|MCGC3_VDIV0_MASK  //bit0:   VDIV0        VCO divider
            //|MCGC3_VDIV1_MASK //bit1:   VDIV1
            //|MCGC3_VDIV2_MASK //bit2:   VDIV2
              | MCGC3_VDIV3_MASK //bit3:   VDIV3
            // |0 //bit4:   
            // |MCGC3_CME_MASK//bit5:   CME      Clock Monitor Enable
              |MCGC3_PLLS_MASK//bit6:   PLLS         PLL select
             // |MCGC3_LOLIE_MASK  //bit7:   LOLIE       Loss of lock interrupt enable
              ;

    //wait for the PLL is locked 
    while ((MCGSC & (MCGSC_LOCK_MASK | MCGSC_CLKST1_MASK)) != (MCGSC_LOCK_MASK | MCGSC_CLKST1_MASK))
    {
       __RESET_WATCHDOG();
    }

    /*Switch to PEE mode from PBE mode*/
    MCGC1 &= 0 //0x3F;
                    | MCGC1_IREFSTEN_MASK //bit0:   IREFSTEN    Internal reference stop enable
                    | MCGC1_IRCLKEN_MASK   //bit1:   IRCLKEN    Internal reference clock enable     
                    | MCGC1_IREFS_MASK        //bit2:   IREFS       Internal reference select
                    | MCGC1_RDIV0_MASK        //bit3:   RDIV    0   Reference divider
                    | MCGC1_RDIV1_MASK        //bit4:   RDIV1
                    | MCGC1_RDIV2_MASK       // bit5:   RDIV 2
                    // | MCGC1_CLKS0_MASK     //bit6:   CLKS0       Clock source select
                    // | MCGC1_CLKS1_MASK    //bit7:   CLKS1
                     ;
    while((MCGSC & (MCGSC_LOCK_MASK | MCGSC_PLLST_MASK | MCGSC_CLKST_MASK) ) != (MCGSC_LOCK_MASK | MCGSC_PLLST_MASK | MCGSC_CLKST_MASK))
    {
        __RESET_WATCHDOG();
    }
#else
	/* MCGC2: BDIV=0,RANGE=1,HGO=1,LP=0,EREFS=1,ERCLKEN=0,EREFSTEN=1 */
	setReg8(MCGC2, 0x36);				 /* Set MCGC2 register */ 
	/* MCGC1: CLKS=2,RDIV=7,IREFS=0,IRCLKEN=1,IREFSTEN=0 */
	setReg8(MCGC1, 0xBA);				 /* Set MCGC1 register */ 
	while(!MCGSC_OSCINIT) { 			 /* Wait until external reference is stable */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	while(MCGSC_IREFST) {				 /* Wait until external reference is selected */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	while((MCGSC & 0x0C) != 0x08) { 	 /* Wait until external clock is selected as a bus clock reference */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	/* MCGC2: BDIV=0,RANGE=1,HGO=1,LP=1,EREFS=1,ERCLKEN=0,EREFSTEN=0 */
	setReg8(MCGC2, 0x3E);				 /* Set MCGC2 register */ 
	/* MCGC1: CLKS=2,RDIV=3,IREFS=0,IRCLKEN=1,IREFSTEN=0 */
	setReg8(MCGC1, 0x9A);				 /* Set MCGC1 register */ 
	/* MCGC3: LOLIE=0,PLLS=1,CME=0,??=0,VDIV=8 */
	setReg8(MCGC3, 0x48);				 /* Set MCGC3 register */ 
	/* MCGC2: LP=0 */
	clrReg8Bits(MCGC2, 0x08);			  
	while(!MCGSC_PLLST) {				 /* Wait until PLL is selected */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	while(!MCGSC_LOCK) {				 /* Wait until PLL is locked */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	/* MCGC1: CLKS=0,RDIV=3,IREFS=0,IRCLKEN=1,IREFSTEN=0 */
	
	setReg8(MCGC1, 0x1A);				 /* Set MCGC1 register */ 
	
	while((MCGSC & 0x0C) != 0x0C) { 	 /* Wait until PLL clock is selected as a bus clock reference */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
#endif
    //flash clock init
    //clear error flags
    FSTAT = 0x30;  
    if( FCDIV_DIVLD == 0 ) {    
        FCDIV = FCDIV_PRDIV8_MASK | 0xF;
    }
    return;
    
}
    
  
void WaitUntillClockStable(void) {
  
	/* MCGC2: BDIV=0,RANGE=1,HGO=1,LP=0,EREFS=1,ERCLKEN=0,EREFSTEN=1 */
	setReg8(MCGC2, 0x36);				 /* Set MCGC2 register */ 
	/* MCGC1: CLKS=2,RDIV=7,IREFS=0,IRCLKEN=1,IREFSTEN=0 */
	setReg8(MCGC1, 0xBA);				 /* Set MCGC1 register */ 
	while(!MCGSC_OSCINIT) { 			 /* Wait until external reference is stable */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	while(MCGSC_IREFST) {				 /* Wait until external reference is selected */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	while((MCGSC & 0x0C) != 0x08) { 	 /* Wait until external clock is selected as a bus clock reference */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	/* MCGC2: BDIV=0,RANGE=1,HGO=1,LP=1,EREFS=1,ERCLKEN=0,EREFSTEN=0 */
	setReg8(MCGC2, 0x3E);				 /* Set MCGC2 register */ 
	/* MCGC1: CLKS=2,RDIV=3,IREFS=0,IRCLKEN=1,IREFSTEN=0 */
	setReg8(MCGC1, 0x9A);				 /* Set MCGC1 register */ 
	/* MCGC3: LOLIE=0,PLLS=1,CME=0,??=0,VDIV=8 */
	setReg8(MCGC3, 0x48);				 /* Set MCGC3 register */ 
	/* MCGC2: LP=0 */
	clrReg8Bits(MCGC2, 0x08);			  
	while(!MCGSC_PLLST) {				 /* Wait until PLL is selected */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	while(!MCGSC_LOCK) {				 /* Wait until PLL is locked */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	}
	/* MCGC1: CLKS=0,RDIV=3,IREFS=0,IRCLKEN=1,IREFSTEN=0 */
	
	setReg8(MCGC1, 0x1A);				 /* Set MCGC1 register */ 
	
	while((MCGSC & 0x0C) != 0x0C) { 	 /* Wait until PLL clock is selected as a bus clock reference */
	 SRS = 0x55;						 /* Reset watchdog counter write 55, AA */
	 SRS = 0xAA;
	} 
}
 
    
void ioInit(void)
{
    //irq config
	#ifdef GFT_GEN2
    	IRQSC = 0   
              //| IRQSC_IRQIE_MASK    // 1 enable interrupt 0-polling
                | IRQSC_IRQPE_MASK      //enable pin function
                | IRQSC_IRQMOD_MASK     // 1 interrupt on falling raising and low high level
              //| IRQSC_IRQEDG_MASK     // 0 falling edge or low level 
    	        //| IRQSC_IRQPDD_MASK     // 0 use internal pullup
    	        ; 

	#else
      //IRQSC = 0   
      //        | IRQSC_IRQIE_MASK    // 1 enable interrupt 0-polling
      //        | IRQSC_IRQPE_MASK      //enable pin function
      //        | IRQSC_IRQMOD_MASK     // 1 interrupt on falling raising and low high level
      //        | IRQSC_IRQEDG_MASK     // 0 falling edge or low level 
  	  //        | IRQSC_IRQPDD_MASK     // 0 use internal pullup
  	  //        ; 
  #endif
  
  
    // TPM2CH0 for Buzz 
    //
    ////////////////////////////////////////////////////////////////////////////////////////
    //
    TPM2SC = 0
           //| TPM2SC_TOF_MASK//timeer overflow flag
           //| TPM2SC_TOIE_MASK//Timer overflow interrupt enable
           //| TPM2SC_CPWMS_MASK// Center-aligned pwm select
           //| TPM2SC_CLKSB_MASK // clock source selects
             | TPM2SC_CLKSA_MASK // 
           //| TPM2SC_PS2 //   prescale 1,2,4,8...
           //| TPM2SC_PS1 //
           //| TPM2SC_PS0 //
           ;
  #if 1
    TPM2C0SC = 0
      	 //| TPM2C0SC_CH0F_MASK	     // 1 enable capture or output compare
   		   //| TPM2C0SC_CH0IE_MASK	 // 1 channel interrupt requests enabled
   		     | TPM2C0SC_MS0B_MASK 	     //  (B:A)
   		   //| TPM2C0SC_MS0A_MASK	     //   0:0 no clock
   		     | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   		   //| TPM2C0SC_ELS0A_MASK	  
   		 ;
    
    TPM2MOD = 9000;		          // 2.6KHz (250 uSec)
    TPM2C0V = TPM2MOD/2;			  // 50% duty cycle, max volume
    
        #endif
#ifdef GFT_GENW2
    TPM1SC = 0
           //| TPM2SC_TOF_MASK//timeer overflow flag
           //| TPM2SC_TOIE_MASK//Timer overflow interrupt enable
           //| TPM2SC_CPWMS_MASK// Center-aligned pwm select
           //| TPM2SC_CLKSB_MASK // clock source selects
             | TPM2SC_CLKSA_MASK // 
           //| TPM2SC_PS2 //   prescale 1,2,4,8...
           //| TPM2SC_PS1 //
           //| TPM2SC_PS0 //
           ;
    TPM1C1SC = 0
       //| TPM2C0SC_CH0F_MASK	       // 1 enable capture or output compare
   		 //| TPM2C0SC_CH0IE_MASK	     // 1 channel interrupt requests enabled
   		   | TPM2C0SC_MS0B_MASK 	     // (B:A)
   		 //| TPM2C0SC_MS0A_MASK	       // 0:0 no clock
   		   | TPM2C0SC_ELS0B_MASK	     // 1:0 fixed system clock p281
   		   | TPM2C0SC_ELS0A_MASK	  
   		 ;
    
    TPM1MOD = 24000;		  // 1KHz (1mSecond)
    TPM1C1V = TPM1MOD/10;	
#if 1
	  TPM1C3SC = 0
       //| TPM2C0SC_CH0F_MASK	       // 1 enable capture or output compare
   		 //| TPM2C0SC_CH0IE_MASK	     // 1 channel interrupt requests enabled
   		   | TPM2C0SC_MS0B_MASK 	     //  (B:A)
   		 //| TPM2C0SC_MS0A_MASK	       //   0:0 no clock
   		   | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   		   | TPM2C0SC_ELS0A_MASK	  
   		 ;  
    TPM1C3V = TPM1MOD/10;	
#endif
	  TPM2C1SC = 0
       //| TPM2C0SC_CH0F_MASK	       // 1 enable capture or output compare
   		 //| TPM2C0SC_CH0IE_MASK	     // 1 channel interrupt requests enabled
   		   | TPM2C0SC_MS0B_MASK 	     // (B:A)
   		 //| TPM2C0SC_MS0A_MASK	       // 0:0 no clock
   		   | TPM2C0SC_ELS0B_MASK	     // 1:0 fixed system clock p281
   		   | TPM2C0SC_ELS0A_MASK	  
   		 ;
   
    TPM2C1V = TPM2MOD/10;	
#endif



#ifdef GFT_GEN2
    TPM1SC = 0
           //| TPM2SC_TOF_MASK//timeer overflow flag
           //| TPM2SC_TOIE_MASK//Timer overflow interrupt enable
           //| TPM2SC_CPWMS_MASK// Center-aligned pwm select
           //| TPM2SC_CLKSB_MASK // clock source selects
             | TPM2SC_CLKSA_MASK // 
           //| TPM2SC_PS2 //   prescale 1,2,4,8...
           //| TPM2SC_PS1 //
           //| TPM2SC_PS0 //
           ;
    TPM1C1SC = 0
      	 //| TPM2C0SC_CH0F_MASK	     // 1 enable capture or output compare
   		   //| TPM2C0SC_CH0IE_MASK	 // 1 channel interrupt requests enabled
   		     | TPM2C0SC_MS0B_MASK 	     //  (B:A)
   		   //| TPM2C0SC_MS0A_MASK	     //   0:0 no clock
   		     | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   		     | TPM2C0SC_ELS0A_MASK	  
   		     ;
    
    TPM1MOD = 24000;		  // 1KHz (1mSecond)
    TPM1C1V = TPM1MOD/10;	

	  TPM1C2SC = 0
       //| TPM2C0SC_CH0F_MASK	     // 1 enable capture or output compare
   		 //| TPM2C0SC_CH0IE_MASK	 // 1 channel interrupt requests enabled
   		   | TPM2C0SC_MS0B_MASK 	     //  (B:A)
   		 //| TPM2C0SC_MS0A_MASK	     //   0:0 no clock
   		   | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   		   | TPM2C0SC_ELS0A_MASK	  
   		 ;  
    TPM1C2V = TPM1MOD/10;	

    TPM2C1SC = 0
      	 //| TPM2C0SC_CH0F_MASK	     // 1 enable capture or output compare
   		   //| TPM2C0SC_CH0IE_MASK	 // 1 channel interrupt requests enabled
   		     | TPM2C0SC_MS0B_MASK 	     //  (B:A)
   		   //| TPM2C0SC_MS0A_MASK	     //   0:0 no clock
   		     | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   		     | TPM2C0SC_ELS0A_MASK	  
   		     ;
   
    TPM2C1V = TPM2MOD/10;	
#endif
// After reset, all pins are configured as high-impedance general-purpose inputs
// with internal pullup devices disabled
//
 #if defined(GFT_GEN2)
   PTAD  = 0x00;			 
   PTADD = 0xFE;	    /*Set output direction*/
   PTAPE = 0x00;		/*Pull-up disable*/
   PTASE = 0x00;	    /*slew rate control*/
   PTADS = 0x00;	    /*drive strength*/

	   
   PTBD  = 0x00;			  
   PTBDD = 0xDF;	  
   PTBPE = 0x00;	   
   PTBSE = 0x00;	    
   PTBDS = 0x00;	    
   
   PTCD  = 0x04;			 
   PTCDD = 0x3C;	 
   PTCPE = 0x03;	   
   PTCSE = 0x00;	   
   PTCDS = 0x00;		  
   
   
   PTDD =  0x00; 		    
   PTDDD = 0xFB;	  
   PTDPE = 0x00;		 
   PTDSE = 0x00;	    
   PTDDS = 0x00;		  
   
   PTED =  0x8C;		    
   PTEDD = 0xFF;	 
   PTEPE = 0x00;		 
   PTESE = 0x00;	    
   PTEDS = 0x00;		  
   
   PTFD  = 0x21;			 
   PTFDD = 0xFD;	  
   PTFPE = 0x00;	   
   PTFSE = 0x00;	    
   PTFDS = 0x00;		 
   
   PTGD =  0x00; 		    
   PTGDD = 0xF0;		   
   PTGPE = 0x0F;		 
   PTGSE = 0x00;	   
   PTGDS = 0x00;		  
#elif defined(GFT_GENW1)    // wirless version
   PTAD =  0x20;         
   PTADD = 0x20;	         /*Set output direction*/
   PTAPE = 0x00;		       /*Pull-up disable*/
   PTASE = 0x00;	         /*slew rate control*/
   PTADS = 0x00;	         /*drive strength*/
	   
   PTBD  = 0x07;	    
   PTBDD = 0x0F;	    
   PTBPE = 0x00;	    
   PTBSE = 0x00;	    
   PTBDS = 0x00;	     
   
   PTCD  = 0x04;	     
   PTCDD = 0x3C;	    
   PTCPE = 0x00;	    
   PTCSE = 0x00;	   
   PTCDS = 0x00;	    
   
   
   PTDD  = 0x80; 	    
   PTDDD = 0x80;	    
   PTDPE = 0x80;	   
   PTDSE = 0x00;	   
   PTDDS = 0x00;		  
   
   PTED =  0x8F;		   
   PTEDD = 0x8F;	  
   PTEPE = 0x00;		 
   PTESE = 0x00;	    
   PTEDS = 0x00;		  
   
   PTFD  = 0x60;			 
   PTFDD = 0x70;	  
   PTFPE = 0x00;	   
   PTFSE = 0x00;	    
   PTFDS = 0x00;		  
   
   PTGD  = 0x00; 		   
   PTGDD = 0x00;		  
   PTGPE = 0x02;		 
   PTGSE = 0x00;	    
   PTGDS = 0x00;		 
#else // GFT_GENW2
   PTAD =  0x00;         
   PTADD = 0x00;	     
   PTAPE = 0x00;		 
   PTASE = 0x00;	     
   PTADS = 0x00;	     
	   
   PTBD  = 0x07;	    
   PTBDD = 0x07;	    
   PTBPE = 0x00;	    
   PTBSE = 0x00;	    
   PTBDS = 0x00;	   
   
   PTCD  = 0x04;	    
   PTCDD = 0x34;//0x3C;	     Changed by Jason Chen, 2014.04.10       for ST pin
   PTCPE = 0x00;	    
   PTCSE = 0x00;	    
   PTCDS = 0x00;	    
   
   
   PTDD  = 0x00; 	    
   PTDDD = 0x00;	    
   PTDPE = 0x00;	   
   PTDSE = 0x00;	    
   PTDDS = 0x00;		 
   
   PTED =  0x8E;   //0x8F,   // Changed by Jason Chen,  RF Chip Select  Control = 0, 2014.04.10
   PTEDD = 0x8F;	  
   PTEPE = 0x00;		 
   PTESE = 0x00;	    
   PTEDS = 0x00;
   		 
   //PTEPE_PTEPE4 = 1; 
   //PTEDD_PTEDD5 = 1;
   //PTEDD_PTEDD6 = 1;
   //PTEDS_PTEDS5 = 1;
   //PTEDS_PTEDS6 = 1;
   //PTED_PTED5   = 1;
   
   PTFD  = 0x63;//0x62			 // Changed by Jason Chen,  RF Power Control = 1, 2014.04.10
   PTFDD = 0x73;//0x72;	     // Changed by Jason Chen,  RF Power Control as output, 2014.04.10
   PTFPE = 0x00;	    
   PTFSE = 0x00;	  
   PTFDS = 0x00;
   //PTFDS = 0x10;//0x00;		  // Changed by Jason Chen to Enable PTF4 Drive Strength, 2014.04.110      for Buzzer
   
   
   PTGD  = 0x00; 		  
   PTGDD = 0x00;		   
   PTGPE = 0x02; //0x02		     // Pull up resister enabled for proximit interrupt, Jason Chen, 11.07.2013
   PTGSE = 0x00;	   
   PTGDS = 0x00;		 
#endif

   if (getHardIDMajor() > 0x1)
      PTCDD_PTCDD3  = 0;
   if (getHardIDMajor() == 0x4)
      PTBDD_PTBDD5 =0;
   PWR_SENS_ON();
}



void Reboot(void)
{
  DisableInterrupts;
  for(;;);
}


//spi2 for accelerometer
void Spi2Init(void)
{
    // gyro use CPOL =1, CPHA =1, MSB first
    //the input clock is BUSCLK 
    unsigned char tmp;
    SPI2C1 = 0x00 //
         //| SPI2C1_SPIE_MASK //interrupt enable for SPRF & MODF
           | SPI2C1_SPE_MASK  //Enable Spi module
         //| SPI2C1_SPTIE_MASK //transmit interrupt enalbe
           | SPI2C1_MSTR_MASK //master mode if 1
           | SPI2C1_CPOL_MASK //clock polarity 1 active low spi clock idle high
           | SPI2C1_CPHA_MASK  //clock phase 1 first edge on spsck occurs at the statrt of the first cycle
         //| SPI2C1_SSOE_MASK  //slave select output enable
         //| SPI2C1_LSBFE_MASK  //LSB first 0 msb first
           ;
    SPI2C2 = 0x00 //
           //| SPI2C2_SPMIE_MASK   //match interrupt enable
           //| SPI2C2_SPIMODE_MASK   //8/16 bit mode
           //| SPI2C2_MODFEN_MASK    //mode fault function enable
           //| SPI2C2_BIDIROE_MASK   // 
             | SPI2C2_SPISWAI_MASK
           //| SPI2C2_SPC0_MASK
           ;
	SPI2BR = 0x00 //  6M
		   //| SPI2BR_SPPR2_MASK   //prescale
		   //| SPI2BR_SPPR1_MASK     // 1-8
		     | SPI2BR_SPPR0_MASK
		   //| SPI2BR_SPR2_MASK    //divisor 
		   //| SPI2BR_SPR1_MASK    // 2,4,8,16,32,64,128,256 
		   //| SPI2BR_SPR0_MASK    //
		   ;

    // dummy read of spi status and data registers
    tmp = SPI2S;  //initiate the master spi device
    tmp = SPI2DH;
	tmp = SPI2DL;
   
}

//IIC
//RTC
void I2CInit()
{
;

}



void buzzerOn(void)
{  
  
    //if( TPM2C0V==0 )
    
   TPM2SC_CLKSA=1;
   TPM2C0V=TPM2MOD/2;
  
  
}  
 
void buzzerOff(void)  
{
#if 0
	  if( TPM2C0V ) 
	  {
		TPM2SC_CLKSA=1;
		TPM2C0V=0;
	  }
#else
		TPM2SC_CLKSB = 0;	 // Disbale the timer
		TPM2SC_CLKSA = 1;
		TPM2C0V=0;
		
		PTFDD_PTFDD4 = 1;
		PTFD_PTFD4	 = 0;
			  
#endif

}
void shutdownRadioGpio(void)
{
	//radio
		
		//MOSI I
		//PTEPE_PTEPE4 = 0;
		PTEDD_PTEDD4 = 1;
		PTED_PTED4 = 0;
		
		//RF_ON
		PTFPE_PTFPE0 = 0;
		PTFDD_PTFDD0 = 1;
		PTFD_PTFD0 = 0; 
	
		//SS_RF
		//PTEPE_PTEPE0 = 0;
		PTEDD_PTEDD0 = 1;
		PTED_PTED0	 = 0;
		//PTBPE_PTBPE0 = 1;
		//PTBDD_PTBDD0 = 0;
		
							  
		
		 PTEDD_PTEDD6 = 1;	 
		 PTED_PTED6 = 0;			   //SCK1  setup as 0
		 
		 
		 PTEDD_PTEDD5 = 1;
		 PTED_PTED5 = 0;			   //MOSI1 setup as 0						  
		 
		 PTCDD_PTCDD3 = 1;			   
		 PTCD_PTCD3 = 0;  
		 
#ifdef  ENABLE_6DS3
		 PTBDD_PTBDD5 = 1; // irq_rf
		 PTBD_PTBD5 = 0; 
#endif
					

}



void sensorPowerOff(void)
{
	 PWR_SENS_OFF();

    SPI2C1 = 0x00;
  
    //CS_AG   
    PTFDD_PTFDD6 = 1;
    PTFD_PTFD6   = 0;                                  
   
    PTBDD_PTBDD1 = 1;
    PTBD_PTBD1 = 0;                          // MOSI2
    
	
    PTBDD_PTBDD2 = 1;                              // SCK2
    PTBD_PTBD2 = 0;  
	
    PTBDD_PTBDD0 = 1;                    //MISO2
	PTBD_PTBD0 = 0;  

}

void prePowerDown(byte mode)
{
    RTC_DISABALE();
    
    LED_ALARM_Off();
    LED_GREEN_Off();  
    LED_YELLOW_Off();
    
    LED_RED_Off();
//	acc_SPIWriteReg(LSM6DS3_INT2_ON_INT1_ADDR,0x45);
    buzzerOff();
                
    TPM1C1SC = 0;
    TPM1C3SC = 0;
    TPM2C1SC = 0;            
    if(mode == 1)        // power off
    {
       (void)SPI_FLASH_Init(1);
       if(usrProfile.lpEnable)        // Added the statement for disbaling RTC wakeup when wireless disbaled, 2015.01.05
       {          
          RTC_Init(2);                 // Added by Jason Chen for waking up power off                        
          KBIPE_KBIPE7= 1;
       }    
          
	   if(gftInfo.proxStat)
       {    
        //prox_start();        
          prox_disableInterupt();    // Added by Jason Chen for disabling Proximit Interrupt, 2014.04.24
          prox_shutdown(); 
       }        

        ADC_Init();
        SPI_FLASH_DeepPowerDown();
        
    //gyro
#ifdef ENABLE_6DS3_G
        //gyro_SPIWriteReg(GYRO_CTRL_REG1, 0);
#endif
         sensorPowerOff();             



       if(usrProfile.lpEnable) 
       {        
           gRadioSleep();                 // Added by Jason Chen, 2014.04.10
       }


    //spi
        (void)SPI_FLASH_Init(0);
    
                          
        shutdownRadioGpio();                  
        IRQSC = IRQSC_IRQPDD_MASK;         // IRQ pull device disabled, and clear IRQ
        
        IRQSC_IRQMOD = 0;                  // Clear IRQ Event slect for Clearing IRQ flag
        IRQSC_IRQACK = 1;                  // clear IRQF
                
//     #if LG_POWER_ON_ENABLE        
//        ENABLE_LOWG_INT;
//     #endif        
        
               
      
       if(gftInfo.proxStat)
       {        
          KBIPE = 0x44;                    //kbi BTN, USB enable  0x44, Added by Jason Chen for disabling Proximity Interrupt, 2014.04.24
       } 
       else
        KBIPE = 0x44;                      //Added by Jason Chen, 2014.05.20 
      
        if(usrProfile.lpEnable)            // Added the statement for disbaling RTC wakeup when wireless disbaled, 2015.01.05
        {          
          KBIPE_KBIPE7= 1;                 // Enable RTC interrupt
        }
                
        KBISC_KBMOD = 0;   //edge or level 1: level & edge                
    } 
    
    
    else  // mode == 0;
    {
       profile_alarm_au_backup =  usrProfile.AlarmMode & PROFILE_ALARM_AU_MSK;     // Added by Jason Chen, 2014.11.26       
       //usrProfile.AlarmMode &= ~PROFILE_ALARM_AU_MSK;                              // Added by Jason Chen, 2014.11.26

       KBIPE = 0x44;       
      
       KBISC_KBMOD = 0;   //edge or level 1: level & edge

       if(!Statup_flag)              // Added by Jason Chen for waking up after 2 minutes, 2017.03.31
       {
        
         if(gftInfo.uploadflag)
           RTC_Init(3);              // Added by Jason Chen for waking up if got upload preparing cmd, 2014.04.09
         else        
           RTC_Init(2);
       } 
       else 
       {
          if(usrProfile.lpEnable)    // Added by Jason Chen, 2017.04.11
            RTC_Init(4);             // Added by Jason Chen for waking up after 2 minutes, 2017.03.31
          else                       // Added by Jason Chen, 2017.04.11
            RTC_Init(2);             // Added by Jason Chen, 2017.04.11
       }
            
       KBIPE_KBIPE7= 1; 
                
     if(gftInfo.proxStat) prox_shutdown();
        
               
    //gyro
      //gyro_SPIWriteReg(GYRO_CTRL_REG1, 0);
    //ss_flash 
       // PTED_PTED7 = 1;        

	   
       if(usrProfile.lpEnable) 
       {  
        
          gRadioSleep();                                 // Added by Jason Chen, 2014.04.10          
        
           
       }
       (void)SPI_FLASH_Init(0);
   
       shutdownRadioGpio();
        IRQSC_IRQACK = 1;
#if LOW_G_WAKEUP_ENABLE            // Commented by Jason Chen, 2013.11.08 for Low G wake up       
        ENABLE_LOWG_INT;        
#endif                                         
        
    }
           
    //////////////////////////////////////////////////
    TPM2SC_CLKSA=0;
    TPM2C0V=0;
    Cpu_Delay100US(100);       
    PTFDD_PTFDD4 = 1;
    PTFD_PTFD4 = 0;                         // Added by Jason Chen, 2014.04.10 for protecting from buzzer warming
    ////////////////////////////////////////////////////

}

extern void buzzerBeepTime(byte time);      // Added by Jason Chen, 2014.01.08
extern byte BiByteCMDFlag;
extern void Verify_Battery_Level(void);

extern byte field_xmit_enable;              // Added by Jason Chen, 2014.12.08
extern byte  upload_mode;                   // Added by Jason Chen, 2016.01.07

void postPowerDown(byte mode)
{
    BiByteCMDFlag = FALSE;
    (void)SPI_FLASH_Init(1);
	
    if(mode==1)
    {       
                                  
    } 

    else if(mode == 2) 
    {  
       //PTCDD_PTCDD0 = 1;
       //PTCDD_PTCDD1 = 1   ;    
       //PTCD_PTCD0   = 1;
       //PTCD_PTCD1   = 1;
       
       PTCD  = 0x04;	    
       PTCDD = 0x3C;	    
       PTCPE = 0x00;	    
       PTCSE = 0x00;	    
       PTCDS = 0x00;	    
                     
       //PTCPE = 0x00;	    
       //PTCSE = 0x00;	    
       //PTCDS = 0x00;	    
       Cpu_Delay100US(10);   
       I2C_driver_Init();
       Cpu_Delay100US(10);   

      if(gftInfo.proxStat) (void)ClearInterruptFlag();
    } 
    else if(mode == 3)
    {
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////          // Added by Jason Chen, 2014.12.08
       field_xmit_enable = 0;                 // Added by Jason Chen, don't allow recording              
       RTC_ENABLE();                          // Internal RTC for 1 mSecond
	              	  	   	  	                                  
     #if SLEEP_MODE_RADIO_OFF
       if(usrProfile.lpEnable) 
       {                           
          gRadioWakup();                           
          SHORT_DELAY();   
                                                   
          Power_Off_Message.gid[0] = 0x00;                      //0x80;     Power off still// New Battery level report                 
          Power_Off_Message.gid[1] = max17047Exist|max14676Exist;
          Power_Off_Message.gid[2] = 0;//(byte)(batData1>>8);   // using previous unplugging packet, 2014.12.08                                       
          Power_Off_Message.gid[3] = 2;//(byte)batData1;          
          //Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
          //Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;   
          if(max17047Exist) 
          {                      
             Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8);    // Added by Jason Chen, 2014.12.05
             Power_Off_Message.gid[7] = (byte)max17047Voltage;         // Added by Jason Chen, 2014.12.05              
          } 
          else if(max14676Exist)                                       // Added by Jason Chen, 2016.01.11
          {                                                            // Added by Jason Chen, 2016.01.11
             Power_Off_Message.gid[6] = (byte)(max14676Voltage>>8);    // Added by Jason Chen, 2016.01.11
             Power_Off_Message.gid[7] = (byte)max14676Voltage;         // Added by Jason Chen, 2016.01.11           
             Power_Off_Message.batPT =  (byte)max14676Charge;
          }                                                            // Added by Jason Chen, 2016.01.11


        #if 1
          if(gftInfo.batState == BAT_STAT_LOW) 
          {            
            Power_Off_Message.gid[2] = 0;
            Power_Off_Message.gid[3] = 3;    //   low power, Power off (byte)batData1;
            
            //RTC_Init(3);                     // Clear RTC internal Interrupt  ??????????????????????????????????????????????????
            KBIPE_KBIPE7= 0;                 // Disable RTC interrupt, don't wake up again because of battery volt less than 3.40V
          }
        #endif          
                    
          lpCmdMail = LP_CMD_OFF;
          lpState=LP_STATE_IDLE;         
                                                      
          mTimer_TimeSet( &imSleepTimer );     // Added by Jason Chen
       }
     #endif 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////          // Added by Jason Chen, 2014.12.08
      
    }
    else  // mode == 0
    {
        //wait for system get stable
        
        RTC_Init(0); 
        
        KBIPE = 0x44;      //kbi BTN, USB enable,  proximity interrupt disabled as well, Jason Chen 2013.11.08
                
         
        if(gftInfo.proxStat) 
        {          
          prox_init(usrProfile.proxEnable);
       
        }

                    
	      SHORT_DELAY();
	    
        RTC_ENABLE();                  // Internal RTC for 1 mSecond
	           
        TPM1C1SC = 0
      	 //| TPM2C0SC_CH0F_MASK	       //   1 enable capture or output compare
   	  	 //| TPM2C0SC_CH0IE_MASK	     //   1 channel interrupt requests enabled
   	    	 | TPM2C0SC_MS0B_MASK 	     //  (B:A)
    		 //| TPM2C0SC_MS0A_MASK	       //   0:0 no clock
   	    	 | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   	    	 | TPM2C0SC_ELS0A_MASK	  
   	  	 ;
        TPM1C3SC = 0
      	 //| TPM2C0SC_CH0F_MASK	       // 1 enable capture or output compare
   	  	 //| TPM2C0SC_CH0IE_MASK	     // 1 channel interrupt requests enabled
   	    	 | TPM2C0SC_MS0B_MASK 	     //  (B:A)
   	  	 //| TPM2C0SC_MS0A_MASK	       //   0:0 no clock
   	    	 | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   	    	 | TPM2C0SC_ELS0A_MASK	  
   	  	 ;  
        TPM2C1SC = 0
      	 //| TPM2C0SC_CH0F_MASK	       // 1 enable capture or output compare
   	  	 //| TPM2C0SC_CH0IE_MASK	     // 1 channel interrupt requests enabled
   	    	 | TPM2C0SC_MS0B_MASK 	     //  (B:A)
   	   	 //| TPM2C0SC_MS0A_MASK	       //   0:0 no clock
   	    	 | TPM2C0SC_ELS0B_MASK	     //   1:0 fixed system clock p281
   	    	 | TPM2C0SC_ELS0A_MASK	  
   	  	 ;
   	  	 
   	  	buzzerOff();                   // Added by Jason Chen, 2014.11.26
   	  	
        DISABLE_LOWG_INT;
        // setup the timer
        // myRTC.action = MY_RTC_ACTION_READ; 
        
        //RTC_Init(0); 

#if 0               
        if(max17047Exist) 
           max17047_process();
        else if(max14676Exist)
			     max14676_process();
		    else
           ADC_Init(); 
#else		    
		    //updateBatStatus();                               // Added by Jason Chen, 2016.01.18
		    if(gftInfo.batState == BAT_STAT_LOW)             // Added by Jason Chen, 2016.01.18
		    {
		       imSleepTimerTime = IM_SLEEP_TIMER;            // Added by Jason Chen, 2016.01.18
           mTimer_TimeSet( &imSleepTimer );              // Added by Jason Chen, 2016.01.18  
		    }
		    
#endif		    
        
        #if 0
        
        // Added by Jason Chen, 2014.01.08
        //////////////////////////////////////////////////////////////////////////////
        Verify_Battery_Level();
        //////////////////////////////////////////////////////////////////////////////
        #endif                                
             
        KBIPE_KBIPE7= 1;           // Enable External RTC Interrupt   
        //gyro
        SPI_MEMS_Gyro_Config();
        //set stream mode
        //gyro_FifoStream();
        
     #if SLEEP_MODE_RADIO_OFF
       if(usrProfile.lpEnable) 
       {                    
          gRadioWakup();                          
          SHORT_DELAY();
#if 0        
          if(!upload_mode) 
          {            
            Power_Off_Message.gid[0] = 0x80;             // New Battery level report, 2014.03.14
            lpCmdMail = LP_CMD_OFF;
            lpState=LP_STATE_IDLE;         
          } 
          else 
          {
          ;
            //lpCmdMail = LP_CMD_ALARM;                    // Restore the upload from the last interrupt point, 2016.01.12
            //lpState=LP_STATE_IDLE;                       // Restore the upload from the last interrupt point, 2016.01.12
            //curSessionIndex--;                           // Restore the upload from the last interrupt point, 2016.01.12
          }
#endif
          mTimer_TimeSet( &imSleepTimer );      // Added by Jason Chen, 2014.01.15          
       }
     #endif 

      DISABLE_LOWG_INT;
      //IRQSC_IRQMOD = 0;          // Clear IRQ Event slect for Clearing IRQ flag, by Jason Chen, 20140303
      IRQSC_IRQACK = 1;    

	  updateBatStatus(); 
    }
}


void gotoStop3(void)
{
    
    __RESET_WATCHDOG();
    prePowerDown(0);
        
         
#if 1
      g_TickSecond     = 0;
      
      McuSetStopMode();
      Cpu_Delay100US(100);
      WaitUntillClockStable();                         // Added by Jason Chen, 2014.04.25
      if(!KBI_RTC_ON) 
      {   
       #if WAKUP_15_SECs_IN_BINDINGM_MODE       
           if( usrProfile.lpEnable && !Radio_Stack_isBound() )                  // Added by Jason Chen, 2015.09.08
             imSleepTimerTime = IM_SLEEP_TIMER;          // Added by Jason Chen, 2015.09.08
         else                                          // Added by Jason Chen, 2015.09.08         
       #endif
	   {
          if(Statup_flag)                              // Added by Jason Chen, 2017.03.30
            Statup_flag--;                             // Added by Jason Chen, 2017.03.30
           imSleepTimerTime = 1;                       // Added by Jason Chen, 2014.11.26
       }
          // imSleepTimerTime = 1;                       // Added by Jason Chen, 2014.11.26
       //imSleepTimerTime = IM_SLEEP_TIMER;            // Added by Jason Chen, 2014.11.26
         mTimer_TimeSet( &imSleepTimer );              // Added by Jason Chen, 2014.01.06 for Sleep wake up by external RTC            
        //totalSecond +=60;
		 
      }
   #if 0                                               // Removed by Jason Chen, 2014.11.26
      if(IRQSC_IRQF)                                   // Removed by Jason Chen, 2014.11.26
      {                                                // Removed by Jason Chen, 2014.11.26
         mTimer_TimeSet( &imSleepTimer );              // Removed by Jason Chen, 2014.11.26
         IRQSC_IRQACK = 1;                             // Removed by Jason Chen, 2014.11.26
      }                                                // Removed by Jason Chen, 2014.11.26
      if((KBI_SW_ON)||(KBI_USB_ON))                    // Removed by Jason Chen, 2014.11.26// Added by Jason Chen, 2014.01.10 for waking up gft by Button from Sleep mode 
      {                                                // Removed by Jason Chen, 2014.11.26
      }                                                // Removed by Jason Chen, 2014.11.26
   #else
      else                                             // Added by Jason Chen, 2014.11.26
      {                                                // Added by Jason Chen, 2014.11.26
         imSleepTimerTime = IM_SLEEP_TIMER;            // Added by Jason Chen, 2014.11.26
         mTimer_TimeSet( &imSleepTimer );              // Added by Jason Chen, 2014.11.26
      }                                                // Added by Jason Chen, 2014.11.26
   #endif
      
#else
    McuSetStopMode();
#endif     
    postPowerDown(0);
}

/******************************************************************************
 * Function:       void interrupt  IRQ_ISR()
 * Input:          
 * Output:          None
 * Note:            None
 *****************************************************************************/
void interrupt  IRQ_ISR()
{	
   IRQSC_IRQACK = 1;
   lgISR();
   return;
}

interrupt void Cpu_Interrupt(void)
{
  //this cause the cpu to reset if ENBDM=0(normal reset)
  asm(BGND);
}

#pragma NO_ENTRY
#pragma NO_EXIT
#pragma MESSAGE DISABLE C5703
void Cpu_Delay100US(word us100)
{
  /* Total irremovable overhead: 16 cycles */
  /* ldhx: 5 cycles overhead (load parameter into register) */
  /* jsr:  5 cycles overhead (jump to subroutine) */
  /* rts:  6 cycles overhead (return from subroutine) */

  /* aproximate irremovable overhead for each 100us cycle (counted) : 8 cycles */
  /* aix:  2 cycles overhead  */
  /* cphx: 3 cycles overhead  */
  /* bne:  3 cycles overhead  */
  /* Disable MISRA rule 19 checking - Octal constant used */
  /*lint -esym( 960, 19)   */
  /* Disable MISRA rule 55 checking - Non-case label used */
  /*lint -esym( 961, 55)   */
  asm {
loop:
    /* 100 us delay block begin */
    psha                               /* (2 c) backup A */
    /*
     * Delay
     *   - requested                  : 100 us @ 24MHz,
     *   - possible                   : 2400 c, 100000 ns
     *   - without removable overhead : 2388 c, 99500 ns
     */
    pshh                               /* (2 c: 83.33 ns) backup H */
    pshx                               /* (2 c: 83.33 ns) backup X */
    ldhx #$0076                        /* (3 c: 125 ns) number of iterations */
label0:
    LDA #85                            /* Reset watchdog counter - 2x write*/
    STA SRS
    LDA #170
    STA SRS
    aix #-1                            /* (2 c: 83.33 ns) decrement H:X */
    cphx #0                            /* (3 c: 125 ns) compare it to zero */
    bne label0                         /* (3 c: 125 ns) repeat 118x */
    pulx                               /* (3 c: 125 ns) restore X */
    pulh                               /* (3 c: 125 ns) restore H */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    nop                                /* (1 c: 41.67 ns) wait for 1 c */
    pula                               /* (2 c) restore A */
    /* 100 us delay block end */
    aix #-1                            /* us100 parameter is passed via H:X registers */
    cphx #0
    bne loop                           /* next loop */
    rts                                /* return from subroutine */
  }
  /* Restore MISRA rule 19 checking - Octal constant used */
  /*lint +esym( 960, 19)   */
  /* Restore MISRA rule 55 checking - Non-case label used */
  /*lint +esym( 961, 55)   */
}

#ifndef BOOTABLE_IMAGE

const byte BackDoorKey[] @0xFFB0 = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

// 0xFFB8-0xFFBC, 0xFFBE are reserved

// NVPROT: FPS7=0,FPS6=0,FPS5=0,FPS4=1,FPS3=1,FPS2=0,FPS1=0,FPDIS=0
//const unsigned char NVPROT_INIT @0x0000FFBD = 0x18; // protect from 0x1A00 (see above)
// NVOPT: KEYEN=0,FNORED=1,??=1,??=1,??=1,??=1,SEC01=1,SEC00=0
const unsigned char NVOPT_INIT @0x0000FFBF = 0x7E;


#endif

