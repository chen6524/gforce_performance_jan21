
/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
* 10/13/2013 -- Added two-way wireless functionality by JASON CHEN
* 11/13/2013 -- Added proximity functionality by JASON CHEN
* 04/22/2014 -- Modified by Jason Chen According to Harry Latest Version
*
* 
********************************************************************************/

#include <hidef.h>      /* for EnableInterrupts macro */
#include "gft.h"
#include "mcu.h"
#include "Usb_Drv.h" 
#include "Usb_Config.h"
#include "hid.h"
#include "mImpact.h"
#include "mHid.h"
#include "kbi.h"
#include "rtc.h"
#include "spi_flash.h"
#include "i2c_driver.h"
#include "rtc_drv.h"
#include "gft.h"
#include "adc.h"
#include "mGyro.h"
#include "mFlash.h"
#include "LSM330DLC.h"
#include "uart.h"
#include "string.h"
#include "MAX44000.h"
#include "MAX17047.h"
#include "MAX14676.h"
#include "utils.h"
#include "gLp.h"
#include "MAX14676.h"
//Faraz
#include "radio.h"

#pragma MESSAGE DISABLE C1420

/*Local function definition*/ 
void SysInit(void);
void buzzerBeep(word time);
byte accHitOnFirstTime =1;
byte mMail @ 0x195F; 
byte usbMail @ 0x195E; 


byte isStop = 0;
byte sessionStartFlag = 0;
GFT_INFO gftInfo;



/*****************************************************************************
 * Init_Sys: Initialize the system
 * Input: 	 None
 * Output:   None
 *
 ****************************************************************************/
extern byte field_xmit_enable;      // Added by Jason Chen for the control of field xmit, 2014.03.19
void SysInit(void)
{
	byte err;
    USBCTL0_USBPU = 0; 
	McuInit(); 
    
	ioInit();	 
	
  PTED_PTED1 = 0;                  // Sensor Voltage off         // Added by Jason Chen, 2016.01.14  
  Cpu_Delay100US(5000);                                          // Added by Jason Chen, 2016.01.14
  PTED_PTED1 = 1;                  // Sensor Voltage on          // Added by Jason Chen, 2016.01.14
  Cpu_Delay100US(1000);                                          // Added by Jason Chen, 2016.01.14

//I2CInit();
//Spi2Init(); 
//ADC_Init();  
	KbiInit();	
    
	gResetRadio();                                                 // Added by Jason chen, 2014.04.10
  err = SPI_FLASH_Init(1);                                       // enable flash spi
	Spi2Init();
  accInitBDs();
//if (KBI_USB_ON)
  //Cpu_Delay100US(10000);                                         // delay 1s for usb 
	
	mHidInit();
    RtcInit();                                                   // internel
	I2C_driver_Init();  

	usrProfileInit();		
	//field_xmit_enable = usrProfile.realXmitEnable;                            // Added by Jason Chen, 20140319		
	memset(&gftInfo, 0, sizeof(GFT_INFO));//init gftInfo
	
	
	//if(KBI_USB_ON)  
  if(usrProfile.lpEnable)                               //  Added by Jason Chen for fixing some bugs, 2014.06.24
  {    
  	if(Radio_Stack_isBound())                           //  Added by Jason Chen for in binding mode  if no binding info even connected with PC, 2014.06.18
  	{
  	  
      //gftInfo.lpMode = GMODE_LP_DISABLE;                                    
      gftInfo.lpMode  = usrProfile.lpEnable;        
  	}
    else
      //gftInfo.lpMode  =  usrProfile.lpEnable;          //  Added by Jason Chen for in binding mode  if no binding info even connected with PC, 2014.06.18
      gftInfo.lpMode = GMODE_LP_BIND;
  } else
    gftInfo.lpMode  = (KBI_USB_ON) ? GMODE_LP_DISABLE : usrProfile.lpEnable;      
		
	gftInfo.batState= BAT_STAT_NORM;
  gftInfo.lpBusy = FALSE;
  gftInfo.pwrMode = usrProfile.pwrMode;
  gftInfo.proxStat = usrProfile.proxEnable;
  gftInfo.uploadflag = FALSE;                                               // Added by Jason Chen,  2014.04.09
	RTC_ENABLE();
	#ifdef GFT_GEN2
	CHARGE_HIGH_CUR 
	#endif    
}


static word tick500mS=0;
static word adcTimer =0;
//#define ADC_TIMEOUT 60
static word ADC_TIMEOUT = 60;                    // Changed by Jason Chen, 2016.01.14
static word accDelayTimer =0;
#define ACC_ARM_DELY_TIMEROUT 1 //second
byte ledToggle = 0;  
static byte ledSilent = 0;                       // Jason Chen Added "static", 2013.09.18

static byte ledSilent_prox = 0;                // added by Jason Chen, 2013.11.12

word buzzTimer=0;
word ledTimerOn =0;
word alarmTimerOn = 0;
word ledTimerOff =0;

word ledAlarmOnTimer;
byte usbTimer;
#define RTC_LED_TIMER    10

void buzzerBeep(word time)
{
    if( buzzTimer == 0 ) {
        buzzTimer = time;
    }
}


void buzzerBeepTime(byte time)      // Added by Jason Chen, 2013.11.21
{
  
  if(time <= 0)
    return;
  
  while(time--) 
  { 
     buzzerOn();      
#if POWEROFF_LED_ON     
     if(gftInfo.batState == BAT_STAT_GREEN) 
     {      
       LED_GREEN_On();
     }
     else if(gftInfo.batState == BAT_STAT_YELLOW) 
     {
       LED_RED_On();      
       LED_GREEN_On();  
     }          
     else if(gftInfo.batState == BAT_STAT_RED) 
     {      
       LED_RED_On()
     }
     
#endif     
     
     Cpu_Delay100US(300);
     buzzerOff();
#if POWEROFF_LED_ON          
     LED_GREEN_Off();
     LED_RED_Off();
#endif     
     Cpu_Delay100US(1700);
  }
}




//int wakrup_count = 0;
void powerOff_RTC_Init(void)                                  // Added by Jason Chen, 2014.12.08
{                                                             // Added by Jason Chen, 2014.12.08
    //PTCD  = 0x04;	                                            // Added by Jason Chen, 2014.12.08
    //PTCDD = 0x3C;	                                            // Added by Jason Chen, 2014.12.08
    //PTCPE = 0x00;	                                            // Added by Jason Chen, 2014.12.08
    //PTCSE = 0x00;	                                            // Added by Jason Chen, 2014.12.08
    //PTCDS = 0x00;                                             // Added by Jason Chen, 2014.12.08
                                                              // Added by Jason Chen, 2014.12.08
    Cpu_Delay100US(100);                                      // Added by Jason Chen, 2014.12.08
    
           

    updateBatStatus();
    Cpu_Delay100US(10);                                       // Added by Jason Chen, 2014.12.08
    RTC_Init(2);                                              // Added by Jason Chen, 2014.12.08

} 

void ledAlarmOn(word time)
{
    if( ledAlarmOnTimer == 0 ) {
        ledAlarmOnTimer = time;
    }
}

enum {
	SW_FSM_ON,
	SW_FSM_SLEEP,
	SW_FSM_WAKE,
	SW_FSM_PRE_SLEEP,
	SW_FSM_RST,
	SW_FSM_SLEEP_WAKE,
	SW_FSM_WAKE_DELY,
	SW_FSM_BKSLP,
	SW_FSM_SLEEP_WAIT,
	
	SW_FSM_SLEEP_POWEROFF                                         // Added by Jason Chen, 2014.12.08
};


enum {
	PROX_FSM_ON,
	PROX_FSM_SLEEP,
	PROX_FSM_WAKE,
	PROX_FSM_PRE_SLEEP,
	PROX_FSM_RST,
	PROX_FSM_SLEEP_WAKE,
	PROX_FSM_WAKE_DELY,
	PROX_FSM_BKSLP,
	PROX_FSM_SLEEP_WAIT,
	PROX_FSM_SLEEP_COUNT
};


enum {
	USB_FSM_OFF,
	USB_FSM_ON,
	USB_FSM_UNPLUG,
	USB_FSM_PLUG,
	USB_FSM_PLUG_WAIT,
	USB_FSM_UNPLUG_WAIT,
	
	USB_FSM_LOW_BATT_OFF,                           // Added by Jason Chen, 2014.04.22   for Low batt Packet sending
	USB_FSM_UNPLUG_OFF                              // Added by Jason Chen, 2014.05.26   for unpligging Packet sending
};


static byte usbFSM;

static byte swFSM = SW_FSM_ON;
static byte accLiveTimer = 100;
//static byte accLivePre;

static byte proxFSM = PROX_FSM_ON;

byte mLedState       = LED_STATE_GREEN;
//0 game mode 
byte wakeup_poweroff = 0;                          // Added by Jason Chen, 2014.12.08 

void ResetADCTimer(void)                           // Added by Jason Chen, 2016.01.14
{
    adcTimer =0;
}

void secondTimerTask(void)
{
    if(wakeup_poweroff) return;                    // Added by Jason Chen, 2014.12.08

    //adc  
    if(!KBI_USB_ON) ADC_TIMEOUT = 60;              // Added by Jason Chen, 2016.01.14
    else ADC_TIMEOUT = 10;                         // Added by Jason Chen, 2016.01.14
      
    if (++adcTimer > ADC_TIMEOUT)
    {        
    	adcProcess();	
  	  adcTimer =0;
  	}                                              // Added by Jason Chen, 2016.01.14


    if( mLedState == LED_STATE_WHITE)
        ;
    else if(gftInfo.lpMode == GMODE_LP_BIND)
      mLedState = LED_STATE_BIND;
	  else if(gftInfo.batState == BAT_STAT_LOW) 
	  {        
	    mLedState = LED_STATE_RED;
	  }
	  else if(gftInfo.batState == BAT_STAT_CHARGED) 
	  {        
		  if(KBI_USB_ON)
		  {            
	 	    mLedState = LED_STATE_GREEN_CON;
		    LED_GREEN_On();
		  }
      else if(gftInfo.batState == BAT_STAT_GREEN) 
      {
        mLedState = LED_STATE_GREEN;
      }
      else if(gftInfo.batState == BAT_STAT_YELLOW) 
      {
        mLedState = LED_STATE_YELLOW;        
      }
      else //if(gftInfo.batState == BAT_STAT_RED) 
      {
        mLedState = LED_STATE_RED;        
      }
	  }
    else if(gftInfo.batState == BAT_STAT_GREEN) 
    {
      mLedState = LED_STATE_GREEN;
    }
    else if(gftInfo.batState == BAT_STAT_YELLOW) 
    {
      mLedState = LED_STATE_YELLOW;        
    }
    else //if(gftInfo.batState == BAT_STAT_RED) 
    {
      mLedState = LED_STATE_RED;        
    }

 // }                                               // Commented by Jason Chen, 2016.01.13
  
  // led flashing
  // ledSilent = 0;	 //test power
  ledToggle ^= 0x1;
  
  if (ledToggle && (!ledSilent)&& (!ledSilent_prox)) 
  {
  	//alarm
	  if ( (usrProfile.AlarmMode & PROFILE_ALARM_ON_MSK) && (!KBI_SW_ON ) && (!KBI_USB_ON ))
	  {
		  if(usrProfile.AlarmMode& PROFILE_ALARM_VI_MSK)
		    LED_ALARM_On();
		  if(usrProfile.AlarmMode& PROFILE_ALARM_AU_MSK && (!isStop))
			  buzzerBeep(30);
	  }
    
  	if( mLedState == LED_STATE_RED)
  	{
  	  LED_RED_On();	  
  	}
	  else if(mLedState == LED_STATE_GREEN)
	  {
      LED_GREEN_On();	 
	  }
	  else if(mLedState == LED_STATE_YELLOW)
	  {
	    LED_GREEN_On();
      LED_RED_On();
	  }
    else if(mLedState == LED_STATE_BLUE)
    {
      LED_BLUE_On();
    }
    else if(mLedState == LED_STATE_BIND)
    {
      LED_BLUE_On();
    }
    else if( mLedState == LED_STATE_WHITE)
  	{
  	  LED_RED_On();
	    LED_GREEN_On();
      LED_BLUE_On();
  	}
	  ledTimerOn = 100;
    alarmTimerOn = 10;
  }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Added by Jason Chen, 2014.11.26
void init_buzzer(void) 
{
    TPM2SC   = TPM2SC_CLKSA_MASK;
    TPM2C0SC = TPM2C0SC_MS0B_MASK | TPM2C0SC_ELS0B_MASK;    
    TPM2MOD = 9000;                       		          // 2.6KHz (250 uSec)
    TPM2C0V = TPM2MOD/2;			                          // 50% duty cycle, max volume
}

void init_LEDs(void) 
{
    //TPM1SC   = TPM2SC_CLKSA_MASK;
    //TPM1C1SC = TPM2C0SC_MS0B_MASK | TPM2C0SC_ELS0B_MASK | TPM2C0SC_ELS0A_MASK;
    
    //TPM1MOD = 24000;		  // 1KHz (1mSecond)
    //TPM1C1V = TPM1MOD/10;	  
    
	  //TPM1C3SC = TPM2C0SC_MS0B_MASK | TPM2C0SC_ELS0B_MASK | TPM2C0SC_ELS0A_MASK;
    //TPM1C3V = TPM1MOD/10;	

	  TPM2C1SC = TPM2C0SC_MS0B_MASK | TPM2C0SC_ELS0B_MASK | TPM2C0SC_ELS0A_MASK;
   
    TPM2C1V = TPM2MOD/10;	    
}

void TurnOffLedandBuzzer(void)                       // Added by Jason Chen, 2014.11.26
{
   LED_ALARM_Off();                                  // Added by Jason Chen, 2014.11.26
   usrProfile.AlarmMode &= ~PROFILE_ALARM_ON_MSK;//PROFILE_ALARM_VI_MSK;    // Added by Jason Chen, 2014.11.26    
    
   LED_GREEN_Off();                                  // Added by Jason Chen, 2014.11.26
   LED_YELLOW_Off();                                 // Added by Jason Chen, 2014.11.26
   LED_RED_Off();                                    // Added by Jason Chen, 2014.11.26
   buzzerOff();                                      // Added by Jason Chen, 2014.11.26
                                
   TPM1C1SC = 0;                                     // Added by Jason Chen, 2014.11.26
   TPM1C3SC = 0;                                     // Added by Jason Chen, 2014.11.26
   TPM2C1SC = 0;                                     // Added by Jason Chen, 2014.11.26
   TPM2C0SC = 0;                                     // Added by Jason Chen, 2014.11.26  
}

void buzzerBeepTimeNew(byte time)                    // Added by Jason Chen, 2013.11.26
{
  
  if(time <= 0)
    return;
  
  init_buzzer();
  init_LEDs();  
  while(time--) 
  { 
     buzzerOn();      
     LED_GREEN_On();
     //LED_BLUE_On();
     //LED_RED_On();
     
          
     Cpu_Delay100US(300);     
     buzzerOff();
     LED_GREEN_Off();
     
     Cpu_Delay100US(1700);
     LED_GREEN_Off();
     LED_RED_Off();
     LED_BLUE_Off();     
  }
}

void GreenLED_SolidOn(void) 
{
   PTFDD_PTFDD5 = 1;      //Solid On   
   PTFD_PTFD5 = 0;  
}

void GreenLED_SolidOff(void) 
{
   PTFDD_PTFDD5 = 1;      //Solid Off   
   PTFD_PTFD5 = 1;  
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define BTN_WAKE_UP_TIMER 0x1FFFF
volatile dword btnWakeUpTimer = BTN_WAKE_UP_TIMER;

volatile dword proxWakeUpTimer = 3000;//BTN_WAKE_UP_TIMER;
volatile byte prox_count = 0;

void ClearPowerOff_Packet(void) 
{ 
   Power_Off_Message.gid[0] = 0;
   Power_Off_Message.gid[1] = 0;
   Power_Off_Message.gid[2] = 0;
   Power_Off_Message.gid[3] = 0;
   Power_Off_Message.gid[4] = 0;
   Power_Off_Message.gid[5] = 0;
   Power_Off_Message.gid[6] = 0;
   Power_Off_Message.gid[7] = 0;   
}

byte Sleep_prox_flag = 0;
#if BACK_CHANNEL_UPLOAD                         // Added by Jason Chen, 2013.12.11
extern byte  upload_mode;;
//byte  upload_end_flag = 0;                    // Added by Jason Chen, 2014.03.21 for preventing duplicated upload
#endif
byte powerCommand = 0;                          // Added by Jason Chen, 2014.05.22

extern void gRadioSleep(void);                  // Added by Jason chen, 2014.12.08

static void proxyTask(void)
{
  static byte buzzOnce_prox =0;
  static byte buzzOnce =0;  
  if ((!ledSilent)&&(!ledSilent_prox))
  {
  	if(!ledTimerOn--)
  	{
       if(mLedState == LED_STATE_GREEN_CON) 
       {
          LED_RED_Off();
          LED_BLUE_Off();
          
       }
       else if( mLedState == LED_STATE_WHITE)
       {
          ;
       }
       else  
       {
      	  LED_GREEN_Off();
          LED_RED_Off();
          LED_BLUE_Off();   
          
       }
  	}
    if(!alarmTimerOn)
    {
       LED_ALARM_Off();
    }
    else
        alarmTimerOn--;                          
  }

    
  //buzz
  if(buzzTimer >0)
  {
  	buzzTimer--;
  	buzzerOn();
  }
  else    
  	buzzerOff();
  if (KBI_USB_ON)
  	return;
   if((gftInfo.proxStat)&&(!upload_mode))            // the upload_mode condition added by Jason chen,2014.05.21
  {                                                     // Avoiding upload interrupt when proximity enabled
    //Proximity control
    switch(proxFSM)
    {
      case PROX_FSM_ON:
  	  default:
  		  if (KBI_USB_ON)
  			   ;		 
        else if(KBI_PROX_ON)            
        {
           if(!Sleep_prox_flag) 
           {          
    		     Sleep_prox_flag = 1;
    		      	
    		     ProxIntState = TRUE;	                       // Added by Jason Chen for controlling Impact recording, 20140304
    		     
    		    #if ENABLE_BUZZER_BEEP_PROX                  // Changed by Jason Chen, 2015.09.01 
    		     buzzerBeepTime(1);  
    		    #endif
    		     
             /////////////////////////////////////////   // Added by Jason Chen for indicating the status of recording, 2014.06.03
           //spiMyFlashGetCurWtAddr();                   
             Power_Off_Message.gid[0] = 0x02;            // indicate that gft can record impacts,    2014.06.03          
             Power_Off_Message.gid[1] = max17047Exist|max14676Exist;;
             Power_Off_Message.gid[2] = (byte)(batData1>>8);                                         
             Power_Off_Message.gid[3] = (byte)batData1;
             Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
             Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;  
             
             if(max17047Exist) 
             {                      
               Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8); 
               Power_Off_Message.gid[7] = (byte)max17047Voltage;                                                 
             } 
             else if(max14676Exist)                                                     // Added by Jason Chen, 2016.01.11
             {                                                                          // Added by Jason Chen, 2016.01.11
               Power_Off_Message.gid[6] = (byte)(max14676Voltage>>8);                   // Added by Jason Chen, 2016.01.11
               Power_Off_Message.gid[7] = (byte)max14676Voltage;                        // Added by Jason Chen, 2016.01.11            
               Power_Off_Message.batPT =  (byte)max14676Charge;
             }                                                                          // Added by Jason Chen, 2016.01.11
                                                           
             /////////////////////////////////////////   // Added by Jason Chen for indicating the status of recording, 2014.06.03
                      
             lpCmdMail = LP_CMD_OFF;     
             lpState   = LP_STATE_IDLE;    		  
             
             isStop =0;		  
  		       accHitOn =0;                       //??????????????????????           
           }
           ledSilent_prox = 0;		  		  		  		  
        } 
        else 
        {
           if(Sleep_prox_flag) 
           {
   		       proxFSM = PROX_FSM_SLEEP_COUNT;
  		       prox_count = 0; 		  
  		       proxTimer = 0;          
           }        
        }
  	    break;
      case PROX_FSM_SLEEP_COUNT:
        if(prox_count > 20) 
        {
           prox_count = 0;
           if(!KBI_PROX_ON) 
    		   {  		          
    		       proxTimer++;
    		       if(proxTimer > 30)           // 100 -- >2 seconds, Consecutive 100 samples equal to High, one sample each 20 milliseconds
    		       {                            // 50  -- >1 second,  Consecutive 50 samples equal to High, one sample each 20 milliseconds
    		        
          		  	proxTimer = 2500;
          		  	prox_count = 0;
          			  ledSilent_prox = 1;
          			  buzzOnce = 0;		
          			  LED_GREEN_On();
          			  proxFSM = PROX_FSM_SLEEP_WAIT;		    		    
    		       } 
    		   } 
    		   else 
    		   {
    		   		proxFSM = PROX_FSM_ON;
    		  	//mLedState = LED_STATE_GREEN;
  		        prox_count = 0;  		 
  		        proxTimer  = 0;
  		        ledSilent_prox = 0;
    		   }
    		    
  		  }
  		  prox_count++;
        break;
      case PROX_FSM_SLEEP_WAIT:
          		
    	  if(!buzzOnce_prox)
  	  	{                				 
      			 #ifdef ENABLE_RADIO
      			   //ClearPowerOff_Packet();
      			   lpCmdMail = LP_CMD_OFF;
                        // Power_Down = TRUE;
                        // Radio_Stack_TimeSet(&PowerDownTimer, POWER_DOWN_TIME);
                        
                        
               Power_Off_Message.gid[0] = 0x03;       // 0x01 means Sleep Packet, added by Jason Chen, 2014.03.04
                                                      // changed to 03 by Jason Chen for the status of recording, 2014.06.03
               ///////////////////////////////////////////////////////////////////added by Jason Chen, 2014.04.24
               Power_Off_Message.gid[1] = max17047Exist|max14676Exist;;
               Power_Off_Message.gid[2] = (byte)(batData1>>8);                                         
               Power_Off_Message.gid[3] = (byte)batData1;
               Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
               Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;                                                
               if(max17047Exist) 
               {                      
                  Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8); 
                  Power_Off_Message.gid[7] = (byte)max17047Voltage;                                                 
               } 
               else if(max14676Exist)                                                     // Added by Jason Chen, 2016.01.11
               {                                                                          // Added by Jason Chen, 2016.01.11
                  Power_Off_Message.gid[6] = (byte)(max14676Voltage>>8);                  // Added by Jason Chen, 2016.01.11
                  Power_Off_Message.gid[7] = (byte)max14676Voltage;                       // Added by Jason Chen, 2016.01.11                     
                  Power_Off_Message.batPT =  (byte)max14676Charge;
               }                                                                          // Added by Jason Chen, 2016.01.11
               ///////////////////////////////////////////////////////////////////added by Jason Chen, 2014.04.24
                                                                                                                                      
      			 #endif    			 
      				 //spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
      				 #if ENABLE_BUZZER_BEEP_PROX                      // Changed by Jason Chen, 2015.09.01
      				   buzzerBeep(30); 
      				 #endif

  	 		}
  	 		else if(buzzOnce_prox == 150)
  	 		{
  	 		    #if ENABLE_BUZZER_BEEP_PROX                         // Changed by Jason Chen, 2015.09.01
  	 				  buzzerBeep(30);
  	 				#endif
  	 		}
  	 		
  	 		if(buzzOnce_prox< 200) 
  	 		{
  	 			buzzOnce_prox++;	
  	 			
  	  	} 
  	  	else 
  	  	{       
  	  	    LED_GREEN_Off();     	  
  	 				
            proxFSM = PROX_FSM_SLEEP;//PROX_FSM_PRE_SLEEP;//SW_FSM_PRE_SLEEP;
            proxTimer = 2000;
            isStop =1;                   //????????????????????????????
            //spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
            //if(needSaveProfile)
            //  profileSave();            	  	
  	  	}
    		break;
          
  	  case PROX_FSM_PRE_SLEEP:		
    		if (proxTimer)
        {
           proxTimer--;
    		}
        else
           proxFSM = PROX_FSM_SLEEP;
    		break;
          
      case PROX_FSM_SLEEP:      
      
        prePowerDown(0); 
        isStop =1;                //????????????????????
        
        //while(--proxWakeUpTimer >2) 
        {     
            if(!KBI_PROX_ON1)
            {              
        	  	  McuSetStopMode();      	
                SHORT_DELAY();      
                WaitUntillClockStable();                         // Added by Jason Chen, 2014.05.05        
                                                                             
                //Cpu_Delay100US(100);//500
                proxWakeUpTimer = 2000;//BTN_WAKE_UP_TIMER;
                
              #if 0
                if(KBI_SW_ON) 
                {
                   proxTimer = 150;   // 3 seconds
                   Cpu_Delay100US(200);
                   while(KBI_SW_ON) 
                   {                    
                      Cpu_Delay100US(200);
                      proxTimer--;
                      //if(proxTimer == 0)           
                      //   break;                  
                   }
                   
                  // if(proxTimer == 0) break;
                }
              #endif                    
            }                    
        }          
        
        
        //while(--proxWakeUpTimer >2) 
        if (!KBI_RTC_ON) 
        {                    
          //powerOff_BattMeasurement(0);
          //break;
        }
            
        if(IRQSC_IRQF)
        {  
          //IRQSC_IRQACK = 1;
          //break;
        }                                 
            
        __RESET_WATCHDOG(); 
        if(KBI_USB_ON)
        {
          //break;
        }                                  
                                
      //#ifdef  ENABLE_RADIO   
  	      
      //lpCmdMail = LP_CMD_PWR_ON;
          
      //#endif
#if 1
     		KBIPE       = 0;	 //kbi diable
  	  	KBISC_KBMOD = 0;   //edge or level 1: level & edge
          
        //btnTimer = 3000;
  		  //RTC_ENABLE();
  		  //accHitOn =0;
  		  //ledSilent_prox = 0;
  		  //proxFSM = PROX_FSM_ON;
  		    		  
  		  isStop =0;		  
  		  accHitOn =0;                       //??????????????????????
        ledSilent_prox = 1;
#endif        
        
        buzzOnce_prox = 0;                 // 2014.05.05                
  		  Sleep_prox_flag = 0;  		  
  		  ProxIntState = FALSE;               // Added by Jason Chen for controlling Impact reacording  		  
  		  postPowerDown(0);               
        proxFSM = PROX_FSM_SLEEP_WAKE;	  
        
        (void)getRtcStamp();                // 2015.01.29          	  
        
  		  break;

  	  case PROX_FSM_SLEEP_WAKE:

    	  ledSilent_prox = 0;		  	  
  #if 0
  	    SPI_FLASH_ResumeFromDeepPowerDown();               // Don't need it because of Sleep mode, 2014.05.05
  		  mLedState = LED_STATE_GREEN_CON;
  		  LED_GREEN_On();
  		  buzzerBeep(30); 
  		  proxTimer = 1000;
  		  proxFSM = PROX_FSM_WAKE_DELY;
  #else	
        imSleepTimerTime = IM_SLEEP_TIMER;            // Added by Jason Chen, 2015.01.28 	  		  
        mTimer_TimeSet( &imSleepTimer );
  		  proxFSM = PROX_FSM_RST;
  #endif		  
  	    break;
      case PROX_FSM_WAKE_DELY: //give lp some time to tx pwr off
    		if(--proxTimer == 0)
    		{
    			proxFSM = PROX_FSM_RST;
    			mMail ='P';
    			LED_GREEN_Off();
          //postPowerDown(1); ?????????????????????
    		}
    		break;
      case PROX_FSM_RST:    
         //Cpu_Delay100US(10);
    		 //if( KBI_PROX_ON1 )
    		 //__asm("BGND");                     // Reboot it , make sure GFT status restore one correct status, 2015.01.27
    		 
    		 //while(1);
    		 proxFSM = PROX_FSM_ON;             // Added by Jason Chen, 2014.05.05
    		 break;
    }  
  }
}
static void TimerTask(void)
{
  static byte buzzOnce =0;  
  
  static byte poweroff_flag = 0;                     // Added by Jason Chen, 2016.01.12


  //button control 
  switch(swFSM)
  {
   
    case SW_FSM_ON:
	  default:
		  if (KBI_USB_ON)
			   ;
		  else if ((KBI_SW_ON )||(powerCommand))      // powerCommand Added by Jason Chen, 2014.05.26
		  {	  	
		  	    btnTimer = 3000;
		  	
				if(powerCommand == 1)
				  btnTimer = 1000;
				else if(powerCommand == 2)                // Added by Jason Chen, 2014.12.05
				  btnTimer = 500;                         // Added by Jason Chen, 2014.12.05
				else if(powerCommand == 3)                // Added by Jason Chen, 2016.01.12, power off after upload finished
				  btnTimer = 1001;                        // Added by Jason Chen, 2016.01.12				  

			  ledSilent = 1;
			  buzzOnce = 0;		
			   if( usrProfile.lpEnable == GMODE_LP_DISABLE ) 
			   	{
			   	LED_BLUE_On();
			   	}
			  else
			  	{
			    LED_RED_On();   
				}// Changed by Jason Chen, 2014.11.26
			  swFSM = SW_FSM_SLEEP_WAIT;
		  }		 
		  else 
		  {
		    ledSilent =0;
		  }		  		 
	    break;
    case SW_FSM_SLEEP_WAIT:
      
  		if ((KBI_SW_ON)||(powerCommand))            // powerCommand Added by Jason Chen, 2014.05.26
  		{
		  	if (btnTimer == 0)
		  	{			 
  				if(!buzzOnce)
  				{
      				 
      				 #ifdef ENABLE_RADIO
      				   
      				   ClearPowerOff_Packet();
      				   lpCmdMail = LP_CMD_OFF;
                        // Power_Down = TRUE;
                        // Radio_Stack_TimeSet(&PowerDownTimer, POWER_DOWN_TIME);
      				 #endif
      				 //spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
      				 buzzerBeep(30); 

  				}
  				else if(buzzOnce == 150)
  				{
  					buzzerBeep(30);  				   
  				}
  				if(buzzOnce< 200) 
  				{
  					buzzOnce++;	
  					LED_RED_Off(); 
					LED_BLUE_Off();
  				} 
  				else 
  				{
  				  poweroff_flag = powerCommand;                     // Added by Jason Chen, 2016.01.12
  				  powerCommand = 0;                                 // Added by Jason Chen, 2014.05.26
  				}
  		  }	  		  	  		   
  		}  		
  		else if (btnTimer == 0)
  		{
  		    		  	
  		  swFSM = SW_FSM_PRE_SLEEP;
        btnTimer = 2000;
        isStop =1;
        if((poweroff_flag != 3)&&(poweroff_flag != 2))                               
            spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
  		  if(needSaveProfile)
  		      profileSave();  			  		  
  		}
  		else
  		{
  			swFSM = SW_FSM_ON;
  			mLedState = LED_STATE_GREEN;
  		}
  		  	
  		if (btnTimer) btnTimer--;  		
  		break;
        
	  case SW_FSM_PRE_SLEEP:
  		if (btnTimer)
      {
         btnTimer--;
  		}  		
      else
         swFSM = SW_FSM_SLEEP;                               // Changed by Jason Chen, 2014.11.26         
  		break;
    case SW_FSM_SLEEP:      
    //McuSetStopMode();
    //SHORT_DELAY();
      prePowerDown(1);  
        //only happens in power off mode wake up by exgternal 1 Minute RTC inerrupt
    case SW_FSM_SLEEP_POWEROFF:                              // Added by Jason Chen, 2014.12.08
    
      TurnOffLedandBuzzer();                                 // Added by Jason Chen, 2014.11.26
      poweroff_reset();                   
      btnWakeUpTimer = 0x13FFF;                              // Added by Jason Chen, 2014.11.26
      while(--btnWakeUpTimer >2) 
      {     
          isStop =1;
      		if(!KBI_SW_ON)
      		{
             GreenLED_SolidOff();                          // Added by Jason Chen, 2014.11.26	    
                          
             if (KBI_USB_ON) break;                        // Added by Jason Chen, 2014.12.08     
             //usbMail = 0;
             if( usrProfile.lpEnable == GMODE_LP_DISABLE) 
             	{
             	
             	 LED_RED_Off();
             	 LED_BLUE_On(); 
				 SHORT_DELAY();
                 max14676_poweroff();  
             	}
			 else
			 	{
			 	
    	  	   McuSetStopMode();  

			 	}
			 
             SHORT_DELAY();
             SHORT_DELAY();                                // Added   by Jason Chen, 2014.04.21
             //SHORT_DELAY();                                                  // Added   by Jason Chen, 2014.04.21
             WaitUntillClockStable();                      // Added   by Jason Chen, 2014.12.08
             btnWakeUpTimer = 0x13FFF;//BTN_WAKE_UP_TIMER; // Changed by Jason Chen, 2014.11.26                                                                                    
      		}

          if(!KBI_RTC_ON)  // RTC interrupt is on 
          {            
            powerOff_RTC_Init();
            wakeup_poweroff = 1;                         // Added by Jason Chen, 2014.12.08            
            g_StartHg = 0;
          //GreenLED_SolidOn();                          // Added by Jason Chen, 2014.11.26      
            break;     		
          }

      		GreenLED_SolidOn();                             // Added by Jason Chen, 2014.11.26      
            		          
          __RESET_WATCHDOG(); 
          if(KBI_USB_ON)
          {
             //buzzerBeepTime(3);                         // Removed by Jason Chen, 2014.11.26
             break;
          }        
      } 
      
      ResetADCTimer();                                  // Added by Jason Chen, 2016.01.14
      if(wakeup_poweroff == 1)                          // Added by Jason Chen, 2014.12.08
      {                                                 // Added by Jason Chen, 2014.12.08
        postPowerDown(3);                               // Added by Jason Chen, 2014.12.08
        swFSM = SW_FSM_SLEEP_WAKE;                      // Added by Jason Chen, 2014.12.08
                                                 // Added by Jason Chen, 2014.12.08
      }
      else 
      {          
       		KBIPE       = 0;	 //kbi diable
    	  	KBISC_KBMOD = 0;   //edge or level 1: level & edge
            
            //btnTimer = 3000;
    		RTC_ENABLE();
    		accHitOn =0;
    		ledSilent = 1;
    		  swFSM = SW_FSM_SLEEP_WAKE;
    		  
    		  GreenLED_SolidOff();               // Added by Jason Chen, 2014.11.26
    		  Cpu_Delay100US(1000);              // Added by Jason Chen, 2014.11.26
          init_buzzer();                     // Added by Jason Chen, 2014.11.26
          init_LEDs();                       // Added by Jason Chen, 2014.11.26
    		  
    		 
      }
      break;
	  case SW_FSM_SLEEP_WAKE:
       if(wakeup_poweroff == 1)                          // Added by Jason Chen, 2014.12.08	  
       {                                                 // Added by Jason Chen, 2014.12.08	  
          swFSM = SW_FSM_WAKE_DELY;                      // Added by Jason Chen, 2014.12.08	  
          imSleepTimerTime = IM_SLEEP_TIMER;             // Added by Jason Chen, 2014.11.26
          mTimer_TimeSet( &imSleepTimer );               // Added by Jason Chen, 2014.12.08
          btnTimer = 1000;                               // Added by Jason Chen, 2014.12.08
                                                // Added by Jason Chen, 2014.12.08
       } 
       else 
       {
   			  ledSilent = 0;
                    
   			  mLedState = LED_STATE_GREEN_CON;
      		LED_GREEN_On();
      		buzzerBeep(30); 
      		btnTimer = 1000;
      		swFSM = SW_FSM_WAKE_DELY;
        	
       }
	   break;
    case SW_FSM_WAKE_DELY: //give lp some time to tx pwr off
      if(wakeup_poweroff == 1)                          // Added by Jason Chen, 2014.12.08
      {                                                 // Added by Jason Chen, 2014.12.08
          if(--btnTimer == 0)                           // Added by Jason Chen, 2014.12.08
          {                                             // Added by Jason Chen, 2014.12.08
             wakeup_poweroff = 0;                       // Added by Jason Chen, 2014.12.08          
             if(usrProfile.lpEnable)                    // Added by Jason Chen, 2014.12.08
               gRadioSleep();                           // Added by Jason Chen, 2014.12.08
             swFSM = SW_FSM_SLEEP_POWEROFF;             // Added by Jason Chen, 2014.12.08
          }                                             // Added by Jason Chen, 2014.12.08
          break;                                        // Added by Jason Chen, 2014.12.08
      }                                                 // Added by Jason Chen, 2014.12.08
      else 
      {        
      		if(--btnTimer == 0)
      		{
      			swFSM = SW_FSM_RST;
      			mMail ='R';
      			LED_GREEN_Off();
            postPowerDown(1);
      		}
      		break;
      }
    case SW_FSM_RST:
		 
  		 if( !KBI_SW_ON  )   
  		 	{
  		 	mMail = 'R';
  			__asm("BGND");

  		 	}
  		 break;
	
  }// switch(swFSM)
  
  #ifdef GFT_GEN2
    IRQSC_IRQACK = 1; //clear irq
  #endif
}


byte secondCnt =0;
#if (VERSION_13 == 0)      			
word rebootWait =500;                                // Changed by Jason Chen from 100 to 500 according to harry latest version, 2014.04.23
byte toReboot = 0;                                   // Added by Jason Chen according to harry latest version, 2014.04.23
#else
byte rebootWait =100;
#endif
extern volatile byte usbEneum;  

#define USB_SLEEP_TIMER       5  //in seconds
#define USB_RADIO_START_TIMER 5
word usbSleepTimer      = 0;
word usbSleepTimerTime  = USB_SLEEP_TIMER;
word usbRadioStartTimer = 0;

extern word RadioPeriodicTimer;                               // Changed by Jason, 2014.12.05
byte unplugFlag = 0;

void main(void) 
{
//Faraz
  //byte Return_Value = 0;
  //byte myId = 0;
  //bool Power_Mode = POWER_OFF;
  //bool old_Usb_Mode =  USB_OFF;
  //byte Count_Upload =0;
   
  //CYFISNP_NODE_EEP_NET_REC_ADR->nodeSeedLsb);
   
  volatile static word mTick =0;
  
  //srand((unsigned int) Radio_Stack_Node_Number());
  
 // byte i;
  static byte SessionEndMarked =0;
  static word mTickSecond =0;
  
 
  
  usbFSM =  KBI_USB_ON ? USB_FSM_ON: USB_FSM_OFF;
  SysInit(); 
  if(mMail !='R')
  	{
		swFSM = SW_FSM_SLEEP_WAKE;
       
  	}
  if(usbMail =='U' )
  	{
  	powerCommand =1;
    usbMail =0;
  	}
  
  spiFlashGetCurWtAddr();
  Initialize_RTC();  //external rtc
  Cpu_Delay100US(1);
  
  if (getHardIDMajor() == 0x3)
     max17047_init();
    
  buzzerOff();
  max17047_GetVer();                   // Added by Jason Chen for checking GFT2 or GFT3, 2013.11.18
  
  if (getHardIDMajor() == 0x4)
     max14676_init();   //enable the power  
  
  
   if(gftInfo.proxStat)
      prox_init(usrProfile.proxEnable);                        
  else 
  {      
    prox_disableInterupt();              // Added by Jason Chen for disabling Proximit Device Interrupt, 2014.05.20
    prox_shutdown();                     // Added by Jason Chen for disable the proximity because of battery still there, 2014.05.20      
  }
        
  if(!getRtcStamp()) 
    mLedState = LED_STATE_RED;
  
  if(KBI_USB_ON)
  	{
  	Statup_flag = 0;    
    Initialize_USBModule();
  	}
  else                                            // Added by Jason Chen, 2017.04.11
  {                                               // Added by Jason Chen, 2017.04.11
    if(usrProfile.lpEnable)                       // Added by Jason Chen, 2017.04.11
    {                                             // Added by Jason Chen, 2017.04.11
      if(Radio_Stack_isBound())                   // Added by Jason Chen, 2017.04.11
      {                                           // Added by Jason Chen, 2017.04.11
        myId = Radio_Stack_Node_Number();         // Added by Jason Chen, 2017.04.11
        if(myId > 0)                              // Added by Jason Chen, 2017.04.11
          Statup_flag = 8 + myId/12;              // Added by Jason Chen, 2017.04.11
        else                                      // Added by Jason Chen, 2017.04.11
          Statup_flag = 8;                        // Added by Jason Chen, 2017.04.11        
      }                                           // Added by Jason Chen, 2017.04.11
      else                                        // Added by Jason Chen, 2017.04.11
        Statup_flag = 0;                          // Added by Jason Chen, 2017.04.11
    }                                             // Added by Jason Chen, 2017.04.11
    else                                          // Added by Jason Chen, 2017.04.11
      Statup_flag = 0;                            // Added by Jason Chen, 2017.04.11
  }               
  EnableInterrupts;                   /* enable interrupts */

  
  //enable keyboard interrupt
  KBISC_KBACK  = 1;
  KBISC_KBIE   = 1;
  if(KBI_USB_ON)
    mTimer_TimeSet(&usbSleepTimer );
  
  for(;KBI_USB_ON;)
  {
    if (Check_USBBus_Status())
    {  
      __RESET_WATCHDOG();
        
      if(usbEneum) break;
      if(mTimer_TimeExpired( &usbSleepTimer, usbSleepTimerTime ) )
        break;
    }
  }

  usbFSM =	KBI_USB_ON ? USB_FSM_ON: USB_FSM_OFF;

  
  //GID 
  ENABLE_LOWG_INT; 
  IRQSC_IRQACK = 1; //clear irq
  Spi2Init();       //lowg and gyro 

  gyroInit();       // Including Gyroscope and Acclerometer
  #if 0
  if(mMail !='P')
     buzzerBeep(30);
  else
  	 mMail = 0;
  #endif
  if (getHardIDMajor() == 0x3)
  	max17047_process();                                         // Measure once time;
  if (getHardIDMajor() == 0x4) 
  	max14676_process();  
    
  updateBatStatus();                                            // Added by Jason Chen, 2016.01.12
  ADC_Init();                                                   // Changed into the place by Jason Chen for initializing the batData value

  //////////////////////////////////////////////                // Added by Jason Chen, 2015.01.27
  spiMyFlashGetCurWtAddr();                                     
  lpRptPkt.impactNum  = spiFlashCurRdBuffer.accEntryMagicCnt;   
  /////////////////////////////////////////////////
  totalSecond = 0;  
  for(;;) 
  {
      __RESET_WATCHDOG(); 
          
    	switch (usbFSM )
    	{
        		case USB_FSM_UNPLUG:
            			if (KBI_USB_ON)
            			{
            			  if(!upload_mode) 
            			  {            			    
              				if(needReboot ==0)
              				   spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
              				needReboot = 1;
							usbMail ='U';
            			  }
            			}
               #if ENABLE_RECORDING_UNBUND                                                         // Changed by Jason Chen, 2015.09.01
            			else if(!isStop&&(gftInfo.batState!= BAT_STAT_LOW)/*&&(!powerCommand)*/) 
            	 #else
            	    else if(!isStop && (gftInfo.lpMode != GMODE_LP_BIND)&&(gftInfo.batState!= BAT_STAT_LOW)/*&&(!powerCommand)*/)             			
            	 #endif
            			{
            				imTask();	
							
            			}                                          
                  //else if (( gftInfo.batState== BAT_STAT_LOW)/*||(powerCommand)*/)
                  else if (( gftInfo.batState== BAT_STAT_LOW)&&(!wakeup_poweroff))        // Changed by jason Chen, 2014.12.08
                  {
#if ENABLE_LOW_BATT_OFF                  
                   #if (ENABLE_SEND_LOW_BATTERY == 1)        // Added by Jason Chen, 2014.04.17 for Sending Turn off packet
                      mTimer_TimeSet( &imSleepTimer );       
                      
                      spiMyFlashGetCurWtAddr();              
                      Power_Off_Message.gid[0] = 0x00;                      
                      Power_Off_Message.gid[1] = max17047Exist|max14676Exist;;
                      Power_Off_Message.gid[2] = 0;//(byte)(batData1>>8);                                         
                      Power_Off_Message.gid[3] = 1;//(byte)batData1;
                      Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
                      Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;   
                      if(max17047Exist) 
                      {                      
                        Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8); 
                        Power_Off_Message.gid[7] = (byte)max17047Voltage;                                                 
                      } 
                      else if(max14676Exist)                                                     // Added by Jason Chen, 2016.01.11
                      {                                                                          // Added by Jason Chen, 2016.01.11
                        Power_Off_Message.gid[6] = (byte)(max14676Voltage>>8);                   // Added by Jason Chen, 2016.01.11
                        Power_Off_Message.gid[7] = (byte)max14676Voltage;                        // Added by Jason Chen, 2016.01.11       
                        Power_Off_Message.batPT = (byte)max14676Charge;
                      }                                                                          // Added by Jason Chen, 2016.01.11
                                                                                                      
                      lpCmdMail = LP_CMD_OFF;     
                      lpState   = LP_STATE_IDLE;
                      buzzerOff();
                      
                      usbFSM = USB_FSM_LOW_BATT_OFF;	
                   #else
              				if(! SessionEndMarked)
              				{
              					spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
              					SessionEndMarked =1;
              				}

					  max14676_poweroff();
                      prePowerDown(1);
                      
                      KBIPE_KBIPE7= 0;                // Disable RTC interrupt, Added by Jason Chen, 2014.04.24
                      
                      //McuSetStopMode();
					 
                      SHORT_DELAY();
                      __asm("BGND");                    
                   
                   #endif                                                                             
#endif                   
                  }            		
            			break;
            	
       #if (ENABLE_SEND_LOW_BATTERY == 1)            		
    	      case USB_FSM_LOW_BATT_OFF:    	          	         
                 if(mTimer_TimeExpired( &imSleepTimer, 3 )) 
                 {

				   max14676_poweroff();
                    if(!KBI_USB_ON) 
                    {                                        
                 				if(! SessionEndMarked)
                 				{
                 					spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
                 					SessionEndMarked =1;
                 				}
                        prePowerDown(1);                                                               
                    
                   #if IIC_ENABLE_IN_STOP    
                       //disable iiC
                              //iic
        					IICC1_IICEN = 0;
        					//
        					PTCDD_PTCDD0 = 1;
        					PTCD_PTCD0   = 1;
        					PTCDD_PTCDD1 = 1;
       						 PTCD_PTCD1   = 1;
                    #endif
                        
                        KBIPE_KBIPE7 = 0;             // Changed from KBIPE2 to KBIPE7, it is bug 2014.12.08// Disable RTC interrupt, Added by Jason Chen, 2014.06.02        
						max14676_poweroff();
						
                        //McuSetStopMode();
                        SHORT_DELAY();
                        SHORT_DELAY();
                        SHORT_DELAY();
                        SHORT_DELAY();
                        KBIPE_KBIPE7 = 0;             // Disable RTC interrupt, Added by Jason Chen, 2014.04.24                   
                        
                      //powerCommand = 0;             // added by Jason Chen, 2014.05.22                   
                        Cpu_Delay100US(10);
                        __asm("BGND");
                    } 
                    else 
                    {
                       Cpu_Delay100US(10);
                       __asm("BGND");                      
                    }                                                       
                 }       
    	           break;
       #endif    	          	          	          	          	           
        		case USB_FSM_PLUG:
        		#if (VERSION_13 == 0)        			  
        		      if(toReboot) break;                                     // Added by Jason Chen according to harry latest version, 2014.04.23        		
        		#endif
        		
                if (KBI_USB_ON)
                {
            		Check_USBBus_Status();
            		HidTask();						                                  /* hid task---mHid.c */	  

                    if(pcConnected)
                    {       
                      if(usrProfile.lpEnable)                               //  Added by Jason Chen for fixing some bugs, 2014.06.24
                      {
                        if(gftInfo.lpMode & GMODE_LP_TEST)
							              ;
                        else if(Radio_Stack_isBound())                      //  Added by Jason Chen for in binding mode  if no binding info even connected with PC, 2014.06.18
                           gftInfo.lpMode = GMODE_LP_DISABLE;
                        else 
                        {
                        #if UPLOAD_BY_WIRE_IN_BINDING_MODE                  // Added by Jason Chen, 2015.09.03     
                           if(pcConnected == 1) 
                             gftInfo.lpMode = GMODE_LP_BIND;                           
                           else
                             gftInfo.lpMode = GMODE_LP_DISABLE;                           
                        #else
                         //gftInfo.lpMode  =  usrProfile.lpEnable;          //  Added by Jason Chen for in binding mode  if no binding info even connected with PC, 2014.06.18
                           gftInfo.lpMode = GMODE_LP_BIND;                                                   
                        #endif
                        }
                      } 
                      else if (gftInfo.lpMode & GMODE_LP_TEST)
					  	             ;
					            else
                           gftInfo.lpMode = GMODE_LP_DISABLE;                                            
                      
                    }
                    else if(mTimer_TimeExpired( &usbRadioStartTimer, USB_RADIO_START_TIMER) )
                    {
                      gftInfo.lpMode  =  usrProfile.lpEnable;
                    }             
                }
            	else            			
            	{
            			  LED_ALARM_Off();                                   
                          usrProfile.AlarmMode  &= ~PROFILE_ALARM_ON_MSK;
                          LED_GREEN_Off();                                   
                          LED_YELLOW_Off();                                 
                          LED_RED_Off();      
            		#if DISABLE_RECORDING_DURING_UNPLUG	
            			  field_xmit_enable = 0;                          // Disable recording during "USB Unplugged", 2015.09.01
            	    #endif

                                                           
                    if(Radio_Stack_isBound())                       //  Added by Jason Chen for in binding mode  if no binding info even connected with PC, 2014.06.19
                    {                                          
                       if((!gftInfo.pwrMode)&&(!upload_mode))       // Added by Jason Chen for sending power off packet, 2014.05.26
                       {

                       gftInfo.proxStat = 0;                            // 2015.01.27        		
                        LED_GREEN_Off();                                          // Added by Jason Chen, 2014.12.05
                        TPM2C1SC = 0;                                             // Added by Jason Chen, 2014.12.05

                         gftInfo.lpMode  =  usrProfile.lpEnable;                      // Added by Jason Chen, 2014.12.05
                        
                         mTimer_TimeSet( &imSleepTimer );                             // Added by Jason Chen, 2014.12.05
                     
                         spiMyFlashGetCurWtAddr();                                    // Added by Jason Chen, 2014.12.05
                         Power_Off_Message.gid[0] = 0x80;                             // Added by Jason Chen, 2014.12.05
                         Power_Off_Message.gid[1] = max17047Exist|max14676Exist;                 // Added by Jason Chen, 2014.12.05
                         Power_Off_Message.gid[2] = (byte)(batData1>>8);              // Added by Jason Chen, 2014.12.05                              
                         Power_Off_Message.gid[3] = (byte)batData1;                   // Added by Jason Chen, 2014.12.05
                         Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); // Added by Jason Chen, 2014.12.05
                         Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;      // Added by Jason Chen, 2014.12.05  
                         if(max17047Exist) 
                         {                      
                           Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8);     // Added by Jason Chen, 2014.12.05
                           Power_Off_Message.gid[7] = (byte)max17047Voltage;          // Added by Jason Chen, 2014.12.05              
                         } 
                         else if(max14676Exist)                                       // Added by Jason Chen, 2016.01.11
                         {                                                            // Added by Jason Chen, 2016.01.11
                           Power_Off_Message.gid[6] = (byte)(max14676Voltage>>8);     // Added by Jason Chen, 2016.01.11
                           Power_Off_Message.gid[7] = (byte)max14676Voltage;          // Added by Jason Chen, 2016.01.11               
                           Power_Off_Message.batPT =  (byte)max14676Charge;
                         }                                                            // Added by Jason Chen, 2016.01.11
                                                                                       
                         lpCmdMail = LP_CMD_OFF;                                      // Added by Jason Chen, 2014.12.05
                         lpState   = LP_STATE_IDLE;                                   // Added by Jason Chen, 2014.12.05
                                                                        // Added by Jason Chen, 2014.12.05
                         buzzerOff();
                        
                         usbFSM = USB_FSM_UNPLUG_OFF;                                                                                                                                           
                       }                       
                       else                                                         // Added by Jason Chen, 2014.10.06
                       {                                                            // Added by Jason Chen, 2014.10.06
                          if(gftInfo.pwrMode)                                       // Added by Jason Chen, 2014.10.06
                          {                                                         // Added by Jason Chen, 2014.10.06
                    			  Cpu_Delay100US(10);                                     // Added by Jason Chen, 2014.10.06
                    				__asm("BGND");                                          // Added by Jason Chen, 2014.10.06
                          }                                                         // Added by Jason Chen, 2014.10.06
                       }                                                            // Added by Jason Chen, 2014.10.06                       
                    } 
                    else 
                    {     
                       if(gftInfo.pwrMode)                                          // Added by Jason Chen, 2014.10.06
                       {                                                            // Added by Jason Chen, 2014.10.06
                    	    Cpu_Delay100US(10);                                       // Added by Jason Chen, 2014.10.06
                    			__asm("BGND");                                            // Added by Jason Chen, 2014.10.06
                       }                                                            // Added by Jason Chen, 2014.10.06
                       else                                                         // Added by Jason Chen, 2014.10.06
                       {
                     #if OFF_UNPLUG_IN_BINDINGM_MODE                          
                         usbFSM = USB_FSM_UNPLUG_OFF;                               //  Added by Jason Chen, 2015.09.08
                     #else
                                                                                    //  Added by Jason Chen for fixing some bugs, 2014.06.24
                         if(!usrProfile.lpEnable)                        
                         {
                            gftInfo.lpMode  =  usrProfile.lpEnable;
                            usbFSM = USB_FSM_UNPLUG_OFF; 
                         } else
                              usbFSM =  KBI_USB_ON ? USB_FSM_ON: USB_FSM_OFF;                                              
                     #endif
                       }
                       
                    }
                  
              		}
            			break;
     #if POWER_OFF_UNPLUG                                         // Option, somebody wants to Power off when unplugging, 2014.01.14            			
        		case USB_FSM_UNPLUG_OFF:                    
					  
					  gftInfo.proxStat =0;                       // 2015.01.27        		
                 if(mTimer_TimeExpired( &imSleepTimer, 1 )&&(!KBI_USB_ON)) 
                 {                
                   if((!gftInfo.pwrMode)&&(!upload_mode))
                   {
                       btnWakeUpTimer = 0x13FFF;                        // Added by Jason, 2014.11.16
                       if(!KBI_USB_ON) 
                       {   
                           ResetADCTimer();                                          // Added by Jason Chen, 2016.01.15
                           LED_GREEN_Off();                                          // Added by Jason Chen, 2014.12.05
                           TPM2C1SC = 0;                                             // Added by Jason Chen, 2014.12.05
                           //Check_USBBus_Status();                                    // Added by Jason Chen, 2014.12.05
                           //Initialize_USBModule();
                           USB_Soft_Detach();                                        // Added by Jason Chen, 2016.02.01                           
                           pcConnected = 0;                                          // Added by Jason Chen, 2014.12.05
                           powerCommand = 2;                                         // Added by Jason Chen, 2014.12.05
                           usbFSM = USB_FSM_OFF;                                     // Added by Jason Chen, 2014.12.05
                           unplugFlag = 1;                                           // Added by Jason Chen, 2016.01.29
						   mMail ='R';
						   break;                                                    // Added by Jason Chen, 2014.12.05                           
                       } 
                       else 
                       {                        
             	            Cpu_Delay100US(10);                                       // Added by Jason Chen, 2014.05.22
             		   	      __asm("BGND");                                                                       
                       }                                                                          
                       //KBIPE = 0;	   //kbi diable
                       //KBISC_KBMOD = 1;   //edge or level 1: level & edge	
                       //RTC_ENABLE();
                   }    
                 
                 //USB_Soft_Detach();
                 //if (--rebootWait ==0) 
                 //if((!gftInfo.pwrMode)&&(!upload_mode)) 
                   { 
                     GreenLED_SolidOff();                                      // Added   by Jason Chen, 2014.11.26
                     Cpu_Delay100US(1000);                                     // Changed by Jason Chen, 2014.11.26                        
                     buzzerBeepTimeNew(1);                               			 // Added   by Jason Chen, 2014.11.26
                     
                     while( KBI_SW_ON )                                        // Added by Jason Chen, 2014.11.26
                     {                                                         // Added by Jason Chen, 2014.11.26
                       LED_GREEN_On();                                         // Added by Jason Chen, 2014.11.26
                       Cpu_Delay100US(1000);                                   // Added by Jason Chen, 2014.11.26
                       LED_GREEN_Off();                                        // Added by Jason Chen, 2014.11.26
                       Cpu_Delay100US(15000);                                  // Added by Jason Chen, 2014.11.26                                                                    
                     }                                                         // Added by Jason Chen, 2014.11.26
                                        
             	       __asm("BGND");
             	       //needReboot = 1;
                   }
                 } 
                 else 
                 {
                    if (KBI_USB_ON) 
                    {                      
                       if(needReboot ==0)
                         spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
                       needReboot = 1;
                    }                      
                 }                 
        		     break;            			
     #endif           		     
        		case USB_FSM_OFF:
            			usbFSM = USB_FSM_UNPLUG_WAIT;	
            			usbTimer = 120;
            			break;
        		  
        		case USB_FSM_ON:
            			usbFSM = USB_FSM_PLUG_WAIT;
            			usbTimer = 20;
                        mTimer_TimeSet(&usbRadioStartTimer);
						//usbMail ='U';
            			break;
        		case USB_FSM_PLUG_WAIT:
            			if (KBI_USB_ON)
            			{
            			  if (usbTimer==0)
            			  {
              				usbFSM = USB_FSM_PLUG;          				
            			  }
            			  usbTimer--;
            			}
            			else
            			  usbFSM = USB_FSM_OFF;
            			break;
        		
        		case USB_FSM_UNPLUG_WAIT:
            			if (!KBI_USB_ON)
            			{
            			  if (usbTimer-- == 0)
            			  {
                			usbFSM = USB_FSM_UNPLUG;
                              //if(SessionEndMarked)
                              
                			imState=IM_STATE_INIT;
                			//sessionStartFlag = 1;
                			//(void)spiFlashGetCurWtAddr();
                			if(!unplugFlag) 
                			{                			  
                			  spiFlashCreateTimeStampEntry(ACC_ENTRY_START);
                              g_StartHg = 1;
                			}
            			  }
            			  
            			}
            			else
             			  usbFSM = USB_FSM_ON;
            			break;        		
      }
   	
   	
    	if (g_TickSecond!= mTickSecond)
    	{
         mTickSecond =  g_TickSecond;
    	   secondTimerTask();
    	   #if 0
    	   if (sessionStartFlag==1)
    	   {
      	   	sessionStartFlag =0;
        		spiFlashCreateTimeStampEntry(ACC_ENTRY_START);
    	   }
    	   else if (sessionStartFlag==2)
    	   {
    	   	  sessionStartFlag =0;
    		    spiFlashCreateTimeStampEntry(ACC_ENTRY_END);
    	   }
    	   #endif    	   
    	}
        
        
      if (mTick != g_Tick1mS)
    	{
          
		mTick = g_Tick1mS;
		
		proxyTask();
		TimerTask();
		
        #ifdef ENABLE_RADIO         
    	    processLp();
        #endif
    		
    		if(!isStop)
    		{
            if(needSaveProfile)
      			{
      				profileSave();
      				needSaveProfile =0;
      			}
      			if(needEraseFlash)
      			{
              LED_GREEN_Off();
              LED_RED_Off();
              LED_YELLOW_Off();
              LED_ALARM_Off();
      	      spiFlashEraseFlash();
			        imSleepTimerTime = IM_SLEEP_TIMER;             // Added by Jason Chen, 2014.11.26
              mTimer_TimeSet( &imSleepTimer );  
      	      needEraseFlash =0;
              rebootWait = 1;
      			}
      			if(lpNeedUnbind)
      			{
              radioUnBind();
              gftInfo.lpMode = GMODE_LP_DISABLE;
              lpNeedUnbind = 0;
      			}
      			if(needReboot)
      			{
                    mMail = 'R';
      				USB_Soft_Detach();
      			  if (--rebootWait ==0) 
          #if (VERSION_13 == 0)        			  
      			  {
      			     toReboot = 1;                           // Added by Jason Chen according to harry latest version, 2014.04.23
      			     needReboot = 0;                         // Added by Jason Chen according to harry latest version, 2014.04.23
      			  }
          #else
				      __asm("BGND");                           // removeded by Jason Chen according to harry latest version, 2014.04.23
          #endif      			        
      			}
          #if (VERSION_13 == 0)      			
      			///////////////////////////////              // Added by Jason Chen according to harry latest version, 2014.04.23
      			if(toReboot)                                 
      			{                                            
      			  if(++rebootWait > 500)                     
      			    __asm("BGND");      			               
      			}
      			///////////////////////////////              // Added by Jason Chen according to harry latest version, 2014.04.23
          #endif      			
            if(needHibernate)
            {
               buzzerBeep(100);
               swFSM = SW_FSM_SLEEP_WAIT;
               btnTimer = 0;
            }
            //calibration
            //if(startCal)
              lowGTask(!KBI_USB_ON);
  	    }//if(!isStop)         
	    }//if (mTick != g_Tick1mS)
  }//for(;;)
}
